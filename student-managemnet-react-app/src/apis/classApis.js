import axiosService from '../services/axiosService';
import { API_URL } from '../constants/apiConstant';

const url = 'classrooms';

export const getAll = () => {
  return axiosService.get(`${API_URL}/${url}`);
};
