import axiosService from '../services/axiosService';
import { API_URL } from '../constants/apiConstant';

const url = 'subjects';

export const getAll = () => {
  return axiosService.get(`${API_URL}/${url}`);
};

export const getById = (id) => {
  return axiosService.get(`${API_URL}/${url}/${id}`);
};

export const getByParam = (params) => {
  return axiosService.get(`${API_URL}/${url}/${params}`);
};

export const save = (subject) => {
  return axiosService.post(`${API_URL}/${url}`, subject);
};

export const update = (subject) => {
  return axiosService.put(`${API_URL}/${url}/${subject.id}`, subject);
};

export const deleteByID = (id) => {
  return axiosService.delete(`${API_URL}/${url}/${id}`);
};
