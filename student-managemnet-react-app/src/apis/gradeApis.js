import axiosService from '../services/axiosService';
import { API_URL } from '../constants/apiConstant';

const url = 'grades';

export const getGradesByClassId = (classID) => {
  return axiosService.get(`${API_URL}/classrooms/${classID}/${url}`);
};

export const getSearchStudentByClassId = (classID, param) => {
  return axiosService.getWithParam(
    `${API_URL}/classrooms/${classID}/${url}`,
    param
  );
};

export const getGradesByStudentId = (studentId) => {
  return axiosService.get(`${API_URL}/students/${studentId}/${url}`);
};

export const updateGradeStudent = (body) => {
  return axiosService.put(`${API_URL}/${url}`, body);
};

export const deleteGradesStudent = (studentId) => {
  return axiosService.delete(`${API_URL}/students/${studentId}/${url}`);
};
