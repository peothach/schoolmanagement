import axios from 'axios';

class AxiosService {
  constructor() {
    const instance = axios.create();
    instance.interceptors.response.use(this.handleSuccess, this.handleError);
    this.instance = instance;
  }

  static handleSuccess(response) {
    return response;
  }

  static handleError(error) {
    return Promise.reject(error);
  }

  get(url) {
    return this.instance.get(url, {
      headers: { Authorization: `Bearer ${localStorage.getItem('TOKEN')}` },
    });
  }

  post(url, body) {
    return this.instance.post(url, body, {
      headers: { Authorization: `Bearer ${localStorage.getItem('TOKEN')}` },
    });
  }

  put(url, body) {
    return this.instance.put(url, body, {
      headers: { Authorization: `Bearer ${localStorage.getItem('TOKEN')}` },
    });
  }

  delete(url) {
    return this.instance.delete(url, {
      headers: { Authorization: `Bearer ${localStorage.getItem('TOKEN')}` },
    });
  }
}

export default new AxiosService();
