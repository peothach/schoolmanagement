import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { findByTestAttr, storeFactory } from './utils/testUtils';
import App from './App';

const setup = () => {
  const store = storeFactory();

  return mount(
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  );
};

// Test demo
test('renders without error', () => {
  const wrapper = setup();
  const component = findByTestAttr(wrapper, 'component-app');

  expect(component).toHaveLength(1);
});
