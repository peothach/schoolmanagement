import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch, useHistory } from 'react-router-dom';
import * as role from './constants/constants';
import { PERSON_ID, ROLE, TOKEN, USER_NAME } from './constants/constants';
import AdminLayout from './pages/admin/AdminLayout';
import Login from './pages/authen/Login';
import NotFound from './pages/error/NotFound';
import Student from './pages/student/Student';
import TeacherLayout from './pages/teacher/TeacherLayout';
import { logout } from './store/actions/authenAction';
import Chat from './pages/chat/Chat';
import ListTopics from './pages/forum/ListTopcis';
import TopicDettails from './pages/forum/TopicDetails';
import PostDetails from './pages/forum/PostDetails';

function App() {
  const dispatch = useDispatch();
  const history = useHistory();
  const teacherLoading = useSelector((state) => state.teachers.loading);
  const studentLoading = useSelector((state) => state.student.loading);
  const studentsLoading = useSelector((state) => state.students.loading);
  const loginLoading = useSelector((state) => state.authen.loading);

  const isLoggedIn = useSelector((state) => state.authen.isLoggedIn);
  const roleLoggedIn = useSelector((state) => state.authen.role);

  const showLoading =
    teacherLoading || studentLoading || studentsLoading || loginLoading;

  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error.response.status === 401) {
        dispatch(logout());

        localStorage.removeItem(TOKEN);
        localStorage.removeItem(USER_NAME);
        localStorage.removeItem(ROLE);
        localStorage.removeItem(PERSON_ID);
      }
      if (error.response.status === 500) {
        history.replace('/404');
      }
      throw error;
    }
  );

  return (
    <div data-test="component-app">
      {showLoading && (
        <Backdrop className="backdrop" open={showLoading}>
          <CircularProgress className="loading" />
        </Backdrop>
      )}
      <Switch>
        <Route path="/admin">
          {isLoggedIn && roleLoggedIn === role.ADMIN && <AdminLayout />}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
        <Route path="/teacher">
          {isLoggedIn && roleLoggedIn === role.TEACHER && <TeacherLayout />}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
        <Route path="/chat">
          {isLoggedIn && <Chat />}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
        <Route path="/student">
          {isLoggedIn && roleLoggedIn === role.STUDENT && <Student />}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
        <Route path="/forum/topics" exact>
          {/* {isLoggedIn && <ListTopics />}
          {!isLoggedIn && <Redirect to="/login" />} */}
          <ListTopics />
        </Route>
        <Route path="/forum/topics/:topicId" exact>
          <TopicDettails />
        </Route>
        <Route path="/forum/topics/:topicId/post/:postId">
          <PostDetails />
        </Route>
        <Route path="/login">
          {isLoggedIn && roleLoggedIn === role.ADMIN && (
            <Redirect to="/admin" />
          )}
          {isLoggedIn && roleLoggedIn === role.TEACHER && (
            <Redirect to="/teacher" />
          )}
          {isLoggedIn && roleLoggedIn === role.STUDENT && (
            <Redirect to="/student" />
          )}
          {!isLoggedIn && <Login />}
        </Route>
        <Route path="/404">
          <NotFound />
        </Route>
        <Route path="*">
          {isLoggedIn && roleLoggedIn === role.ADMIN && (
            <Redirect to="/admin" />
          )}
          {isLoggedIn && roleLoggedIn === role.TEACHER && (
            <Redirect to="/teacher" />
          )}
          {isLoggedIn && roleLoggedIn === role.STUDENT && (
            <Redirect to="/student" />
          )}
          {!isLoggedIn && <Redirect to="/login" />}
        </Route>
      </Switch>
    </div>
  );
}

export default App;
