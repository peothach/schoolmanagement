import axios from 'axios';
import { API_URL } from '../constants/apiConstant';
import { TOKEN } from '../constants/constants';

const apiCaller = (endPoint, method, body) => {
  const token = localStorage.getItem(TOKEN);
  return axios({
    method,
    url: `${API_URL}/${endPoint}`,
    data: body,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).catch((err) => {
    console.log(err);
  });
};

export const apiCallerv2 = ({ url, method, headers, params, data }) => {
  const token = localStorage.getItem(TOKEN);
  return axios({
    url: `${API_URL}/${url}`,
    method,
    headers: {
      ...headers,
      Authorization: `Bearer ${token || ''}`,
    },
    params,
    data,
  });
};

export default apiCaller;
