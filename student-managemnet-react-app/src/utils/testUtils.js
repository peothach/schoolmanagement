import { createStore, applyMiddleware } from 'redux';

import rootReducer from '../store/reducers';
import { middlewares } from '../store';

/**
 * Helper function the find DOM element by attribute
 * @param {Object} wrapper
 * @param {string} val
 * @returns
 */
export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test='${val}']`);
};

export const storeFactory = (initialState) => {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  );
};
