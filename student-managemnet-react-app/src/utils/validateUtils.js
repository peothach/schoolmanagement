import moment from 'moment';

export const FULL_NAME = 'Fullname';
export const ADDRESS = 'Address';
export const PHONE_NUMBER = 'Phone number';
export const BIRTH_DAY = 'Birthday';
export const SUBJECT = 'Subject';

const PHONE_PATTERN_MESSAGE = 'must be in pattern: 0xxxxxxxxx!';
const BIRTH_DAY_MESSAGE = 'must be in the past!';
const TEXT_INPUT_MESSAGE = 'must not be blank!';
const SELECT_INPUT_MESSAGE = 'must selects an option!';

export const validateTextInput = (field, val) => {
  if (val.trim().length === 0) {
    return {
      field,
      content: `${field} ${TEXT_INPUT_MESSAGE}`,
    };
  }

  return null;
};

export const validateBirthday = (field, val) => {
  if (moment(val).isSameOrAfter(moment())) {
    return {
      field,
      content: `${field} ${BIRTH_DAY_MESSAGE}`,
    };
  }
  return null;
};

export const validatePhoneNumber = (field, val) => {
  if (!val.match(/^0[0-9]{9,10}$/)) {
    return {
      field,
      content: `${field} ${PHONE_PATTERN_MESSAGE}`,
    };
  }
  return null;
};

export const validateSelection = (field, val) => {
  if (val === '') {
    return {
      field,
      content: `${field} ${SELECT_INPUT_MESSAGE}`,
    };
  }
  return null;
};
