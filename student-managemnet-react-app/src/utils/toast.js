import { toast } from 'react-toastify';

export const toastError = (message) => {
  toast.error(message);
};

export const toastSuccess = (message) => {
  toast.success(message);
};
