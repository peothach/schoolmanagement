import { subjectTypes } from '../../constants/actionTypes';
import { toastSuccess, toastError } from '../../utils/toast';

const initialState = [];

const subjects = (state = initialState, action) => {
  let currentState = [...state];
  switch (action.type) {
    case subjectTypes.FETCH_SUBJECTS: {
      return [...state];
    }

    case subjectTypes.FETCH_SUBJECTS_SUCCESS:
    case subjectTypes.SEARCH_SORT_FILTER_SUBJECT_SUCCESS: {
      currentState = action.payload.content;
      toastSuccess('Get list subject succesfull');
      return [...currentState];
    }

    case subjectTypes.FETCH_SUBJECTS_FAILED:
    case subjectTypes.SEARCH_SORT_FILTER_SUBJECT_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return [...state];
    }

    case subjectTypes.ADD_SUBJECT_SUCCESS: {
      toastSuccess('Add subject successful');
      const newState = Object.assign([], currentState);
      newState.push(action.payload);
      currentState = newState;
      return [...currentState];
    }

    case subjectTypes.ADD_SUBJECT_FAILED:
    case subjectTypes.DELETE_SUBJECT_FAILED:
    case subjectTypes.UPDATE_SUBJECT_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return [...currentState];
    }

    case subjectTypes.DELETE_SUBJECT_SUCCESS: {
      toastSuccess('Delete subject successful');
      const newState = Object.assign([], currentState);
      const index = newState.findIndex(
        (subject) => subject.id === action.payload
      );
      if (index !== -1) {
        newState.splice(index, 1);
        currentState = newState;
      }
      return [...currentState];
    }

    case subjectTypes.UPDATE_SUBJECT_SUCCESS: {
      const newState = Object.assign([], currentState);
      const index = newState.findIndex(
        (subject) => subject.id === action.payload.id
      );
      if (index !== -1) {
        newState[index] = action.payload;
        currentState = newState;
      }
      toastSuccess('Update subject successful');
      return [...currentState];
    }

    default:
      return state;
  }
};

export default subjects;
