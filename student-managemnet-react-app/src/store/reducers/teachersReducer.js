import * as type from '../../constants/teacherTypes';

// Initial state of the teacher state
export const initialState = {
  teachers: [],
  teacher: null,
  pageable: {
    page: 0,
    limit: 0,
    totalElements: 0,
    totalPage: 0,
  },
  loading: false,
  error: null,
  showAlertDialog: false,
  actionStatus: null,
  subjects: [],
};

/**
 * @function teachersReducer
 * @param {Object} state - Previous state before reducer
 * @param {Object} action - Action sent to reducer
 * @returns {Object} - New state after reducer
 */
const teachersReducer = (state = initialState, action = {}) => {
  const newState = {
    ...state,
    loading: false,
    pageable: {
      ...state.pageable,
    },
    teachers: [...state.teachers],
    teacher: { ...state.teacher },
    actionStatus: { ...state.actionStatus },
    subjects: [...state.subjects],
  };

  switch (action.type) {
    case type.SHOW_TEACHER_DIALOG:
      return {
        ...newState,
        showAlertDialog: true,
      };
    case type.HIDE_TEACHER_DIALOG:
      return {
        ...newState,
        showAlertDialog: false,
      };
    case type.GET_TEACHERS_REQUESTED:
    case type.GET_TEACHER_ID_REQUESTED:
    case type.ADD_TEACHER_REQUESTED:
    case type.UPDATE_TEACHER_REQUESTED:
    case type.DELETE_TEACHER_REQUESTED:
    case type.GET_SUBJECTS_REQUESTED:
      return {
        ...newState,
        loading: true,
      };
    case type.GET_TEACHERS_SUCCESS:
      return {
        ...newState,
        teachers: action.payload.teachers.content,
        pageable: {
          page: action.payload.teachers.page,
          limit: action.payload.teachers.limit,
          totalElements: action.payload.teachers.totalElements,
          totalPage: action.payload.teachers.totalPage,
        },
      };
    case type.GET_TEACHERS_FAILED:
    case type.GET_TEACHER_ID_FAILED:
      return {
        ...newState,
        error: action.message,
      };
    case type.GET_TEACHER_ID_SUCCESS:
      return {
        ...newState,
        teacher: action.payload.teacher,
      };
    case type.ADD_TEACHER_SUCCESS:
    case type.ADD_TEACHER_FAILED:
    case type.UPDATE_TEACHER_SUCCESS:
    case type.UPDATE_TEACHER_FAILED:
    case type.DELETE_TEACHER_SUCCESS:
    case type.DELETE_TEACHER_FAILED:
    case type.GET_SUBJECTS_FAILED:
      return {
        ...newState,
        actionStatus: {
          status: action.payload.status,
          title: action.payload.title,
          content: action.payload.content,
          account: action.payload.account ? action.payload.account : null,
        },
      };
    case type.GET_SUBJECTS_SUCCESS:
      return {
        ...newState,
        subjects: action.payload.subjects,
      };
    default:
      return state;
  }
};

export default teachersReducer;
