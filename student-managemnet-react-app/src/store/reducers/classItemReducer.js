import { GET_CLASS } from '../../constants/actionTypes';

const initialState = {
  name: '',
  status: true,
};

const classItem = (state = initialState, action) => {
  let currentClass = state;
  if (action.type === GET_CLASS) {
    currentClass = action.payload;
    return currentClass;
  }
  return currentClass;
};

export default classItem;
