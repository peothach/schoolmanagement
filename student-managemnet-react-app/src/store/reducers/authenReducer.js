import _ from 'lodash';
import * as type from '../../constants/authenTypes';
import {
  LOADING,
  SUCCESS,
  FAILED,
  ROLE,
  TOKEN,
  USER_NAME,
} from '../../constants/constants';

const initialState = {
  token: '',
  username: '',
  role: '',
  personId: '',
  roles: [],
  error: null,
  success: null,
  loading: false,
  isLoggedIn: false,
  passwordChecking: null,
};

/**
 * Function that retrieves authen data from local storage
 * @returns {Object} - Stored authen data
 */
const retrieveToken = () => {
  const token = localStorage.getItem(TOKEN);
  const username = localStorage.getItem(USER_NAME);
  const role = localStorage.getItem(ROLE);

  return token
    ? {
        token,
        username,
        role,
        isLoggedIn: true,
      }
    : null;
};

// If there's authen that's stored in local storage,
// Initialize with authen data
const tokenData = retrieveToken();
let initialStateAfter;
if (tokenData) {
  initialStateAfter = {
    ...initialState,
    ...tokenData,
  };
} else {
  initialStateAfter = {
    ...initialState,
  };
}

/**
 * @function authenReducer
 * @param {Object} state - Previous State
 * @param {Object} action - Action dispatch to reducer
 * @returns {Object} - New state
 */
const authenReducer = (state = initialStateAfter, action = {}) => {
  const newState = _.cloneDeep(state);
  switch (action.type) {
    case type.GET_ROLES_REQUEST:
    case type.LOGIN_REQUEST:
    case type.CHANGE_PASSWORD_REQUEST:
      return {
        ...newState,
        loading: true,
      };
    case type.INPUT_PASSWORD_REQUEST:
    case type.CHECK_PASSWORD_REQUEST:
      return {
        ...newState,
        passwordChecking: LOADING,
      };
    case type.CLEAR_PASSWORD_REQUEST:
      return {
        ...newState,
        passwordChecking: null,
      };
    case type.LOGOUT_REQUEST:
      return {
        ...initialState,
      };

    case type.GET_ROLES_SUCCESS:
      return {
        ...newState,
        roles: action.payload.roles,
        loading: false,
      };
    case type.LOGIN_SUCCESS:
      return {
        ...newState,
        loading: false,
        token: action.payload.data.jwt,
        username: action.payload.data.username,
        role: action.payload.data.roles[0],
        personId: action.payload.data.personId,
        isLoggedIn: true,
      };
    case type.CHECK_PASSWORD_SUCCESS:
      return {
        ...newState,
        passwordChecking: SUCCESS,
      };
    case type.CHANGE_PASSWORD_SUCCESS:
      return {
        ...newState,
        loading: false,
        success: action.payload.success,
      };

    case type.GET_ROLES_FAILED:
    case type.LOGIN_FAILED:
    case type.CHANGE_PASSWORD_FAILED:
      return {
        ...newState,
        loading: false,
        error: action.payload.error,
      };
    case type.CHECK_PASSWORD_FAILED:
      return {
        ...newState,
        passwordChecking: FAILED,
      };

    case type.HIDE_ALERT_DIALOG:
      return {
        ...newState,
        loading: false,
        error: null,
        success: null,
      };
    default:
      return state;
  }
};

export default authenReducer;
