import { combineReducers } from 'redux';
import authenReducer from './authenReducer';
import classItem from './classItemReducer';
import classes from './classReducer';
import gradeItem from './gradeItemReducer';
import grades from './gradeReducer';
import loading from './loadingReducer';
import studentReducer from './studentReducer';
import studentsManagementReducer from './studentsManagamentReducer';
import subjectItem from './subjectItemReducer';
import subjects from './subjectReducer';
import teacherGrades from './teacherGradeReducer';
import teachersReducer from './teachersReducer';
import message from './messageReducer';
import topicsReducer from './topicsReducer';
import postsReducer from './postsReducer';

const rootReducer = combineReducers({
  subjects,
  subjectItem,
  loading,
  teacherGrades,
  gradeItem,
  grades,
  classes,
  classItem,
  message,
  teachers: teachersReducer,
  student: studentReducer,
  students: studentsManagementReducer,
  authen: authenReducer,
  topics: topicsReducer,
  posts: postsReducer,
});

export default rootReducer;
