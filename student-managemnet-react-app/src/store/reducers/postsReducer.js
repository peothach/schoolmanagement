import _ from 'lodash';
import { forumTypes } from '../../constants/forumTypes';

const initialState = {
  posts: [],
  comments: [],
  loading: false,
  actionStatus: null,
};

const postsReducer = (state = initialState, action = {}) => {
  const newState = _.cloneDeep(state);

  switch (action.type) {
    case forumTypes.GET_TOPIC_DETAIL_REQUEST:
    case forumTypes.GET_POST_DETAIL_REQUEST:
    case forumTypes.CREATE_COMMENT_REQUEST:
      return {
        ...newState,
        loading: true,
      };
    case forumTypes.GET_TOPIC_DETAIL_SUCCESS:
      return {
        ...newState,
        loading: false,
        posts: action.payload.posts,
      };
    case forumTypes.GET_POST_DETAIL_SUCCESS:
      return {
        ...newState,
        loading: false,
        comments: action.payload.comments,
      };
    case forumTypes.CREATE_COMMENT_SUCCESS:
      return {
        ...newState,
        loading: false,
      };
    case forumTypes.GET_TOPIC_DETIAL_FAILED:
    case forumTypes.GET_POST_DETIAL_FAILED:
      return {
        ...newState,
        loading: false,
        actionStatus: {
          status: action.payload.status,
          title: action.payload.title,
          content: action.payload.content,
        },
      };
    default:
      return state;
  }
};

export default postsReducer;
