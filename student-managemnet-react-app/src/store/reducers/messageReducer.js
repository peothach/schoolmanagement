const initialState = [];

const message = (state = initialState, action) => {
  let currentState = [...state];
  switch (action.type) {
    case 'SAVE_MESSAGE': {
      const newState = Object.assign([], currentState);
      newState.push(action.payload);
      currentState = newState;
      return [...currentState];
    }
    case 'GET_MESSAGE': {
      currentState = action.payload;
      return [...currentState];
    }
    default:
      return state;
  }
};

export default message;
