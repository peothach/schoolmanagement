import _ from 'lodash';
import * as type from '../../constants/studentsManagementTypes';

// Initial state of the students state
const initialState = {
  students: [],
  student: null,
  pageable: {
    page: 0,
    limit: 0,
    totalElements: 0,
  },
  loading: false,
  showAlertDialog: false,
  actionStatus: null,
  classrooms: [],
};

/**
 * @function studentsManagementReducer
 * @param {Object} state - Previous state
 * @param {Oject} action - Action to dispatch to reducer
 * @returns {Object} - New state
 */
const studentsManagementReducer = (state = initialState, action = {}) => {
  const newState = _.cloneDeep(state);
  switch (action.type) {
    case type.GET_STUDENTS_REQUEST:
    case type.GET_STUDENT_ID_REQUESTED:
    case type.ADD_STUDENT_REQUESTED:
    case type.UPDATE_STUDENT_REQUESTED:
    case type.GET_CLASSROOM_REQUESTED:
      return {
        ...newState,
        loading: true,
      };
    case type.GET_STUDENTS_SUCCESS:
      return {
        ...newState,
        loading: false,
        students: action.payload.students.content,
        pageable: {
          page: action.payload.students.page,
          limit: action.payload.students.limit,
          totalElements: action.payload.students.totalElements,
        },
      };
    case type.GET_STUDENT_ID_SUCCESS:
      return {
        ...newState,
        loading: false,
        student: action.payload.student,
      };
    case type.GET_CLASSROOM_SUCCESS:
      return {
        ...newState,
        loading: false,
        classrooms: action.payload.classrooms,
      };
    case type.GET_STUDENTS_FAILED:
    case type.ADD_STUDENT_SUCCESS:
    case type.ADD_STUDENT_FAILED:
    case type.UPDATE_STUDENT_SUCCESS:
    case type.UPDATE_STUDENT_FAILED:
    case type.GET_CLASSROOM_FAILED:
      return {
        ...newState,
        loading: false,
        actionStatus: {
          status: action.payload.status,
          title: action.payload.title,
          content: action.payload.content,
          account: action.payload.account ? action.payload.account : null,
        },
      };
    case type.HIDE_STUDENT_DIALOG:
      return {
        ...newState,
        loading: false,
        actionStatus: null,
      };
    default:
      return state;
  }
};

export default studentsManagementReducer;
