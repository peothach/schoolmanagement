import {
  FETCH_CLASSES,
  ADD_CLASS,
  UPDATE_CLASS,
  DELETE_CLASS,
  SEARCH_SORT_FILTER_CLASS,
} from '../../constants/actionTypes';

const initialState = [];

const classes = (state = initialState, action) => {
  let currentClasses = [...state];
  switch (action.type) {
    case FETCH_CLASSES:
    case SEARCH_SORT_FILTER_CLASS:
      currentClasses = action.payload.content;
      return [...currentClasses];
    case ADD_CLASS: {
      const newClasses = JSON.parse(JSON.stringify(currentClasses));
      newClasses.push(action.payload);
      currentClasses = newClasses;
      return [...currentClasses];
    }
    case DELETE_CLASS: {
      const newClasses = JSON.parse(JSON.stringify(currentClasses));
      const index = newClasses.findIndex(
        (classObject) => classObject.id === action.payload.id
      );
      if (index !== -1) {
        newClasses.splice(index, 1);
        currentClasses = newClasses;
      }
      return [...currentClasses];
    }
    case UPDATE_CLASS: {
      const newClasses = JSON.parse(JSON.stringify(currentClasses));
      const index = newClasses.findIndex(
        (classObject) => classObject.id === action.payload.id
      );
      if (index !== -1) {
        newClasses[index] = action.payload;
        currentClasses = newClasses;
      }
      return currentClasses;
    }
    default:
      return state;
  }
};

export default classes;
