import { loadingTypes } from '../../constants/actionTypes';

const initialState = false;

const loading = (state = initialState, action) => {
  switch (action.type) {
    case loadingTypes.SHOW_LOADING:
      return true;
    case loadingTypes.HIDE_LOADING:
      return false;
    default:
      return state;
  }
};

export default loading;
