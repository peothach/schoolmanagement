import * as type from '../../constants/GradeTypes';

export const initialState = {
  grades: [],
  grade: null,
  pageable: {
    page: 0,
    limit: 0,
    totalElements: 0,
    totalPages: 0,
  },
  loading: false,
  error: null,
  showAlertDialog: false,
  actionStatus: null,
};

const gradeReducer = (state = initialState, action = {}) => {
  const newState = {
    ...state,
    loading: false,
    pageable: { ...state.pageable },
    grades: [...state.grades],
    grade: { ...state.grade },
    actionStatus: { ...state.actionStatus },
  };

  switch (action.type) {
    case type.SHOW_GRADE_DIALOG:
      return { ...newState, showAlertDialog: true };
    case type.HIDE_GRADE_DIALOG:
      return { ...newState, showAlertDialog: false };
    case type.TEACHER_GET_CLASS_GRADE_REQUESTED:
    case type.TEACHER_GET_GRADE_REQUESTED:
    case type.TEACHER_ADD_GRADE_REQUESTED:
    case type.TEACHER_UPDATE_GRADE_REQUESTED:
    case type.TEACHER_DELETE_GRADE_REQUESTED:
      return {
        ...newState,
        loading: true,
      };
    case type.TEACHER_GET_CLASS_GRADE_SUCCESS:
      return {
        ...newState,
        grades: action.payload.grades.content,
        pageable: {
          page: action.payload.grades.page,
          limit: action.payload.grades.limit,
          totalElements: action.payload.grades.totalElements,
          totalPages: action.payload.grades.totalPages,
        },
      };
    case type.TEACHER_GET_GRADE_SUCCESS:
      return {
        ...newState,
        grade: action.payload.grade,
      };
    case type.TEACHER_GET_CLASS_GRADE_FAILED:
    case type.TEACHER_GET_GRADE_FAILED:
      return {
        ...newState,
        error: action.payload.error,
      };
    case type.TEACHER_ADD_GRADE_SUCCESS:
    case type.TEACHER_ADD_GRADE_FAILED:
    case type.TEACHER_UPDATE_GRADE_SUCCESS:
    case type.TEACHER_UPDATE_GRADE_FAILED:
    case type.TEACHER_DELETE_GRADE_SUCCESS:
    case type.TEACHER_DELETE_GRADE_FAILED:
      return {
        ...newState,
        actionStatus: {
          status: action.payload.status,
          title: action.payload.title,
          content: action.payload.content,
          account: action.payload.account,
        },
      };
    default:
      return state;
  }
};

export default gradeReducer;
