import { gradesTypes } from '../../constants/actionTypes';
import { toastSuccess, toastError } from '../../utils/toast';

const initialState = [];

const grades = (state = initialState, action) => {
  let currentState = [...state];
  switch (action.type) {
    case gradesTypes.FETCH_GRADES: {
      return [...state];
    }

    case gradesTypes.FETCH_GRADES_SUCCESS: {
      currentState = action.payload;
      toastSuccess('Get list grades succesfull');
      return [...currentState];
    }

    case gradesTypes.FETCH_GRADES_FAILED:
    case gradesTypes.SEARCH_GRADES_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return [...state];
    }

    case gradesTypes.SEARCH_GRADES_SUCCESS: {
      currentState = action.payload;
      toastSuccess('Search student succesfull');
      return [...currentState];
    }

    case gradesTypes.DELETE_GRADE_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return [...currentState];
    }

    case gradesTypes.DELETE_GRADE_SUCCESS: {
      toastSuccess('Delete grades student succesfull');
      const newState = Object.assign([], currentState);
      const index = newState.findIndex(
        (student) => student.studentId === action.payload
      );
      if (index !== -1) {
        newState.splice(index, 1);
        currentState = newState;
      }
      return [...currentState];
    }

    default:
      return state;
  }
};

export default grades;
