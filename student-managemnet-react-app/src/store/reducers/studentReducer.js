import _ from 'lodash';
import moment from 'moment';
import * as type from '../../constants/studentTypes';

export const initialState = {
  student: null,
  loading: false,
  error: null,
  grades: [],
  pageable: {
    page: 0,
    limit: 0,
    totalElements: 0,
  },
};

/**
 * @function studentReducer
 * @param {Object} state - Previous State
 * @param {Object} action - Action dispatch to reducer
 * @returns {Object} - New state
 */
const studentReducer = (state = initialState, action = {}) => {
  const newState = _.cloneDeep(state);

  switch (action.type) {
    case type.GET_STUDENT_INFO_REQUEST:
    case type.GET_STUDENT_MARKS_REQUEST:
      return {
        ...newState,
        loading: true,
      };
    case type.GET_STUDENT_INFO_SUCCESS:
      return {
        ...newState,
        loading: false,
        student: {
          ...action.payload.student,
          birthdate: moment(action.payload.student.birthdate).format(
            'DD-MM-YYYY'
          ),
        },
      };
    case type.GET_STUDENT_MARKS_SUCCESS:
      return {
        ...newState,
        loading: false,
        grades: action.payload.grades.content,
        pageable: {
          page: action.payload.grades.page,
          limit: action.payload.grades.limit,
          totalElements: action.payload.grades.totalElements,
        },
      };
    case type.GET_STUDENT_INFO_FAILED:
    case type.GET_STUDENT_MARKS_FAILED:
      return {
        ...newState,
        loading: false,
        error: action.payload.message,
      };
    default:
      return state;
  }
};

export default studentReducer;
