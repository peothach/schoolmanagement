import { gradesTypes } from '../../constants/actionTypes';
import { toastSuccess, toastError } from '../../utils/toast';

const initialState = {};

const gradeItem = (state = initialState, action) => {
  switch (action.type) {
    case gradesTypes.GET_GRADE_SUCCESS: {
      toastSuccess('Grades already update');
      return { ...action.payload };
    }
    case gradesTypes.UPDATE_GRADE_FAILED:
    case gradesTypes.GET_GRADE_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return state;
    }
    case gradesTypes.UPDATE_GRADE_SUCCESS: {
      toastSuccess('Update student succesfull');
      return { ...action.payload };
    }
    default:
      return state;
  }
};

export default gradeItem;
