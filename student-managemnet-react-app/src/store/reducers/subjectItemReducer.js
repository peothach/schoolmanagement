import { subjectTypes } from '../../constants/actionTypes';
import { toastSuccess, toastError } from '../../utils/toast';

const initialState = {
  id: 0,
  name: '',
  status: true,
};

const subjectItem = (state = initialState, action) => {
  switch (action.type) {
    case subjectTypes.GET_SUBJECT_SUCCESS: {
      toastSuccess('Subject already update');
      return { ...action.payload };
    }
    case subjectTypes.GET_SUBJECT_FAILED: {
      const { message } = action.payload;
      toastError(message);
      return state;
    }
    default:
      return state;
  }
};

export default subjectItem;
