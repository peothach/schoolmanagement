import _ from 'lodash';
import { forumTypes } from '../../constants/forumTypes';

const initialState = {
  topics: [],
  loading: false,
  actionStatus: null,
};

const topicsReducer = (state = initialState, action = {}) => {
  const newState = _.cloneDeep(state);

  switch (action.type) {
    case forumTypes.GET_TOPICS_REQUEST:
      return {
        ...newState,
        loading: true,
      };
    case forumTypes.GET_TOPICS_SUCCESS:
      return {
        ...newState,
        loading: false,
        topics: action.payload.topics,
      };
    case forumTypes.GET_TOPICS_FAILED:
      return {
        ...newState,
        loading: false,
        actionStatus: {
          status: action.payload.status,
          title: action.payload.title,
          content: action.payload.content,
        },
      };
    default:
      return state;
  }
};

export default topicsReducer;
