import { call, put, takeEvery } from 'redux-saga/effects';

import * as type from '../../../constants/teacherTypes';
import {
  getTeachers,
  getTeacherById,
  addTeacher,
  deleteTeacher,
  getSubjects,
  updateTeacher,
} from '../requests/teachersRequest';

/**
 * Function that handles GET_TEACHERS_REQUESTED action
 *
 * @function handleGetTeachers
 * @param {Object} payload - Action payload
 */
function* handleGetTeachers({ payload }) {
  try {
    const teachers = yield call(getTeachers, payload.params);
    yield put({ type: type.GET_TEACHERS_SUCCESS, payload: { teachers } });
  } catch (error) {
    yield put({ type: type.GET_TEACHERS_FAILED, message: error.message });
  }
}

/**
 * Function that handles GET_TEACHER_ID_REQUESTED action
 *
 * @function handleGetTeacherById
 * @param {Object} payload - Action payload
 */
function* handleGetTeacherById({ payload }) {
  try {
    const teacher = yield call(getTeacherById, payload.id);
    yield put({ type: type.GET_TEACHER_ID_SUCCESS, payload: { teacher } });
  } catch (error) {
    yield put({ type: type.GET_TEACHER_ID_FAILED, message: error.message });
  }
}

/**
 * Function that handles ADD_TEACHER_REQUESTED action
 *
 * @function handleAddTeacher
 * @param {Object} payload - Action payload
 */
function* handleAddTeacher({ payload }) {
  try {
    const teacher = yield call(addTeacher, payload.teacher);

    yield put({
      type: type.ADD_TEACHER_SUCCESS,
      payload: {
        status: 'success',
        title: 'Add Success!',
        content: `Add teacher is success.`,
        account: teacher.account,
      },
    });

    const teachers = yield call(getTeachers, payload.params);

    yield put({ type: type.GET_TEACHERS_SUCCESS, payload: { teachers } });
    yield put({ type: type.SHOW_TEACHER_DIALOG });
  } catch (error) {
    yield put({
      type: type.ADD_TEACHER_FAILED,
      payload: {
        status: 'failed',
        title: 'Add Failed!',
        content: 'Something happend! Please try again later.',
        account: null,
      },
    });
    yield put({ type: type.SHOW_TEACHER_DIALOG });
  }
}

/**
 * Function that handles UPDATE_TEACHER_REQUESTED action
 *
 * @function handleUpdateTeacher
 * @param {Object} payload - Action payload
 */
function* handleUpdateTeacher({ payload }) {
  try {
    const teacher = yield call(updateTeacher, payload.teacher);
    yield put({
      type: type.UPDATE_TEACHER_SUCCESS,
      payload: {
        status: 'success',
        title: 'Update Success!',
        content: `Update teacher is success.`,
        account: teacher.account,
      },
    });

    const teachers = yield call(getTeachers, payload.params);
    yield put({ type: type.GET_TEACHERS_SUCCESS, payload: { teachers } });

    yield put({ type: type.SHOW_TEACHER_DIALOG });
  } catch (error) {
    yield put({
      type: type.UPDATE_TEACHER_FAILED,
      payload: {
        status: 'failed',
        title: 'Update Failed!',
        content: 'Something happend! Please try again later.',
        account: null,
      },
    });
    yield put({ type: type.SHOW_TEACHER_DIALOG });
  }
}

/**
 * Function that handles DELETE_TEACHER_REQUESTED action
 *
 * @function handleDeleteTeacher
 * @param {Object} payload - Action payload
 */
function* handleDeleteTeacher({ payload }) {
  try {
    yield call(deleteTeacher, payload.id);
    yield put({
      type: type.DELETE_TEACHER_SUCCESS,
      payload: {
        status: 'success',
        title: 'Delete Success!',
        content: 'Delete Teacher Successfully!',
      },
    });

    const teachers = yield call(getTeachers, payload.params);
    yield put({ type: type.GET_TEACHERS_SUCCESS, payload: { teachers } });

    yield put({ type: type.SHOW_TEACHER_DIALOG });
  } catch (error) {
    yield put({
      type: type.DELETE_TEACHER_FAILED,
      payload: {
        status: 'failed',
        title: 'Delete Failed!',
        content: 'Something happend! Please try again later.',
      },
    });
    yield put({ type: type.SHOW_TEACHER_DIALOG });
  }
}

/**
 * Function that handles GET_SUBJECTS_REQUESTED action
 *
 * @function handleDeleteTeacher
 */
function* handleGetSubjects() {
  try {
    const subjects = yield call(getSubjects);
    yield put({
      type: type.GET_SUBJECTS_SUCCESS,
      payload: {
        subjects,
      },
    });
  } catch (error) {
    yield put({
      type: type.GET_SUBJECTS_FAILED,
      payload: {
        status: 'failed',
        title: 'Get Subject Failed!',
        content: 'Something happend! Please try again later.',
      },
    });
    yield put({ type: type.SHOW_TEACHER_DIALOG });
  }
}

function* teachersHandler() {
  yield takeEvery(type.GET_TEACHERS_REQUESTED, handleGetTeachers);
  yield takeEvery(type.GET_TEACHER_ID_REQUESTED, handleGetTeacherById);
  yield takeEvery(type.ADD_TEACHER_REQUESTED, handleAddTeacher);
  yield takeEvery(type.UPDATE_TEACHER_REQUESTED, handleUpdateTeacher);
  yield takeEvery(type.DELETE_TEACHER_REQUESTED, handleDeleteTeacher);
  yield takeEvery(type.GET_SUBJECTS_REQUESTED, handleGetSubjects);
}

export default teachersHandler;
