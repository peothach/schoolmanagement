import { call, put, takeEvery } from 'redux-saga/effects';

import { forumTypes } from '../../../constants/forumTypes';
import { getTopics } from '../requests/topicsRequest';

const STATUS_FAILED = 'failed';
const ERROR_MESSAGE = 'Something happend! Please try again later.';
const ERROR_LOAD_TITLE = 'Load Failed!';

function* handleGetTopics({ payload }) {
  try {
    const topics = yield call(getTopics, payload.params);
    yield put({ type: forumTypes.GET_TOPICS_SUCCESS, payload: { topics } });
  } catch (err) {
    yield put({
      type: forumTypes.GET_TOPICS_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: ERROR_LOAD_TITLE,
        content: ERROR_MESSAGE,
      },
    });
  }
}

function* topicsHandler() {
  yield takeEvery(forumTypes.GET_TOPICS_REQUEST, handleGetTopics);
}

export default topicsHandler;
