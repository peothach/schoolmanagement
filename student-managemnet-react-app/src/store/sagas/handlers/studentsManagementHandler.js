import { call, put, takeEvery } from 'redux-saga/effects';

import * as type from '../../../constants/studentsManagementTypes';
import {
  getStudents,
  addStuent,
  getStudentById,
  updateStudent,
  deleteStudent,
  getClassrooms,
} from '../requests/studentsManagementRequest';

const ERROR_MESSAGE = 'Something happend! Please try again later.';
const STATUS_FAILED = 'failed';
const STATUS_SUCCESS = 'success';

/**
 * Function that handles GET_STUDENTS_REQUEST action
 * @param {Object} payload - Action payload
 */
function* handleGetStudents({ payload }) {
  try {
    const students = yield call(getStudents, payload.params);
    yield put({ type: type.GET_STUDENTS_SUCCESS, payload: { students } });
  } catch (error) {
    yield put({
      type: type.GET_STUDENTS_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Load Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles GET_STUDENT_ID_REQUESTED action
 * @param {Object} payload- Action payload
 */
function* handleGetStudentById({ payload }) {
  try {
    const student = yield call(getStudentById, payload.id);
    yield put({
      type: type.GET_STUDENT_ID_SUCCESS,
      payload: {
        student,
      },
    });
  } catch (error) {
    yield put({
      type: type.GET_STUDENT_ID_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Load Student Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles ADD_STUDENT_REQUESTED action
 * @param {Object} payload- Action payload
 */
function* handleAddStudent({ payload }) {
  try {
    const student = yield call(addStuent, payload.student);
    yield put({
      type: type.ADD_STUDENT_SUCCESS,
      payload: {
        status: STATUS_SUCCESS,
        title: 'Add Success!',
        content: 'Add student is success.',
        account: student.account,
      },
    });

    const students = yield call(getStudents, payload.params);
    yield put({ type: type.GET_STUDENTS_SUCCESS, payload: { students } });
  } catch (error) {
    yield put({
      type: type.ADD_STUDENT_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Add Student Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles UPDATE_STUDENT_REQUESTED action
 * @param {Object} payload- Action payload
 */
function* handleUpdateStudent({ payload }) {
  try {
    const student = yield call(updateStudent, payload.student);
    yield put({
      type: type.UPDATE_STUDENT_SUCCESS,
      payload: {
        status: STATUS_SUCCESS,
        title: 'Update Success!',
        content: 'Update student is success.',
        account: student.account,
      },
    });

    const students = yield call(getStudents, payload.params);
    yield put({ type: type.GET_STUDENTS_SUCCESS, payload: { students } });
  } catch (error) {
    yield put({
      type: type.UPDATE_STUDENT_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Update Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles DELETE_STUDENT_REQUESTED action
 * @param {Object} payload- Action payload
 */
function* handleDeleteStudent({ payload }) {
  try {
    yield call(deleteStudent, payload.id);

    const students = yield call(getStudents, payload.params);
    yield put({ type: type.GET_STUDENTS_SUCCESS, payload: { students } });
  } catch (error) {
    yield put({
      type: type.DELETE_STUDENT_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Delete Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles GET_CLASSROOM_REQUESTED action
 */
function* handleGetClassroom() {
  try {
    const classrooms = yield call(getClassrooms);
    yield put({ type: type.GET_CLASSROOM_SUCCESS, payload: { classrooms } });
  } catch (error) {
    put({
      type: type.GET_CLASSROOM_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Get classrooms Failed!',
        content: ERROR_MESSAGE,
        account: null,
      },
    });
  }
}

/**
 * @functon studentsManagementHandler
 */
function* studentsManagementHandler() {
  yield takeEvery(type.GET_STUDENTS_REQUEST, handleGetStudents);
  yield takeEvery(type.GET_STUDENT_ID_REQUESTED, handleGetStudentById);
  yield takeEvery(type.ADD_STUDENT_REQUESTED, handleAddStudent);
  yield takeEvery(type.UPDATE_STUDENT_REQUESTED, handleUpdateStudent);
  yield takeEvery(type.DELETE_STUDENT_REQUESTED, handleDeleteStudent);
  yield takeEvery(type.GET_CLASSROOM_REQUESTED, handleGetClassroom);
}

export default studentsManagementHandler;
