import { call, put, takeEvery } from 'redux-saga/effects';

import { forumTypes } from '../../../constants/forumTypes';
import {
  getPostsByTopic,
  getPostById,
  createComment,
} from '../requests/postsRequest';

const STATUS_FAILED = 'failed';
const ERROR_MESSAGE = 'Something happend! Please try again later.';
const ERROR_LOAD_TITLE = 'Load Failed!';

function* handleGetPostsByTopic({ payload }) {
  try {
    const posts = yield call(getPostsByTopic, payload.id, payload.params);
    yield put({
      type: forumTypes.GET_TOPIC_DETAIL_SUCCESS,
      payload: { posts },
    });
  } catch (err) {
    yield put({
      type: forumTypes.GET_TOPIC_DETIAL_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: ERROR_LOAD_TITLE,
        content: ERROR_MESSAGE,
      },
    });
  }
}

function* handleGetPostById({ payload }) {
  try {
    const comments = yield call(getPostById, payload.id, payload.params);
    yield put({
      type: forumTypes.GET_POST_DETAIL_SUCCESS,
      payload: { comments },
    });
  } catch (err) {
    yield put({
      type: forumTypes.GET_POST_DETIAL_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: ERROR_LOAD_TITLE,
        content: ERROR_MESSAGE,
      },
    });
  }
}

/**
 * Function that handles ADD_STUDENT_REQUESTED action
 * @param {Object} payload- Action payload
 */
function* handleCreateComment({ payload }) {
  try {
    yield call(createComment, payload.data);
    yield put({
      type: forumTypes.CREATE_COMMENT_SUCCESS,
    });

    const comments = yield call(getPostById, payload.id);
    yield put({
      type: forumTypes.GET_POST_DETAIL_SUCCESS,
      payload: { comments },
    });
  } catch (error) {
    yield put({
      type: forumTypes.CREATE_COMMENT_FAILED,
      payload: {
        status: STATUS_FAILED,
        title: 'Add Comment Failed!',
        content: ERROR_MESSAGE,
      },
    });
  }
}

function* postsHandler() {
  yield takeEvery(forumTypes.GET_TOPIC_DETAIL_REQUEST, handleGetPostsByTopic);
  yield takeEvery(forumTypes.GET_POST_DETAIL_REQUEST, handleGetPostById);
  yield takeEvery(forumTypes.CREATE_COMMENT_REQUEST, handleCreateComment);
}

export default postsHandler;
