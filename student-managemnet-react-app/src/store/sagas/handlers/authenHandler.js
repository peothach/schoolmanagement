import { call, put, takeEvery } from 'redux-saga/effects';

import {
  requestLogin,
  requestCheckPassword,
  requestChangePasswrod,
  getRoles,
} from '../requests/authenRequest';
import * as type from '../../../constants/authenTypes';
import * as role from '../../../constants/constants';

/**
 * Function that handles LOGIN_REQUEST action
 * @param {Object} payload
 */
function* handleRequestLogin({ payload }) {
  try {
    const data = yield call(requestLogin, payload.data);
    yield put({ type: type.LOGIN_SUCCESS, payload: { data } });

    // Store authen data to local storage
    localStorage.setItem(role.TOKEN, data.jwt);
    localStorage.setItem(role.USER_NAME, data.username);
    localStorage.setItem(role.ROLE, data.roles[0]);
    localStorage.setItem(role.PERSON_ID, data.personId);
  } catch (error) {
    if (error.response) {
      const statusCode = error.response.status;
      if (statusCode === 400 || statusCode === 401) {
        error.message = 'Username or password incorrect!';
      } else if (error.response.status === 403) {
        error.message = 'Your role is not supported!';
      }
    }
    yield put({
      type: type.LOGIN_FAILED,
      payload: {
        error: {
          status: 'FAILED',
          title: 'Login Failed!',
          message: error.message,
        },
      },
    });
  }
}

/**
 * Function that handles CHECK_PASSWORD_REQUEST action
 * @param {Object} payload
 */
function* handleCheckPassword({ payload }) {
  try {
    yield call(requestCheckPassword, payload.data);
    yield put({ type: type.CHECK_PASSWORD_SUCCESS });
  } catch (error) {
    yield put({
      type: type.CHECK_PASSWORD_FAILED,
    });
  }
}

/**
 * Function that handles CHANGE_PASSWORD_REQUEST action
 * @param {Object} payload
 */
function* handleChangePassword({ payload }) {
  try {
    yield call(requestChangePasswrod, payload.data);
    yield put({
      type: type.CHANGE_PASSWORD_SUCCESS,
      payload: {
        success: {
          status: 'SUCCESS',
          title: 'Change Password Success!',
          message: 'Password is changed successfully.',
        },
      },
    });
  } catch (error) {
    yield put({
      type: type.CHANGE_PASSWORD_FAILED,
      payload: {
        error: {
          status: 'FAILED',
          title: 'Change Password Failed!',
          message: error.message,
        },
      },
    });
  }
}

/**
 * Function that handles GET_ROLES_REQUEST action
 */
function* handleGetRoles() {
  try {
    const roles = yield call(getRoles);
    yield put({ type: type.GET_ROLES_SUCCESS, payload: { roles } });
  } catch (error) {
    yield put({
      type: type.GET_ROLES_FAILED,
      payload: {
        error: {
          status: 'FAILED',
          title: 'Load Roles Failed!',
          message: error.message,
        },
      },
    });
  }
}

/**
 * @function authenHandler
 */
function* authenHandler() {
  yield takeEvery(type.LOGIN_REQUEST, handleRequestLogin);
  yield takeEvery(type.CHECK_PASSWORD_REQUEST, handleCheckPassword);
  yield takeEvery(type.CHANGE_PASSWORD_REQUEST, handleChangePassword);
  yield takeEvery(type.GET_ROLES_REQUEST, handleGetRoles);
}

export default authenHandler;
