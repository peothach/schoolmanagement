import { call, put, takeEvery } from 'redux-saga/effects';

import * as type from '../../../constants/studentTypes';
import { getStudent, getStudentMarks } from '../requests/studentRequest';

/**
 * Function that handles GET_STUDENT_INFO_REQUEST action
 *
 * @function handleGetStudent
 * @param {Object} payload - Contain id student
 */
function* handleGetStudent({ payload }) {
  try {
    const student = yield call(getStudent, payload.id);
    yield put({ type: type.GET_STUDENT_INFO_SUCCESS, payload: { student } });
  } catch (error) {
    yield put({
      type: type.GET_STUDENT_INFO_FAILED,
      payload: { message: error.message },
    });
  }
}

/**
 * Function that handles GET_STUDENT_MARKS_REQUEST action
 *
 * @function handleGetStudentMarks
 * @param {Object} payload - Contains student id and request params
 */
function* handleGetStudentMarks({ payload }) {
  try {
    const grades = yield call(getStudentMarks, payload.id, payload.params);
    yield put({ type: type.GET_STUDENT_MARKS_SUCCESS, payload: { grades } });
  } catch (error) {
    yield put({
      type: type.GET_STUDENT_MARKS_FAILED,
      payload: { message: error.message },
    });
  }
}

/**
 * @function studentHandler
 */
function* studentHandler() {
  yield takeEvery(type.GET_STUDENT_INFO_REQUEST, handleGetStudent);
  yield takeEvery(type.GET_STUDENT_MARKS_REQUEST, handleGetStudentMarks);
}

export default studentHandler;
