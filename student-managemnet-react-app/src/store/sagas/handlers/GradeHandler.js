import { call, put, takeEvery } from 'redux-saga/effects';
import * as type from '../../../constants/GradeTypes';
import {
  getAllClassGrades,
  deleteGrade,
  getGrade,
  updateGrade,
} from '../requests/GradeRequest';

function* handleGetAllClassGrades({ payload }) {
  try {
    const grades = yield call(getAllClassGrades, payload.params);
    yield put({
      type: type.TEACHER_GET_CLASS_GRADE_SUCCESS,
      payload: { grades },
    });
  } catch (error) {
    yield put({
      type: type.TEACHER_GET_CLASS_GRADE_FAILED,
      error: error.message,
    });
  }
}

function* handleGetGrade({ payload }) {
  try {
    const grade = yield call(getGrade, payload.id);
    yield put({ type: type.TEACHER_GET_GRADE_SUCCESS, payload: { grade } });
  } catch (error) {
    yield put({ type: type.TEACHER_GET_GRADE_FAILED, message: error.message });
  }
}

function* handleDeleteGrade({ payload }) {
  try {
    yield call(deleteGrade, payload.id);
    yield put({
      type: type.TEACHER_DELETE_GRADE_SUCCESS,
      payload: {
        status: 'success',
        title: 'Delete Success!',
        content: 'Delete Grade Successfully!',
      },
    });

    const grades = yield call(getAllClassGrades, payload.params);
    yield put({
      type: type.TEACHER_GET_CLASS_GRADE_SUCCESS,
      payload: { grades },
    });

    yield put({ type: type.SHOW_GRADE_DIALOG });
  } catch (error) {
    yield put({
      type: type.TEACHER_DELETE_GRADE_FAILED,
      payload: {
        status: 'failed',
        title: 'Delete Failed!',
        content: 'Something happend! Please try again later.',
      },
    });
    yield put({ type: type.SHOW_GRADE_DIALOG });
  }
}

function* handleUpdateGrade({ payload }) {
  try {
    yield call(updateGrade, payload.grade);
    yield put({
      type: type.TEACHER_UPDATE_GRADE_SUCCESS,
      payload: {
        status: 'success',
        title: 'Update Success!',
        content: 'Update Grade Successfully!',
      },
    });
    const grades = yield call(getAllClassGrades, payload.params);
    yield put({
      type: type.TEACHER_GET_CLASS_GRADE_SUCCESS,
      payload: { grades },
    });

    yield put({ type: type.SHOW_GRADE_DIALOG });
  } catch (error) {
    yield put({
      type: type.TEACHER_UPDATE_GRADE_FAILED,
      payload: {
        status: 'failed',
        title: 'Update Failed!',
        content: 'Something happend! Please try again later.',
      },
    });
    yield put({ type: type.SHOW_GRADE_DIALOG });
  }
}

function* gradeHandler() {
  yield takeEvery(
    type.TEACHER_GET_CLASS_GRADE_REQUESTED,
    handleGetAllClassGrades
  );
  yield takeEvery(type.TEACHER_DELETE_GRADE_REQUESTED, handleDeleteGrade);
  yield takeEvery(type.TEACHER_GET_GRADE_REQUESTED, handleGetGrade);
  yield takeEvery(type.TEACHER_UPDATE_GRADE_REQUESTED, handleUpdateGrade);
}

export default gradeHandler;
