import { all } from 'redux-saga/effects';

import teachersHandler from './handlers/teachersHandler';
import studentHandler from './handlers/studentHandler';
import studentsManagementHandler from './handlers/studentsManagementHandler';
import authenHandler from './handlers/authenHandler';

import gradeHandler from './handlers/GradeHandler';
import topicsHandler from './handlers/topicsHandler';
import postsHandler from './handlers/postsHandler';

export default function* rootSaga() {
  yield all([
    gradeHandler(),
    teachersHandler(),
    studentHandler(),
    studentsManagementHandler(),
    authenHandler(),
    topicsHandler(),
    postsHandler(),
  ]);
}
