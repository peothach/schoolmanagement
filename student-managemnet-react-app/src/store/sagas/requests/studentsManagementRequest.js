import { apiCallerv2 } from '../../../utils/apiCaller';

/**
 * Function that calls api to get list of students
 *
 * @function getStudents
 * @param {Object} params - Request params
 * @returns {Promise} - Promise object that represent students data
 */
export const getStudents = (params) => {
  return apiCallerv2({
    url: 'students',
    params,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to get student by id
 * @function getStudentById
 * @param {number} id - Student id
 * @returns {Promise} - Promise object that represents student data
 */
export const getStudentById = (id) => {
  return apiCallerv2({
    url: `students/${id}`,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to create new student
 * @function addStuent
 * @param {Object} student - Student information
 * @returns {Promise} - Promise object
 */
export const addStuent = (student) => {
  return apiCallerv2({
    url: 'students',
    method: 'POST',
    data: student,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to update student
 * @param {Object} student - Student data
 * @returns {Promise} - Promise object
 */
export const updateStudent = (student) => {
  return apiCallerv2({
    url: `students/${student.id}`,
    method: 'PUT',
    data: student,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const deleteStudent = (id) => {
  return apiCallerv2({
    url: `students/${id}`,
    method: 'DELETE',
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to get all classrooms
 * @function getClassrooms
 * @returns {Promise} - Promise object
 */
export const getClassrooms = () => {
  return apiCallerv2({
    url: `classrooms/all`,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};
