import { apiCallerv2 } from '../../../utils/apiCaller';

/**
 * Function that calls api to request login
 * @param {Object} - Credential data
 * @returns {Promise} - Promise object
 */
export const requestLogin = (data) => {
  return apiCallerv2({
    url: 'login',
    method: 'POST',
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to check password of current user
 * @param {Object} data - Password data
 * @returns {Promise}
 */
export const requestCheckPassword = (data) => {
  return apiCallerv2({
    url: 'accounts/checkPassword',
    method: 'POST',
    data,
  });
};

/**
 * Function that calls api to change password
 * @param {*} data - password data
 * @returns {Promise} - Promise object
 */
export const requestChangePasswrod = (data) => {
  return apiCallerv2({
    url: 'accounts/changePassword',
    method: 'PUT',
    data,
  });
};

/**
 * Function that calls api to get list of roles
 * @returns {Promise} - Promise object contains list of roles
 */
export const getRoles = () => {
  return apiCallerv2({
    url: 'roles/all',
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};
