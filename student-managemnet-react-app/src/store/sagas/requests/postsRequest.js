import { apiCallerv2 } from '../../../utils/apiCaller';

export const getPostsByTopic = (id, params) => {
  return apiCallerv2({
    url: `subforums/${id}/threads?count=true`,
    params,
  })
    .then((res) => res.data.content)
    .catch((err) => {
      throw err;
    });
};

export const getPostById = (id, params) => {
  return apiCallerv2({
    url: `threads/${id}/posts`,
    params,
  })
    .then((res) => res.data.content)
    .catch((err) => {
      throw err;
    });
};

export const createComment = (data) => {
  return apiCallerv2({
    url: `posts`,
    method: 'POST',
    data,
  });
};
