import { apiCallerv2 } from '../../../utils/apiCaller';

/**
 *   Function that call api to get a student by id
 *
 * @param {number} id - Student id
 * @returns {Promise} - Promise object that represents a student object
 */
export const getStudent = (id) => {
  return apiCallerv2({
    url: `students/${id}`,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 *  Function that calls api to get a student's grades
 *
 * @param {number} id - Student id
 * @param {Object} params - Request params
 * @returns {Promise} - Promise object that contains list of student's grades
 */
export const getStudentMarks = (id, params) => {
  return apiCallerv2({
    url: `students/${id}/grades`,
    params,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};
