import { apiCallerv2 } from '../../../utils/apiCaller';

export const getAllClassGrades = (params) => {
  return apiCallerv2({
    url: `classrooms/${params.classroomId}/subjects/${params.subjectId}/grades`,
    method: 'GET',
    params: {
      page: params.pageIndex,
      limit: params.pageSize,
    },
  })
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
};

export const deleteGrade = (id) => {
  return apiCallerv2({
    url: `grades/${id}`,
    method: 'DELETE',
  })
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
};

export const getGrade = (id) => {
  return apiCallerv2({ url: `grades/${id}` })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const updateGrade = (grade) => {
  const data = [...grade];
  return apiCallerv2({ url: `grades`, method: 'PUT', data })
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
};
