import { apiCallerv2 } from '../../../utils/apiCaller';

export const getTopics = (params) => {
  return apiCallerv2({
    url: 'subforums',
    params,
  })
    .then((res) => res.data)
    .catch((err) => {
      throw err;
    });
};
