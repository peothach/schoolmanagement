import { apiCallerv2 } from '../../../utils/apiCaller';

/**
 * Function that calls api to get teachers
 *
 * @function getTeachers
 * @param {Oject} params - Query params for api request
 * @returns {Promise} - Promise object represents the list of teachers data
 */
export const getTeachers = (params) => {
  return apiCallerv2({
    url: 'teachers',
    method: 'GET',
    params: {
      pageSize: params.pageSize,
      pageIndex: params.page,
      fullName: params.fullName ? params.fullName : '',
    },
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to get a teacher by id
 *
 * @function getTeacherById
 * @param {number} id - Teacher Id
 * @returns {Promise} - Promise object that represents a teacher data
 */
export const getTeacherById = (id) => {
  return apiCallerv2({ url: `teachers/${id}` })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to create a new teacher
 *
 * @function addTeacher
 * @param {Object} teacher - Teacher data
 * @returns {Promise} - Promise object
 */
export const addTeacher = (teacher) => {
  const data = {
    ...teacher,
    status: true,
  };

  return apiCallerv2({ url: 'teachers', method: 'POST', data })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to update a teacher
 * @param {Object} teacher - Teacher data
 * @returns {Promise} - Promise object
 */
export const updateTeacher = (teacher) => {
  const data = {
    ...teacher,
    status: true,
  };

  return apiCallerv2({ url: `teachers/${data.id}`, method: 'PUT', data })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that calls api to delete a teacher
 *
 * @function deleteTeacher
 * @param {number} id - Teacher id
 * @returns {Promise} - Promise object
 */
export const deleteTeacher = (id) => {
  return apiCallerv2({ url: `teachers/${id}`, method: 'DELETE' })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

/**
 * Function that call api to get all subjects
 *
 * @returns {Promise} - Promise object that presents list of subjects
 */
export const getSubjects = () => {
  return apiCallerv2({ url: 'subjects' })
    .then((response) => response.data.content)
    .catch((error) => {
      throw error;
    });
};
