import { loadingTypes } from '../../constants/actionTypes';

export const showLoading = () => {
  return {
    type: loadingTypes.SHOW_LOADING,
  };
};

export const hideLoading = () => {
  return {
    type: loadingTypes.HIDE_LOADING,
  };
};
