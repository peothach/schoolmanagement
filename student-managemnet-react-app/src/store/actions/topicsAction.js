import { forumTypes } from '../../constants/forumTypes';

export const getTopics = (params) => {
  return {
    type: forumTypes.GET_TOPICS_REQUEST,
    payload: {
      params,
    },
  };
};
