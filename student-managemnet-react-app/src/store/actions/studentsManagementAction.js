import * as type from '../../constants/studentsManagementTypes';

/**
 * Function that returns GET_STUDENTS_REQUEST action
 * @function getStudents
 * @returns {Object} - Action object
 */
export const getStudents = (params) => {
  return {
    type: type.GET_STUDENTS_REQUEST,
    payload: {
      params,
    },
  };
};

/**
 * Function that returns GET_STUDENT_ID_REQUESTED action
 * @function getStudentById
 * @param {number} id - Student id
 * @returns
 */
export const getStudentById = (id) => {
  return {
    type: type.GET_STUDENT_ID_REQUESTED,
    payload: {
      id,
    },
  };
};

/**
 * Function that returns ADD_STUDENT_REQUESTED action
 *
 * @function addStudent
 * @param {Object} student - Student data
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const addStudent = (student, params) => {
  return {
    type: type.ADD_STUDENT_REQUESTED,
    payload: {
      student,
      params,
    },
  };
};

/**
 * Fuction that returns UPDATE_STUDENT_REQUESTED action
 *
 * @function updateStudent
 * @param {Object} student - Student data
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const updateStudent = (student, params) => {
  return {
    type: type.UPDATE_STUDENT_REQUESTED,
    payload: {
      student,
      params,
    },
  };
};

/**
 * Function that returns DELETE_STUDENT_REQUESTED action
 *
 * @function deleteStudent
 * @param {number} id - Id of the student
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const deleteStudent = (id, params) => {
  return {
    type: type.DELETE_STUDENT_REQUESTED,
    payload: {
      id,
      params,
    },
  };
};

/**
 * Function that returns GET_CLASSROOM_REQUESTED action
 *
 * @function getClassrooms
 * @returns {Object} - Action object
 */
export const getClassrooms = () => {
  return {
    type: type.GET_CLASSROOM_REQUESTED,
  };
};

/**
 * Function that dispatch SHOW_STUDENT_DIALOG action
 *
 * @function showStudentDialog
 * @returns {object} - Action object
 */
export const showStudentDialog = () => {
  return {
    type: type.SHOW_STUDENT_DIALOG,
  };
};

/**
 * Function that dispatch HIDE_STUDENT_DIALOG action
 *
 * @function showStudentDialog
 * @returns {Object} - Action object
 */
export const hideStudentDialog = () => {
  return {
    type: type.HIDE_STUDENT_DIALOG,
  };
};
