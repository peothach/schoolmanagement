import * as type from '../../constants/studentTypes';

/**
 * Function that returns GET_STUDENT_INFO_REQUEST action
 * @param {number} id - Student id
 * @returns {Object} - Action object
 */
export const getStudentInfo = (id) => {
  return {
    type: type.GET_STUDENT_INFO_REQUEST,
    payload: {
      id,
    },
  };
};

/**
 * Function that returns GET_STUDENT_MARKS_REQUEST action
 * @param {number} id - Student id
 * @param {Object} params - Request params
 * @returns {Object} - Action object
 */
export const getStudentMarks = (id, params) => {
  return {
    type: type.GET_STUDENT_MARKS_REQUEST,
    payload: {
      id,
      params,
    },
  };
};
