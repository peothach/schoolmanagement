import * as subjectApis from '../../apis/subjectApis';
import { subjectTypes } from '../../constants/actionTypes';
import { hideLoading, showLoading } from './loadingAction';

export const actFetchListSubject = () => {
  return {
    type: subjectTypes.FETCH_SUBJECTS,
  };
};

export const actFetchListSubjectSuccess = (data) => {
  return {
    type: subjectTypes.FETCH_SUBJECTS_SUCCESS,
    payload: data,
  };
};

export const actFetchListSubjectFailed = (error) => {
  return {
    type: subjectTypes.FETCH_SUBJECTS_FAILED,
    payload: error,
  };
};

export const actFetchListSubjectRequest = () => {
  return (dispatch) => {
    dispatch(actFetchListSubject());
    dispatch(showLoading());
    return subjectApis
      .getAll()
      .then((res) => {
        dispatch(actFetchListSubjectSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actFetchListSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actAddSubjectSuccess = (subject) => {
  return {
    type: subjectTypes.ADD_SUBJECT_SUCCESS,
    payload: subject,
  };
};

export const actAddSubjectFailed = (error) => {
  return {
    type: subjectTypes.ADD_SUBJECT_FAILED,
    payload: error,
  };
};

export const actAddSubjectRequest = (subject) => {
  const newSubject = JSON.parse(JSON.stringify(subject));
  newSubject.status = true;
  return (dispatch) => {
    dispatch(showLoading());
    return subjectApis
      .save(newSubject)
      .then((res) => {
        dispatch(actAddSubjectSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actAddSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actGetSubjectSuccess = (subject) => {
  return {
    type: subjectTypes.GET_SUBJECT_SUCCESS,
    payload: subject,
  };
};

export const actGetSubjectFailed = (error) => {
  return {
    type: subjectTypes.GET_SUBJECT_FAILED,
    payload: error,
  };
};

export const actGetSubjectRequest = (id) => {
  return (dispatch) => {
    dispatch(showLoading());
    return subjectApis
      .getById(id)
      .then((res) => {
        dispatch(actGetSubjectSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actGetSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actDeleteSubjectSuccess = (id) => {
  return {
    type: subjectTypes.DELETE_SUBJECT_SUCCESS,
    payload: id,
  };
};

export const actDeleteSubjectFailed = (error) => {
  return {
    type: subjectTypes.DELETE_SUBJECT_FAILED,
    payload: error,
  };
};

export const actDeleteSubjectRequest = (id) => {
  return (dispatch) => {
    dispatch(showLoading());
    return subjectApis
      .deleteByID(id)
      .then(() => {
        dispatch(actDeleteSubjectSuccess(id));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actDeleteSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actUpdateSubjectSuccess = (subject) => {
  return {
    type: subjectTypes.UPDATE_SUBJECT_SUCCESS,
    payload: subject,
  };
};

export const actUpdateSubjectFailed = (error) => {
  return {
    type: subjectTypes.UPDATE_SUBJECT_FAILED,
    payload: error,
  };
};

export const actUpdateSubjectRequest = (subject) => {
  return (dispatch) => {
    dispatch(showLoading());
    return subjectApis
      .update(subject)
      .then((res) => {
        dispatch(actUpdateSubjectSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actUpdateSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actSearchSortFilterSubjectSuccess = (subjects) => {
  return {
    type: subjectTypes.SEARCH_SORT_FILTER_SUBJECT_SUCCESS,
    payload: subjects,
  };
};

export const actSearchSortFilterSubjectFailed = (error) => {
  return {
    type: subjectTypes.SEARCH_SORT_FILTER_SUBJECT_FAILED,
    payload: error,
  };
};

export const actSearchSortFilterSubjectRequest = (params) => {
  return (dispatch) => {
    dispatch(showLoading());
    return subjectApis
      .getByParam(params)
      .then((res) => {
        dispatch(actSearchSortFilterSubjectSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actSearchSortFilterSubjectFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};
