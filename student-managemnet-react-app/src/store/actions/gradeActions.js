import * as type from '../../constants/GradeTypes';
import * as gradeApis from '../../apis/gradeApis';
import { gradesTypes } from '../../constants/actionTypes';
import { hideLoading, showLoading } from './loadingAction';

export const showGradeDialog = () => {
  return {
    type: type.SHOW_GRADE_DIALOG,
  };
};

export const hideGradeDialog = () => {
  return {
    type: type.HIDE_GRADE_DIALOG,
  };
};

export const getAllClassGrades = (params) => {
  return {
    type: type.TEACHER_GET_CLASS_GRADE_REQUESTED,
    payload: {
      params,
    },
  };
};

export const getGrade = (id) => {
  return {
    type: type.TEACHER_GET_GRADE_REQUESTED,
    payload: {
      id,
    },
  };
};

export const addGrade = (grade, params) => {
  return {
    type: type.TEACHER_ADD_GRADE_REQUESTED,
    payload: {
      grade,
      params,
    },
  };
};

export const updateGrade = (grade, params) => {
  return {
    type: type.TEACHER_UPDATE_GRADE_REQUESTED,
    payload: {
      grade,
      params,
    },
  };
};

export const deleteGrade = (id, params) => {
  return {
    type: type.TEACHER_DELETE_GRADE_REQUESTED,
    payload: {
      id,
      params,
    },
  };
};

export const actFetchListGrade = () => {
  return {
    type: gradesTypes.FETCH_GRADES,
  };
};

export const actFetchListGradeSuccess = (data) => {
  return {
    type: gradesTypes.FETCH_GRADES_SUCCESS,
    payload: data,
  };
};

export const actFetchListGradeFailed = (error) => {
  return {
    type: gradesTypes.FETCH_GRADES_FAILED,
    payload: error,
  };
};

export const actFetchListGradeRequest = (classID) => {
  return (dispatch) => {
    dispatch(actFetchListGrade());
    dispatch(showLoading());
    return gradeApis
      .getGradesByClassId(classID)
      .then((res) => {
        dispatch(actFetchListGradeSuccess(res.data.content));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actFetchListGradeFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actSearchGradeSuccess = (data) => {
  return {
    type: gradesTypes.SEARCH_GRADES_SUCCESS,
    payload: data,
  };
};

export const actSearchGradeFailed = (error) => {
  return {
    type: gradesTypes.SEARCH_GRADES_FAILED,
    payload: error,
  };
};

export const actSearchGradeRequest = (classID, param) => {
  return (dispatch) => {
    dispatch(actFetchListGrade());
    dispatch(showLoading());
    return gradeApis
      .getSearchStudentByClassId(classID, param)
      .then((res) => {
        dispatch(actSearchGradeSuccess(res.data.content));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actSearchGradeFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actGetGradeSuccess = (data) => {
  return {
    type: gradesTypes.GET_GRADE_SUCCESS,
    payload: data,
  };
};

export const actGetGradeFailed = (error) => {
  return {
    type: gradesTypes.GET_GRADE_FAILED,
    payload: error,
  };
};

export const actGetGradeRequest = (studentId, studentName) => {
  return (dispatch) => {
    dispatch(showLoading());
    return gradeApis
      .getGradesByStudentId(studentId)
      .then((res) => {
        const data = {
          name: studentName,
          grades: res.data.content,
        };
        dispatch(actGetGradeSuccess(data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actGetGradeFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actUpdateGradeSuccess = (data) => {
  return {
    type: gradesTypes.UPDATE_GRADE_SUCCESS,
    payload: data,
  };
};

export const actUpdateGradeFailed = (error) => {
  return {
    type: gradesTypes.UPDATE_GRADE_FAILED,
    payload: error,
  };
};

export const actUpdateGradeRequest = (body) => {
  return (dispatch) => {
    dispatch(showLoading());
    return gradeApis
      .updateGradeStudent(body)
      .then((res) => {
        dispatch(actUpdateGradeSuccess(res.data));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actUpdateGradeFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};

export const actDeleteGradeSuccess = (data) => {
  return {
    type: gradesTypes.DELETE_GRADE_SUCCESS,
    payload: data,
  };
};

export const actDeleteGradeFailed = (error) => {
  return {
    type: gradesTypes.DELETE_GRADE_FAILED,
    payload: error,
  };
};

export const actDeleteGradeRequest = (studentId) => {
  return (dispatch) => {
    dispatch(showLoading());
    return gradeApis
      .deleteGradesStudent(studentId)
      .then(() => {
        dispatch(actDeleteGradeSuccess(studentId));
        dispatch(hideLoading());
      })
      .catch((error) => {
        dispatch(actDeleteGradeFailed(error.toJSON()));
        dispatch(hideLoading());
      });
  };
};
