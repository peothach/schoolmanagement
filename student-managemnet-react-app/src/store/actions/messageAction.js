export const saveMessage = (data) => {
  return {
    type: 'SAVE_MESSAGE',
    payload: data,
  };
};

export const getMessage = (data) => {
  return {
    type: 'GET_MESSAGE',
    payload: data,
  };
};
