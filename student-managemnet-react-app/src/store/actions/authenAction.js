import * as type from '../../constants/authenTypes';

/**
 * Function that returns LOGIN_REQUEST action
 * @param {Object} data
 * @returns {Object} - action object
 */
export const login = (data) => {
  return {
    type: type.LOGIN_REQUEST,
    payload: {
      data,
    },
  };
};

/**
 * Function that returns LOGOUT_REQUEST action
 * @returns {Object} - action object
 */
export const logout = () => {
  return {
    type: type.LOGOUT_REQUEST,
  };
};

/**
 * Function that returns CHECK_PASSWORD_REQUEST action
 * @param {Object} data - Password data
 * @returns {Object} - Action object
 */
export const checkPassword = (data) => {
  return {
    type: type.CHECK_PASSWORD_REQUEST,
    payload: {
      data,
    },
  };
};

export const inputPassword = () => {
  return {
    type: type.INPUT_PASSWORD_REQUEST,
  };
};

export const clearPassword = () => {
  return {
    type: type.CLEAR_PASSWORD_REQUEST,
  };
};

/**
 * Function that returns CHANGE_PASSWORD_REQUEST action
 * @param {Object} data - Password data
 * @returns {Object} - Action object
 */
export const changePassword = (data) => {
  return {
    type: type.CHANGE_PASSWORD_REQUEST,
    payload: {
      data,
    },
  };
};

/**
 * Function that returns GET_ROLES_REQUEST action
 * @returns {Object} - action object
 */
export const getRoles = () => {
  return {
    type: type.GET_ROLES_REQUEST,
  };
};

/**
 * Function that return HIDE_ALERT_DIALOG action
 * @returns {Object} - action object
 */
export const hideAlertDialog = () => {
  return {
    type: type.HIDE_ALERT_DIALOG,
  };
};
