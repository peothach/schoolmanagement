import { forumTypes } from '../../constants/forumTypes';

export const getPostsByTopic = (id, params = {}) => {
  return {
    type: forumTypes.GET_TOPIC_DETAIL_REQUEST,
    payload: {
      id,
      params,
    },
  };
};

export const getPostById = (id) => {
  return {
    type: forumTypes.GET_POST_DETAIL_REQUEST,
    payload: {
      id,
    },
  };
};

export const createComment = (data, id) => {
  return {
    type: forumTypes.CREATE_COMMENT_REQUEST,
    payload: {
      data,
      id,
    },
  };
};
