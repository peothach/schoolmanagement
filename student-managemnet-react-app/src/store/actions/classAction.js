import {
  FETCH_CLASSES,
  ADD_CLASS,
  GET_CLASS,
  UPDATE_CLASS,
  DELETE_CLASS,
  SEARCH_SORT_FILTER_CLASS,
} from '../../constants/actionTypes';
import apiCaller from '../../utils/apiCaller';

export const actFetchClasses = (classes) => {
  return {
    type: FETCH_CLASSES,
    payload: classes,
  };
};

export const actFetchClassesRequest = () => {
  return (dispatch) => {
    return apiCaller(`classrooms`, 'GET', null).then((res) => {
      dispatch(actFetchClasses(res.data));
    });
  };
};

export const actAddClass = (classObject) => {
  return {
    type: ADD_CLASS,
    payload: classObject,
  };
};

export const actAddClassRequest = (classObject) => {
  return (dispatch) => {
    return apiCaller(`classrooms`, 'POST', classObject).then((res) => {
      dispatch(actAddClass(res.data));
    });
  };
};

export const actGetClass = (classObject) => {
  return {
    type: GET_CLASS,
    payload: classObject,
  };
};

export const actGetClassRequest = (id) => {
  return (dispatch) => {
    return apiCaller(`classrooms/${id}`, 'GET', null).then((res) => {
      dispatch(actGetClass(res.data));
    });
  };
};

export const actUpdateClass = (classObject) => {
  return {
    type: UPDATE_CLASS,
    payload: classObject,
  };
};

export const actUpdateClassRequest = (classObject) => {
  return (dispatch) => {
    return apiCaller(`classrooms/${classObject.id}`, 'PUT', classObject).then(
      (res) => {
        dispatch(actUpdateClass(res.data));
      }
    );
  };
};

export const actDeleteClass = (classObject) => {
  return {
    type: DELETE_CLASS,
    payload: classObject,
  };
};

export const actDeleteClassRequest = (classObject) => {
  return (dispatch) => {
    return apiCaller(`classrooms/${classObject.id}`, 'PUT', classObject).then(
      () => {
        dispatch(actDeleteClass(classObject));
      }
    );
  };
};

export const actSearchSortFilterClass = (classes) => {
  return {
    type: SEARCH_SORT_FILTER_CLASS,
    payload: classes,
  };
};

export const actSearchSortFilterClassRequest = (slug) => {
  return (dispatch) => {
    return apiCaller(`classrooms${slug}`, 'GET', null).then((res) => {
      dispatch(actSearchSortFilterClass(res.data));
    });
  };
};
