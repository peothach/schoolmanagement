import * as type from '../../constants/teacherTypes';

/**
 * Function that dispatch SHOW_TEACHER_DIALOG action
 *
 * @function showTeacherDialog
 * @returns {object} - Action object
 */
export const showTeacherDialog = () => {
  return {
    type: type.SHOW_TEACHER_DIALOG,
  };
};

/**
 * Function that dispatch HIDE_TEACHER_DIALOG action
 *
 * @function hideTeacherDialog
 * @returns {Object} - Action object
 */
export const hideTeacherDialog = () => {
  return {
    type: type.HIDE_TEACHER_DIALOG,
  };
};

/**
 * Function that dispath GET_TEACHERS_REQUESTED action
 *
 * @function getTeachers
 * @param {Object} params - Query parameters of api request
 * @returns {Object} - Action object
 */
export const getTeachers = (params) => {
  return {
    type: type.GET_TEACHERS_REQUESTED,
    payload: {
      params,
    },
  };
};

/**
 * Function that dispatch GET_TEACHER_ID_REQUESTED action
 *
 * @function getTeacherById
 * @param {number} id - Id of the teacher
 * @returns {Object} - Action object
 */
export const getTeacherById = (id) => {
  return {
    type: type.GET_TEACHER_ID_REQUESTED,
    payload: {
      id,
    },
  };
};

/**
 * Function that dispatch ADD_TEACHER_REQUESTED action
 *
 * @function addTeachers
 * @param {Object} teacher - Teacher data
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const addTeacher = (teacher, params) => {
  return {
    type: type.ADD_TEACHER_REQUESTED,
    payload: {
      teacher,
      params,
    },
  };
};

/**
 * Fuction that dispatch UPDATE_TEACHER_REQUESTED action
 *
 * @function updateTeacher
 * @param {Object} teacher - Teacher data
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const updateTeacher = (teacher, params) => {
  return {
    type: type.UPDATE_TEACHER_REQUESTED,
    payload: {
      teacher,
      params,
    },
  };
};

/**
 * Function that dispatch DELETE_TEACHER_REQUESTED action
 *
 * @function deleteTeacher
 * @param {number} id - Id of the teacher
 * @param {Object} params - Query params for api request
 * @returns {Object} - Action object
 */
export const deleteTeacher = (id, params) => {
  return {
    type: type.DELETE_TEACHER_REQUESTED,
    payload: {
      id,
      params,
    },
  };
};

/**
 * Function that dispatch GET_SUBJECTS_REQUESTED action
 *
 * @function getSubjects
 * @returns {Object} - Action object
 */
export const getSubjects = () => {
  return {
    type: type.GET_SUBJECTS_REQUESTED,
  };
};
