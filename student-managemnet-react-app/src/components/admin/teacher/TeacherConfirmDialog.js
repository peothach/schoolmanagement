import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import classes from './TeacherConfirmDialog.module.css';

/**
 * Component for showing confirm dialog before process an action
 */
const TeacherConfirmDialog = (props) => {
  const { open, title, content, onClose, onOk } = props;

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{ classes: { root: classes.dialog } }}
      >
        <DialogTitle classes={{ root: classes['title-warning'] }}>
          {title}
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText classes={{ root: classes.content }}>
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={onClose}
            classes={{ root: classes.button }}
            autoFocus
          >
            CANCEL
          </Button>
          <Button
            onClick={onOk}
            classes={{ root: `${classes.button} ${classes['button-ok']}` }}
          >
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

TeacherConfirmDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
};

export default TeacherConfirmDialog;
