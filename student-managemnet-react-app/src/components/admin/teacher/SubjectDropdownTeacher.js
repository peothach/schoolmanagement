import React from 'react';
import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import classes from './CustomInput.module.css';

/**
 * Component for selecting subject
 */
const SubjectDropdownTeacher = (props) => {
  const { subjects, value, onChange, error } = props;

  return (
    <div className={classes['form-control']}>
      <label htmlFor="subject">Teaching:</label>
      <Select
        id="subject"
        value={value}
        onChange={onChange}
        variant="outlined"
        classes={{ root: classes.select }}
        error={error}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {subjects.map((subject) => (
          <MenuItem key={subject.id} value={subject.id}>
            {subject.name}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

SubjectDropdownTeacher.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  onChange: PropTypes.func,
  error: PropTypes.bool,
  subjects: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
};

export default SubjectDropdownTeacher;
