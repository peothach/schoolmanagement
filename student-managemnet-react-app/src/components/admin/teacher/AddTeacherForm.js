import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getSubjects } from '../../../store/actions/teachersAction';
import {
  ADDRESS,
  BIRTH_DAY,
  FULL_NAME,
  PHONE_NUMBER,
  SUBJECT,
  validateBirthday,
  validatePhoneNumber,
  validateSelection,
  validateTextInput,
} from '../../../utils/validateUtils';
import classes from './AddTeacherForm.module.css';
import GenderRadioTeacher from './GenderRadioTeacher';
import SubjectDropdownTeacher from './SubjectDropdownTeacher';
import TextInputTeacher from './TextInputTeacher';

/**
 * Component for showing add teacher form dialog
 */
const AddTeacherForm = (props) => {
  const { open, onAdd, onClose } = props;
  const dispatch = useDispatch();

  // Selected gender value for gender input
  const [genderAdd, setGenderAdd] = useState('MALE');
  // Fullname value for fullname input
  const [fullNameAdd, setFullNameAdd] = useState('');
  // Birthday value for birthday input
  const [birthdateAdd, setBirthdateAdd] = useState('');
  // Phone number value for phoneNumber input
  const [phoneNumberAdd, setPhoneNumberAdd] = useState('');
  // Address value for address input
  const [addressAdd, setAddressAdd] = useState('');
  // Selected subject for subject input
  const [subjectAdd, setSubjectAdd] = useState('');
  // Error information when user input incorrectly
  const [errorMessage, setErrorMessage] = useState(null);

  // List of subjects
  const subjects = useSelector((state) => state.teachers.subjects);

  // Current date for initial value when user pick birthday
  const currentDate = moment().format('YYYY-MM-DD');

  useEffect(() => {
    // Get list of subject when component first mount
    dispatch(getSubjects());
  }, [dispatch]);

  useEffect(() => {
    // Selecte the first element of subject list when it loaded
    setSubjectAdd(subjects.length > 0 ? subjects[0].id : '');
  }, [subjects.length]);

  /**
   * Handler for fullname changes
   * @param {Object} event - Change event
   */
  const onFullNameChangedHandler = (event) => {
    setFullNameAdd(event.target.value);
  };

  /**
   * Handler for birthday changes
   * @param {Object} event - Change event
   */
  const onBirthdayChangedHandler = (event) => {
    setBirthdateAdd(event.target.value);
  };

  /**
   * Handler for phone number changes
   * @param {Object} event - Change event
   */
  const onPhoneChangedHandler = (event) => {
    setPhoneNumberAdd(event.target.value);
  };

  /**
   * Handler for address changes
   * @param {Object} event - Change event
   */
  const onAddressChangedHandler = (event) => {
    setAddressAdd(event.target.value);
  };

  /**
   * Handler for selected gender changes
   * @param {Object} event - Change event
   */
  const onGenderChangeHandler = (event) => {
    setGenderAdd(event.target.value);
  };

  /**
   * Handler for selected subject changes
   * @param {Object} event - Change event
   */
  const onSubjectChangedHandler = (event) => {
    setSubjectAdd(event.target.value);
  };

  // Handler when add button is clicked
  const onSubmitHandler = () => {
    let error = null;

    // Check error for fullName input
    error = error || validateTextInput(FULL_NAME, fullNameAdd);
    // Check erorr for birthday input
    error = error || validateBirthday(BIRTH_DAY, birthdateAdd);
    // Check error for phoneNumber input
    error = error || validatePhoneNumber(PHONE_NUMBER, phoneNumberAdd);
    // Check error for address input
    error = error || validateTextInput(ADDRESS, addressAdd);
    // Check error if subject is selected or not
    error = error || validateSelection(SUBJECT, subjectAdd);

    // Set error message to show
    if (error) {
      setErrorMessage(error);
      return;
    }

    const formData = {
      fullName: fullNameAdd,
      birthdate: birthdateAdd,
      gender: genderAdd,
      phoneNumber: phoneNumberAdd,
      address: addressAdd,
      subject: subjectAdd,
    };

    onAdd(formData);

    // Reset error when form is submitted
    setErrorMessage(null);
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        setErrorMessage(null);
        onClose();
      }}
      className={classes.dialog}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Add Teacher</DialogTitle>
      <DialogContent dividers>
        <div className={classes.error}>
          {errorMessage && <p>{errorMessage.content}</p>}
        </div>
        <form className={classes.root} noValidate onSubmit={onSubmitHandler}>
          <TextInputTeacher
            id="fullname"
            label="Fullname"
            onChange={onFullNameChangedHandler}
            error={errorMessage && errorMessage.field === FULL_NAME}
          />
          <TextInputTeacher
            id="data"
            label="Birthday"
            type="date"
            defaultValue={currentDate}
            onChange={onBirthdayChangedHandler}
            error={errorMessage && errorMessage.field === BIRTH_DAY}
          />
          <GenderRadioTeacher
            value={genderAdd}
            onChange={onGenderChangeHandler}
          />
          <TextInputTeacher
            id="phone"
            label="Phone"
            onChange={onPhoneChangedHandler}
            error={errorMessage && errorMessage.field === PHONE_NUMBER}
          />
          <TextInputTeacher
            id="address"
            label="Address"
            onChange={onAddressChangedHandler}
            error={errorMessage && errorMessage.field === ADDRESS}
          />
          <SubjectDropdownTeacher
            subjects={subjects}
            value={subjectAdd}
            onChange={onSubjectChangedHandler}
            error={errorMessage && errorMessage.field === SUBJECT}
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setErrorMessage(null);
            onClose();
          }}
          className={classes['form-button__cancel']}
        >
          Cancel
        </Button>
        <Button
          onClick={onSubmitHandler}
          className={classes['form-button__ok']}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

AddTeacherForm.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
};

AddTeacherForm.defaultProps = {
  open: false,
};

export default AddTeacherForm;
