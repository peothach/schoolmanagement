import React from 'react';
import PropTypes from 'prop-types';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import classes from './CustomInput.module.css';

/**
 * Component for choosing gender
 */
const GenderRadioTeacher = (props) => {
  const { value, onChange } = props;
  return (
    <div className={classes['form-control']}>
      <label htmlFor="gender">Gender:</label>
      <RadioGroup
        id="gender"
        className={classes['form-radio-group']}
        name="gender"
        value={value}
        onChange={onChange}
      >
        <FormControlLabel value="MALE" control={<Radio />} label="Male" />
        <FormControlLabel value="FEMALE" control={<Radio />} label="Female" />
      </RadioGroup>
    </div>
  );
};

GenderRadioTeacher.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export default GenderRadioTeacher;
