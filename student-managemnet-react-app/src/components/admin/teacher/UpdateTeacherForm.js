import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import classes from './UpdateTeacherForm.module.css';
import TextInputTeacher from './TextInputTeacher';
import GenderRadioTeacher from './GenderRadioTeacher';
import SubjectDropdownTeacher from './SubjectDropdownTeacher';
import {
  getTeacherById,
  getSubjects,
} from '../../../store/actions/teachersAction';
import {
  validateTextInput,
  validatePhoneNumber,
  validateBirthday,
  validateSelection,
  FULL_NAME,
  ADDRESS,
  BIRTH_DAY,
  PHONE_NUMBER,
  SUBJECT,
} from '../../../utils/validateUtils';

/**
 * Component for showing update teacher form dialog
 */
const UpdateTeacherForm = (props) => {
  const dispatch = useDispatch();
  const { open, id, onUpdate, onClose } = props;

  // Selected gender value for gender input
  const [gender, setGender] = useState('MALE');
  // Fullname value for fullname input
  const [fullName, setFullName] = useState('');
  // Birthday value for birthday input
  const [birthdate, setBirthdate] = useState('');
  // Phone number value for phoneNumber input
  const [phoneNumber, setPhoneNumber] = useState('');
  // Address value for address input
  const [address, setAddress] = useState('');
  // Selected subject for subject input
  const [subject, setSubject] = useState('');
  // Error information when user input incorrectly
  const [errorMessage, setErrorMessage] = useState(null);

  // Teacher detail information
  const teacher = useSelector((state) => state.teachers.teacher);
  // List of subjects
  const subjects = useSelector((state) => state.teachers.subjects);

  useEffect(() => {
    // Get a teacher detail by id
    dispatch(getTeacherById(id));
    // Get list of subjectss
    dispatch(getSubjects());

    // Clear input fields
    return () => {
      setFullName('');
      setGender('MALE');
      setBirthdate('');
      setPhoneNumber('');
      setAddress('');
      setSubject('');
    };
  }, [dispatch]);

  // Set value for input fields when teacher is loaded
  useEffect(() => {
    setFullName(teacher.fullName || '');
    setGender(teacher.gender || 'MALE');
    setBirthdate(teacher.birthdate || '');
    setPhoneNumber(teacher.phoneNumber || '');
    setAddress(teacher.address || '');
    setSubject((teacher.subject && teacher.subject.id) || '');
  }, [
    teacher.fullName,
    teacher.gender,
    teacher.birthdate,
    teacher.phoneNumber,
    teacher.address,
    subjects.length,
  ]);

  /**
   * Handler for fullname changes
   * @param {Object} event - Change event
   */
  const onFullNameChangedHandler = (event) => {
    setFullName(event.target.value);
  };

  /**
   * Handler for birthday changes
   * @param {Object} event - Change event
   */
  const onBirthdayChangedHandler = (event) => {
    setBirthdate(event.target.value);
  };

  /**
   * Handler for phone number changes
   * @param {Object} event - Change event
   */
  const onPhoneChangedHandler = (event) => {
    setPhoneNumber(event.target.value);
  };

  /**
   * Handler for address changes
   * @param {Object} event - Change event
   */
  const onAddressChangedHandler = (event) => {
    setAddress(event.target.value);
  };

  /**
   * Handler for selected gender changes
   * @param {Object} event - Change event
   */
  const onGenderChangeHandler = (event) => {
    setGender(event.target.value);
  };

  /**
   * Handler for selected subject changes
   * @param {Object} event - Change event
   */
  const onSubjectChangedHandler = (event) => {
    setSubject(event.target.value);
  };

  /**
   * Handler for submit update teacher form
   */
  const onUpdateSubmitHandler = () => {
    let error = null;
    // Check error for fullName input
    error = error || validateTextInput(FULL_NAME, fullName);
    // Check erorr for birthday input
    error = error || validateBirthday(BIRTH_DAY, birthdate);
    // Check error for phoneNumber input
    error = error || validatePhoneNumber(PHONE_NUMBER, phoneNumber);
    // Check error for address input
    error = error || validateTextInput(ADDRESS, address);
    // Check error if subject is selected or not
    error = error || validateSelection(SUBJECT, subject);

    // Set error message to show
    if (error) {
      setErrorMessage(error);
      return;
    }

    const formData = {
      id,
      fullName,
      birthdate,
      gender,
      address,
      phoneNumber,
      subject,
    };

    onUpdate(formData);

    // Reset error information when form is submitted
    setErrorMessage(null);
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        setErrorMessage(null);
        onClose();
      }}
    >
      <DialogTitle id="update-form-dialog-title">Update Teacher</DialogTitle>
      <DialogContent dividers>
        <div className={classes['update-error']}>
          {errorMessage && <p>{errorMessage.content}</p>}
        </div>
        <form noValidate onSubmit={onUpdateSubmitHandler}>
          <TextInputTeacher
            id="fullname"
            label="Fullname"
            value={fullName}
            onChange={onFullNameChangedHandler}
            error={errorMessage && errorMessage.field === FULL_NAME}
          />
          <TextInputTeacher
            id="data"
            label="Birthday"
            type="date"
            value={birthdate}
            onChange={onBirthdayChangedHandler}
            error={errorMessage && errorMessage.field === BIRTH_DAY}
          />
          <GenderRadioTeacher value={gender} onChange={onGenderChangeHandler} />
          <TextInputTeacher
            id="phone"
            label="Phone"
            value={phoneNumber}
            onChange={onPhoneChangedHandler}
            error={errorMessage && errorMessage.field === PHONE_NUMBER}
          />
          <TextInputTeacher
            id="address"
            label="Address"
            value={address}
            onChange={onAddressChangedHandler}
            error={errorMessage && errorMessage.field === ADDRESS}
          />
          <SubjectDropdownTeacher
            subjects={subjects}
            value={subject}
            onChange={onSubjectChangedHandler}
            error={errorMessage && errorMessage.field === SUBJECT}
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setErrorMessage(null);
            onClose();
          }}
          className={classes['update-form-button__cancel']}
        >
          Cancel
        </Button>
        <Button
          onClick={onUpdateSubmitHandler}
          className={classes['update-form-button__ok']}
        >
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

UpdateTeacherForm.propTypes = {
  open: PropTypes.bool,
  id: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
};

UpdateTeacherForm.defaultProps = {
  open: false,
};

export default UpdateTeacherForm;
