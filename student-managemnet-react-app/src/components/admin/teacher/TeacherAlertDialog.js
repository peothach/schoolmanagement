import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import classes from './TeacherAlertDialog.module.css';

/**
 * Component for showing alert dialog when user done a action
 */
const TeacherAlertDialog = (props) => {
  const { open, title, content, account, status, onClose } = props;

  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{ classes: { root: classes.dialog } }}
    >
      <DialogTitle
        classes={{
          root:
            status === 'success'
              ? classes['title-success']
              : classes['title-failed'],
        }}
      >
        {title}
      </DialogTitle>
      <DialogContent dividers>
        <DialogContentText classes={{ root: classes.content }}>
          {content}
        </DialogContentText>
        {account && (
          <DialogContentText classes={{ root: classes.content }}>
            Account: <span>{account}</span>
          </DialogContentText>
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={onClose}
          classes={{ root: classes['button-ok'] }}
          autoFocus
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

TeacherAlertDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  account: PropTypes.string,
  status: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

export default TeacherAlertDialog;
