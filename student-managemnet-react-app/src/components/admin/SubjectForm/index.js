import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import {
  actAddSubjectRequest,
  actUpdateSubjectRequest,
} from '../../../store/actions/subjectAction';

const SubjectForm = () => {
  const [subject, setSubject] = useState({
    name: '',
  });

  const subjectFromStore = useSelector((state) => state.subjectItem);

  useEffect(() => {
    setSubject(subjectFromStore);
  }, [subjectFromStore]);

  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: { ...subject },
    validationSchema: Yup.object({
      name: Yup.string().required('Name must be not null'),
    }),
    onSubmit: (values, { resetForm }) => {
      if (subject.id === undefined || subject.id === 0) {
        dispatch(actAddSubjectRequest(values));
      } else {
        dispatch(actUpdateSubjectRequest(values));
      }
      resetForm();
      setSubject({
        name: '',
      });
    },
  });

  const onClear = () => {
    formik.resetForm();
    setSubject({
      name: '',
    });
  };

  return (
    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-4">
      <div className="panel panel-warning">
        <div className="panel-heading">
          <h5 className="panel-title">Add Subject</h5>
        </div>
        <div className="panel-body mt-5">
          <form onSubmit={formik.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                id="name"
                name="name"
                type="text"
                className="form-control my-2"
                onChange={formik.handleChange}
                value={formik.values.name}
              />
              {formik.touched.name && formik.errors.name ? (
                <p className="text-danger">{formik.errors.name}</p>
              ) : null}
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-warning">
                {subject.id === undefined || subject.id === 0
                  ? 'Save'
                  : 'Update'}
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-danger"
                onClick={onClear}
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SubjectForm;
