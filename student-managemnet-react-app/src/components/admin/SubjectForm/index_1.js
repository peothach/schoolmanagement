import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  actAddSubjectRequest,
  actUpdateSubjectRequest,
} from '../../../store/actions/subjectAction';

const SubjectForm = () => {
  const [subject, setSubject] = useState({
    name: '',
  });

  const subjectFromStore = useSelector((state) => state.subjectItem);

  useEffect(() => {
    setSubject(subjectFromStore);
  }, [subjectFromStore]);

  const dispatch = useDispatch();

  const onChange = (e) => {
    setSubject({
      name: e.target.value,
    });
  };

  const onCancel = () => {
    setSubject({
      name: '',
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (subject.id === undefined) {
      dispatch(actAddSubjectRequest(subject));
    } else {
      dispatch(actUpdateSubjectRequest(subject));
    }
    onCancel();
  };

  return (
    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-4">
      <div className="panel panel-warning">
        <div className="panel-heading">
          <h5 className="panel-title">Add Subject</h5>
        </div>
        <div className="panel-body mt-5">
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <label htmlFor="name">Name :</label>
              <input
                id="name"
                type="text"
                className="form-control my-2"
                name="name"
                value={subject.name}
                onChange={onChange}
              />
            </div>
            <br />
            <div className="text-center">
              <button type="submit" className="btn btn-warning">
                {subject.id === undefined ? 'Save' : 'Update'}
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-danger"
                onClick={onCancel}
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SubjectForm;
