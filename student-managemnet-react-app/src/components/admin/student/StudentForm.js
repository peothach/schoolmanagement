import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dropdown from '../common/Dropdown';
import GenderRadioGroup from '../common/GenderRadioGroup';

import classes from './StudentForm.module.css';
import TextInput from '../common/TextInput';
import useInput from './use-input';
import {
  getStudentById,
  getClassrooms,
} from '../../../store/actions/studentsManagementAction';

export const ADD_NEW = 'ADD_NEW';
export const UPDATE = 'UPDATE';

const validateEmpty = (value) => value.trim() !== '';
const validatePhone = (value) => value.match(/^0[0-9]{9,10}$/);
const validateBirthday = (value) => moment(value).isBefore(moment());
const validateSelection = (value) => value !== '';

/**
 * Component for showing add/update student form dialog
 */
const UpdateTeacherForm = (props) => {
  const dispatch = useDispatch();
  const { title, type, open, id, onAdd, onUpdate, onClose } = props;

  const student = useSelector((state) => state.students.student);
  const classrooms = useSelector((state) => state.students.classrooms);

  const {
    value: fullnameValue,
    setValue: setFullnameValue,
    isValid: fullnameIsValid,
    hasError: fullnameHasError,
    onChangeHandler: onFullnameChangeHandler,
    onBlurHandler: onFullNameBlurHandler,
    reset: resetFullname,
  } = useInput('', validateEmpty);

  // Current date for initial value when user pick birthday
  const currentDate = moment().format('YYYY-MM-DD');
  const {
    value: birthdayValue,
    setValue: setBirthdayValue,
    isValid: birthdayIsValid,
    hasError: birthdayHasError,
    onChangeHandler: onBirthdayChangeHandler,
    onBlurHandler: onBirthdayBlurHandler,
    reset: resetBirthday,
  } = useInput(currentDate, validateBirthday);

  const {
    value: genderValue,
    setValue: setGenderValue,
    isValid: genderIsValid,
    hasError: genderHasError,
    onChangeHandler: onGenderChangeHandler,
    reset: resetGender,
  } = useInput('MALE', validateSelection);

  const {
    value: addressValue,
    setValue: setAddressValue,
    isValid: addressIsValid,
    hasError: addressHasError,
    onChangeHandler: onAddressChangeHandler,
    onBlurHandler: onAddressBlurHandler,
    reset: resetAddress,
  } = useInput('', validateEmpty);

  const {
    value: phoneValue,
    setValue: setPhoneValue,
    isValid: phoneIsValid,
    hasError: phoneHasError,
    onChangeHandler: onPhoneChangeHandler,
    onBlurHandler: onPhoneBlurHandler,
    reset: resetPhone,
  } = useInput('', validatePhone);

  const {
    value: classroomValue,
    setValue: setClassroomValue,
    isValid: classroomIsValid,
    hasError: classroomHasError,
    onChangeHandler: onClassroomChangeHandler,
    onBlurHandler: onClassroomBlurHandler,
    reset: resetClassroom,
  } = useInput('', validateSelection);

  useEffect(() => {
    // If update form, get student information
    if (type === UPDATE) {
      dispatch(getStudentById(id));
    }

    // Get list of class rooms
    dispatch(getClassrooms());

    // Clear input fields
    return () => {
      resetFullname();
      resetGender();
      resetBirthday();
      resetPhone();
      resetAddress();
      resetClassroom();
    };
  }, [dispatch]);

  // Set value for input fields when student is loaded
  useEffect(() => {
    if (type === UPDATE && student) {
      setFullnameValue(student?.fullName);
      setGenderValue(student?.gender);
      setBirthdayValue(student?.birthdate);
      setPhoneValue(student?.phoneNumber);
      setAddressValue(student?.address);
      setClassroomValue(student?.classroom?.id);
    }
  }, [
    student?.fullName,
    student?.gender,
    student?.birthdate,
    student?.phoneNumber,
    student?.address,
    classrooms.length,
  ]);

  let formIsValid = false;

  if (
    fullnameIsValid &&
    genderIsValid &&
    birthdayIsValid &&
    addressIsValid &&
    phoneIsValid &&
    classroomIsValid
  ) {
    formIsValid = true;
  }

  /**
   * Handler for submit update student form
   */
  const onSubmitHandler = () => {
    if (!formIsValid) {
      return;
    }

    const formData = {
      id,
      fullName: fullnameValue,
      birthdate: birthdayValue,
      gender: genderValue,
      address: addressValue,
      phoneNumber: phoneValue,
      classroomId: classroomValue,
    };

    if (type === ADD_NEW) {
      onAdd(formData);
    } else {
      onUpdate(formData);
    }

    resetFullname();
    resetGender();
    resetBirthday();
    resetPhone();
    resetAddress();
    resetClassroom();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent dividers>
        <form noValidate>
          <TextInput
            id="fullname"
            label="Fullname"
            value={fullnameValue}
            onChange={onFullnameChangeHandler}
            onBlur={onFullNameBlurHandler}
            error={fullnameHasError}
          />
          <div className={classes.error}>
            {fullnameHasError && <p>Fullname must not be blank!</p>}
          </div>
          <TextInput
            id="data"
            label="Birthday"
            type="date"
            value={birthdayValue}
            onChange={onBirthdayChangeHandler}
            onBlur={onBirthdayBlurHandler}
            error={birthdayHasError}
          />
          <div className={classes.error}>
            {birthdayHasError && <p>Birthday must be in the past!</p>}
          </div>
          <GenderRadioGroup
            value={genderValue}
            onChange={onGenderChangeHandler}
          />
          <div className={classes.error}>
            {genderHasError && <p>You must choose a gender!</p>}
          </div>
          <TextInput
            id="phone"
            label="Phone"
            value={phoneValue}
            onChange={onPhoneChangeHandler}
            onBlur={onPhoneBlurHandler}
            error={phoneHasError}
          />
          <div className={classes.error}>
            {phoneHasError && <p>Phone must follow pattern: 0xxxxxxxxx!</p>}
          </div>
          <TextInput
            id="address"
            label="Address"
            value={addressValue}
            onChange={onAddressChangeHandler}
            onBlur={onAddressBlurHandler}
            error={addressHasError}
          />
          <div className={classes.error}>
            {addressHasError && <p>Address must not be blank!</p>}
          </div>
          <Dropdown
            id="classroom"
            label="Classroom"
            list={classrooms}
            value={classroomValue}
            onChange={onClassroomChangeHandler}
            onBlur={onClassroomBlurHandler}
            error={classroomHasError}
          />
          <div className={classes.error}>
            {classroomHasError && <p>You must choose a classroom!</p>}
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} className={classes['form-button__cancel']}>
          Cancel
        </Button>
        <Button
          onClick={onSubmitHandler}
          className={classes['form-button__ok']}
          disabled={!formIsValid}
        >
          {type === ADD_NEW ? 'Add' : 'Update'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

UpdateTeacherForm.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.string,
  title: PropTypes.string.isRequired,
  id: PropTypes.number,
  onClose: PropTypes.func.isRequired,
  onAdd: PropTypes.func,
  onUpdate: PropTypes.func,
};

UpdateTeacherForm.defaultProps = {
  open: false,
  type: ADD_NEW,
};

export default UpdateTeacherForm;
