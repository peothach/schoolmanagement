import { useState } from 'react';

/**
 * Custom Hook for input field
 * @param {func} validate - Function that validate input value
 * @returns
 */
const useInput = (initialVal, validate) => {
  // Value that user entered
  const [value, setValue] = useState(initialVal);
  // Whether input field is edited or not
  const [isTouched, setIsTouched] = useState(false);

  // Check the value user entered is valid or not
  const isValid = validate(value);
  // Input field has error when user entered invalid value and input field has been editted
  const hasError = !isValid && isTouched;

  // Input field onChange handler
  const onChangeHandler = (event) => {
    setValue(event.target.value);
  };

  // Input field onBlur handler
  const onBlurHandler = () => {
    setIsTouched(true);
  };

  // Reset to initial state
  const reset = () => {
    setValue(initialVal);
    setIsTouched(false);
  };

  return {
    value,
    setValue,
    isValid,
    hasError,
    onChangeHandler,
    onBlurHandler,
    reset,
  };
};

export default useInput;
