/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable prefer-object-spread */
import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { actUpdateGradeRequest } from '../../../store/actions/gradeActions';

const GradeForm = () => {
  const [grades, setGrades] = useState({});

  const gradesFromStore = useSelector((store) => store.gradeItem);

  const dispatch = useDispatch();

  useEffect(() => {
    setGrades(gradesFromStore);
  }, [gradesFromStore]);

  const handleChange = (e, index) => {
    const newGrades = { ...grades };
    if (isNaN(e.target.value) || e.target.value === '') {
      newGrades.grades[index].mark = e.target.value;
    } else {
      newGrades.grades[index].mark = parseInt(e.target.value, 10);
    }
    setGrades(newGrades);
  };

  const renderInputGrades = () => {
    if (grades && grades.grades !== undefined) {
      return grades.grades.map((grade, index) => {
        return (
          <div className="form-group" key={grade.id}>
            <label htmlFor={grade.subject.name}>{grade.subject.name}</label>
            <input
              id={grade.subject.name}
              name="mark"
              type="number"
              className="form-control my-2"
              value={grade.mark}
              onChange={(e) => handleChange(e, index)}
            />
            {grade.error !== undefined ? (
              <p className="text-danger">{grade.error}</p>
            ) : null}
          </div>
        );
      });
    }
    return null;
  };

  const validData = () => {
    let isValid = true;
    const newGrades = Object.assign({}, grades);
    newGrades.grades.map((grade) => {
      if (grade.mark < 0 || grade.mark === '') {
        isValid = false;
        grade.error = `${grade.subject.name} must be greater than 0 and required`;
      }
    });
    setGrades(newGrades);
    return isValid;
  };

  const onSave = () => {
    const newGrades = Object.assign({}, grades);
    const body = [];
    newGrades.grades.map((grade) => {
      body.push({
        id: grade.id,
        mark: grade.mark,
      });
    });
    if (validData()) {
      var myModalEl = document.getElementById('exampleModal');
      var modal = bootstrap.Modal.getInstance(myModalEl); // Returns a Bootstrap modal instance
      modal.hide();
      dispatch(actUpdateGradeRequest(body));
    }
  };

  return (
    <div>
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Edit Grades Student
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              />
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  id="name"
                  name="name"
                  type="text"
                  className="form-control my-2"
                  defaultValue={grades.name}
                  readOnly
                />
              </div>
              {renderInputGrades()}
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                onClick={onSave}
              >
                Save changes
              </button>
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GradeForm;
