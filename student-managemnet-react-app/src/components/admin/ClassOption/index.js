import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actSearchSortFilterClassRequest } from '../../../store/actions/classAction';

const ClassOption = () => {
  const dispatch = useDispatch();

  const history = useHistory();

  const [option, setOption] = useState({
    q: '',
    sort: '',
    order: '',
    status: '',
  });

  const renderUrl = () => {
    let result = '?';

    result += Object.entries(option)
      .filter(([, val]) => val !== '' && val !== 'all')
      .map(([key, val]) => `${key}=${val}`)
      .join('&');

    if (result !== '?') {
      result = result.replace('sort', '_sort');
      result = result.replace('order', '_order');
    } else {
      result = '';
    }

    return result;
  };

  const onAction = () => {
    const url = renderUrl();

    history.push(`${history.location.pathname}${url}`);

    dispatch(actSearchSortFilterClassRequest(url));
  };

  const onChange = (e) => {
    const { target } = e;
    const { name } = target;
    const { value } = target;
    if (name === 'order') {
      setOption({
        ...option,
        sort: 'name',
        [name]: value,
      });
      return;
    }
    setOption({
      ...option,
      [name]: value,
    });
  };

  useEffect(() => {
    onAction();
  }, [option]);

  return (
    <div className="row mt-3">
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder="Keyword..."
            name="q"
            value={option.q}
            onChange={onChange}
          />
          <span className="input-group-btn">
            <button
              className="btn btn-primary"
              type="button"
              onClick={onAction}
            >
              <span className="fa fa-search mr-5">&nbsp;Search</span>
            </button>
          </span>
        </div>
      </div>
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div className="row">
          <div className="col">
            <label htmlFor="status">Sort name :</label>
            <select
              id="status"
              className="form-select mt-2"
              required="required"
              name="order"
              value={option.order}
              onChange={onChange}
              selceted={option.order}
            >
              <option value="" disabled>
                -----Sort-----
              </option>
              <option value="asc">A-Z</option>
              <option value="desc">Z-A</option>
            </select>
          </div>
          <div className="col">
            <label htmlFor="status">Filter :</label>
            <select
              id="status"
              className="form-select mt-2"
              required="required"
              name="active"
              value={option.status}
              onChange={onChange}
              selceted={option.status}
            >
              <option value="" disabled>
                -----Filter-----
              </option>
              <option value="all">All</option>
              <option value="true">Active</option>
              <option value="false">Inactive</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ClassOption;
