import axios from 'axios';
import FileDownload from 'js-file-download';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAll } from '../../../apis/classApis';
import {
  actFetchListGradeRequest,
  actSearchGradeRequest,
} from '../../../store/actions/gradeActions';
import { hideLoading, showLoading } from '../../../store/actions/loadingAction';
import { toastError } from '../../../utils/toast';

const index = () => {
  const dispatch = useDispatch();

  const [classList, setClassList] = useState([]);

  const [idClassSelected, setIdClassSelected] = useState(0);

  const [search, setSearch] = useState({
    q: '',
  });

  const gradeItem = useSelector((state) => state.gradeItem);

  useEffect(() => {
    dispatch(showLoading());
    getAll()
      .then((res) => {
        dispatch(hideLoading());
        setClassList(res.data.content);
        setIdClassSelected(res.data.content[0].id);
      })
      .catch((error) => {
        dispatch(hideLoading());
        const { message } = error;
        toastError(message);
      });
  }, []);

  useEffect(() => {
    if (idClassSelected !== 0) {
      dispatch(actFetchListGradeRequest(idClassSelected));
    }
  }, [idClassSelected, gradeItem]);

  const renderOption = () => {
    if (classList.length > 0) {
      return classList.map((classItem) => {
        return (
          <option key={classItem.id} value={classItem.id}>
            {classItem.name}
          </option>
        );
      });
    }
    return null;
  };

  const handleChangeClass = (e) => {
    setIdClassSelected(e.target.value);
  };

  const onHandleSearch = (e) => {
    const { target } = e;
    const { name } = target;
    const { value } = target;
    setSearch({
      [name]: value,
    });
  };

  const onSearch = () => {
    dispatch(actSearchGradeRequest(idClassSelected, search));
  };

  const exportExcel = () => {
    axios({
      url: `http://localhost:8080/api/classrooms/${idClassSelected}/grades/export/excel`,
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('TOKEN')}`,
      },
      responseType: 'blob', // Important
    }).then((response) => {
      FileDownload(response.data, 'report.xlsx');
    });
  };

  return (
    <div className="row">
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
        <button type="button" onClick={exportExcel}>
          Export Excel
        </button>
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder="Student name..."
            name="q"
            value={search.q}
            onChange={onHandleSearch}
          />
          <span className="input-group-btn">
            <button
              className="btn btn-primary"
              type="button"
              onClick={onSearch}
            >
              <span className="fa fa-search mr-5">&nbsp;Search</span>
            </button>
          </span>
        </div>
      </div>
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
        <select
          className="form-select"
          aria-label="Default select example"
          value={idClassSelected}
          onChange={handleChangeClass}
        >
          {renderOption()}
        </select>
      </div>
    </div>
  );
};

export default index;
