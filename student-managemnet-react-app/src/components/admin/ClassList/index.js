import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  actGetClassRequest,
  actDeleteClassRequest,
} from '../../../store/actions/classAction';

const ClassList = () => {
  const classes = useSelector((store) => store.classes);

  const dispatch = useDispatch();

  const onGetClass = (id) => {
    dispatch(actGetClassRequest(id));
  };

  const onDeleteClass = (classObject) => {
    const isComfirm = confirm('Are you sure ?');
    if (isComfirm) {
      dispatch(
        actDeleteClassRequest({
          ...classObject,
          status: false,
        })
      );
    }
  };

  const renderClass = () => {
    let result = null;

    result = classes.map((classObject, index) => {
      return (
        <tr key={classObject.id}>
          <td>{index + 1}</td>
          <td>{classObject.name}</td>
          <td className="text-center">
            <span
              className={`badge ${
                classObject.status === true ? 'bg-success' : 'bg-secondary'
              } text-light`}
            >
              {classObject.status === true ? 'Active' : 'Inactive'}
            </span>
          </td>
          <td className="text-center">
            <button type="button" className="btn btn-warning">
              <i
                className="far fa-edit pe-1"
                onClick={() => onGetClass(classObject.id)}
              >
                &nbsp;Edit
              </i>
            </button>
            &nbsp;
            <button type="button" className="btn btn-danger">
              <i
                className="far fa-trash-alt pe-1"
                onClick={() => onDeleteClass(classObject)}
              >
                &nbsp;Delete
              </i>
            </button>
          </td>
        </tr>
      );
    });

    return result;
  };

  return (
    <div className="row mt-5">
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              <th className="text-center">No.</th>
              <th className="text-center">Name</th>
              <th className="text-center">Status</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>{renderClass()}</tbody>
        </table>
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <a className="page-link" href="/" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                1
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                2
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                3
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default ClassList;
