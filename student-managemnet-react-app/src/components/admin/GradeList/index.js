/* eslint-disable guard-for-in */
/* eslint-disable no-param-reassign */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  actGetGradeRequest,
  actDeleteGradeRequest,
} from '../../../store/actions/gradeActions';

const GradeList = () => {
  const gradeList = useSelector((state) => state.grades);

  const dispatch = useDispatch();

  const addPropertiy = (object, propName, propValue) => {
    object[propName] = propValue;
  };

  const customGradeList = () => {
    const newList = Object.assign([], gradeList);
    for (let i = 0; i < newList.length; i++) {
      for (let j = 0; j < newList[i].grades.length; j++) {
        addPropertiy(
          newList[i],
          newList[i].grades[j].subjectName,
          newList[i].grades[j].mark
        );
      }
    }
    return newList;
  };

  const newGradeList = customGradeList();

  const renderHeader = () => {
    if (newGradeList.length > 0) {
      const header = Object.keys(newGradeList[0]);
      return header.map((key) => {
        if (key !== 'grades' && key !== 'active') {
          return (
            // eslint-disable-next-line react/no-array-index-key
            <th key={key} className="text-center">
              {key.toUpperCase()}
            </th>
          );
        }
        return null;
      });
    }
    return null;
  };

  const renderDynamicTD = (grade) => {
    let result = null;
    result = Object.keys(grade).map((key) => {
      if (key !== 'grades' && key !== 'active') {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <td key={key} className="text-center">
            {grade[key]}
          </td>
        );
      }
      return null;
    });

    return result;
  };

  const onEdit = (studentId, studentName) => {
    dispatch(actGetGradeRequest(studentId, studentName));
  };

  const onDelete = (studentId) => {
    const isComfirm = confirm('Are you sure ?');
    if (isComfirm) {
      dispatch(actDeleteGradeRequest(studentId));
    }
  };

  const renderGrades = () => {
    let result = null;
    // if (gradeList[0].grades[0].active === false) {
    //   return null;
    // }
    result = gradeList.map((grade) => {
      return (
        <tr key={grade.studentId}>
          {renderDynamicTD(grade)}
          <td className="text-center">
            <button
              type="button"
              className="btn btn-warning"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
              onClick={() => onEdit(grade.studentId, grade.fullName)}
            >
              <i className="far fa-edit pe-1">&nbsp;Edit</i>
            </button>
            &nbsp;
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => onDelete(grade.studentId)}
            >
              <i className="far fa-trash-alt pe-1">&nbsp;Delete</i>
            </button>
          </td>
        </tr>
      );
    });

    return result;
  };

  return (
    <div className="row mt-5">
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              {renderHeader()}
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>{renderGrades()}</tbody>
        </table>
      </div>
    </div>
  );
};

export default GradeList;
