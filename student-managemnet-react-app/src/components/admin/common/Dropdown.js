import React from 'react';
import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import classes from './CustomInput.module.css';

/**
 * Component for custom dropdown
 */
const Dropdown = (props) => {
  const { id, list, value, label, onChange, onBlur, error } = props;

  return (
    <div className={classes['form-control']}>
      <label htmlFor={id}>{label}:</label>
      <Select
        id={id}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        variant="outlined"
        classes={{ root: classes.select }}
        error={error}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        {list.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.name}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

Dropdown.propTypes = {
  id: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  error: PropTypes.bool,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
};

export default Dropdown;
