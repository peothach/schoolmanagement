import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

import classes from './CustomInput.module.css';

// Style for input label
const inputLabelStyles = {
  classes: {
    focused: classes['input-label__focus'],
  },
};

// Style for input field
const inputWrapperStyles = {
  classes: {
    notchedOutline: classes['input-wapper'],
    focused: classes['input-wrapper__focus'],
  },
};

/**
 * Component for input fields
 */
const TextInput = (props) => {
  const { id, type, label, value, defaultValue, error, onChange, onBlur } =
    props;
  return (
    <div className={classes['form-control']}>
      <label htmlFor={id}>{label}:</label>
      <TextField
        id={id}
        type={type}
        variant="outlined"
        size="small"
        value={value}
        defaultValue={defaultValue}
        error={error}
        onChange={onChange}
        onBlur={onBlur}
        InputLabelProps={inputLabelStyles}
        InputProps={inputWrapperStyles}
      />
    </div>
  );
};

TextInput.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  defaultValue: PropTypes.string,
  error: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};

TextInput.defaultProps = {
  type: 'text',
};

export default TextInput;
