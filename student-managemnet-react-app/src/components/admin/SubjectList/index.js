import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  actDeleteSubjectRequest,
  actGetSubjectRequest,
} from '../../../store/actions/subjectAction';

const SubjectList = () => {
  const subjectList = useSelector((state) => state.subjects);

  const dispatch = useDispatch();

  const onDeleteSubject = (id) => {
    const isConfirm = confirm('Are you sure ?');
    if (isConfirm) {
      dispatch(actDeleteSubjectRequest(id));
    }
  };

  const onGetSubject = (id) => {
    dispatch(actGetSubjectRequest(id));
  };

  const renderSubject = () => {
    let result = null;

    result = subjectList.map((subject, index) => {
      return (
        <tr key={subject.id}>
          <td>{index + 1}</td>
          <td>{subject.name}</td>
          <td className="text-center">
            <span
              className={`badge ${
                subject.status === true ? 'bg-success' : 'bg-secondary'
              } text-light`}
            >
              {subject.status === true ? 'Active' : 'Inactive'}
            </span>
          </td>
          <td className="text-center">
            <button type="button" className="btn btn-warning">
              <i
                className="far fa-edit pe-1"
                onClick={() => onGetSubject(subject.id)}
              >
                &nbsp;Edit
              </i>
            </button>
            &nbsp;
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => onDeleteSubject(subject.id)}
            >
              <i className="far fa-trash-alt pe-1">&nbsp;Delete</i>
            </button>
          </td>
        </tr>
      );
    });

    return result;
  };

  return (
    <div className="row mt-5">
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              <th className="text-center">No.</th>
              <th className="text-center">Name</th>
              <th className="text-center">Status</th>
              <th className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>{renderSubject()}</tbody>
        </table>
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <a className="page-link" href="/" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                1
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                2
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/">
                3
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="/" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default SubjectList;
