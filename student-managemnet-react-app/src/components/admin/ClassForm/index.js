import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import {
  actAddClassRequest,
  actUpdateClassRequest,
} from '../../../store/actions/classAction';

const ClassForm = () => {
  const [classObject, setClassObject] = useState({
    id: 0,
    name: '',
    status: true,
  });

  const classFromStore = useSelector((state) => state.classItem);

  useEffect(() => {
    setClassObject(classFromStore);
  }, [classFromStore]);

  const dispatch = useDispatch();

  const onClear = () => {
    setClassObject({
      name: '',
      status: true,
    });
  };

  const onChange = (e) => {
    const { target } = e;
    const { name } = target;
    const { value } = target;
    let finalValue = value;
    if (target.type === 'select-one') {
      finalValue = value === 'true';
    }
    setClassObject({
      ...classObject,
      [name]: finalValue,
    });
  };

  const onSave = (e) => {
    e.preventDefault();
    if (classObject.id === undefined || classObject.id === 0) {
      dispatch(actAddClassRequest(classObject));
    } else {
      dispatch(actUpdateClassRequest(classObject));
    }
    setClassObject({
      name: '',
      status: true,
    });
  };
  return (
    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-4">
      <div className="panel panel-warning">
        <div className="panel-heading">
          <h5 className="panel-title">Add Class</h5>
        </div>
        <div className="panel-body">
          <form onSubmit={onSave}>
            <div className="form-group">
              <label htmlFor="name">Name :</label>
              <input
                id="name"
                type="text"
                className="form-control my-2"
                name="name"
                value={classObject.name}
                onChange={onChange}
              />
            </div>

            <label htmlFor="status">Status :</label>
            <select
              id="status"
              className="form-select mt-2"
              required="required"
              name="status"
              value={classObject.status}
              selected={classObject.status}
              onChange={onChange}
            >
              <option value="true">Active</option>
              <option value="false">Inactive</option>
            </select>
            <br />
            <div className="text-center">
              <button type="submit" className="btn btn-warning">
                {classObject.id === undefined || classObject.id === 0
                  ? 'Save'
                  : 'Update'}
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-danger"
                onClick={onClear}
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ClassForm;
