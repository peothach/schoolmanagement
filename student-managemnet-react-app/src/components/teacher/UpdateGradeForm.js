import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import classes from './UpdateGradeForm.module.css';

// Style for input label
const inputLabelStyles = {
  classes: {
    focused: classes['update-input-label__focus'],
  },
};

// Style for input field
const inputWrapperStyles = {
  classes: {
    notchedOutline: classes['update-input-wapper'],
    focused: classes['update-input-wrapper__focus'],
  },
};

const GradeUpdateForm = (props) => {
  const {
    open,
    id,
    studentName,
    studentBirthdate,
    mark,
    subjectName,
    onUpdate,
    onClose,
  } = props;
  const [updatedMark, setUpdatedMark] = useState(mark);
  const [errorMessage, setErrorMessage] = useState(null);

  const onMarkChangeHandler = (event) => {
    setUpdatedMark(event.target.value);
  };

  const onUpdateSubmitHandler = () => {
    if (/[a-zA-z]/.test(updatedMark)) {
      setErrorMessage({
        field: 'mark',
        content: 'Mark must be a number',
      });
      return;
    }

    if (updatedMark > 10 || updatedMark < 0) {
      setErrorMessage({
        field: 'mark',
        content: 'Mark must be greater than 0 and less than 10',
      });
      return;
    }

    if (updatedMark === mark) {
      setErrorMessage({
        field: 'mark',
        content: 'Mark has not been changed',
      });
      return;
    }

    const formData = {
      id,
      mark: Number(updatedMark),
      active: true,
    };
    onUpdate(formData);
    setErrorMessage(null);
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        setErrorMessage(null);
        onClose();
      }}
    >
      <DialogTitle id="update-form-dialog-title">Update mark</DialogTitle>
      <DialogContent dividers>
        <div className={classes['update-error']}>
          {errorMessage && <p>{errorMessage.content}</p>}
        </div>
        <form noValidate onSubmit={onUpdateSubmitHandler}>
          <div style={{ marginBottom: '30px' }}>
            <p
              className="form-label"
              style={{ marginBottom: '5px', fontWeight: '500' }}
            >
              Full name: {studentName}
            </p>
            <p
              className="form-label"
              style={{ marginBottom: '5px', fontWeight: '500' }}
            >
              Birthdate: {studentBirthdate}
            </p>
            <p
              className="form-label"
              style={{ marginBottom: '5px', fontWeight: '500' }}
            >
              Subject: {subjectName}
            </p>
          </div>
          <div className={classes['update-form-control']}>
            <label htmlFor="mark">Mark: </label>
            <TextField
              id="mark"
              label="mark"
              variant="outlined"
              size="small"
              value={updatedMark}
              InputLabelProps={inputLabelStyles}
              InputProps={inputWrapperStyles}
              onChange={onMarkChangeHandler}
              error={errorMessage && errorMessage.field === 'mark'}
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setErrorMessage(null);
            onClose();
          }}
          className={classes['update-form-button__cancel']}
        >
          Cancel
        </Button>
        <Button
          onClick={onUpdateSubmitHandler}
          className={classes['update-form-button__ok']}
        >
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

GradeUpdateForm.propTypes = {
  open: PropTypes.bool,
  id: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  subjectName: PropTypes.string,
  mark: PropTypes.number,
  studentName: PropTypes.string.isRequired,
  studentBirthdate: PropTypes.string.isRequired,
};

GradeUpdateForm.defaultProps = {
  open: false,
};

export default GradeUpdateForm;
