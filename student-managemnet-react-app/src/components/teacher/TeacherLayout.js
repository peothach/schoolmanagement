import React from 'react';
import PropTypes from 'prop-types';

import classes from '../common/Layout.module.css';
import Header from '../common/Header';
import TeacherSidebar from './TeacherSidebar';

/**
 * Layout Common
 * @param {node} children contains main content of the page
 * @returns
 */
const Layout = ({ children }) => {
  return (
    <div>
      <Header />
      <TeacherSidebar />
      <main className={classes.main}>{children}</main>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
