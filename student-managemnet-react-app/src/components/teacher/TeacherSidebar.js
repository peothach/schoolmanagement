import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { PERSON_ID, ROLE, TOKEN, USER_NAME } from '../../constants/constants';
import logo from '../../images/logo.png';
import { logout } from '../../store/actions/authenAction';
import classes from '../common/Sidebar.module.css';

/**
 * Left Sidebar Common
 */
const TeacherSidebar = () => {
  const dispatch = useDispatch();

  const onLogoutHandler = () => {
    dispatch(logout());

    localStorage.removeItem(TOKEN);
    localStorage.removeItem(USER_NAME);
    localStorage.removeItem(ROLE);
    localStorage.removeItem(PERSON_ID);
  };

  return (
    <div className={classes.sidebar}>
      <div className={classes['sidebar-header']}>
        <div className={classes['sidebar-logo']}>
          <img src={logo} alt="logo" />
        </div>
      </div>
      <div className={classes['sidebar-menu']}>
        <ul>
          <li>
            <NavLink
              to="/teacher/profile"
              className={classes['sidebar-link']}
              activeClassName={classes.active}
            >
              <i className="position fas fa-chalkboard-teacher" />
              <span>Profile</span>
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/teacher/grades"
              className={classes['sidebar-link']}
              activeClassName={classes.active}
            >
              <i className="position fas fa-users" />
              <span>Grade</span>
            </NavLink>
          </li>
        </ul>
        <ul>
          <li>
            <button
              type="button"
              className={`${classes['sidebar-link']} ${classes['sidebar-btn']}`}
              onClick={onLogoutHandler}
            >
              <i className="fas fa-sign-out-alt" />
              <span>Logout</span>
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default TeacherSidebar;
