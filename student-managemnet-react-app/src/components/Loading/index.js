import React, { useState, useEffect } from 'react';

import { CircleToBlockLoading } from 'react-loadingg';
import { useSelector } from 'react-redux';
import classes from './style.module.css';

const Loading = () => {
  const [loading, setLoading] = useState(false);

  const showLoading = useSelector((state) => state.loading);

  useEffect(() => {
    setLoading(showLoading);
  }, [showLoading]);

  const renderLoading = () => {
    let result = null;

    if (loading) {
      result = (
        <div className={classes.loading}>
          <CircleToBlockLoading speed={2} size="large" />
        </div>
      );
    } else {
      result = '';
    }

    return result;
  };

  return renderLoading();
};

export default Loading;
