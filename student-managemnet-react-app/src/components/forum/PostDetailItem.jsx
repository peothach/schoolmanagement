import React from 'react';
import PropTypes from 'prop-types';

import classes from './PostDetailItem.module.css';
import avatarIcon from '../../images/man.png';

const PostDetailItem = (props) => {
  const { authorName, postTime, postTitle, postContent } = props;

  return (
    <div className={classes['post-detail__post']}>
      <div className={classes['post-detail__author']}>
        <img
          src={avatarIcon}
          alt="author"
          className={classes['post-detail__author-img']}
        />
        <div className={classes['post-detail__info']}>
          <span className={classes['post-detail__author-name']}>
            {authorName}
          </span>
          <span className={classes['post-detail__info-time']}>{postTime}</span>
        </div>
      </div>
      <div className={classes['post-detail__divider']} />
      <div className={classes['post-detail__post-body']}>
        <h3 className={classes['post-detail__post-title']}>{postTitle}</h3>
        <p className={classes['post-detail__post-content']}>{postContent}</p>
      </div>
    </div>
  );
};

PostDetailItem.propTypes = {
  authorName: PropTypes.string.isRequired,
  postTime: PropTypes.string.isRequired,
  postTitle: PropTypes.string.isRequired,
  postContent: PropTypes.string.isRequired,
};

export default PostDetailItem;
