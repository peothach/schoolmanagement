import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import classes from './TopicItem.module.css';
import topicImage from '../../images/books.png';

const TopicItem = (props) => {
  const history = useHistory();
  const { topicId, topicName, numOfPosts, activityTime, lastPostContent } =
    props;

  const handleItemClick = () => {
    history.push(`/forum/topics/${topicId}`);
  };

  return (
    <div className={classes['all-topics-container']} onClick={handleItemClick}>
      <div className={classes['all-topics-container__left']}>
        <img
          src={topicImage}
          alt="topic icons"
          className={classes['all-topics-img']}
        />
        <span className={classes['all-topics-name']}>{topicName}</span>
      </div>
      <div className={classes['all-topics-container__right']}>
        <div className={classes['topic-right__row-top']}>
          <span
            className={`${classes['topic-right__post']} ${classes['topic-right-title']}`}
          >
            Posts
          </span>
          <span
            className={`${classes['topic-right__post-content']} ${classes['topic-right-content']}`}
          >
            {numOfPosts}
          </span>
          {/* <span
            className={`${classes['topic-right__people']} ${classes['topic-right-title']}`}
          >
            People
          </span>
          <span
            className={`${classes['topic-right__people-content']} ${classes['topic-right-content']}`}
          >
            {numOfPeople}
          </span> */}
          <span
            className={`${classes['topic-right__activity']} ${classes['topic-right-title']}`}
          >
            Activity
          </span>
          <span
            className={`${classes['topic-right__activity-content']} ${classes['topic-right-content']}`}
          >
            {activityTime}
          </span>
        </div>
        <div className={classes['all-topics-divider']} />
        <div className={classes['topic-right-row-bottom']}>
          <span
            className={`${classes['topic-right__last-post']} ${classes['topic-right-title']}`}
          >
            Last post
          </span>
          <span
            className={`${classes['topic-right__last-post-content']} ${classes['topic-right-content']}`}
          >
            {lastPostContent}
          </span>
        </div>
      </div>
    </div>
  );
};

TopicItem.propTypes = {
  topicId: PropTypes.number.isRequired,
  topicName: PropTypes.string.isRequired,
  numOfPosts: PropTypes.number.isRequired,
  activityTime: PropTypes.string.isRequired,
  lastPostContent: PropTypes.string.isRequired,
};

export default TopicItem;
