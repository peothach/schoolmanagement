import React from 'react';
import PropTypes from 'prop-types';

import classes from './Comment.module.css';
import avatarIcon from '../../images/man.png';

const Comment = (props) => {
  const { author, datetime, content } = props;

  return (
    <div className={classes['post-detail__comment']}>
      <div className={classes['post-detail_comment-account']}>
        <img
          src={avatarIcon}
          alt="avatar"
          className={classes['post-detail__comment-account-img']}
        />
        <span className={classes['post-detail__comment-account-name']}>
          {author}
        </span>
        <span className={classes['post-detail__coment-divider']}>-</span>
        <span className={classes['post-detail__comment-time']}>{datetime}</span>
      </div>
      <div className={classes['post-detail__comment-content']}>
        <p className={classes['post-detail__comment-text']}>{content}</p>
      </div>
    </div>
  );
};

Comment.propTypes = {
  author: PropTypes.string.isRequired,
  datetime: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export default Comment;
