import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import classes from './TopicSidebar.module.css';
import TopicSidebarItem from './TopicSidebarItem';
import { getTopics } from '../../store/actions/topicsAction';

const TopicSidebar = () => {
  const dispatch = useDispatch();

  const topics = useSelector((state) => state.topics.topics);

  useEffect(() => {
    dispatch(getTopics());
  }, []);

  return (
    <section className={classes['list-topics-section']}>
      <div className={classes['list-topics-container']}>
        <div className={classes['list-topics-header']}>
          <h3 className={classes['list-topics-title']}>Topics</h3>
        </div>
        <div className={classes['list-topics-body']}>
          {topics.map((item) => (
            <TopicSidebarItem
              key={item.id}
              topicId={item.id}
              topicName={item.name}
              numOfPosts={item.threadCount}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default TopicSidebar;
