import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import classes from './TopicSidebarItem.module.css';
import topicImage from '../../images/books.png';

const TopicSidebarItem = (props) => {
  const { topicId, topicName, numOfPosts } = props;
  const history = useHistory();

  const handleItemClick = () => {
    history.push(`/forum/topics/${topicId}`);
  };

  return (
    <div className={classes['topic-item']} onClick={handleItemClick}>
      <div className={classes['topic-item-container']}>
        <img
          src={topicImage}
          alt="topic icon"
          className={classes['topic-item-img']}
        />
        <div className={classes['topic-item-body']}>
          <span className={classes['topic-item-body__name']}>{topicName}</span>
          <span className={classes['topic-item-body__post']}>
            {numOfPosts} {numOfPosts > 1 ? 'posts' : 'post'}
          </span>
        </div>
      </div>
    </div>
  );
};

TopicSidebarItem.propTypes = {
  topicId: PropTypes.number.isRequired,
  topicName: PropTypes.string.isRequired,
  numOfPosts: PropTypes.number.isRequired,
};

export default TopicSidebarItem;
