import React, { useState } from 'react';
import PropTypes from 'prop-types';

import avatarIcon from '../../images/man.png';
import classes from './CommentInput.module.css';

const CommentInput = (props) => {
  const { handleSubmitComment } = props;
  const [content, setContent] = useState('');

  const handleOnChange = (event) => {
    setContent(event.target.value);
  };

  const handleSumit = () => {
    handleSubmitComment(content);

    setContent('');
  };

  return (
    <div className={classes['post-detail__new-comment']}>
      <img
        src={avatarIcon}
        alt="avatar"
        className={classes['post-detail__avatar']}
      />
      <input
        type="text"
        value={content}
        placeholder="Enter comment..."
        onChange={handleOnChange}
        className={classes['post-detail__new-input']}
      />
      <button
        type="button"
        className={classes['post-detail__new-btn']}
        onClick={handleSumit}
        disabled={!content}
      >
        <i className="fas fa-paper-plane" />
      </button>
    </div>
  );
};

CommentInput.propTypes = {
  handleSubmitComment: PropTypes.func.isRequired,
};

export default CommentInput;
