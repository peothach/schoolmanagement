import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

import classes from './PostItem.module.css';
import avatarIcon from '../../images/man.png';

const PostItem = (props) => {
  const history = useHistory();
  const { postId, topicId, title, content, author, datetime, comments } = props;

  const handleItemClick = () => {
    history.push(`/forum/topics/${topicId}/post/${postId}`);
  };

  return (
    <div className={classes['post-item']} onClick={handleItemClick}>
      <div className={classes['post-item-container']}>
        <div className={classes['post-item__body']}>
          <h3 className={classes['post-item__body-title']}>{title}</h3>
          <p>{content}</p>
        </div>
        <div className={classes['post-item__divider']} />
        <div className={classes['post-item__footer']}>
          <div className={classes['post-item__author']}>
            <img
              src={avatarIcon}
              alt="author"
              className={classes['post-item__author-img']}
            />
            <span
              className={`${classes['post-item__author-name']} ${classes['post-item-text']}`}
            >
              {author}
            </span>
          </div>
          <div
            className={`${classes['post-item__date']} ${classes['post-item-text']}`}
          >
            <span>{datetime}</span>
          </div>
          <div className={classes['post-item__comment']}>
            <i className="far fa-comment-alt" />
            <span
              className={`${classes['post-item__comment-count']} ${classes['post-item-text']}`}
            >
              {comments}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

PostItem.propTypes = {
  postId: PropTypes.number.isRequired,
  topicId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  datetime: PropTypes.string.isRequired,
  comments: PropTypes.number.isRequired,
};

export default PostItem;
