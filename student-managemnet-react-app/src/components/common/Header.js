import React from 'react';
import { useSelector } from 'react-redux';

import classes from './Header.module.css';
import { ADMIN, TEACHER, STUDENT } from '../../constants/constants';

/**
 * Header Common
 */
const Header = () => {
  const role = useSelector((state) => state.authen.role);
  return (
    <header className={classes.header}>
      <div className={classes['header-title']}>
        <h5>School Management</h5>
      </div>
      <div className={classes['header-info']}>
        <i className="fas fa-user-tie" />
        {role === ADMIN && <span>Admin</span>}
        {role === TEACHER && <span>Teacher</span>}
        {role === STUDENT && <span>Student</span>}
      </div>
    </header>
  );
};

export default Header;
