import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import classes from './Sidebar.module.css';
import logo from '../../images/logo.png';
import ChangePasswrodDialog from './ChangePasswordDialog';
import {
  logout,
  changePassword,
  hideAlertDialog,
} from '../../store/actions/authenAction';
import LoginAlertDialog from '../../pages/authen/LoginAlertDialog';
import {
  ADMIN,
  TEACHER,
  STUDENT,
  TOKEN,
  USER_NAME,
  ROLE,
  PERSON_ID,
} from '../../constants/constants';

/**
 * Left Sidebar Common
 */
const Sidebar = () => {
  const dispatch = useDispatch();

  const username = useSelector((state) => state.authen.username);
  const roleLoggedIn = useSelector((state) => state.authen.role);
  const error = useSelector((state) => state.authen.error);
  const success = useSelector((state) => state.authen.success);

  const [showChangePassword, setShowChangePassword] = useState(false);

  const onChangePasswordClickHandler = () => {
    setShowChangePassword(true);
  };

  const onChangePasswordHandler = (data) => {
    dispatch(changePassword(data));

    setShowChangePassword(false);
  };

  const onLogoutHandler = () => {
    dispatch(logout());

    localStorage.removeItem(TOKEN);
    localStorage.removeItem(USER_NAME);
    localStorage.removeItem(ROLE);
    localStorage.removeItem(PERSON_ID);
  };

  // Handler for closing alert dialog when login failed
  const handleOnAlertClose = () => {
    dispatch(hideAlertDialog());
  };

  useEffect(() => {
    if (success) {
      dispatch(logout());

      localStorage.removeItem(TOKEN);
      localStorage.removeItem(USER_NAME);
      localStorage.removeItem(ROLE);
      localStorage.removeItem(PERSON_ID);
    }
  }, [success]);

  return (
    <div>
      {error && (
        <LoginAlertDialog
          open={!!error}
          onClose={handleOnAlertClose}
          title={error.title}
          message={error.message}
        />
      )}
      {showChangePassword && (
        <ChangePasswrodDialog
          open={showChangePassword}
          onChangePassword={onChangePasswordHandler}
          onClose={() => {
            setShowChangePassword(false);
          }}
        />
      )}
      <div className={classes.sidebar}>
        <div className={classes['sidebar-header']}>
          <div className={classes['sidebar-logo']}>
            <img src={logo} alt="logo" />
          </div>
        </div>
        <div className={classes['sidebar-menu']}>
          {(roleLoggedIn === STUDENT || roleLoggedIn === TEACHER) && (
            <div>
              <div className={classes['student-sidebar__account']}>
                <p>{username}</p>
              </div>
              <ul>
                <li>
                  <NavLink
                    to="/chat"
                    className={classes['sidebar-link']}
                    activeClassName={classes.active}
                  >
                    <i className="position fas fa-comment-alt" />
                    <span>Chat</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/forum/topics"
                    className={classes['sidebar-link']}
                    activeClassName={classes.active}
                  >
                    <i className="position fab fa-snapchat-ghost" />
                    <span>Forum</span>
                  </NavLink>
                </li>
              </ul>
            </div>
          )}

          {roleLoggedIn === ADMIN && (
            <ul>
              <li>
                <NavLink
                  to="/admin/teachers"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fa-chalkboard-teacher" />
                  <span>Teacher</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/students"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fa-users" />
                  <span>Student</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/classes"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fa-house-user" />
                  <span>Class</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/subjects"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fa-book-open" />
                  <span>Subject</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin/marks"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fas fa-marker" />
                  <span>Mark</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/chat"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fas fa-comment-alt" />
                  <span>Chat</span>
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/forum/topics"
                  className={classes['sidebar-link']}
                  activeClassName={classes.active}
                >
                  <i className="position fab fa-snapchat-ghost" />
                  <span>Forum</span>
                </NavLink>
              </li>
            </ul>
          )}
          <ul>
            <li>
              <button
                type="button"
                className={`${classes['sidebar-link']} ${classes['sidebar-btn']}`}
                onClick={onChangePasswordClickHandler}
              >
                <i className="fas fa-key" />
                <span>Password</span>
              </button>
            </li>
            <li>
              <button
                type="button"
                className={`${classes['sidebar-link']} ${classes['sidebar-btn']}`}
                onClick={onLogoutHandler}
              >
                <i className="fas fa-sign-out-alt" />
                <span>Logout</span>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
