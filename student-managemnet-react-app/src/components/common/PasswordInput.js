import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';

import classes from './PasswordInput.module.css';
import { FAILED, LOADING, SUCCESS } from '../../constants/constants';

// Style for input label
const passwordLabelStyles = {
  classes: {
    focused: classes['input-label__focus'],
  },
};

// Style for input field
const passwordWrapperStyles = {
  classes: {
    notchedOutline: classes['input-wapper'],
    focused: classes['input-wrapper__focus'],
  },
};

/**
 * Component for input fields
 */
const PasswordInput = (props) => {
  const {
    id,
    type,
    label,
    value,
    defaultValue,
    error,
    onChange,
    onBlur,
    status,
  } = props;

  return (
    <div className={classes['form-control']}>
      <label htmlFor={id}>{label}:</label>
      <TextField
        id={id}
        type={type}
        variant="outlined"
        size="small"
        value={value}
        defaultValue={defaultValue}
        error={error}
        onChange={onChange}
        onBlur={onBlur}
        InputLabelProps={passwordLabelStyles}
        InputProps={passwordWrapperStyles}
      />
      {status === LOADING && (
        <CircularProgress className="loading" size="1rem" />
      )}
      {status === SUCCESS && (
        <p className={classes['status-success']}>
          <i className="fas fa-check-circle" />
        </p>
      )}
      {status === FAILED && (
        <p className={classes['status-failed']}>
          <i className="fas fa-times-circle" />
        </p>
      )}
    </div>
  );
};

PasswordInput.propTypes = {
  id: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  defaultValue: PropTypes.string,
  error: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  status: PropTypes.string,
};

PasswordInput.defaultProps = {
  type: 'text',
};

export default PasswordInput;
