import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  checkPassword,
  inputPassword,
  clearPassword,
} from '../../store/actions/authenAction';

/**
 * Custom Hook for input field
 * @param {func} validate - Function that validate input value
 * @returns
 */
const useInputPassword = (initialVal, validate) => {
  const dispatch = useDispatch();
  const passwordChecking = useSelector(
    (state) => state.authen.passwordChecking
  );

  // Value that user entered
  const [value, setValue] = useState(initialVal);
  // Whether input field is edited or not
  const [isTouched, setIsTouched] = useState(false);

  // Check the value user entered is valid or not
  const isValid = validate(value);
  // Input field has error when user entered invalid value and input field has been editted
  const hasError = !isValid && isTouched;

  useEffect(() => {
    const timer = setTimeout(() => {
      if (isValid) {
        dispatch(checkPassword({ oldPassword: value }));
      }
    }, 1000);

    if (!isValid) {
      dispatch(clearPassword());
    }

    return () => {
      clearTimeout(timer);
    };
  }, [value]);

  // Input field onChange handler
  const onChangeHandler = (event) => {
    if (isValid) {
      dispatch(inputPassword());
    }
    setValue(event.target.value);
  };

  // Input field onBlur handler
  const onBlurHandler = () => {
    setIsTouched(true);
  };

  // Reset to initial state
  const reset = () => {
    setValue(initialVal);
    setIsTouched(false);
  };

  return {
    value,
    setValue,
    isValid,
    hasError,
    checkingStatus: passwordChecking,
    onChangeHandler,
    onBlurHandler,
    reset,
  };
};

export default useInputPassword;
