import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import TextInput from '../admin/common/TextInput';
import useInput from '../admin/student/use-input';
import useInputPassword from './use-input-password';

import classes from './ChangePasswordDialog.module.css';
import { FAILED } from '../../constants/constants';
import PasswordInput from './PasswordInput';

const ChangePasswordDialog = (props) => {
  const { open, onChangePassword, onClose } = props;
  const {
    value: oldPassword,
    isValid: oldPasswordIsValid,
    hasError: oldPasswordHasError,
    checkingStatus: oldPasswordStatus,
    onChangeHandler: onOldPasswordChangeHandler,
    onBlurHandler: onOldPasswordBlurHandler,
    reset: resetOldPassword,
  } = useInputPassword('', (value) => value.trim() !== '');

  const {
    value: newPassword,
    isValid: newPasswordIsValid,
    hasError: newPasswordHasError,
    onChangeHandler: onNewPasswordChangeHandler,
    onBlurHandler: onNewPasswordBlurHandler,
    reset: resetNewPassword,
  } = useInput('', (value) => value.trim() !== '');

  const {
    value: confirmPassword,
    isValid: confirmPasswordIsValid,
    hasError: confirmPasswordHasError,
    onChangeHandler: onConfirmPasswordChangeHandler,
    onBlurHandler: onConfirmPasswordBlurHandler,
    reset: resetConfirmPassword,
  } = useInput(
    '',
    ((newPass, confirmPass) => newPass === confirmPass).bind(this, newPassword)
  );

  useEffect(() => {
    return () => {
      resetOldPassword();
      resetNewPassword();
      resetConfirmPassword();
    };
  }, []);

  let formIsValid = false;
  if (oldPasswordIsValid && newPasswordIsValid && confirmPasswordIsValid) {
    formIsValid = true;
  }

  const onSubmitHandler = () => {
    if (!formIsValid) {
      return;
    }

    const formData = {
      oldPassword,
      newPassword,
    };

    onChangePassword(formData);

    resetOldPassword();
    resetNewPassword();
    resetConfirmPassword();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle id="form-dialog-title">Change password</DialogTitle>
      <DialogContent dividers>
        <form noValidate>
          <PasswordInput
            id="password"
            label="Password"
            type="password"
            value={oldPassword}
            onChange={onOldPasswordChangeHandler}
            onBlur={onOldPasswordBlurHandler}
            error={oldPasswordHasError}
            status={oldPasswordStatus}
          />
          <div className={classes.error}>
            {oldPasswordHasError && <p>Old password must not be empty!</p>}
            {oldPasswordStatus && oldPasswordStatus === FAILED && (
              <p>Old password incorrect!</p>
            )}
          </div>
          <TextInput
            id="newPassword"
            label="New Password"
            type="password"
            value={newPassword}
            onChange={onNewPasswordChangeHandler}
            onBlur={onNewPasswordBlurHandler}
            error={newPasswordHasError}
          />
          <div className={classes.error}>
            {newPasswordHasError && <p>New password must not be empty!</p>}
          </div>
          <TextInput
            id="confirmPassword"
            label="confirmPassword"
            type="password"
            value={confirmPassword}
            onChange={onConfirmPasswordChangeHandler}
            onBlur={onConfirmPasswordBlurHandler}
            error={confirmPasswordHasError}
          />
          <div className={classes.error}>
            {confirmPasswordHasError && <p>New password mismatched!</p>}
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} className={classes['form-button__cancel']}>
          Cancel
        </Button>
        <Button
          onClick={onSubmitHandler}
          className={classes['form-button__ok']}
          disabled={!formIsValid}
        >
          Change
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ChangePasswordDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onChangePassword: PropTypes.func.isRequired,
};

ChangePasswordDialog.defaultProps = {
  open: false,
};

export default ChangePasswordDialog;
