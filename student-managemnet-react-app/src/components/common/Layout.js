import React from 'react';
import PropTypes from 'prop-types';

import classes from './Layout.module.css';
import Header from './Header';
import Sidebar from './Sidebar';

/**
 * Layout Common
 * @param {node} children contains main content of the page
 * @returns
 */
const Layout = ({ children }) => {
  return (
    <div>
      <Header />
      <Sidebar />
      <main className={classes.main}>{children}</main>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
