export const ADMIN = 'ADMINISTRATOR';
export const STUDENT = 'STUDENT';
export const TEACHER = 'TEACHER';

export const TOKEN = 'TOKEN';
export const USER_NAME = 'USER_NAME';
export const PERSON_ID = 'PERSON_ID';
export const ROLE = 'ROLE';

export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const FAILED = 'FAILED';
