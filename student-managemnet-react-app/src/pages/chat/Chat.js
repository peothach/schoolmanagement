import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import Sidebar from '../../components/common/Sidebar';
import { API_URL } from '../../constants/apiConstant';
import axiosService from '../../services/axiosService';
import classes from './Chat.module.css';
import { saveMessage, getMessage } from '../../store/actions/messageAction';
import { toastError } from '../../utils/toast';

let stompClient = null;

const Chat = () => {
  let rooms = [];
  const URL_GET_SUPPORTERS = `${API_URL}/supporters`;
  const URL_GET_ACTIVE_USERS = `${API_URL}/active`;
  const URL_GET_MESSAGE = `${API_URL}/conversations`;
  const URL_GET_CONVERSATION = `${API_URL}/conversations/recipient`;

  const [inputMessage, setInputMessage] = useState('');
  const [message, setMessage] = useState([]);
  const [listSupporter, setListSupporter] = useState([]);
  const [activeUsers, setActiveUsers] = useState([]);
  const [userChatting, setUserChatting] = useState('');
  const [conversationID, setConversationID] = useState(null);
  const messageFromStore = useSelector((state) => state.message);

  const dispatch = useDispatch();

  const onMessageReceived = (payload) => {
    dispatch(saveMessage(JSON.parse(payload.body)));
  };

  const onActiveUser = (payload) => {
    setActiveUsers(JSON.parse(payload.body));
  };

  const onConversation = (payload) => {
    const data = JSON.parse(payload.body);
    setConversationID(data.id);
    stompClient.subscribe(
      `/conversations/${data.id}/queue/messages`,
      onMessageReceived,
      {
        'X-Authorization': ` ${localStorage.getItem('TOKEN')}`,
      }
    );
  };

  const onConnected = () => {
    stompClient.subscribe(`/topic/active`, onActiveUser, {
      'X-Authorization': ` ${localStorage.getItem('TOKEN')}`,
    });
    stompClient.subscribe(
      `/user/${localStorage.getItem('USER_NAME')}/new/conversation`,
      onConversation,
      {
        'X-Authorization': ` ${localStorage.getItem('TOKEN')}`,
      }
    );
    axiosService.get(`${API_URL}/conversations`).then((res) => {
      rooms = res.data;
      rooms.forEach((room) => {
        stompClient.subscribe(
          `/conversations/${room.id}/queue/messages`,
          onMessageReceived,
          {
            'X-Authorization': ` ${localStorage.getItem('TOKEN')}`,
          }
        );
      });
    });
  };

  const onError = (err) => {
    toastError(err);
  };

  const connect = () => {
    const sock = new SockJS(
      'http://ec2-13-229-232-181.ap-southeast-1.compute.amazonaws.com:8081/ws'
    );
    stompClient = Stomp.over(sock);
    stompClient.connect(
      { 'X-Authorization': ` ${localStorage.getItem('TOKEN')}` },
      onConnected,
      onError
    );
  };

  const changeMessage = (e) => {
    setInputMessage(e.target.value);
  };

  const onClickUserChatting = (username) => {
    setUserChatting(username);
    axiosService
      .get(`${URL_GET_CONVERSATION}?username=${username}`)
      .then((res) => {
        setConversationID(res.data.id);
        axiosService
          .get(`${URL_GET_MESSAGE}/${res.data.id}/messages`)
          .then((response) => {
            dispatch(getMessage(response.data));
          });
      })
      .catch((error) => {
        if (error.response.status === 404) {
          stompClient.send(
            '/app/conversations',
            { 'X-Authorization': ` ${localStorage.getItem('TOKEN')}` },
            JSON.stringify({ usernames: [username] })
          );
        }
      });
  };

  const sendMessage = () => {
    if (conversationID === null) return;
    stompClient.send(
      '/app/chat',
      { 'X-Authorization': ` ${localStorage.getItem('TOKEN')}` },
      JSON.stringify({ conversationId: conversationID, content: inputMessage })
    );
    setInputMessage('');
  };

  const renderSupporter = () => {
    let contents = null;

    if (localStorage.getItem('USER_NAME') === 'admin') {
      return null;
    }

    contents = listSupporter.map((supporter) => {
      return (
        <div
          key={supporter}
          className={classes['people__body-row']}
          onClick={() => {
            onClickUserChatting(supporter);
          }}
        >
          <div className={classes['people__body-wrapper']}>
            <div className={classes['people__body-avatar']}>
              <span>SP</span>
            </div>
            <div className={classes['people__body-info']}>
              <span className={classes['people__body-info-name']}>
                {supporter}
              </span>
              <span className={classes['people__body-info-status']}>
                {activeUsers.indexOf(supporter) !== -1 ? 'online' : 'offline'}
              </span>
            </div>
          </div>
        </div>
      );
    });
    return contents;
  };

  const renderActiveUsers = () => {
    let contents = null;

    contents = activeUsers.map((user) => {
      if (user === 'admin' || user === localStorage.getItem('USER_NAME')) {
        return null;
      }

      return (
        <div
          key={user}
          className={classes['people__body-row']}
          onClick={() => {
            onClickUserChatting(user);
          }}
        >
          <div className={classes['people__body-wrapper']}>
            <div className={classes['people__body-avatar']}>
              <span>{user.substring(0, 1).toUpperCase()}</span>
            </div>
            <div className={classes['people__body-info']}>
              <span className={classes['people__body-info-name']}>{user}</span>
              <span className={classes['people__body-info-status']}>
                online
              </span>
            </div>
          </div>
        </div>
      );
    });

    return contents;
  };

  const renderNewMessage = () => {
    let contents = null;

    if (message.length === 0) {
      return null;
    }

    contents = message.map((msg) => {
      if (msg.conversationId !== conversationID) {
        return null;
      }
      return msg.senderUsername === localStorage.getItem('USER_NAME') ? (
        <div key={msg.id} className={classes['chat__body-content-row']}>
          <div className={classes['chat__body-sender']}>
            <div className={classes['chat__body-content-sender-msg']}>
              <span>{msg.content}</span>
            </div>
          </div>
        </div>
      ) : (
        <div key={msg.id} className={classes['chat__body-content-row']}>
          <div className={classes['chat__body-receiver']}>
            <div className={classes['chat__body-content-receiver-avatar']}>
              <span
                className={classes['chat__body-content-receiver-avatar-name']}
              >
                {userChatting.substring(0, 1).toUpperCase()}
              </span>
            </div>
            <div className={classes['chat__body-content-receiver-msg']}>
              <span>{msg.content}</span>
            </div>
          </div>
        </div>
      );
    });

    return <div className={classes['chat__body-content']}>{contents}</div>;
  };

  const renderUserChatting = () => {
    if (userChatting === '') {
      return null;
    }
    return (
      <div className={classes.chat__header}>
        <div className={classes['chat__header-avatar']}>
          <span className={classes['chat__header-avatar-name']}>
            {userChatting.substring(0, 1).toUpperCase()}
          </span>
          <div className={classes['chat__header-avatar-status']} />
        </div>
        <div className={classes['chat__header-info']}>
          <h4 className={classes['chat__header-info-name']}>{userChatting}</h4>
          <span className={classes['chat__header-info-status']}>online</span>
        </div>
      </div>
    );
  };

  window.addEventListener('beforeunload', function () {
    stompClient.close();
  });

  useEffect(() => {
    connect();
  }, []);

  useEffect(() => {
    axiosService.get(URL_GET_SUPPORTERS).then((res) => {
      setListSupporter(res.data);
    });
  }, []);

  useEffect(() => {
    axiosService.get(URL_GET_ACTIVE_USERS).then((res) => {
      setActiveUsers(res.data);
    });
  }, []);

  useEffect(() => {
    setMessage(messageFromStore);
  }, [messageFromStore]);

  return (
    <div>
      <Sidebar />

      <main className={classes.main}>
        <section className={classes['chat-section']}>
          <div className={classes.chat}>
            {renderUserChatting()}
            <div className={classes.chat__body}>
              <div className={classes['chat__body-content-wrapper']}>
                {renderNewMessage()}
              </div>
              <div className={classes['chat__body-options']}>
                <input
                  type="text"
                  className={classes['chat__body-options-input']}
                  placeholder="Enter new message"
                  value={inputMessage}
                  onChange={changeMessage}
                />
                <button
                  type="button"
                  className={classes['chat__body-options-submit']}
                  onClick={sendMessage}
                >
                  <i className="fas fa-paper-plane" />
                </button>
              </div>
            </div>
          </div>
        </section>
        <section className={classes['people-section']}>
          <div className={classes['people-list']}>
            <div className={classes.people__header}>
              <div className={classes['people__header-title']}>
                <p>Chat</p>
              </div>
              <div className={classes['people__header-options']}>
                <button
                  type="button"
                  className={classes['people__header-btn-add']}
                >
                  <i className="fas fa-plus" />
                </button>
              </div>
            </div>
            <div className={classes.people__body}>
              {renderSupporter()}
              {renderActiveUsers()}
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default Chat;
