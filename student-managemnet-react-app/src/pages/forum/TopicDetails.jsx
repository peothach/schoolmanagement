import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import classes from './TopicDetails.module.css';
import TopicSidebar from '../../components/forum/TopicSidebar';
import Sidebar from '../../components/common/Sidebar';
import Header from '../../components/common/Header';
import PostItem from '../../components/forum/PostItem';

import { getPostsByTopic } from '../../store/actions/postsAction';

const TopicDettails = () => {
  const dispatch = useDispatch();
  const { topicId } = useParams();

  const posts = useSelector((state) => state.posts.posts);

  useEffect(() => {
    dispatch(getPostsByTopic(topicId));
  }, [topicId]);

  return (
    <div>
      <Header />
      <Sidebar />
      <main className={classes.main}>
        <section className={classes['all-posts-section']}>
          <div className={classes['all-posts']}>
            <div className={classes['all-posts__header']}>
              <h3 className={classes['all-posts__header-title']}>Math Topic</h3>
              <button
                type="button"
                className={classes['all-posts__header-btn-add']}
              >
                New Post
              </button>
            </div>
            <div className={classes['all-posts-body']}>
              {posts.map((item) => (
                <PostItem
                  key={item.id}
                  postId={item.id}
                  topicId={parseInt(topicId, 10)}
                  title={item.title}
                  content="Click to see content..."
                  author={item.authorUsername}
                  datetime={item.createdTime}
                  comments={item.postCount}
                />
              ))}
            </div>
          </div>
        </section>
        <TopicSidebar />
      </main>
    </div>
  );
};

export default TopicDettails;
