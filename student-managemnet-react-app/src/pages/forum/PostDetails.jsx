import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import classes from './PostDetails.module.css';
import Sidebar from '../../components/common/Sidebar';
import TopicSidebar from '../../components/forum/TopicSidebar';
import Header from '../../components/common/Header';
import PostDetailItem from '../../components/forum/PostDetailItem';
import CommentInput from '../../components/forum/CommentInput';
import { getPostById, createComment } from '../../store/actions/postsAction';
import Comment from '../../components/forum/Comment';

const PostDetails = () => {
  const dispatch = useDispatch();
  const { postId } = useParams();

  const data = useSelector((state) => state.posts.comments);
  const username = useSelector((state) => state.authen.username);

  console.log(username);

  const post = data[0];
  const comments = data.slice(1);

  useEffect(() => {
    dispatch(getPostById(postId));
  }, []);

  const handleCommentCommit = (content) => {
    const commentData = {
      authorUsername: username,
      content,
      thread: {
        id: postId,
      },
    };

    dispatch(createComment(commentData, postId));
  };

  return (
    <div>
      <Header />
      <Sidebar />
      <main className={classes.main}>
        <section className={classes['post-detail-section']}>
          <div className={classes['post-detail']}>
            {post && (
              <PostDetailItem
                authorName={post.thread.authorUsername}
                postTime="4 minute ago..."
                postTitle={post.thread.title}
                postContent={post.content}
              />
            )}

            <div className={classes['post-detail__comments']}>
              <CommentInput handleSubmitComment={handleCommentCommit} />
              {comments.length > 0 && (
                <div className={classes['post-detail__list-comments']}>
                  <div className={classes['post-detail__comments-summary']}>
                    <span className={classes['post-detail__comments-count']}>
                      {comments.length}{' '}
                      {comments.length > 1 ? 'replies' : 'reply'}
                    </span>
                  </div>
                  <div className={classes['post-detail__comments-divider']} />
                  <div className={classes['post-detail__comments-list']}>
                    {comments.map((item) => (
                      <Comment
                        key={item.id}
                        author={item.authorUsername}
                        content={item.content}
                        datetime={item.createdTime}
                      />
                    ))}
                  </div>
                </div>
              )}
            </div>
          </div>
        </section>
        <TopicSidebar />
      </main>
    </div>
  );
};

export default PostDetails;
