import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Sidebar from '../../components/common/Sidebar';
import classes from './ListTopics.module.css';
import TopicSidebar from '../../components/forum/TopicSidebar';
import Header from '../../components/common/Header';
import TopicItem from '../../components/forum/TopicItem';
import { getTopics } from '../../store/actions/topicsAction';

const ListTopics = () => {
  const dispatch = useDispatch();

  const topics = useSelector((state) => state.topics.topics);

  useEffect(() => {
    dispatch(getTopics());
  }, []);

  return (
    <div>
      <Header />
      <Sidebar />
      <main className={classes.main}>
        <section className={classes['all-topics-section']}>
          <div className={classes['all-topics']}>
            <div className={classes['all-topics__header']}>
              <h3 className={classes['all-topics__header-title']}>
                All Topics
              </h3>
            </div>
            <div className={classes['all-topics-body']}>
              {topics.map((item) => (
                <TopicItem
                  key={item.id}
                  topicName={item.name}
                  topicId={item.id}
                  numOfPosts={item.threadCount}
                  activityTime={item.createdTime}
                  lastPostContent={item.description}
                />
              ))}
            </div>
          </div>
        </section>
        <TopicSidebar />
      </main>
    </div>
  );
};

export default ListTopics;
