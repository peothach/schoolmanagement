import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ClassForm from '../../components/admin/ClassForm';
import ClassOption from '../../components/admin/ClassOption';
import ClassList from '../../components/admin/ClassList';

import { actFetchClassesRequest } from '../../store/actions/classAction';

const ClassManagementPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actFetchClassesRequest());
  }, []);

  return (
    <div className="subject p-5">
      <div>
        <h3>Class</h3>
      </div>
      <div className="row mt-5">
        <ClassForm />
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <ClassOption />
          <ClassList />
        </div>
      </div>
    </div>
  );
};

export default ClassManagementPage;
