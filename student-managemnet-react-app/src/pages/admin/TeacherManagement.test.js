import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import { findByTestAttr, storeFactory } from '../../utils/testUtils';
import TeacherManagement from './TeacherManagement';
import { initialState } from '../../store/reducers/teachersReducer';

const setup = (state) => {
  const store = storeFactory(state);
  return mount(
    <Provider store={store}>
      <TeacherManagement />
    </Provider>
  );
};

describe('test renders', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });

  it('renders without errors', () => {
    const teacherComponent = findByTestAttr(
      wrapper,
      'component-teacher-management'
    );
    expect(teacherComponent).toHaveLength(1);
  });

  it('does not show add techer form dialog', () => {
    const addDialog = findByTestAttr(wrapper, 'add-dialog');

    expect(addDialog).toHaveLength(0);
  });

  it('does not show update teacher form dialog', () => {
    const updateDialog = findByTestAttr(wrapper, 'update-dialog');
    expect(updateDialog).toHaveLength(0);
  });

  it('does not show alert dialog', () => {
    const alertDialog = findByTestAttr(wrapper, 'alert-dialog');

    expect(alertDialog).toHaveLength(0);
  });

  it('does not show confirm dialog', () => {
    const confirmDialog = findByTestAttr(wrapper, 'confirm-dialog');

    expect(confirmDialog).toHaveLength(0);
  });
});

describe('if table has no data', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      teachers: {
        ...initialState,
      },
    });
  });

  it('does not enable previous page button', () => {
    const prevButton = findByTestAttr(wrapper, 'prev-button');

    expect(prevButton.props().disabled).toBe(true);
  });

  it('does not enable next page button', () => {
    const nextButton = findByTestAttr(wrapper, 'next-button');

    expect(nextButton.props().disabled).toBe(true);
  });

  it('renders No Teachers row', () => {
    const noTeacherRow = findByTestAttr(wrapper, 'no-teacher-row');

    expect(noTeacherRow).toHaveLength(1);
  });
});

describe('if table has data', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      teachers: {
        ...initialState,
        teachers: [
          {
            id: 0,
            fullName: 'Name',
            gender: 'MALE',
            address: 'Address',
            phoneNumber: '0123456789',
            birthdate: '2000-01-01',
          },
        ],
        pageable: {
          page: 1,
          limit: 8,
          totalElements: 24,
          totalPage: 3,
        },
      },
    });
  });

  it('does not render No Teachers row', () => {
    const noTeacherRow = findByTestAttr(wrapper, 'no-teacher-row');

    expect(noTeacherRow).toHaveLength(0);
  });

  it('enables previous page button', () => {
    const prevButton = findByTestAttr(wrapper, 'prev-button');

    expect(prevButton.props().disabled).toBe(false);
  });

  it('enables next page button', () => {
    const nextButton = findByTestAttr(wrapper, 'next-button');

    expect(nextButton.props().disabled).toBe(false);
  });
});
