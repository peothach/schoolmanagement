import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import AlertDialog from '../../components/admin/common/AlertDialog';
import ConfirmDialog from '../../components/admin/common/ConfirmDialog';

import classes from './StudentsManagement.module.css';
import StudentForm, {
  ADD_NEW,
  UPDATE,
} from '../../components/admin/student/StudentForm';
import {
  getStudents,
  updateStudent,
  addStudent,
  deleteStudent,
  hideStudentDialog,
} from '../../store/actions/studentsManagementAction';

// Contant for how many rows in a page that shows on a table
const ROW_PER_PAGE = 3;

/**
 * Component for showing student management page of the admin.
 */
const StudentsManagement = () => {
  const dispatch = useDispatch();

  // List of students
  const students = useSelector((state) => state.students.students);
  // Pageable information
  const { page, limit, totalElements } = useSelector(
    (state) => state.students.pageable
  );
  // Information after done a action
  const actionStatus = useSelector((state) => state.students.actionStatus);

  // Current page of the table
  const [currentPage, setCurrentPage] = useState(0);
  // Whether showing add student form
  const [showAddForm, setShowAddForm] = useState(false);
  // Whether showing update student form
  const [showUpdateForm, setShowUpdateForm] = useState(false);
  // Show confirm detele dialog
  const [confirmDelete, setConfirmDelete] = useState(null);
  // Id of student to update
  const [studentId, setStudentId] = useState(null);
  // search value that user entered
  const [searchValue, setSearchVal] = useState('');

  useEffect(() => {
    // Get list of students
    dispatch(getStudents({ page: currentPage, limit: ROW_PER_PAGE }));
  }, [dispatch, currentPage]);

  // Handler for previous page button is clicked
  const onPrevPageHandler = () => {
    setCurrentPage(page - 1);
  };

  // Handler for next page button is clicked
  const onNextPageHandler = () => {
    setCurrentPage(page + 1);
  };

  // Handler for input seach value change
  const onSearchInputChangeHnadler = (event) => {
    setSearchVal(event.target.value);
  };

  // Handler seach button is clicked
  const onSearchHandler = () => {
    dispatch(
      getStudents({
        q: searchValue,
        limit: ROW_PER_PAGE,
      })
    );
  };

  /**
   * Handler for add student
   * @param {Object} data - Student data
   */
  const onAddStudentHandler = (data) => {
    // dispatch addStudent action
    dispatch(
      addStudent(data, {
        q: searchValue,
        page: currentPage,
        limit: ROW_PER_PAGE,
      })
    );

    // Close dialog form
    setShowAddForm(false);
  };

  /**
   * Handler for update student
   * @param {Object} data - Student data
   */
  const onUpdateStudentHandler = (data) => {
    dispatch(
      updateStudent(data, {
        q: searchValue,
        page: currentPage,
        limit: ROW_PER_PAGE,
      })
    );

    setShowUpdateForm(false);
  };

  /**
   * Handler for clicking detail student button
   * @param {number} id - Student id
   */
  const onStudentDetailClickHandler = (id) => {
    setStudentId(id);
    setShowUpdateForm(true);
  };

  /**
   * Handler for clicking delete student button
   * @param {number} id - Student id
   */
  const onDeleteClickHandler = (id) => {
    // Updat state for showing confirm delete dialog
    setConfirmDelete({
      show: true,
      title: 'Delete Student!',
      content: 'Are you sure to delete?',
      onClose: () => {
        setConfirmDelete(null);
      },
      onOk: () => {
        // Dispatch delete student action
        dispatch(
          deleteStudent(id, {
            q: searchValue,
            page: currentPage,
            limit: ROW_PER_PAGE,
          })
        );
        setConfirmDelete(null);
      },
    });
  };

  /**
   * Handler for closing alert dialog
   */
  const onCloseAlertDialog = () => {
    dispatch(hideStudentDialog());
  };

  // from (min) element of current page
  const from = Math.min(totalElements, page * limit + 1);
  // to (max) element index of current
  const to = Math.min(totalElements, (page + 1) * limit);
  // pageable information of the table
  const pageableInfor = `${from}-${to} of ${totalElements}`;

  return (
    <div>
      {actionStatus && (
        <AlertDialog
          open={!!actionStatus}
          onClose={onCloseAlertDialog}
          title={actionStatus.title}
          content={actionStatus.content}
          account={actionStatus.account}
          status={actionStatus.status}
        />
      )}

      {confirmDelete && (
        <ConfirmDialog
          open={confirmDelete.show}
          title={confirmDelete.title}
          content={confirmDelete.content}
          onClose={confirmDelete.onClose}
          onOk={confirmDelete.onOk}
        />
      )}

      {showAddForm && (
        <StudentForm
          open={showAddForm}
          title="New Student Form"
          type={ADD_NEW}
          onClose={() => {
            setShowAddForm(false);
          }}
          onAdd={onAddStudentHandler}
        />
      )}

      {showUpdateForm && (
        <StudentForm
          open={showUpdateForm}
          title="Update Student Form"
          id={studentId}
          type={UPDATE}
          onClose={() => {
            setShowUpdateForm(false);
          }}
          onUpdate={onUpdateStudentHandler}
        />
      )}
      <section>
        <div className={classes['student-management-table-wrapper']}>
          <div className={classes['student-management-table-header']}>
            <h3>Student Management</h3>
            <div
              className={classes['student-management-table-header__actions']}
            >
              <button
                type="button"
                className={classes['button-add']}
                onClick={() => {
                  setShowAddForm(true);
                }}
              >
                Add
              </button>
              <div
                className={classes['student-management-table-search__wrapper']}
              >
                <button type="button" onClick={onSearchHandler}>
                  <i className="fas fa-search" />
                </button>
                <input
                  onChange={onSearchInputChangeHnadler}
                  type="text"
                  placeholder="Input name to search"
                />
              </div>
            </div>
          </div>
          <div className={classes['student-management-table-container']}>
            <table className={classes['student-management-table']}>
              <thead>
                <tr>
                  <th>No.</th>
                  <th style={{ width: '15%' }}>Username</th>
                  <th style={{ width: '20%' }}>FullName</th>
                  <th style={{ width: '15%' }}>Gender</th>
                  <th style={{ width: '15%' }}>Phone</th>
                  <th style={{ width: '20%' }}>Address</th>
                  <th colSpan="2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {students.length > 0 &&
                  students.map((student, index) => (
                    <tr key={student.id}>
                      <td>{index + 1}</td>
                      <td>{student.account}</td>
                      <td>{student.fullName}</td>
                      <td>{student.gender}</td>
                      <td>{student.phoneNumber}</td>
                      <td>{student.address}</td>
                      <td
                        className={classes['student-management-table-actions']}
                      >
                        <button
                          type="button"
                          onClick={() => {
                            onStudentDetailClickHandler(student.id);
                          }}
                          className={
                            classes['student-management-table-actions__button']
                          }
                        >
                          <i className="fas fa-eye" />
                        </button>
                      </td>
                      <td
                        className={classes['student-management-table-actions']}
                      >
                        <button
                          type="button"
                          onClick={() => onDeleteClickHandler(student.id)}
                          className={
                            classes['student-management-table-actions__button']
                          }
                        >
                          <i className="fas fa-trash-alt" />
                        </button>
                      </td>
                    </tr>
                  ))}
                {students.length === 0 && (
                  <tr>
                    <td
                      colSpan="7"
                      className={classes['student-management-table-no-row']}
                    >
                      No Students
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className={classes['student-management-table-footer']}>
            <p className={classes['student-management-table-footer__info']}>
              {pageableInfor}
            </p>
            <div
              className={classes['student-management-table-footer__actions']}
            >
              <button
                type="button"
                disabled={page === 0}
                onClick={onPrevPageHandler}
              >
                <i className="fas fa-chevron-left" />
              </button>
              <button
                type="button"
                onClick={onNextPageHandler}
                disabled={
                  totalElements !== 0
                    ? page >= Math.ceil(totalElements / ROW_PER_PAGE) - 1
                    : true
                }
              >
                <i className="fas fa-chevron-right" />
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default StudentsManagement;
