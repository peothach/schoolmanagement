import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import SubjectForm from '../../components/admin/SubjectForm';
import SubjectList from '../../components/admin/SubjectList';
import SubjectOption from '../../components/admin/SubjectOption';
import { actFetchListSubjectRequest } from '../../store/actions/subjectAction';

const SubjectManagementPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actFetchListSubjectRequest());
  }, []);

  return (
    <div className="subject p-5">
      <div>
        <h3>Subject</h3>
      </div>
      <div className="row mt-5">
        <SubjectForm />
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <SubjectOption />
          <SubjectList />
        </div>
      </div>
    </div>
  );
};

export default SubjectManagementPage;
