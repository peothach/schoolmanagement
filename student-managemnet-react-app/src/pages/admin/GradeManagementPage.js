import React from 'react';
import GradeList from '../../components/admin/GradeList';
import GradeOption from '../../components/admin/GradeOption';
import GradeForm from '../../components/admin/GradeForm';

const SubjectManagementPage = () => {
  return (
    <div className="subject p-5">
      <div>
        <h3>Grade</h3>
      </div>
      <GradeForm />
      <div className="row mt-5 justify-content-center">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <GradeOption />
          <GradeList />
        </div>
      </div>
    </div>
  );
};

export default SubjectManagementPage;
