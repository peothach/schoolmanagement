import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import classes from './TeacherManagement.module.css';
import TeacherAlertDialog from '../../components/admin/teacher/TeacherAlertDialog';
import TeacherConfirmDialog from '../../components/admin/teacher/TeacherConfirmDialog';
import AddTeacherForm from '../../components/admin/teacher/AddTeacherForm';
import UpdateTeacherForm from '../../components/admin/teacher/UpdateTeacherForm';
import {
  getTeachers,
  addTeacher,
  deleteTeacher,
  hideTeacherDialog,
  updateTeacher,
} from '../../store/actions/teachersAction';

// Contant for how many rows in a page that shows on a table
const ROW_PER_PAGE = 8;

/**
 * Component for showing teacher management page of the admin.
 */
const TeacherManagement = () => {
  const dispatch = useDispatch();
  const searchRef = useRef();

  // List of teachers
  const teachers = useSelector((state) => state.teachers.teachers);
  // Pageable information
  const { page, limit, totalElements } = useSelector(
    (state) => state.teachers.pageable
  );
  // Whether showing alert dialog
  const isShowDialog = useSelector((state) => state.teachers.showAlertDialog);
  // Information after done a action
  const actionStatus = useSelector((state) => state.teachers.actionStatus);

  // Whether showing add teacher form dialog
  const [showAddForm, setShowAddForm] = useState(false);
  // Show confirm detele dialog
  const [confirmDeleteDialog, setConfirmDeleteDialog] = useState(null);
  // Id of teacher to update
  const [updateTeacherId, setUpdateTeacherId] = useState(null);
  // Whether showing update teacher form dialog
  const [showUpdateForm, setShowUpdateForm] = useState(false);
  // Current page of the table
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    // Get list of teachers
    dispatch(getTeachers({ page: currentPage, pageSize: ROW_PER_PAGE }));
  }, [dispatch, currentPage]);

  /**
   * Handler function for add teacher button is clicked
   */
  const onShowAddFormHandler = () => {
    setShowAddForm(true);
  };

  /**
   * Handler for previous page button is clicked
   */
  const onPrevPageHandler = () => {
    setCurrentPage(page - 1);
  };

  /**
   * Handler for next page button is clicked
   */
  const onNextPageHandler = () => {
    setCurrentPage(page + 1);
  };

  /**
   * Handler seach button is clicked
   */
  const onSearchHandler = () => {
    const searchValue = searchRef.current.value;
    dispatch(
      getTeachers({
        page: currentPage,
        pageSize: ROW_PER_PAGE,
        fullName: searchValue,
      })
    );
  };

  /**
   * Handler for add teacher form when ok button is clicked
   * @param {Object} data - Teacher data
   */
  const onAddTeacherHandler = (data) => {
    // dispatch addTeacher action
    dispatch(addTeacher(data, { page: currentPage, pageSize: ROW_PER_PAGE }));

    // Close dialog form
    setShowAddForm(false);
  };

  /**
   * Handler for update teacher form when ok button is clicked
   * @param {Object} data - Teacher data
   */
  const onUpdateTeacherHandler = (data) => {
    dispatch(
      updateTeacher(data, { page: currentPage, pageSize: ROW_PER_PAGE })
    );

    setShowUpdateForm(false);
  };

  /**
   * Handler for clicking detail teacher button
   * @param {number} id - Teacher id
   */
  const onTeacherDetailClicked = (id) => {
    setUpdateTeacherId(id);
    setShowUpdateForm(true);
  };

  /**
   * Handler for clicking delete teacher button
   * @param {number} id - Teacher id
   */
  const onDeleteClickHandler = (id) => {
    // Updat state for showing confirm delete dialog
    setConfirmDeleteDialog({
      show: true,
      title: 'Delete teacher!',
      content: 'Are you sure to delete?',
      onClose: () => {
        setConfirmDeleteDialog((state) => ({ ...state, show: false }));
      },
      onOk: () => {
        // Dispatch delete teacher action
        dispatch(
          deleteTeacher(id, { page: currentPage, pageSize: ROW_PER_PAGE })
        );
        setConfirmDeleteDialog(false);
      },
    });
  };

  /**
   * Handler for closing add teacher form dialog
   */
  const onCloseAddFormHandler = () => {
    setShowAddForm(false);
  };

  /**
   * Handler for closing update teacher form dialog
   */
  const onCloseUpdateFormHandler = () => {
    setShowUpdateForm(false);
  };

  /**
   * Handler for closing alert dialog
   */
  const onCloseAlertDialog = () => {
    dispatch(hideTeacherDialog());
  };

  // from (min) element of current page
  const from = Math.min(totalElements, page * limit + 1);
  // to (max) element index of current
  const to = Math.min(totalElements, (page + 1) * limit);
  // pageable information of the table
  const pageableInfor = `${from}-${to} of ${totalElements}`;

  return (
    <div data-test="component-teacher-management">
      {isShowDialog && (
        <TeacherAlertDialog
          open={isShowDialog}
          onClose={onCloseAlertDialog}
          title={actionStatus.title}
          content={actionStatus.content}
          account={actionStatus.account}
          status={actionStatus.status}
          data-test="alert-dialog"
        />
      )}

      {confirmDeleteDialog && (
        <TeacherConfirmDialog
          open={confirmDeleteDialog.show}
          title={confirmDeleteDialog.title}
          content={confirmDeleteDialog.content}
          onClose={confirmDeleteDialog.onClose}
          onOk={confirmDeleteDialog.onOk}
          data-test="confirm-dialog"
        />
      )}

      {showAddForm && (
        <AddTeacherForm
          open={showAddForm}
          onClose={onCloseAddFormHandler}
          onAdd={onAddTeacherHandler}
          data-test="add-dialog"
        />
      )}

      {showUpdateForm && (
        <UpdateTeacherForm
          open={showUpdateForm}
          id={updateTeacherId}
          onClose={onCloseUpdateFormHandler}
          onUpdate={onUpdateTeacherHandler}
          data-test="update-dialog"
        />
      )}

      <section>
        <div className={classes['table-wrapper']}>
          <div className={classes['table-header']}>
            <h3>Teacher Management</h3>
            <div className={classes['table-header__actions']}>
              <button
                type="button"
                className={classes['button-add']}
                onClick={onShowAddFormHandler}
              >
                Add
              </button>
              <div className={classes['table-search__wrapper']}>
                <button type="button" onClick={onSearchHandler}>
                  <i className="fas fa-search" />
                </button>
                <input
                  ref={searchRef}
                  type="text"
                  placeholder="Input name to search"
                />
              </div>
            </div>
          </div>
          <div className={classes['table-container']}>
            <table className={classes.table} data-test="table">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Username</th>
                  <th>FullName</th>
                  <th>Gender</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th colSpan="2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {teachers.length > 0 &&
                  teachers.map((teacher, index) => (
                    <tr key={teacher.id} data-test="table-row">
                      <td>{index}</td>
                      <td>{teacher.account}</td>
                      <td>{teacher.fullName}</td>
                      <td>{teacher.gender}</td>
                      <td>{teacher.phoneNumber}</td>
                      <td>{teacher.address}</td>
                      <td className={classes['table-actions']}>
                        <button
                          type="button"
                          onClick={() => {
                            onTeacherDetailClicked(teacher.id);
                          }}
                          className={classes['table-actions__button']}
                        >
                          <i className="fas fa-eye" />
                        </button>
                      </td>
                      <td className={classes['table-actions']}>
                        <button
                          type="button"
                          onClick={() => {
                            onDeleteClickHandler(teacher.id);
                          }}
                          className={classes['table-actions__button']}
                        >
                          <i className="fas fa-trash-alt" />
                        </button>
                      </td>
                    </tr>
                  ))}
                {teachers.length === 0 && (
                  <tr data-test="no-teacher-row">
                    <td colSpan="7" className={classes['table-no-row']}>
                      No Teachers
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className={classes['table-footer']}>
            <p className={classes['table-footer__info']}>{pageableInfor}</p>
            <div className={classes['table-footer__actions']}>
              <button
                type="button"
                disabled={page === 0}
                onClick={onPrevPageHandler}
                data-test="prev-button"
              >
                <i className="fas fa-chevron-left" />
              </button>
              <button
                type="button"
                onClick={onNextPageHandler}
                disabled={
                  totalElements !== 0
                    ? page >= Math.ceil(totalElements / ROW_PER_PAGE) - 1
                    : true
                }
                data-test="next-button"
              >
                <i className="fas fa-chevron-right" />
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default TeacherManagement;
