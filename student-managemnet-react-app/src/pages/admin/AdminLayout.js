import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Layout from '../../components/common/Layout';
import Loading from '../../components/Loading';
import ClassManagementPage from './ClassManagementPage';
import GradeManagementPage from './GradeManagementPage';
import StudentsManagement from './StudentsManagement';
import SubjectManagementPage from './SubjectManagementPage';
import TeacherManagement from './TeacherManagement';

const AdminLayout = () => {
  return (
    <Layout>
      <ToastContainer autoClose={3000} />
      <Loading />
      <Switch>
        <Route path="/admin" exact>
          <Redirect to="/admin/teachers" />
        </Route>
        <Route path="/admin/teachers">
          <TeacherManagement />
        </Route>
        <Route path="/admin/subjects">
          <SubjectManagementPage />
        </Route>
        <Route path="/admin/marks">
          <GradeManagementPage />
        </Route>
        <Route path="/admin/students">
          <StudentsManagement />
        </Route>
        <Route path="/admin/classes">
          <ClassManagementPage />
        </Route>
        {/* another admin page go here */}
      </Switch>
    </Layout>
  );
};

export default AdminLayout;
