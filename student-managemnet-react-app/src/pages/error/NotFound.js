import React from 'react';

import classes from './NotFound.module.css';
import logo from '../../images/404.png';

const NotFound = () => {
  return (
    <div className={classes['not-found']}>
      <h3>Page Not Found!</h3>
      <img src={logo} alt="not found" />
    </div>
  );
};

export default NotFound;
