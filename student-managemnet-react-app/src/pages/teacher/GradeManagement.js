import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getTeacherById } from '../../store/actions/teachersAction';
import {
  getAllClassGrades,
  deleteGrade,
  updateGrade,
  hideGradeDialog,
} from '../../store/actions/gradeActions';

import GradeUpdateForm from '../../components/teacher/UpdateGradeForm';
import classes from './Teacher.module.css';
import TeacherConfirmDialog from '../../components/admin/teacher/TeacherConfirmDialog';
import TeacherAlertDialog from '../../components/admin/teacher/TeacherAlertDialog';

const GradeManagement = () => {
  const dispatch = useDispatch();
  const teacher = useSelector((state) => state.teachers.teacher);
  const grades = useSelector((state) => state.teacherGrades.grades);
  const pageable = useSelector((state) => state.teacherGrades.pageable);
  const isShowDialog = useSelector(
    (state) => state.teacherGrades.showAlertDialog
  );
  // Information after done a action
  const actionStatus = useSelector((state) => state.teacherGrades.actionStatus);

  const [classroomId, setClassroomId] = useState(null);
  const [classroomName, setClassroomName] = useState(null);
  const [deleteConfirmDialog, setDeleteConfirmDialog] = useState(null);
  const [showUpdateForm, setShowUpdateForm] = useState(false);
  const [updatedGradeId, setupdatedGradeId] = useState(null);
  const [updatedMark, setUpdatedMark] = useState(null);
  const [updatedStudentName, setupdatedStudentName] = useState(null);
  const [updatedStudentBirthdate, setUpdatedStudentBirthdate] = useState(null);

  useEffect(() => {
    dispatch(getTeacherById(2));
  }, []);

  const chooseClassroom = (id, subjectId, name) => {
    setClassroomId(id);
    setClassroomName(name);
    dispatch(
      getAllClassGrades({
        classroomId: id,
        subjectId,
        pageSize: 5,
        pageIndex: 0,
      })
    );
  };

  const onDeleteButtonClick = (id, subjectId, gradeId) => {
    setDeleteConfirmDialog({
      show: true,
      title: 'Delete teacher!',
      content: 'Are you sure to delete?',
      onClose: () => {
        setDeleteConfirmDialog((state) => ({ ...state, show: false }));
      },
      onOk: () => {
        dispatch(
          deleteGrade(gradeId, {
            classroomId: id,
            subjectId,
            pageSize: 5,
            pageIndex: grades.length === 1 ? pageable.page - 1 : pageable.page,
          })
        );
        setDeleteConfirmDialog(false);
      },
    });
  };

  const getPageContent = (id, subjectId, pageIndex) => {
    dispatch(
      getAllClassGrades({
        classroomId: id,
        subjectId,
        pageSize: 5,
        pageIndex,
      })
    );
  };

  const pageList = ((page) => {
    const result = [];
    for (let i = 0; i < page.totalPages; i++) {
      result.push(
        <li key={i} className={classes.pageItem}>
          <a
            onClick={() => {
              getPageContent(classroomId, teacher.subject.id, i);
            }}
            href
          >
            {i + 1}
          </a>
        </li>
      );
    }
    return result;
  })(pageable);

  const onClickUpdateButton = (
    gradeId,
    studentName,
    studentBirthdate,
    mark
  ) => {
    setupdatedGradeId(gradeId);
    setupdatedStudentName(studentName);
    setUpdatedStudentBirthdate(studentBirthdate);
    setUpdatedMark(mark);
    setShowUpdateForm(true);
  };

  const onCloseUpdateForm = () => {
    setShowUpdateForm(false);
  };

  const onUpdateUpdateForm = (data) => {
    dispatch(
      updateGrade([data], {
        classroomId,
        subjectId: teacher.subject.id,
        pageSize: 5,
        pageIndex: pageable.page,
      })
    );
    setShowUpdateForm(false);
  };

  const onCloseAlertDialog = () => {
    dispatch(hideGradeDialog());
  };

  return (
    <div className={classes.infoCard}>
      {isShowDialog && (
        <TeacherAlertDialog
          open={isShowDialog}
          onClose={onCloseAlertDialog}
          title={actionStatus.title}
          content={actionStatus.content}
          account={actionStatus.account}
          status={actionStatus.status}
          data-test="alert-dialog"
        />
      )}
      {deleteConfirmDialog && (
        <TeacherConfirmDialog
          open={deleteConfirmDialog.show}
          title={deleteConfirmDialog.title}
          content={deleteConfirmDialog.content}
          onClose={deleteConfirmDialog.onClose}
          onOk={deleteConfirmDialog.onOk}
          data-test="confirm-dialog"
        />
      )}
      {showUpdateForm && (
        <GradeUpdateForm
          open={showUpdateForm}
          id={updatedGradeId}
          studentName={updatedStudentName}
          studentBirthdate={updatedStudentBirthdate}
          mark={updatedMark}
          subjectName={teacher.subject.name}
          onClose={onCloseUpdateForm}
          onUpdate={onUpdateUpdateForm}
        />
      )}
      {teacher && (
        <div className={classes.pageTitle}>
          <div className={classes.titleContainer}>
            <h1 className={classes.pageTitle}>Grade management</h1>
          </div>
          <div className={classes.dropdownContainer}>
            <div className={classes.dropdown}>
              <button type="button" className={classes.dropButton}>
                Classes
              </button>
              <div className={classes.dropdownContent}>
                {teacher.classrooms &&
                  teacher.classrooms.map((element) => (
                    <a
                      key={element.id}
                      onClick={() => {
                        chooseClassroom(
                          element.id,
                          teacher.subject.id,
                          element.name
                        );
                      }}
                      href="\\#"
                    >
                      {element.name}
                    </a>
                  ))}
              </div>
            </div>
          </div>
          {classroomId && grades.length > 0 && (
            <>
              <div className={classes.bodyContainer}>
                <table className={classes.classTable}>
                  <thead>
                    <th scope="col" className={classes.idColumn}>
                      Id
                    </th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Birthdate</th>
                    <th scope="col">Class</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Grade</th>
                    <th className={classes.actionColumn} scope="col">
                      Action
                    </th>
                  </thead>
                  <tbody>
                    {grades.map((element, index) =>
                      element.active ? (
                        <tr key={element.id}>
                          <td>{index + 1}</td>
                          <td>{element.fullName}</td>
                          <td>
                            {String(element.birthdate)
                              .split('-')
                              .reduce((first, second) => {
                                return `${second}-${first}`;
                              })}
                          </td>
                          <td>{classroomName}</td>
                          <td>{teacher.subject.name}</td>
                          <td>
                            {element.grades[0].active
                              ? element.grades[0].mark
                              : ''}
                          </td>
                          <td className={classes.actionColumn}>
                            <button
                              type="button"
                              className={classes.editButton}
                              onClick={() => {
                                onClickUpdateButton(
                                  element.grades[0].id,
                                  element.fullName,
                                  String(element.birthdate)
                                    .split('-')
                                    .reduce((first, second) => {
                                      return `${second}-${first}`;
                                    }),
                                  element.grades[0].mark
                                );
                              }}
                            >
                              Edit
                            </button>
                            <button
                              type="button"
                              className={classes.deleteButton}
                              onClick={() =>
                                onDeleteButtonClick(
                                  classroomId,
                                  teacher.subject.id,
                                  element.grades[0].id
                                )
                              }
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      ) : (
                        <tr key={element.id}>
                          <td>{index + 1}</td>
                          <td>{element.fullName}</td>
                          <td>
                            {String(element.birthdate)
                              .split('-')
                              .reduce((first, second) => {
                                return `${second}-${first}`;
                              })}
                          </td>
                          <td>{classroomName}</td>
                          <td>{teacher.subject.name}</td>
                          <td>
                            {element.grades[0].active
                              ? element.grades[0].mark
                              : ''}
                          </td>
                          <td className={classes.actionColumn}>
                            <button
                              type="button"
                              className={classes.disableButton}
                            >
                              Edit
                            </button>
                            <button
                              type="button"
                              className={classes.disableButton}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      )
                    )}
                  </tbody>
                </table>
              </div>
              {pageable.totalPages > 1 && (
                <div className={classes.pageListContainer}>
                  <ul className={classes.pageList}>
                    {pageList}
                    {pageable.page + 1 < pageable.totalPages ? (
                      <li key="next" className={classes.pageNext}>
                        <a
                          onClick={() => {
                            getPageContent(
                              classroomId,
                              teacher.subject.id,
                              pageable.page + 1
                            );
                          }}
                          href
                        >
                          Next
                        </a>
                      </li>
                    ) : (
                      ''
                    )}
                  </ul>
                </div>
              )}
              <div className={classes.footerContainer}>
                <button type="button" className={classes.editAllButton}>
                  Edit All
                </button>
              </div>
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default GradeManagement;
