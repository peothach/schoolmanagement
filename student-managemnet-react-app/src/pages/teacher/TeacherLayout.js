import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from '../../components/teacher/TeacherLayout';
import TeacherProfile from './TeacherProfile';
import GradeManagement from './GradeManagement';

const TeacherLayout = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/teacher/profile">
          <TeacherProfile />
        </Route>
        <Route path="/teacher/grades">
          <GradeManagement />
        </Route>
      </Switch>
    </Layout>
  );
};

export default TeacherLayout;
