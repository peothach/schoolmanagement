import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import classes from './Teacher.module.css';

import { getTeacherById } from '../../store/actions/teachersAction';

const TeacherProfile = () => {
  const dispatch = useDispatch();
  const teacher = useSelector((state) => state.teachers.teacher);

  useEffect(() => {
    dispatch(getTeacherById(2));
  }, []);

  return (
    <div className={classes.infoCard}>
      {teacher && (
        <>
          <div className={classes.titleContainer}>
            <h1 className={classes.pageTitle}>Teacher Profile</h1>
          </div>
          <div className={classes.bodyContainer}>
            <div className={classes.teacherInfo}>
              <div className={classes.infoGroup}>
                <p className={classes.infoLabel}>Full name:</p>
                <p className={classes.infoContent}>{teacher.fullName}</p>
              </div>
              <div className={classes.infoGroup}>
                <p className={classes.infoLabel}>Birthdate:</p>
                <p className={classes.infoContent}>
                  {String(teacher.birthdate)
                    .split('-')
                    .reduce((first, second) => {
                      return `${second}-${first}`;
                    })}
                </p>
              </div>
              <div className={classes.infoGroup}>
                <p className={classes.infoLabel}>Address:</p>
                <p className={classes.infoContent}>{teacher.address}</p>
              </div>
              <div className={classes.infoGroup}>
                <p className={classes.infoLabel}>Phone Number:</p>
                <p className={classes.infoContent}>{teacher.phoneNumber}</p>
              </div>
              {teacher.subject && (
                <div className={classes.infoGroup}>
                  <p className={classes.infoLabel}>Subject:</p>
                  <p className={classes.infoContent}>{teacher.subject.name}</p>
                </div>
              )}
              <div className={classes.infoGroup}>
                <p className={classes.infoLabel}>Gender:</p>
                <p className={classes.infoContent}>{teacher.gender}</p>
              </div>
            </div>
            {teacher.classrooms && teacher.classrooms.length > 0 && (
              <div className={classes.classInfo}>
                <table className={classes.classTable}>
                  <thead>
                    <tr>
                      <th className={classes.idColumn} id="id">
                        Id
                      </th>
                      <th id="name">Class</th>
                    </tr>
                  </thead>
                  <tbody>
                    {teacher.classrooms.map((classroom, index) => (
                      <tr key={classroom.id}>
                        <td className={classes.idColumn}>{index + 1}</td>
                        <td>{classroom.name}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default TeacherProfile;
