import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import classes from './Student.module.css';
import avatarMale from '../../images/man.png';
import avatarFemale from '../../images/woman.png';
import Sidebar from '../../components/common/Sidebar';
import {
  getStudentInfo,
  getStudentMarks,
} from '../../store/actions/studentActions';

// Contant for how many rows in a page that shows on a table
const ROW_PER_PAGE = 7;

const Student = () => {
  const searchRef = useRef();
  const dispatch = useDispatch();

  const studentId = useSelector((state) => state.authen.personId);
  const student = useSelector((state) => state.student.student);
  const grades = useSelector((state) => state.student.grades);
  // Pageable information
  const { page, limit, totalElements } = useSelector(
    (state) => state.student.pageable
  );

  // Current page of the table
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    dispatch(getStudentInfo(studentId));
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      getStudentMarks(studentId, { page: currentPage, limit: ROW_PER_PAGE })
    );
  }, [dispatch, currentPage]);

  // Search handler
  const onSeachHandler = () => {
    const search = searchRef.current.value;

    dispatch(
      getStudentMarks(studentId, {
        q: search,
        page: currentPage,
        limit: ROW_PER_PAGE,
      })
    );
  };

  /**
   * Handler for previous page button is clicked
   */
  const onPrevPageHandler = () => {
    setCurrentPage(page - 1);
  };

  /**
   * Handler for next page button is clicked
   */
  const onNextPageHandler = () => {
    setCurrentPage(page + 1);
  };

  // Compute avarage grade of student
  let avarage = 0;
  if (grades.length > 0) {
    const marks = grades.map((grade) => grade.mark);
    avarage = marks.reduce((a, b) => a + b) / marks.length;
  }

  // from (min) element of current page
  const from = Math.min(totalElements, page * limit + 1);
  // to (max) element index of current
  const to = Math.min(totalElements, (page + 1) * limit);
  // pageable information of the table
  const pageableInfor = `${from}-${to} of ${totalElements}`;

  return (
    <div>
      <Sidebar />

      <main className={classes['student-main']}>
        <section className={classes['student-main__information']}>
          <div className={classes['student-avatar']}>
            <img
              src={student?.gender === 'FEMALE' ? avatarFemale : avatarMale}
              alt=""
            />
          </div>
          <div className={classes['student-info-main']}>
            <h3>{student?.fullName}</h3>
            <p>Class: {student?.classroom?.name}</p>
          </div>
          <div className={classes['student-info-second']}>
            <div className={classes['student-info-second__phone']}>
              <p>
                <span>Phone:</span> {student?.phoneNumber}
              </p>
            </div>
            <div className={classes['student-info-second__bithday']}>
              <p>
                <span>Birthday:</span> {student?.birthdate}
              </p>
            </div>
            <div className={classes['student-info-second__email']}>
              <p>
                <span>Gender:</span>{' '}
                {student?.gender === 'MALE' ? 'Male' : 'Female'}
              </p>
            </div>
            <div className={classes['student-info-second__address']}>
              <p>
                <span>Address:</span> {student?.address}
              </p>
            </div>
          </div>
        </section>
        <section className={classes['student-main__table']}>
          <div className={classes['student-table-wrapper']}>
            <div className={classes['student-table-header']}>
              <div className={classes['student-table-summary']}>
                <div className={classes['student-table-summary__gpa']}>
                  <p>
                    <span>GPA:</span> {avarage}
                  </p>
                </div>
                <div className={classes['student-table-summary__subjects']}>
                  <p>
                    <span>Total Subjects:</span> {grades.length}
                  </p>
                </div>
              </div>
              <div className={classes['student-table-header__actions']}>
                <div className={classes['student-table-search__wrapper']}>
                  <button type="button" onClick={onSeachHandler}>
                    <i className="fas fa-search" />
                  </button>
                  <input
                    type="text"
                    placeholder="Input subject to search"
                    ref={searchRef}
                  />
                </div>
              </div>
            </div>
            <div className={classes['student-table-container']}>
              <table>
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Subject</th>
                    <th>Grades</th>
                  </tr>
                </thead>
                <tbody>
                  {grades.map((grade, index) => (
                    <tr key={grade.id}>
                      <td>{index + 1}</td>
                      <td>{grade.subject.name}</td>
                      <td>{grade.mark}</td>
                    </tr>
                  ))}
                  {grades.length === 0 && (
                    <tr data-test="no-teacher-row">
                      <td colSpan="3" className={classes['table-no-row']}>
                        No Data
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            <div className={classes['student-table-footer']}>
              <p>{pageableInfor}</p>
              <div className={classes['student-table-footer__actions']}>
                <button
                  type="button"
                  disabled={page === 0}
                  onClick={onPrevPageHandler}
                >
                  <i className="fas fa-chevron-left" />
                </button>
                <button
                  type="button"
                  onClick={onNextPageHandler}
                  disabled={
                    totalElements !== 0
                      ? page >= Math.ceil(totalElements / ROW_PER_PAGE) - 1
                      : true
                  }
                >
                  <i className="fas fa-chevron-right" />
                </button>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default Student;
