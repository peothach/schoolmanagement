import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import classes from './LoginAlertDialog.module.css';

/**
 * Component for showing alert dialog when user login failed
 */
const LoginAlertDialog = (props) => {
  const { open, title, message, onClose } = props;

  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{ classes: { root: classes.dialog } }}
    >
      <DialogTitle
        classes={{
          root: classes['title-failed'],
        }}
      >
        {title}
      </DialogTitle>
      <DialogContent dividers>
        <DialogContentText classes={{ root: classes.message }}>
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={onClose}
          classes={{ root: classes['button-ok'] }}
          autoFocus
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

LoginAlertDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default LoginAlertDialog;
