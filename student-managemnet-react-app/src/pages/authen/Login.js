import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// import { Redirect } from 'react-router-dom';

import classes from './Login.module.css';
import logo from '../../images/logo.png';
import LoginAlertDialog from './LoginAlertDialog';
// import * as roleConstant from '../../constants/constants';
import {
  getRoles,
  login,
  hideAlertDialog,
} from '../../store/actions/authenAction';

const Login = () => {
  const dispatch = useDispatch();

  const roles = useSelector((state) => state.authen.roles);
  const error = useSelector((state) => state.authen.error);
  // const isLoggedIn = useSelector((state) => state.authen.isLoggedIn);
  // const roleLoggedIn = useSelector((state) => state.authen.role);

  const [selectedRole, setSelectedRole] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // Handler for dropdown roles' onChange event
  const onRoleChangeHandler = (event) => {
    setSelectedRole(event.target.value);
  };

  // Handler for username input's onChange event
  const onUsernameChangeHandler = (event) => {
    setUsername(event.target.value);
  };

  // Handler for password input's onChange event
  const onPasswordChangeHandler = (event) => {
    setPassword(event.target.value);
  };

  // Get list of roles for user choose a role to login
  useEffect(() => {
    dispatch(getRoles());
  }, [dispatch]);

  // Set default role selected
  useEffect(() => {
    if (roles.length !== 0) {
      setSelectedRole(roles[0].id);
    }
  }, [roles.length]);

  // Handler for closing alert dialog when login failed
  const handleOnAlertClose = () => {
    dispatch(hideAlertDialog());
  };

  // Handle submit form
  const onSubmitHandler = (event) => {
    event.preventDefault();

    dispatch(
      login({
        username,
        password,
        roleId: selectedRole,
      })
    );
  };

  // If user login success, redirect to page based on role
  // if (isLoggedIn) {
  //   if (roleLoggedIn === roleConstant.ADMIN) return <Redirect to="/admin" />;
  //   if (roleLoggedIn === roleConstant.TEACHER)
  //     return <Redirect to="/teacher" />;
  //   if (roleLoggedIn === roleConstant.STUDENT)
  //     return <Redirect to="/student" />;
  // }

  return (
    <div>
      {error && (
        <LoginAlertDialog
          open={!!error}
          onClose={handleOnAlertClose}
          title={error.title}
          message={error.message}
        />
      )}
      <div className={classes.login}>
        <div className={classes.login__left}>
          <form className={classes.login__form} onSubmit={onSubmitHandler}>
            <h1 className={classes.login__title}>Sign In</h1>

            <div className={classes.login__options}>
              <div className={classes.login__roles}>
                <label htmlFor="role">Your are: </label>
                <select
                  name="role"
                  value={selectedRole}
                  onChange={onRoleChangeHandler}
                >
                  {roles.map((role) => (
                    <option key={role.id} value={role.id}>
                      {role.name.toLowerCase()}
                    </option>
                  ))}
                  {roles.length === 0 && <option value="">None</option>}
                </select>
              </div>
            </div>

            <div className={classes.login__box}>
              <i className="bx bx-user login__icon" />
              <input
                type="text"
                placeholder="Username"
                className={classes.login__input}
                onChange={onUsernameChangeHandler}
              />
            </div>

            <div className={classes.login__box}>
              <i className="bx bx-lock login__icon" />
              <input
                type="password"
                placeholder="Password"
                className={classes.login__input}
                onChange={onPasswordChangeHandler}
              />
            </div>

            {/* <a href="/" className={classes.login__forgot}>
            Forgot password?
          </a> */}
            <button type="submit" className={classes.login__button}>
              Sign In
            </button>
          </form>
        </div>
        <div className={classes.login__right}>
          <img src={logo} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Login;
