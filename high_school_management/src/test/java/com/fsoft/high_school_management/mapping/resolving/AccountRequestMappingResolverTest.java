package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.AccountRequestBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AccountRequestMappingResolverTest {

    private AccountRequestMappingResolver accountRequestMappingResolver = new AccountRequestMappingResolver();

    private IAccountRepository accountRepository = Mockito.mock(IAccountRepository.class);

    private List<Account> accountList;

    @BeforeEach
    void setup(){
        accountRequestMappingResolver.setAccountRepository(accountRepository);
        accountList = new ArrayList<>();
        accountList.add(new Account(1L, "Hung", "Admin123*", true));
        accountList.add(new Account(2L, "Khoa", "Admin123*", true));
        accountList.add(new Account(3L, "Pham", "Admin123*", true));
    }

    @Test
    void given_aRawAccountRequestBody_when_callResolve_then_returnNewRawAccount() {
        Account sampleAccount = accountList.get(0);
        AccountRequestBody accountRequestBody = new AccountRequestBody(null, sampleAccount.getUsername(), sampleAccount.getPassword(), true);

        Account expectedAccount = new Account();
        Account actualAccount = accountRequestMappingResolver.resolve(accountRequestBody, (Class<Account>) new Account().getClass());
        Assertions.assertEquals(expectedAccount.getId(), actualAccount.getId());
        Assertions.assertEquals(expectedAccount.getUsername(), actualAccount.getUsername());
    }

    @Test
    void given_aAccountRequestBodyNotLinkedWithARecordInDB_when_callResolve_then_throwRecordNotFoundException() {

        Account sampleAccount = accountList.get(0);
        AccountRequestBody accountRequestBody = new AccountRequestBody(100L, sampleAccount.getUsername(), sampleAccount.getPassword(), true);
        Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Class<Account> accountType = (Class<Account>) new Account().getClass();
        Assertions.assertThrows(RecordNotFoundException.class, ()->accountRequestMappingResolver.resolve(accountRequestBody, accountType));
    }

    @Test
    void given_aAccountRequestBodyLinkedWithARecordInDB_when_callResolve_then_returnTheLinkedAccount() {

        Account sampleAccount = accountList.get(0);
        AccountRequestBody accountRequestBody = new AccountRequestBody(sampleAccount.getId(), sampleAccount.getUsername(), sampleAccount.getPassword(), true);
        Account expectedAccount = accountList.stream().filter(account -> account.getId() == accountRequestBody.getId()).findFirst().get();
        Mockito.when(accountRepository.findById(accountRequestBody.getId())).thenReturn(Optional.of(expectedAccount));
        Account actualAccount = accountRequestMappingResolver.resolve(accountRequestBody, (Class<Account>) new Account().getClass());
        Assertions.assertEquals(expectedAccount.getId(), actualAccount.getId());
        Assertions.assertEquals(expectedAccount.getUsername(), actualAccount.getUsername());
    }
}