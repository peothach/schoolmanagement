package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
class SubjectRequestMappingResolverTest {

    private SubjectRequestMappingResolver subjectRequestMappingResolver = new SubjectRequestMappingResolver();

    private ISubjectRepository subjectRepository = Mockito.mock(ISubjectRepository.class);

    private AuditableEntityHelper auditableEntityHelper = Mockito.mock(AuditableEntityHelper.class);

    private List<Subject> subjectList;

    private Account persistedAccount;

    @BeforeEach
    void setup(){
        Date today = new Date();
        subjectRequestMappingResolver.setSubjectRepository(subjectRepository);
        subjectRequestMappingResolver.setDtoToAuditableEntityHelper(auditableEntityHelper);
        persistedAccount = new Account(1L, "Hung", "Admin123*", true);
        subjectList = new ArrayList<>();
        subjectList.add(new Subject(1L, "Subject_1", persistedAccount, today , persistedAccount, today, true));
        subjectList.add(new Subject(2L, "Subject_2", persistedAccount, today , persistedAccount, today, true));
        subjectList.add(new Subject(3L, "Subject_3", persistedAccount, today , persistedAccount, today, true));
    }

    @Test
    void given_aRawSubjectRequestBody_when_callResolve_then_returnNewRawSubjectWithAuditableFields() {
        Date today = new Date();
        Subject sampleSubject = subjectList.get(0);
        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(null, sampleSubject.getName());

        Subject expectedSubject = new Subject();
        expectedSubject.setCreatedBy(persistedAccount);
        expectedSubject.setCreatedDate(today);
        expectedSubject.setLastModifiedBy(persistedAccount);
        expectedSubject.setLastModifiedDate(today);
        Mockito.when(auditableEntityHelper.setupAuditableFields(Mockito.any(Subject.class))).thenReturn(expectedSubject);

        Subject actualSubject = subjectRequestMappingResolver.resolve(subjectRequestBody, (Class<Subject>) new Subject().getClass());

        Assertions.assertEquals(expectedSubject.getId(), actualSubject.getId());
        Assertions.assertEquals(expectedSubject.getName(), actualSubject.getName());
        Assertions.assertEquals(expectedSubject.getCreatedBy().getId(), actualSubject.getCreatedBy().getId());
        Assertions.assertEquals(expectedSubject.getCreatedBy().getUsername(), actualSubject.getCreatedBy().getUsername());
    }

    @Test
    void given_aSubjectRequestBodyNotLinkedWithARecordInDB_when_callResolve_then_throwRecordNotFoundException() {

        Subject sampleSubject = subjectList.get(0);
        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(100L, sampleSubject.getName());
        Mockito.when(subjectRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Class<Subject> subjectType = (Class<Subject>) new Subject().getClass();
        Assertions.assertThrows(RecordNotFoundException.class, ()->subjectRequestMappingResolver.resolve(subjectRequestBody, subjectType));
    }

    @Test
    void given_aSubjectRequestBodyLinkedWithARecordInDB_when_callResolve_then_returnTheLinkedSubject() {

        Date today = new Date();
        Subject sampleSubject = subjectList.get(0);
        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(sampleSubject.getId(), sampleSubject.getName());

        Subject expectedSubject = subjectList.stream().filter(subject -> subject.getId().equals(subjectRequestBody.getId())).findFirst().get();
        Mockito.when(subjectRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(expectedSubject));

        expectedSubject.setCreatedBy(persistedAccount);
        expectedSubject.setCreatedDate(today);
        expectedSubject.setLastModifiedBy(persistedAccount);
        expectedSubject.setLastModifiedDate(today);
        Mockito.when(auditableEntityHelper.setupAuditableFields(Mockito.any(Subject.class))).thenReturn(expectedSubject);

        Subject actualSubject = subjectRequestMappingResolver.resolve(subjectRequestBody, (Class<Subject>) new Subject().getClass());

        Assertions.assertEquals(expectedSubject.getId(), actualSubject.getId());
        Assertions.assertEquals(expectedSubject.getName(), actualSubject.getName());
        Assertions.assertEquals(expectedSubject.getCreatedBy().getId(), actualSubject.getCreatedBy().getId());
        Assertions.assertEquals(expectedSubject.getCreatedBy().getUsername(), actualSubject.getCreatedBy().getUsername());
        Assertions.assertEquals(expectedSubject.getLastModifiedDate(), actualSubject.getLastModifiedDate());
    }
}