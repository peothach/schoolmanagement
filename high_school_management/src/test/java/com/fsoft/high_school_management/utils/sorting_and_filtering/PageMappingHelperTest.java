package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.dto.SubjectResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.mapping.IMapper;
import com.fsoft.high_school_management.mapping.SubjectMapper;
import static com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper.toPageDTO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

class PageMappingHelperTest {

    List<Subject> subjectList;

    List<SubjectResponseBody> subjectResponseBodyList;

    IMapper<Subject, SubjectRequestBody, SubjectResponseBody> mapper = Mockito.mock(SubjectMapper.class);

    @BeforeEach
    public void setup() throws ParseException {
        subjectList = new ArrayList<>();
        subjectList.add(new Subject(1L, "Subject_1"));
        subjectList.add(new Subject(2L, "Subject_2"));

        AccountResponseBody accountResponseBody = new AccountResponseBody();
        accountResponseBody.setId(1L);
        accountResponseBody.setUsername("Hung");
        accountResponseBody.setActive(true);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SubjectResponseBody subjectResponseBody_1 = new SubjectResponseBody(subjectList.get(0).getId(), subjectList.get(0).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_2 = new SubjectResponseBody(subjectList.get(1).getId(), subjectList.get(1).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        subjectResponseBodyList = Arrays.asList(subjectResponseBody_1, subjectResponseBody_2);
    }

    @Test
    void given_aPageSubject_when_callToPageDTO_then_returnPageSubjectResponseBodyDTO() {

        List<Subject> subjects = subjectList;
        Page<Subject> pagedResponse = new PageImpl(subjects);
        Mockito.when(mapper.toDTOs(subjectList)).thenReturn(subjectResponseBodyList);

        PageDTO<SubjectResponseBody> auditableDTOPageDTO = toPageDTO(pagedResponse, mapper);
        Assertions.assertEquals(2, auditableDTOPageDTO.getNumberOfElement());
        Assertions.assertEquals(1L, auditableDTOPageDTO.getContent().get(0).getId());
    }

    @Test
    void given_aEmptyPageSubject_when_callToPageDTO_then_returnEmptyPageSubjectResponseBodyDTO() {

        List<Subject> subjects = new ArrayList<>();
        Page<Subject> pagedResponse = new PageImpl(subjects);
        Mockito.when(mapper.toDTOs(subjectList)).thenReturn(new ArrayList<>());

        PageDTO<SubjectResponseBody> auditableDTOPageDTO = toPageDTO(pagedResponse, mapper);
        Assertions.assertEquals(0, auditableDTOPageDTO.getNumberOfElement());
        Assertions.assertEquals(subjects, auditableDTOPageDTO.getContent());
    }
}