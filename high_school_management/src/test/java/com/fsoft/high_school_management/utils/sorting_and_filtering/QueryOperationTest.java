package com.fsoft.high_school_management.utils.sorting_and_filtering;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class QueryOperationTest {

    @Test
    void given_anNullValue_when_callGetSimpleOperation_then_throwIllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> QueryOperation.getSimpleOperation(null));
    }

    @Test
    void given_anInvalidInputString_when_callGetSimpleOperation_then_returnNull() {
        Assertions.assertEquals(null, QueryOperation.getSimpleOperation("*"));
    }

    @Test
    void given_anValidInputString_when_callGetSimpleOperation_then_returnQueryOperation() {
        Assertions.assertEquals(QueryOperation.LIKE, QueryOperation.getSimpleOperation(":"));
    }
}