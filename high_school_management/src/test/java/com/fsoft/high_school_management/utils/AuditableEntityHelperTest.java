package com.fsoft.high_school_management.utils;

import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.repository.IAccountRepository;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AuditableEntityHelperTest {

    private IAccountRepository accountRepository = Mockito.mock(IAccountRepository.class);

    private AuditableEntityHelper<Subject> auditableEntityHelper = new AuditableEntityHelper();

    @Test
    void given_aRawSubjectEntity_when_callMapping_then_returnSubjectEntityWithAuditFields() {

        Subject sampleSubject = new Subject(1L, "Subject_1");
        Account persistedAccount = new Account(1L, "Hung", "Admin123*", true);
        auditableEntityHelper.setAccountRepository(accountRepository);
        Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(persistedAccount));
        Subject mapping = auditableEntityHelper.setupAuditableFields(sampleSubject);
        Assertions.assertEquals(sampleSubject.getId(), mapping.getId());
        Assertions.assertEquals(sampleSubject.getName(), mapping.getName());
        Assertions.assertEquals(persistedAccount.getId(), mapping.getLastModifiedBy().getId());
    }
}