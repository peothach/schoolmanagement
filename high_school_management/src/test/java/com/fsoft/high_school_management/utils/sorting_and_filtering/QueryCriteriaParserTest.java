package com.fsoft.high_school_management.utils.sorting_and_filtering;
import com.fsoft.high_school_management.dto.QueryCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class QueryCriteriaParserTest {

    @Test
    void given_anOperationStringInURL_when_callParse_then_returnListOfSearchCriteria() {

        //in the URL it would be something like
        //localhost:8080/api/subjects?filter=name:david,age>16&pageSize=10&pageIndex=0
        // This name:david,age>16 is our operationString which follow the pattern (filedName)(operation)(value?)
        String operationString = "name:David,age>20";
        QueryCriteria firstQueryCriteria = new QueryCriteria("name", QueryOperation.LIKE, "David");
        QueryCriteria secondQueryCriteria = new QueryCriteria("age", QueryOperation.GREATER_THAN, "20");

        List<QueryCriteria> queryCriteriaList = QueryCriteriaParser.parseOperationString(operationString);

        Assertions.assertEquals(firstQueryCriteria.getKey(), queryCriteriaList.get(0).getKey());
        Assertions.assertEquals(firstQueryCriteria.getOperation(), queryCriteriaList.get(0).getOperation());
        Assertions.assertEquals(firstQueryCriteria.getValue(), queryCriteriaList.get(0).getValue());
        Assertions.assertEquals(secondQueryCriteria.getKey(), queryCriteriaList.get(1).getKey());
        Assertions.assertEquals(secondQueryCriteria.getOperation(), queryCriteriaList.get(1).getOperation());
        Assertions.assertEquals(secondQueryCriteria.getValue(), queryCriteriaList.get(1).getValue());
    }

    @Test
    void given_anNullValueForOperationStringInURL_when_callParse_then_throwIllegalArgumentException() {

        //in the URL it would be something like
        //localhost:8080/api/subjects?filter=name:david,age>16&pageSize=10&pageIndex=0
        // This name:david,age>16 is our operationString which follow the pattern (filedName)(operation)(value?)
        Assertions.assertThrows(IllegalArgumentException.class, () -> QueryCriteriaParser.parseOperationString(null));
    }

    @Test
    void given_anOneInvalidValueForOperationStringInURL_when_callParse_then_returnNull() {

        //in the URL it would be something like
        //localhost:8080/api/subjects?filter=name:david,age>16&pageSize=10&pageIndex=0
        // This name:david,age>16 is our operationString which follow the pattern (filedName)(operation)(value?)
        Assertions.assertThrows(IllegalArgumentException.class, () -> QueryCriteriaParser.parseOperationString("age(20"));
    }


    @Test
    void given_anMixedInvalidAndValidValueForOperationStringInURL_when_callParse_then_returnNull() {

        //in the URL it would be something like
        //localhost:8080/api/subjects?filter=name:david,age>16&pageSize=10&pageIndex=0
        // This name:david,age>16 is our operationString which follow the pattern (filedName)(operation)(value?)
        String operationString = "name:David,age)20";
        QueryCriteria firstQueryCriteria = new QueryCriteria("name", QueryOperation.LIKE, "David");
        QueryCriteria secondQueryCriteria = new QueryCriteria("age", QueryOperation.GREATER_THAN, "20");

        List<QueryCriteria> queryCriteriaList = QueryCriteriaParser.parseOperationString(operationString);

        Assertions.assertEquals(1, queryCriteriaList.size());
        Assertions.assertEquals(firstQueryCriteria.getKey(), queryCriteriaList.get(0).getKey());
        Assertions.assertEquals(firstQueryCriteria.getOperation(), queryCriteriaList.get(0).getOperation());
        Assertions.assertEquals(firstQueryCriteria.getValue(), queryCriteriaList.get(0).getValue());
    }
}