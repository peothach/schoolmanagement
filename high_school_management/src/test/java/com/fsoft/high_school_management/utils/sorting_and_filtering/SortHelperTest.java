package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.QueryCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


class SortHelperTest {

    private static List<QueryCriteria> queryCriteriaList;

    @BeforeAll
    public static void setup(){
        QueryCriteria firstQueryCriteria = new QueryCriteria("name", QueryOperation.SORT_ASC, null);
        QueryCriteria secondQueryCriteria = new QueryCriteria("age", QueryOperation.SORT_DESC, null);
        queryCriteriaList = new ArrayList<>();
        queryCriteriaList.add(firstQueryCriteria);
        queryCriteriaList.add(secondQueryCriteria);
    }

    @Test
    void given_anNullValueForOperationStringInURL_when_callGenerateSortList_then_returnListOfSearchCriteria() {
        List<Sort> sortList = SortHelper.generateSortList(queryCriteriaList);
        Assertions.assertTrue(sortList.get(0).getOrderFor("name").getDirection().isAscending());
        Assertions.assertTrue(sortList.get(1).getOrderFor("age").getDirection().isDescending());
    }

    @Test
    void given_anNullValueForOperationStringInURL_when_callGenerateSortList_then_ThrowIllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> SortHelper.generateSortList(null));
    }

    @Test
    void given_anSortList_when_callCombineSort_then_returnListOfSearchCriteria() {
        List<Sort> sortList = Arrays.asList(Sort.by(Sort.Order.asc("name")),Sort.by(Sort.Order.desc("age")));

        Optional<Sort> combineSort = SortHelper.combineSort(sortList);

        Assertions.assertTrue(combineSort.isPresent());
        Assertions.assertTrue(combineSort.get().getOrderFor("name").isAscending());
        Assertions.assertTrue(combineSort.get().getOrderFor("age").isDescending());
    }
}