package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.dto.ClassroomResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.mapping.ClassroomMapper;
import com.fsoft.high_school_management.service.IClassroomService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ClassroomController.class)
public class ClassControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IClassroomService classroomService;

    @MockBean
    private ClassroomMapper classroomMapper;

    private List<Classroom> classroomList;

    private List<ClassroomResponseBody> classroomResponseBodyList;

    private int pageSize = 5;

    private int pageIndex = 0;

    @BeforeEach
    void setup() {
        classroomList = new ArrayList<>();
        classroomResponseBodyList = new ArrayList<>();

        for (int i = 1; i <= 6; i++) {
            Classroom classroom = new Classroom();
            classroom.setId((long) i);
            classroom.setName("10A" + i);
            classroomList.add(classroom);
        }

        AccountResponseBody accountResponseBody = new AccountResponseBody();
        accountResponseBody.setId(1L);
        accountResponseBody.setUsername("Sang");
        accountResponseBody.setActive(true);

        for (Classroom classroom : classroomList) {
            ClassroomResponseBody classroomResponseBody = new ClassroomResponseBody(
                    classroom.getId(),
                    new Date(),
                    accountResponseBody,
                    new Date(),
                    accountResponseBody,
                    true,
                    classroom.getName()
            );
            classroomResponseBodyList.add(classroomResponseBody);
        }

        PageDTO<ClassroomResponseBody> pageDTO = new PageDTO<>();
        pageDTO.setContent(classroomResponseBodyList);
        pageDTO.setCurrentPage(pageIndex);
        pageDTO.setNumberOfElement(pageSize);
    }

    @Test
    void given_sixElementInMockDatabaseAndRequestParams_when_callEndpointGetAll_then_returnPageClassroomDTOAndStatus200() throws Exception {
        ClassroomResponseBody expectedFirstItem = classroomResponseBodyList.get(0);
        List<Classroom> pagingClassroomList = classroomList.stream().skip(pageIndex * pageSize).limit(pageSize).collect(Collectors.toList());
        PageImpl<Classroom> pageSubject = new PageImpl<>(pagingClassroomList);
        List<ClassroomResponseBody> pagingClassroomResponseBodyList = classroomResponseBodyList.stream().skip(pageIndex * pageSize).limit(pageSize).collect(Collectors.toList());
        Mockito.when(classroomService.getAll(Optional.empty(),Optional.empty(),Optional.empty(),Optional.empty(),Optional.empty(),Optional.empty())).thenReturn(pageSubject);
        Mockito.when(classroomMapper.toDTOs(pageSubject.getContent())).thenReturn(pagingClassroomResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/classes")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(pagingClassroomResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", Matchers.is(expectedFirstItem.getName())));
    }
}