package com.fsoft.high_school_management.controller;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.dto.SubjectResponseBody;
import com.fsoft.high_school_management.dto.TeacherRequestBody;
import com.fsoft.high_school_management.dto.TeacherResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.common.Gender;
import com.fsoft.high_school_management.mapping.IMapper;
import com.fsoft.high_school_management.mapping.TeacherMapper;
import com.fsoft.high_school_management.service.ITeacherService;

import com.fsoft.high_school_management.utils.sorting_and_filtering.TeacherFilter;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TeacherController.class)
class TeacherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ITeacherService teacherService;

    @MockBean
    private TeacherMapper teacherMapper;

    private List<Teacher> teacherList;

    private List<TeacherResponseBody> teacherResponseBodyList;

    private int pageSize = 5;

    private int pageIndex = 0;

    @BeforeEach
    void setup() {
        final String[] names = {"Albus Dumbledore",
                "Minerva McGonagall",
                "Severus Snape",
                "Filius Flitwick",
                "Pomona Sprout",
                "Horace Slughorn"};

        Subject subject = new Subject(1L, "Magic");
        teacherResponseBodyList = new ArrayList<>();
        teacherList = new ArrayList<>();

        for (int i = 1; i <= 6; i++) {
            Account account = new Account((long) i, names[i - 1], "just a password", true);
            Teacher teacher = new Teacher();
            teacher.setId((long) i);
            teacher.setFullName(names[i - 1]);
            teacher.setAddress("just some address");
            teacher.setActive(true);
            teacher.setBirthdate(Date.valueOf("1900-04-29"));
            teacher.setGender(Gender.MALE);
            teacher.setPhoneNumber("012345678" + i);
            teacher.setSubject(subject);
            teacher.setAccount(account);
            teacherList.add(teacher);
        }

        AccountResponseBody accountResponseBody = new AccountResponseBody();
        accountResponseBody.setId(10L);
        accountResponseBody.setUsername("username");
        accountResponseBody.setActive(true);

        for (Teacher teacher : teacherList) {
            SubjectResponseBody subjectResponseBody = new SubjectResponseBody(
                    teacher.getSubject().getId(),
                    teacher.getSubject().getName(),
                    new java.util.Date(),
                    accountResponseBody,
                    new java.util.Date(),
                    accountResponseBody,
                    true
            );
            TeacherResponseBody teacherResponseBody = new TeacherResponseBody(
                    teacher.getId(),
                    new java.util.Date(),
                    accountResponseBody,
                    new java.util.Date(),
                    accountResponseBody,
                    true,
                    teacher.getFullName(),
                    teacher.getBirthdate(),
                    teacher.getAddress(),
                    teacher.getPhoneNumber(),
                    teacher.getGender(),
                    subjectResponseBody,
                    teacher.getAccount().getUsername(),
                    null
            );
            teacherResponseBodyList.add(teacherResponseBody);
        }

        PageDTO<TeacherResponseBody> pageDTO = new PageDTO<>();
        pageDTO.setContent(teacherResponseBodyList);
        pageDTO.setCurrentPage(pageIndex);
        pageDTO.setNumberOfElement(pageSize);
    }

    @Test
    void given_sixElementInMockDatabaseAndRequestParams_when_callEndpointGetAll_then_returnPageTeacherDTOAndStatus200() throws Exception {
        TeacherResponseBody expectedFirstItem = teacherResponseBodyList.get(0);
        List<Teacher> pagingTeacherList = teacherList.stream().skip((long) pageIndex * pageSize)
                .filter(Teacher::getActive)
                .limit(pageSize).collect(Collectors.toList());
        PageImpl<Teacher> teacherPage = new PageImpl<>(pagingTeacherList);
        List<TeacherResponseBody> pagingTeacherResponseBodyList = teacherResponseBodyList.stream().skip((long) pageIndex * pageSize)
                .filter(TeacherResponseBody::getActive)
                .limit(pageSize).collect(Collectors.toList());

        Mockito.when(teacherService.getAllActive(new TeacherFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty())).thenReturn(teacherPage);
        Mockito.when(teacherMapper.toDTOs(teacherPage.getContent())).thenReturn(pagingTeacherResponseBodyList);


        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/teacher");
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(pagingTeacherResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].fullName", Matchers.is(expectedFirstItem.getFullName())));

    }

    @Test
    void given_sixElementInMockDatabaseAndSortRequestParam_when_callEndpointGetAll_then_returnPageTeacherDTOAndStatus200() throws Exception {
        String sortParam = "birthdate";

        List<Teacher> sortedTeacherList = teacherList.stream()
                .filter(Teacher::getActive)
                .sorted(Comparator.comparing(Teacher::getBirthdate))
                .limit(pageSize)
                .collect(Collectors.toList());

        PageImpl<Teacher> sortedTeacherPage = new PageImpl<>(sortedTeacherList);

        List<TeacherResponseBody> sortedTeacherResponseBodyList = teacherResponseBodyList.stream()
                .filter(TeacherResponseBody::getActive)
                .sorted(Comparator.comparing(TeacherResponseBody::getBirthdate))
                .limit(pageSize)
                .collect(Collectors.toList());

        TeacherResponseBody expectedFirstItem = sortedTeacherResponseBodyList.get(0);

        Mockito.when(teacherService.getAllActive(new TeacherFilter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()), Optional.empty(), Optional.empty(), Optional.of(sortParam), Optional.empty())).thenReturn(sortedTeacherPage);
        Mockito.when(teacherMapper.toDTOs(sortedTeacherPage.getContent())).thenReturn(sortedTeacherResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/teacher?sort=" + sortParam);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(sortedTeacherResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].fullName", Matchers.is(expectedFirstItem.getFullName())));
    }

}
