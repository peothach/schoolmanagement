package com.fsoft.high_school_management.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.dto.SubjectResponseBody;
import com.fsoft.high_school_management.dto.common.Ids;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.mapping.IMapper;
import com.fsoft.high_school_management.mapping.SubjectMapperImpl;
import com.fsoft.high_school_management.service.IGenericService;
import com.fsoft.high_school_management.service.implementation.SubjectService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SubjectController.class)
class SubjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IGenericService<Subject> subjectService;

    @MockBean
    private IMapper<Subject, SubjectRequestBody, SubjectResponseBody> subjectMapper;

    private List<Subject> subjectList;

    private List<SubjectResponseBody> subjectResponseBodyList;

    private int pageSize = 5;

    private int pageIndex = 0;

    @Configuration
    public static class TestConfig {

        @Bean
        @Primary
        public IMapper<Subject, SubjectRequestBody, SubjectResponseBody> subjectMapper() {
            return new SubjectMapperImpl();
        }

        @Bean
        @Primary
        public IGenericService<Subject> subjectService() {
            return new SubjectService();
        }
    }

    @BeforeEach
    void setup() throws ParseException {

        subjectList = new ArrayList<>();
        subjectList.add(new Subject(1L, "A_Subject_1"));
        subjectList.add(new Subject(2L, "A_Subject_2"));
        subjectList.add(new Subject(3L, "A_Subject_3"));
        subjectList.add(new Subject(1L, "Subject_4"));
        subjectList.add(new Subject(2L, "A_Subject_5"));
        subjectList.add(new Subject(3L, "A_Subject_6"));

        AccountResponseBody accountResponseBody = new AccountResponseBody();
        accountResponseBody.setId(1L);
        accountResponseBody.setUsername("Hung");
        accountResponseBody.setActive(true);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SubjectResponseBody subjectResponseBody_1 = new SubjectResponseBody(subjectList.get(0).getId(), subjectList.get(0).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_2 = new SubjectResponseBody(subjectList.get(1).getId(), subjectList.get(1).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_3 = new SubjectResponseBody(subjectList.get(2).getId(), subjectList.get(2).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_4 = new SubjectResponseBody(subjectList.get(3).getId(), subjectList.get(3).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_5 = new SubjectResponseBody(subjectList.get(4).getId(), subjectList.get(4).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        SubjectResponseBody subjectResponseBody_6 = new SubjectResponseBody(subjectList.get(5).getId(), subjectList.get(5).getName(),simpleDateFormat.parse("10/10/2021"),accountResponseBody, simpleDateFormat.parse("10/10/2021"), accountResponseBody, true);
        subjectResponseBodyList = Arrays.asList(subjectResponseBody_1, subjectResponseBody_2, subjectResponseBody_3, subjectResponseBody_4, subjectResponseBody_5, subjectResponseBody_6);

        PageDTO<SubjectResponseBody> pageDTO = new PageDTO<SubjectResponseBody>();
        pageDTO.setContent(subjectResponseBodyList);
        pageDTO.setCurrentPage(pageIndex);
        pageDTO.setNumberOfElement(pageSize);
    }

    @Test
    void given_sixElementInMockDatabaseAndRequestParams_when_callEndpointGetAll_then_returnPageSubjectDTOAndStatus200() throws Exception {
        SubjectResponseBody expectedFirstItem = subjectResponseBodyList.get(0);
        List<Subject> pagingSubjectList = subjectList.stream().skip(pageIndex * pageSize).limit(pageSize).collect(Collectors.toList());
        PageImpl<Subject> pageSubject = new PageImpl<>(pagingSubjectList);
        List<SubjectResponseBody> pagingSubjectResponseBodyList = subjectResponseBodyList.stream().skip(pageIndex * pageSize).limit(pageSize).collect(Collectors.toList());
        Mockito.when(subjectService.getAll(null, null, pageSize, pageIndex)).thenReturn(pageSubject);
        Mockito.when(subjectMapper.toDTOs(pageSubject.getContent())).thenReturn(pagingSubjectResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(pagingSubjectResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_sixElementInMockDatabaseAndFilterRequestParam_when_callEndpointGetAll_then_returnPageSubjectDTOAndStatus200() throws Exception {
        String queryValue = "A_";
        String filterString = "name:" + queryValue;
        List<Subject> filteredSubjectList = subjectList.stream()
                                                        .filter(subject -> subject.getName().contains(queryValue))
                                                        .collect(Collectors.toList());
        PageImpl<Subject> pageSubject = new PageImpl<>(filteredSubjectList);
        List<SubjectResponseBody> filteredSubjectResponseBodyList = subjectResponseBodyList.stream()
                                                        .filter(subject -> subject.getName().contains(queryValue))
                                                        .collect(Collectors.toList());
        SubjectResponseBody expectedFirstItem = filteredSubjectResponseBodyList.get(0);

        Mockito.when(subjectService.getAll(filterString, null, pageSize, pageIndex)).thenReturn(pageSubject);
        Mockito.when(subjectMapper.toDTOs(pageSubject.getContent())).thenReturn(filteredSubjectResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects?filter="+filterString+"&")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(filteredSubjectResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_sixElementInMockDatabaseAndSortRequestParam_when_callEndpointGetAll_then_returnPageSubjectDTOAndStatus200() throws Exception {
        String sortString  = "name+";

        List<Subject> filteredSubjectList = subjectList.stream()
                                                        .sorted(Comparator.comparing(Subject::getName))
                                                        .limit(5)
                                                        .collect(Collectors.toList());
        PageImpl<Subject> pageSubject = new PageImpl<>(filteredSubjectList);
        List<SubjectResponseBody> filteredSubjectResponseBodyList = subjectResponseBodyList.stream()
                                                        .sorted(Comparator.comparing(SubjectResponseBody::getName))
                                                        .limit(5)
                                                        .collect(Collectors.toList());
        SubjectResponseBody expectedFirstItem = filteredSubjectResponseBodyList.get(0);

        Mockito.when(subjectService.getAll(null, sortString, pageSize, pageIndex)).thenReturn(pageSubject);
        Mockito.when(subjectMapper.toDTOs(pageSubject.getContent())).thenReturn(filteredSubjectResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects?sort="+sortString)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(filteredSubjectResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_sixElementInMockDatabaseAndFilterAndSortAndPagingRequestParams_when_callEndpointGetAll_then_returnPageSubjectDTOAndStatus200() throws Exception {
        String sortString  = "name-";
        String queryValue = "A_";
        String filterString = "name:" + queryValue;
        int size = 2;
        int page = 1;

        List<Subject> filteredSubjectList = subjectList.stream()
                                                        .filter(subject -> subject.getName().contains(queryValue))
                                                        .sorted(Comparator.comparing(Subject::getName).reversed())
                                                        .skip(page*size)
                                                        .limit(size)
                                                        .collect(Collectors.toList());
        PageImpl<Subject> pageSubject = new PageImpl<>(filteredSubjectList);
        List<SubjectResponseBody> filteredSubjectResponseBodyList = subjectResponseBodyList.stream()
                                                        .filter(subject -> subject.getName().contains(queryValue))
                                                        .sorted(Comparator.comparing(SubjectResponseBody::getName).reversed())
                                                        .skip(page*size)
                                                        .limit(size)
                                                        .collect(Collectors.toList());
        SubjectResponseBody expectedFirstItem = filteredSubjectResponseBodyList.get(0);

        Mockito.when(subjectService.getAll(filterString, sortString, size, page)).thenReturn(pageSubject);
        Mockito.when(subjectMapper.toDTOs(pageSubject.getContent())).thenReturn(filteredSubjectResponseBodyList);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects?filter="+filterString+"&sort="+sortString+"&size="+size+"&page="+page)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(filteredSubjectResponseBodyList.size())))
                .andExpect(jsonPath("$.content[0].id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_aRequiredId_when_callEndpointGetOne_then_returnTheSubjectAndStatus200() throws Exception {

        Long id = 1L;
        Mockito.when(subjectService.getOne(id)).thenReturn(subjectList.get(0));
        Mockito.when(subjectMapper.toDTO(subjectList.get(0))).thenReturn(this.subjectResponseBodyList.get(0));
        SubjectResponseBody expectedFirstItem = subjectResponseBodyList.get(0);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects/" + id)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_anInvalidId_when_callEndpointGetOne_then_returnStatus400() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/subjects/2.3")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isBadRequest());
    }

    @Test
    void given_aSubjectObject_when_callEndpointCreate_then_returnTheSavedSubjectAndStatus200() throws Exception {

        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(subjectList.get(0).getName());
        Mockito.when(subjectMapper.toEntity(Mockito.any(SubjectRequestBody.class))).thenReturn(subjectList.get(0));
        Mockito.when(subjectService.create(Mockito.any(Subject.class))).thenReturn(subjectList.get(0));
        Mockito.when(subjectMapper.toDTO(Mockito.any(Subject.class))).thenReturn(subjectResponseBodyList.get(0));
        SubjectResponseBody expectedFirstItem = subjectResponseBodyList.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/subjects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(subjectRequestBody));
        mockMvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_aSubjectObjectAndValidId_when_callEndpointUpdate_then_returnTheSavedSubjectAndStatus200() throws Exception {
        Long id = 2L;
        String changing = "Hung";
        Subject subject = subjectList.stream().filter(s -> s.getId() == id).findFirst().get();
        SubjectResponseBody subjectResponseBody = subjectResponseBodyList.stream().filter(s -> s.getId() == subject.getId()).findFirst().get();
        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(subject.getName() + changing);
        Mockito.when(subjectMapper.toEntity(Mockito.any(SubjectRequestBody.class))).thenReturn(subject);
        subject.setName(subjectRequestBody.getName());
        Mockito.when(subjectService.update(Mockito.any(Subject.class))).thenReturn(subject);
        subjectResponseBody.setName(subject.getName());
        Mockito.when(subjectMapper.toDTO(Mockito.any(Subject.class))).thenReturn(subjectResponseBody);
        SubjectResponseBody expectedFirstItem = subjectResponseBody;

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder request = MockMvcRequestBuilders
                .put("/api/subjects/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(subjectRequestBody));
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", Matchers.is(expectedFirstItem.getId().intValue())))
                .andExpect(jsonPath("name", Matchers.is(expectedFirstItem.getName())));
    }

    @Test
    void given_aSubjectAndInvalidId_when_callEndpointUpdate_then_returnStatus400() throws Exception {
        Long id = 100L;
        Mockito.when(subjectMapper.toEntity(Mockito.any(SubjectRequestBody.class))).thenThrow(RecordNotFoundException.class);
        SubjectRequestBody subjectRequestBody = new SubjectRequestBody(subjectList.get(0).getName() + "Hung");

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder request = MockMvcRequestBuilders
                .put("/api/subjects/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(subjectRequestBody));
        mockMvc.perform(request)
                .andExpect(status().isNotFound());
    }

    @Test
    void given_anId_when_callEndpointDelete_then_returnStatus200() throws Exception {
        long id = 1L;
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {

                Optional<Subject> requiredSubject = subjectList.stream().filter(subject -> subject.getId() == id).findFirst();
                if(requiredSubject.isPresent()){
                    requiredSubject.get().setActive(false);
                    return true;
                }
                else return false;
            }
        }).when(subjectService).deleteById(Mockito.anyLong());

        RequestBuilder request = MockMvcRequestBuilders
                .delete("/api/subjects/"+ id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    void given_anInvalidId_when_callEndpointDelete_then_returnStatus404() throws Exception {
        long id = 100L;
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {

                Optional<Subject> requiredSubject = subjectList.stream().filter(subject -> subject.getId() == id).findFirst();
                if(requiredSubject.isPresent()){
                    requiredSubject.get().setActive(false);
                    return true;
                }
                else throw new RecordNotFoundException();
            }
        }).when(subjectService).deleteById(Mockito.anyLong());

        RequestBuilder request = MockMvcRequestBuilders
                .delete("/api/subjects/"+ id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request)
                .andExpect(status().isNotFound());
    }

    @Test
    void given_anMultipleValidId_when_callEndpointMultipleDelete_then_returnStatus200() throws Exception {
        Long[] idArray = {1L, 2L, 3L};
        Ids ids = new Ids(idArray);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                for (Long id : ids.getIdArray()) {
                    Optional<Subject> requiredSubject = subjectList.stream().filter(subject -> subject.getId() == id).findFirst();
                    if (requiredSubject.isPresent())
                        requiredSubject.get().setActive(false);
                    else
                        return false;
                }
                return true;
            }
        }).when(subjectService).deleteByIds(Mockito.any(Long[].class));

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder request = MockMvcRequestBuilders
                .delete("/api/subjects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ids));
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    void given_anMultipleInvalidId_when_callEndpointMultipleDelete_then_returnStatus400() throws Exception {
        Long[] idArray = {1L, 2L, 300L};
        Ids ids = new Ids(idArray);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                for (Long id : ids.getIdArray()) {
                    Optional<Subject> requiredSubject = subjectList.stream().filter(subject -> subject.getId() == id).findFirst();
                    if (requiredSubject.isPresent())
                        requiredSubject.get().setActive(false);
                    else
                        throw new RecordNotFoundException();
                }
                return true;
            }
        }).when(subjectService).deleteByIds(Mockito.any(Long[].class));

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder request = MockMvcRequestBuilders
                .delete("/api/subjects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ids));
        mockMvc.perform(request)
                .andExpect(status().isNotFound());
    }
}
