package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGenericRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class SubjectServiceTest {

    private static final SubjectService subjectService = new SubjectService();

    private static final IGenericRepository<Subject> subjectRepository = Mockito.mock(ISubjectRepository.class);

    private static List<Subject> subjectList;

    @BeforeAll
    public static void setup(){
        subjectList = new ArrayList<>();
        subjectList.add(new Subject(1L, "A_Subject_1"));
        subjectList.add(new Subject(2L, "A_Subject_2"));
        subjectList.add(new Subject(3L, "A_Subject_3"));
        subjectList.add(new Subject(4L, "Subject_4"));
        subjectList.add(new Subject(5L, "A_Subject_5"));
        subjectList.add(new Subject(6L, "A_Subject_6"));
        subjectService.setRepository(subjectRepository);
    }

    @Test
    void given_aFakeSixElementAndFilterString_when_callGetAll_then_returnRequiredSubject() throws Exception {
        String queryValue = "A_";
        String filterString = "name:" + queryValue;
        int size = 5;
        int page = 0;

        List<Subject> filteredSubjectList = subjectList.stream()
                .filter(subject -> subject.getName().contains(queryValue))
                .skip(page*size)
                .limit(size)
                .collect(Collectors.toList());

        PageImpl<Subject> expectedPage = new PageImpl<Subject>(filteredSubjectList);
        Mockito.when(subjectRepository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(expectedPage);
        Page<Subject> actualPage = subjectService.getAll(filterString, null, size, page);

        Subject expectedFirstItem = expectedPage.getContent().get(0);
        Subject actualFirstItem = actualPage.getContent().get(0);
        Assertions.assertEquals(expectedFirstItem.getId(), actualFirstItem.getId());
        Assertions.assertEquals(expectedFirstItem.getName(), actualFirstItem.getName());
    }

    @Test
    void given_aFakeSixElementAndSortString_when_callGetAll_then_returnRequiredSubject() throws Exception {
        String sortString  = "name-";
        int size = 5;
        int page = 0;

        List<Subject> filteredSubjectList = subjectList.stream()
                .sorted(Comparator.comparing(Subject::getName).reversed())
                .skip(page*size)
                .limit(size)
                .collect(Collectors.toList());

        PageImpl<Subject> expectedPage = new PageImpl<Subject>(filteredSubjectList);
        Mockito.when(subjectRepository.findAll(Mockito.any(Pageable.class))).thenReturn(expectedPage);
        Page<Subject> actualPage = subjectService.getAll(null, sortString, size, page);

        Subject expectedFirstItem = expectedPage.getContent().get(0);
        Subject actualFirstItem = actualPage.getContent().get(0);
        Assertions.assertEquals(expectedFirstItem.getId(), actualFirstItem.getId());
        Assertions.assertEquals(expectedFirstItem.getName(), actualFirstItem.getName());
    }

    @Test
    void given_aFakeSixElementAndFilterAndSortString_when_callGetAll_then_returnRequiredSubject() throws Exception {
        String sortString  = "name+";
        String queryValue = "A_";
        String filterString = "name:" + queryValue;
        int size = 5;
        int page = 0;

        List<Subject> filteredSubjectList = subjectList.stream()
                .filter(subject -> subject.getName().contains(queryValue))
                .sorted(Comparator.comparing(Subject::getName))
                .skip(page*size)
                .limit(size)
                .collect(Collectors.toList());

        PageImpl<Subject> expectedPage = new PageImpl<Subject>(filteredSubjectList);
        Mockito.when(subjectRepository.findAll(Mockito.any(Specification.class), Mockito.any(Pageable.class))).thenReturn(expectedPage);
        Page<Subject> actualPage = subjectService.getAll(filterString, sortString, size, page);

        Subject expectedFirstItem = expectedPage.getContent().get(0);
        Subject actualFirstItem = actualPage.getContent().get(0);
        Assertions.assertEquals(expectedFirstItem.getId(), actualFirstItem.getId());
        Assertions.assertEquals(expectedFirstItem.getName(), actualFirstItem.getName());
    }

    @Test
    void given_aFakeSixElementAndInvalidSortString_when_callGetAll_then_throwIllegalArgumentException() throws Exception {
        String sortString  = "name(";
        int size = 5;
        int page = 0;
        Assertions.assertThrows(IllegalArgumentException.class, () -> subjectService.getAll(null, sortString, size, page));
    }

    @Test
    void given_aFakeSixElementAndInvalidFilterString_when_callGetAll_then_throwIllegalArgumentException() throws Exception {
        String filterString  = "name(Hung";
        int size = 5;
        int page = 0;
        Assertions.assertThrows(IllegalArgumentException.class, () -> subjectService.getAll(filterString, null, size, page));
    }

    @Test
    void given_aValidId_when_callGetASubject_then_returnRequiredSubject(){
        Subject inputSubject = subjectList.get(0);
        Mockito.when(subjectRepository.findByIdAndActiveIsTrue(inputSubject.getId())).thenReturn(Optional.of(inputSubject));

        Subject outputSubject = subjectService.getOne(inputSubject.getId());

        Assertions.assertEquals(inputSubject.getId(), outputSubject.getId());
        Assertions.assertEquals(inputSubject.getName(), outputSubject.getName());
    }

    @Test
    void given_aInvalidId_when_callGetASubject_then_throwRecordNotFoundException(){
        long randomId = Mockito.anyLong();
        Mockito.when(subjectRepository.findByIdAndActiveIsTrue(randomId)).thenReturn(Optional.empty());
        Assertions.assertThrows(RecordNotFoundException.class, () -> subjectService.getOne(randomId));
    }

    @Test
    void given_aSubjectObject_when_callSaveASubject_then_returnSavedSubject(){
        Subject inputSubject = subjectList.get(0);
        Mockito.when(subjectRepository.save(inputSubject)).thenReturn(inputSubject);

        Subject outputSubject = subjectService.create(inputSubject);

        Assertions.assertEquals(inputSubject.getId(), outputSubject.getId());
        Assertions.assertEquals(inputSubject.getName(), outputSubject.getName());
    }

    @Test
    void given_aSubjectObject_when_callUpdateSubject_then_returnUpdateSubject(){
        Subject inputSubject = subjectList.get(0);
        inputSubject.setName("Graph");
        Mockito.when(subjectRepository.save(inputSubject)).thenReturn(inputSubject);

        Subject outputSubject = subjectService.update(inputSubject);

        Assertions.assertEquals(inputSubject.getId(), outputSubject.getId());
        Assertions.assertEquals("Graph", outputSubject.getName());
    }

    @Test
    void given_id_when_callDeletedSubject_then_changeActiveFieldToFalse(){
        Subject inputSubject = subjectList.get(0);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                inputSubject.setActive(false);
                return null;
            }
        }).when(subjectRepository).deactivateById(inputSubject.getId());

        subjectService.deleteById(inputSubject.getId());
        Mockito.when(subjectRepository.findById(inputSubject.getId())).thenReturn(Optional.of(inputSubject));
        Subject actualSubject = subjectService.getOne(inputSubject.getId());
        Assertions.assertEquals(false, actualSubject.getActive());
    }
}