package com.fsoft.high_school_management;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HighSchoolManagementApplicationTests {

	@Test
	void contextLoads() {
		// This a temporary assertion, we will change it later when perform intergartion test
		Assertions.assertTrue(true);
	}
}
