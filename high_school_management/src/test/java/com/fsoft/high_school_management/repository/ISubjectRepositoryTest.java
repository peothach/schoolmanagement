package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.Subject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class ISubjectRepositoryTest {

    @Autowired
    private ISubjectRepository subjectRepository;

    @Autowired
    private IAccountRepository accountRepository;

    private static Account account;

    @BeforeEach
    public void setup(){
        account = new Account(1L, "Hung", "Admin123*", true);
        accountRepository.save(account);
    }

    @Test
    void given_id_when_callDeactivateById_then_changeActiveFieldToFalse() {
        Date today = new Date();

        subjectRepository.save(new Subject("Subject_1", account, today, account, today, true));

        subjectRepository.deactivateById(1L);

        Assertions.assertEquals(Optional.empty(), subjectRepository.findByIdAndActiveIsTrue(1L));
    }
}