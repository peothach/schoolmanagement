INSERT INTO public.accounts(active, password, username)
VALUES
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','admin'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','HungDQ'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','SangNX'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','ThachNH'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','LuongDV'),
    (TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','LinhNV'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','VuTV'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','DuyVV'),
	(TRUE,'$2a$10$9y6WAausHYtvwMUOHj9qQuLQTgaZn.Bz04w2EG6pSAn1w9wvUtPXi','TuDV');

INSERT INTO public.roles(name, active, created_date, last_modified_date, created_by_account_id, last_modified_by_account_id)
VALUES
	( 'ADMINISTRATOR',TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 'TEACHER', TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 'STUDENT', TRUE, '2021-07-15', '2021-07-15', 1, 1);

INSERT INTO public.Accounts_Roles(account_id, role_id, active, created_date, last_modified_date, created_by_account_id, last_modified_by_account_id)
VALUES
	( 1, 1,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 2, 2,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 3, 2,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 4, 2,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 5, 2,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 6, 3,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 7, 3,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 8, 3,TRUE, '2021-07-15', '2021-07-15', 1, 1),
	( 9, 3,TRUE, '2021-07-15', '2021-07-15', 1, 1);

INSERT INTO public.subjects(
	 active, created_date, last_modified_date, name, created_by_account_id, last_modified_by_account_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', 'Math', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'Physics', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'Chemistry', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'Literature', 1, 1);

INSERT INTO public.classrooms(
	active, created_date, last_modified_date, name, created_by_account_id, last_modified_by_account_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', '10A1', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', '10A2', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', '11B1', 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', '12C2', 1, 1);

INSERT INTO public.teachers(
	active, created_date, last_modified_date, address, birthdate, full_name, gender, phone_number, created_by_account_id, last_modified_by_account_id, account_id, subject_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1999-10-10','Doan Quoc Hung', 'MALE', '0999999888', 1, 1, 2, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1994-10-10','Nguyen Xuan Sang', 'MALE', '0991999888', 1, 1, 3, 2),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1997-10-10','Nguyen Huu Thach', 'MALE', '0939999888', 1, 1, 4, 3),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1994-10-10','Le Van Luong', 'MALE', '0989999888', 1, 1, 5, 4);

INSERT INTO public.students(
	active, created_date, last_modified_date, address, birthdate, full_name, gender, phone_number, created_by_account_id, last_modified_by_account_id, account_id, classroom_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1999-10-10','Nguyen Van Linh', 'MALE', '0993999888', 1, 1, 6, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1994-10-10','Tran Van Vu', 'MALE', '0991999188', 1, 1, 7, 1),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1997-10-10','Vo Van Duy', 'MALE', '0939999848', 1, 1, 8, 3),
	( TRUE, '2021-07-15', '2021-07-15', 'VN', '1994-10-10','Dinh Van Tu', 'MALE', '0989999988', 1, 1, 9, 4);

INSERT INTO public.grades(
	active, created_date, last_modified_date, mark, created_by_account_id, last_modified_by_account_id, student_id, subject_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', 8, 1, 1, 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', 7, 1, 1, 1, 2),
	( TRUE, '2021-07-15', '2021-07-15', 6, 1, 1, 1, 3),
	( TRUE, '2021-07-15', '2021-07-15', 9, 1, 1, 1, 4),
	( TRUE, '2021-07-15', '2021-07-15', 8, 1, 1, 2, 4),
	( TRUE, '2021-07-15', '2021-07-15', 7, 1, 1, 2, 3),
	( TRUE, '2021-07-15', '2021-07-15', 6, 1, 1, 2, 2),
	( TRUE, '2021-07-15', '2021-07-15', 9, 1, 1, 2, 1),
	( TRUE, '2021-07-15', '2021-07-15', 8, 1, 1, 3, 1),
	( TRUE, '2021-07-15', '2021-07-15', 7, 1, 1, 3, 2),
	( TRUE, '2021-07-15', '2021-07-15', 6, 1, 1, 3, 3),
	( TRUE, '2021-07-15', '2021-07-15', 9, 1, 1, 3, 4),
	( TRUE, '2021-07-15', '2021-07-15', 8, 1, 1, 4, 1),
	( TRUE, '2021-07-15', '2021-07-15', 7, 1, 1, 4, 2),
	( TRUE, '2021-07-15', '2021-07-15', 6, 1, 1, 4, 3),
	( TRUE, '2021-07-15', '2021-07-15', 9, 1, 1, 4, 4);

INSERT INTO public.teachers_classrooms(
	active, created_date, last_modified_date, created_by_account_id, last_modified_by_account_id, classroom_id, teacher_id)
	VALUES
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 1, 1),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 2, 1),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 3, 2),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 4, 3),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 2, 3),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 4, 4),
	( TRUE, '2021-07-15', '2021-07-15', 1, 1, 1, 4);