package com.fsoft.high_school_management.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsoft.high_school_management.dto.APIError;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class ExceptionHandlerFilter extends OncePerRequestFilter {

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            APIError apiError = new APIError(HttpStatus.UNAUTHORIZED, e.getClass().getSimpleName(), e.getMessage());
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(convertObjectToJson(apiError));
        }
    }

    private String convertObjectToJson(Object object) throws JsonProcessingException {
        Assert.notNull(object, "Object is null and can not be used to convert to JSON");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}