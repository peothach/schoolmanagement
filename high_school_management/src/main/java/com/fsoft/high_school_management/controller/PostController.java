package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.PostDto;
import com.fsoft.high_school_management.entity.Post;
import com.fsoft.high_school_management.service.implementation.PostService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/posts")
@Api(value = "Post API")
public class PostController {
    @Autowired
    PostService postService;

    @PostMapping
    public ResponseEntity<PostDto> save(@RequestBody PostDto dto) {
        Post saved = postService.save(dto);
        PostDto responseDto = new PostDto(saved);
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDto> find(@PathVariable("id") Long id) {
        Post post = postService.find(id);
        PostDto responseDto = new PostDto(post);
        return ResponseEntity.ok(responseDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PostDto> saved(@PathVariable("id") Long id, @RequestBody PostDto dto) {
        Post saved = postService.save(id, dto);
        PostDto responseDto = new PostDto(saved);
        return ResponseEntity.ok(responseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        postService.delete(id);
        return ResponseEntity.ok("Post is deleted successfully");
    }
}
