package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.TeacherRequestBody;
import com.fsoft.high_school_management.dto.TeacherResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.common.Gender;
import com.fsoft.high_school_management.mapping.TeacherMapper;
import com.fsoft.high_school_management.service.ITeacherService;
import com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper;
import com.fsoft.high_school_management.utils.sorting_and_filtering.TeacherFilter;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/teachers")
@Api(value = "Teacher APIs")
public class TeacherController extends GenericController<Teacher, TeacherRequestBody, TeacherResponseBody> {

    @Autowired
    ITeacherService teacherService;

    @Autowired
    TeacherMapper teacherMapper;


    @PostMapping(value = "/classrooms/{id}", produces = "application/json")
    public ResponseEntity<Object> setClassrooms(@PathVariable Long id,
                                                @Valid @RequestBody List<Long> classroomIds) {
        teacherService.setClassroom(id, classroomIds);
        return this.get(id);
    }

    @GetMapping
    public ResponseEntity<Object> getAllActive(@RequestParam(value = "fullName", required = false) Optional<String> fullname,
                                         @RequestParam(value = "subjectId", required = false) Optional<Long> subjectId,
                                         @RequestParam(value = "address", required = false) Optional<String> address,
                                         @RequestParam(value = "phoneNumber", required = false) Optional<String> phoneNumber,
                                         @RequestParam(value = "gender", required = false) Optional<Gender> gender,
                                         @RequestParam(value = "pageSize", required = false) Optional<Integer> pageSize,
                                         @RequestParam(value = "pageIndex", required = false) Optional<Integer> pageIndex,
                                         @RequestParam(value = "sort", required = false) Optional<String> sort,
                                         @RequestParam(value = "order", required = false) Optional<String> order) {


        Page<Teacher> teachers = teacherService.getAllActive(new TeacherFilter(fullname,subjectId,address,phoneNumber,gender), pageSize, pageIndex, sort, order);
        PageDTO<TeacherResponseBody> response = PageMappingHelper.toPageDTO(teachers, teacherMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
