package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.ForumPageDTO;
import com.fsoft.high_school_management.dto.SubforumDto;
import com.fsoft.high_school_management.dto.ThreadDto;
import com.fsoft.high_school_management.entity.Subforum;
import com.fsoft.high_school_management.entity.Thread;
import com.fsoft.high_school_management.service.ISubforumService;
import com.fsoft.high_school_management.service.IThreadService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/subforums")
@Api(value = "Subforum API")
public class SubforumController {

    @Autowired
    ISubforumService subforumService;
    @Autowired
    IThreadService threadService;

    @PostMapping
    public ResponseEntity<SubforumDto> save(@RequestBody SubforumDto dto) {
        Subforum saved = subforumService.save(dto);
        SubforumDto responseDto = new SubforumDto(saved);
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubforumDto> findOne(@PathVariable("id") Long id) {
        Subforum subforum = subforumService.find(id);
        SubforumDto responseDto = new SubforumDto(subforum);
        responseDto = subforumService.countThread(responseDto);
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping
    public ResponseEntity<List<SubforumDto>> find() {
        List<Subforum> subforums = subforumService.find();
        List<SubforumDto> responseDtos = subforums.stream().map(SubforumDto::new).collect(Collectors.toList());
        responseDtos = responseDtos.stream().map(subforumService::countThread).collect(Collectors.toList());
        return ResponseEntity.ok(responseDtos);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SubforumDto> save(@RequestBody SubforumDto dto, @PathVariable("id") Long id) {
        Subforum saved = subforumService.save(dto, id);
        SubforumDto responseDto = new SubforumDto(saved);
        return ResponseEntity.ok(responseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        subforumService.delete(id);
        return ResponseEntity.ok("Subforum is successfully deleted.");
    }

    @GetMapping("/{id}/threads")
    public ResponseEntity<ForumPageDTO<ThreadDto>> findAllThreads(@PathVariable("id") Long subforumId,
                                                                  @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                  @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                                                                  @RequestParam(value = "count", required = false, defaultValue = "false") boolean count) {
        Page<Thread> threadPage = threadService.findBySubforum(subforumId, page, size);
        List<ThreadDto> dtoList = threadPage.getContent().stream().map(ThreadDto::new).collect(Collectors.toList());
        if(count){
            dtoList = dtoList.stream().map(threadService::countPost).collect(Collectors.toList());
        }
        ForumPageDTO<ThreadDto> responseDto = new ForumPageDTO<>(dtoList,
                threadPage.getSize(),
                threadPage.getNumber(),
                threadPage.getTotalPages(),
                threadPage.getTotalElements());
        return ResponseEntity.ok(responseDto);
    }
}
