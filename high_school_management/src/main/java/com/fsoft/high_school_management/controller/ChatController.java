package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.ChatMessageRequestBody;
import com.fsoft.high_school_management.dto.ChatMessageResponseBody;
import com.fsoft.high_school_management.dto.Participants;
import com.fsoft.high_school_management.entity.ChatMessage;
import com.fsoft.high_school_management.entity.Conversation;
import com.fsoft.high_school_management.service.IAccountService;
import com.fsoft.high_school_management.service.IActiveSessionManager;
import com.fsoft.high_school_management.service.implementation.ChatMessageService;
import com.fsoft.high_school_management.service.implementation.ConversationService;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ChatController {

    private SimpMessagingTemplate messagingTemplate;

    private ChatMessageService chatMessageService;

    private ConversationService conversationService;

    private IAccountService accountService;

    private IActiveSessionManager activeSessionManager;

    @Autowired
    public ChatController(SimpMessagingTemplate messagingTemplate, ChatMessageService chatMessageService, ConversationService conversationService, IAccountService accountService, IActiveSessionManager activeSessionManager) {
        this.messagingTemplate = messagingTemplate;
        this.chatMessageService = chatMessageService;
        this.conversationService = conversationService;
        this.accountService = accountService;
        this.activeSessionManager = activeSessionManager;
    }

    @MessageMapping("/chat")
    public void processChatMessage(@Payload ChatMessageRequestBody chatMessageRequestBody) {
        ChatMessage saved = chatMessageService.save(chatMessageRequestBody);
        ChatMessageResponseBody messageBody = new ChatMessageResponseBody(saved);
        messagingTemplate.convertAndSendToUser(chatMessageRequestBody.getConversationId().toString(), "/queue/messages", messageBody);
    }

    @MessageMapping("/conversations")
    public void processCreatingConversation(@Payload Participants participants){
        Conversation conversation = conversationService.create(participants);
        Set<String> usernames = new HashSet<>(participants.getUsernames());
        usernames = conversationService.getAllParticipants(usernames);
        for(String username : usernames) {
            String path = "/user/" + username + "/new/conversation";
            messagingTemplate.convertAndSend(path, conversation);
        }
    }

    @MessageExceptionHandler
    public void handleException(Throwable exception) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String currentUsername = userDetails.getUsername();
        String path = "/user/" + currentUsername + "/new/conversation";
        messagingTemplate.convertAndSend(path, exception.getMessage());
    }

    @GetMapping("/api/conversations")
    public ResponseEntity<List<Conversation>> findConversationsOfCurrentUser(){
        return ResponseEntity.ok(conversationService.findAllByAccount());
    }

    @GetMapping("/api/conversations/{id}/messages")
    public ResponseEntity<List<ChatMessageResponseBody>> findMessagesInAConversation(@PathVariable("id") Long conversationId) {
        List<ChatMessage> chatMessages = chatMessageService.findAllByConversation(conversationId);
        List<ChatMessageResponseBody> chatMessageBodies = chatMessages.stream().map(ChatMessageResponseBody::new).collect(Collectors.toList());
        return ResponseEntity.ok(chatMessageBodies);
    }

    @GetMapping("/api/conversations/recipient")
    public ResponseEntity<Conversation> findConversationBasedOnRecipientUsername(@RequestParam("username") String username) {
        Conversation conversation = conversationService.findByRecipientUsername(username);
        return ResponseEntity.ok(conversation);
    }

    @GetMapping("/api/supporters")
    public ResponseEntity<List<String>> findSupporters(){
        return ResponseEntity.ok(accountService.getAllAdministratorNames());
    }

    @GetMapping("/api/active")
    public ResponseEntity<Set<String>> findActiveUsers(){
        Set<String> activeUsers = activeSessionManager.getAll();
        return ResponseEntity.ok(activeUsers);
    }
}
