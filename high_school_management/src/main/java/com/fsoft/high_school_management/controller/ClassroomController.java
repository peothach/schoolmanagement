package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.ClassroomRequestBody;
import com.fsoft.high_school_management.dto.ClassroomResponseBody;
import com.fsoft.high_school_management.dto.StudentGradeResponseBody;
import com.fsoft.high_school_management.dto.StudentResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.mapping.ClassroomMapper;
import com.fsoft.high_school_management.mapping.StudentGradeMapper;
import com.fsoft.high_school_management.mapping.StudentMapper;
import com.fsoft.high_school_management.service.IClassroomService;
import com.fsoft.high_school_management.service.IExcelGeneratingService;
import com.fsoft.high_school_management.service.IGradeService;
import com.fsoft.high_school_management.service.IStudentService;
import com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper;
import io.swagger.annotations.Api;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("api/classrooms")
@Api(value = "Class")
public class ClassroomController extends GenericController<Classroom, ClassroomRequestBody, ClassroomResponseBody> {

    private IClassroomService classroomService;

    private ClassroomMapper classroomMapper;

    private IStudentService studentService;

    private StudentMapper studentMapper;

    private StudentGradeMapper studentGradeMapper;

    private IGradeService gradeService;

    private IExcelGeneratingService excelGeneratingService;

    @Autowired
    public void setGradeService(IGradeService gradeService) {
        this.gradeService = gradeService;
    }

    @Autowired
    public void setStudentGradeMapper(StudentGradeMapper studentGradeMapper) {
        this.studentGradeMapper = studentGradeMapper;
    }

    @Autowired
    public void setStudentService(IStudentService studentService) {
        this.studentService = studentService;
    }

    @Autowired
    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Autowired
    public void setClassroomService(IClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    @Autowired
    public void setClassroomMapper(ClassroomMapper classroomMapper) {
        this.classroomMapper = classroomMapper;
    }

    @Autowired
    public void setExcelGeneratingService(IExcelGeneratingService excelGeneratingService) {
        this.excelGeneratingService = excelGeneratingService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ClassroomResponseBody>> getAllClassroom() {
        List<Classroom> classroomList = this.classroomService.getListOfAllClassrooms();
        List<ClassroomResponseBody> classroomResponseBodyList = classroomMapper.toDTOs(classroomList);
        return new ResponseEntity<>(classroomResponseBodyList, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<PageDTO<ClassroomResponseBody>> getAllClasses(
            @RequestParam(value = "q", required = false) Optional<String> keyword,
            @RequestParam(value = "active", required = false) Optional<Boolean> active,
            @RequestParam(value = "_order", required = false) Optional<String> order,
            @RequestParam(value = "_sort", required = false) Optional<String> sort,
            @RequestParam(value = "limit", required = false) Optional<Integer> pageSize,
            @RequestParam(value = "page", required = false) Optional<Integer> pageIndex) {
        Page<Classroom> classrooms = classroomService.getAll(keyword, active, order, sort, pageSize, pageIndex);
        PageDTO<ClassroomResponseBody> response = PageMappingHelper.toPageDTO(classrooms, classroomMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * API get all domain model objects
     * HTTP GET
     * Method = getAll - this method is absolutely acted as an endpoint for all filtering, ordering, paging when we wanna fetch data
     * In the URL it would be something like localhost:8080/api/person?filter=name:david,age>16&sort=age-&pageSize=10&pageIndex=0
     * This    name:david,age>16     is our operationString which follow the pattern (filedName)(operation)(value?)
     *
     * @return ResponseEntity<>(Student, HttpStatus.OK) stand for an response http package
     * @requestParam q String full-text search keyword
     * @requestParam active Boolean filter based on active field
     * @requestParam _sort String field we decide for sorting
     * @requestParam _order String the order is asc or desc
     * @requestParam page String is the page size default 0
     * @requestParam size String is the page index default 5
     */
    @GetMapping("/{classroomId}/students")
    public ResponseEntity<PageDTO<StudentResponseBody>> getAll(
            @PathVariable("classroomId") long classroomId,
            @RequestParam(value = "q", required = false) String search,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {
        Boolean direction = true;
        if (sort != null && order != null && order.toLowerCase(Locale.ROOT).equals("desc")) {
            direction = false;
        }
        Page<Student> pageSubject = studentService.getAll(classroomId, search, active, sort, direction, pageSize, pageIndex);
        PageDTO<StudentResponseBody> response = PageMappingHelper.toPageDTO(pageSubject, studentMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{classroomId}/subjects/{subjectId}/grades")
    public ResponseEntity<PageDTO<StudentGradeResponseBody>> getAllGradesInAClassroomForOnlyOneSubject(
            @PathVariable("classroomId") Long classroomId,
            @PathVariable("subjectId") Long subjectId,
            @RequestParam(value = "q", required = false) String searchKeyword,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {
        boolean direction = !"desc".equalsIgnoreCase(order);
        Page<Grade> grades = gradeService.getAllGradesOfAClassroomInOnlyOneSubject(classroomId, subjectId, searchKeyword, active, sort, direction, pageSize, pageIndex);
        PageDTO<StudentGradeResponseBody> response = PageMappingHelper.toPageDTO(grades, studentGradeMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{classroomId}/grades")
    public ResponseEntity<PageDTO<StudentGradeResponseBody>> getAllGradesInAClassroomForALlSubjects(
            @PathVariable("classroomId") Long classroomId,
            @RequestParam(value = "q", required = false) String searchKeyword,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {
        boolean direction = !"desc".equalsIgnoreCase(order);
        Page<Grade> grades = gradeService.getAllGradesOfAClassroomInAllSubjects(classroomId, searchKeyword, active, sort, direction, pageSize, pageIndex);
        PageDTO<StudentGradeResponseBody> response = PageMappingHelper.toPageDTO(grades, studentGradeMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/{classroomId}/grades/export/excel")
    public ResponseEntity<InputStreamResource> exportAllGradesOfAllSubjectsInAClassroomToExcel(
            @PathVariable("classroomId") Long classroomId) throws IOException {

        Page<Grade> grades = gradeService.getAllGradesOfAClassroomInAllSubjects(classroomId, null, true, null, true, Integer.MAX_VALUE, 0);
        List<StudentGradeResponseBody> studentGradeResponseBodies = studentGradeMapper.toDTOs(grades.getContent());
        ByteArrayInputStream excelFile = excelFile = excelGeneratingService.exportGradesOfALlSubjectInAClassroomToExcel(studentGradeResponseBodies, classroomId);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment;filename=report.xlsx");
        headers.add("Content-Type", "application/vnd.ms-excel");
        //headers.add("Content-Disposition", "attachment; filename=grades_classroom_"+classroomId+".xlsx");
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(excelFile));
    }

    @GetMapping("/{classroomId}/subjects/{subjectId}/grades/export/excel")
    public ResponseEntity<InputStreamResource> exportAllGradesOfASubjectsInAClassroomToExcel(
            @PathVariable("classroomId") Long classroomId,
            @PathVariable("subjectId") Long subjectId) throws IOException {
        Page<Grade> grades = gradeService.getAllGradesOfAClassroomInOnlyOneSubject(classroomId, subjectId, null, true, null, true, Integer.MAX_VALUE, 0);
        List<StudentGradeResponseBody> studentGradeResponseBodies = studentGradeMapper.toDTOs(grades.getContent());
        ByteArrayInputStream excelFile = excelGeneratingService.exportGradesOfASubjectInAClassroomToExcel(studentGradeResponseBodies, classroomId, subjectId);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/vnd.ms-excel");
        headers.add("Content-Disposition", "attachment;filename=grades_subject_"+subjectId+"_classroom_"+classroomId+".xlsx");
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(excelFile));
    }
}
