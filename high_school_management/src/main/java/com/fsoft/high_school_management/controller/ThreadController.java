package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.ForumPageDTO;
import com.fsoft.high_school_management.dto.PostDto;
import com.fsoft.high_school_management.dto.ThreadDto;
import com.fsoft.high_school_management.entity.Post;
import com.fsoft.high_school_management.entity.Thread;
import com.fsoft.high_school_management.service.IPostService;
import com.fsoft.high_school_management.service.IThreadService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/threads")
@Api(value = "Thread API")
public class ThreadController {

    @Autowired
    IThreadService threadService;
    @Autowired
    IPostService postService;

    @GetMapping("/{id}")
    public ResponseEntity<ThreadDto> find(@PathVariable("id") Long id, @RequestParam(value = "count", required = false, defaultValue = "false") boolean count) {
        Thread thread = threadService.find(id);
        ThreadDto responseDto = new ThreadDto(thread);
        if (count){
            responseDto = threadService.countPost(responseDto);
        }
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping
    public ResponseEntity<ForumPageDTO<ThreadDto>> find(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                        @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                                                        @RequestParam(value = "count", required = false, defaultValue = "false") boolean count) {
        Page<Thread> threadPage = threadService.find(page, size);
        List<ThreadDto> dtoList = threadPage.getContent().stream().map(ThreadDto::new).collect(Collectors.toList());
        if (count){
            dtoList = dtoList.stream().map(threadService::countPost).collect(Collectors.toList());
        }
        ForumPageDTO<ThreadDto> responseDto = new ForumPageDTO<>(dtoList,
                threadPage.getSize(),
                threadPage.getNumber(),
                threadPage.getTotalPages(),
                threadPage.getTotalElements());
        return ResponseEntity.ok(responseDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ThreadDto> save(@PathVariable("id") Long id, ThreadDto dto) {
        Thread saved = threadService.save(id, dto);
        ThreadDto responseDto = new ThreadDto(saved);
        return ResponseEntity.ok(responseDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id){
        threadService.delete(id);
        return ResponseEntity.ok(("Thread is successfully deleted."));
    }

    @GetMapping("/{id}/posts")
    public ResponseEntity<ForumPageDTO<PostDto>> findAllPosts(@PathVariable("id") Long threadId,
                                                              @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                              @RequestParam(value = "size", required = false, defaultValue = "10") int size){
        Page<Post> postPage = postService.findByThread(threadId, page, size);
        List<PostDto> dtoList = postPage.getContent().stream().map(PostDto::new).collect(Collectors.toList());
        ForumPageDTO<PostDto> responseDto = new ForumPageDTO<>(dtoList,
                postPage.getSize(),
                postPage.getNumber(),
                postPage.getTotalPages(),
                postPage.getTotalElements());
        return ResponseEntity.ok(responseDto);
    }
}
