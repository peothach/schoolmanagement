package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.GradeRequestBody;
import com.fsoft.high_school_management.dto.GradeResponseBody;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.mapping.GradeMapper;
import com.fsoft.high_school_management.service.IGradeService;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/grades")
@Api(value = "Grade API")
public class GradeController extends GenericController<Grade, GradeRequestBody, GradeResponseBody>{

    private IGradeService gradeService;

    private GradeMapper gradeMapper;

    @Autowired
    public void setGradeService(IGradeService gradeService) {
        this.gradeService = gradeService;
    }

    @Autowired
    public void setGradeMapper(GradeMapper gradeMapper) {
        this.gradeMapper = gradeMapper;
    }

    @PutMapping()
    public ResponseEntity<List<GradeResponseBody>> update(@Valid @RequestBody GradeRequestBody[] requestDTOs) {
        List<GradeResponseBody> gradeResponseBodyList = new ArrayList<>();
        for(GradeRequestBody requestDTO : requestDTOs){
            Assert.notNull(requestDTO.getId(), "Missing grade id to perform request");
            Grade entity = gradeMapper.toEntity(requestDTO);
            entity = gradeService.update(entity);
            GradeResponseBody responseDTO = gradeMapper.toDTO(entity);
            gradeResponseBodyList.add(responseDTO);
        }
        return ResponseEntity.ok(gradeResponseBodyList);
    }
}
