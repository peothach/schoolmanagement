package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.GradeResponseBody;
import com.fsoft.high_school_management.dto.StudentRequestBody;
import com.fsoft.high_school_management.dto.StudentResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.mapping.GradeMapper;
import com.fsoft.high_school_management.mapping.StudentMapper;
import com.fsoft.high_school_management.service.IGradeService;
import com.fsoft.high_school_management.service.IStudentService;
import com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper;
import io.swagger.annotations.Api;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/students")
@Api(value = "Student API")
public class StudentController extends GenericController<Student, StudentRequestBody, StudentResponseBody>{

    private IStudentService studentService;

    private IGradeService gradeService;

    private StudentMapper studentMapper;

    private GradeMapper gradeMapper;

    @Autowired
    public void setStudentService(IStudentService studentService) {
        this.studentService = studentService;
    }

    @Autowired
    public void setGradeService(IGradeService gradeService) {
        this.gradeService = gradeService;
    }

    @Autowired
    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Autowired
    public void setGradeMapper(GradeMapper gradeMapper) {
        this.gradeMapper = gradeMapper;
    }

    /**
     * API get all domain model objects
     * HTTP GET
     * Method = getAll - this method is absolutely acted as an endpoint for all filtering, ordering, paging when we wanna fetch data
     * In the URL it would be something like localhost:8080/api/person?filter=name:david,age>16&sort=age-&pageSize=10&pageIndex=0
     * This    name:david,age>16     is our operationString which follow the pattern (filedName)(operation)(value?)
     *
     * @requestParam q String full-text search keyword
     * @requestParam active Boolean filter based on active field
     * @requestParam _sort String field we decide for sorting
     * @requestParam _order String the order is asc or desc
     * @requestParam page String is the page size default 0
     * @requestParam size String is the page index default 5
     * @return ResponseEntity<>(Student, HttpStatus.OK) stand for an response http package
     */
    @GetMapping()
    public ResponseEntity<PageDTO<StudentResponseBody>> getAll(
            @RequestParam(value = "q", required = false) String searchKey,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {

        Boolean direction = true;
        if(sort != null && order != null && order.toLowerCase(Locale.ROOT).equals("desc")) {
            direction = false;
        }
        Page<Student> pageSubject = studentService.getAll(searchKey, active, sort, direction, pageSize,pageIndex);
        PageDTO<StudentResponseBody> response = PageMappingHelper.toPageDTO(pageSubject, studentMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{studentId}/grades")
    public ResponseEntity<PageDTO<GradeResponseBody>> getGrades(
            @PathVariable("studentId") long studentId,
            @RequestParam(value = "q", required = false) String searchKey,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {

        Boolean direction = true;
        if(sort != null && order != null && order.toLowerCase(Locale.ROOT).equals("desc")) {
            direction = false;
        }
        Page<Grade> gradePage = gradeService.getGradesOfAStudent(studentId, searchKey, sort, direction, pageSize, pageIndex);
        PageDTO<GradeResponseBody> response = PageMappingHelper.toPageDTO(gradePage, gradeMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/grades")
    public ResponseEntity<String> deleteGrades(@PathVariable(name = "id") Long id) {
        this.gradeService.deactivateAllGradesOfAStudent(id);
        return ResponseEntity.ok("Delete all grades of this student successfully");
    }
}
