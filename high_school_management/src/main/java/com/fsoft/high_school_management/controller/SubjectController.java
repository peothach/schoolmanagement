package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.dto.SubjectResponseBody;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.mapping.SubjectMapper;
import com.fsoft.high_school_management.service.ISubjectService;
import com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping("/api/subjects")
@Api(value = "Subject API")
public class SubjectController extends GenericController<Subject, SubjectRequestBody, SubjectResponseBody>{

    private ISubjectService subjectService;

    private SubjectMapper subjectMapper;

    @Autowired
    public void setSubjectService(ISubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @Autowired
    public void setSubjectMapper(SubjectMapper subjectMapper) {
        this.subjectMapper = subjectMapper;
    }

    /**
     * API get all domain model objects
     * HTTP GET
     * Method = getAll - this method is absolutely acted as an endpoint for all filtering, ordering, paging when we wanna fetch data
     * In the URL it would be something like localhost:8080/api/person?filter=name:david,age>16&sort=age-&pageSize=10&pageIndex=0
     * This    name:david,age>16     is our operationString which follow the pattern (filedName)(operation)(value?)
     *
     * @requestParam q String full-text search keyword
     * @requestParam active Boolean filter based on active field
     * @requestParam _sort String field we decide for sorting
     * @requestParam _order String the order is asc or desc
     * @requestParam page String is the page size default 0
     * @requestParam size String is the page index default 5
     * @return ResponseEntity<>(Student, HttpStatus.OK) stand for an response http package
     */
    @GetMapping()
    public ResponseEntity<PageDTO<SubjectResponseBody>> getAll(
            @RequestParam(value = "q", required = false) String searchKey,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "_order", required = false) String order,
            @RequestParam(value = "_sort", required = false) String sort,
            @RequestParam(value = "limit", required = false, defaultValue = "5") Integer pageSize,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageIndex) {
        Boolean direction = true;
        if(sort != null && order != null && order.toLowerCase(Locale.ROOT).equals("desc")) {
            direction = false;
        }
        Page<Subject> pageSubject = subjectService.getAll(searchKey, active, sort, direction, pageSize,pageIndex);
        PageDTO<SubjectResponseBody> response = PageMappingHelper.toPageDTO(pageSubject, subjectMapper);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

