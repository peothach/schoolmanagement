package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import com.fsoft.high_school_management.dto.common.AuditableDTO;
import com.fsoft.high_school_management.dto.common.Ids;
import com.fsoft.high_school_management.mapping.IMapper;
import com.fsoft.high_school_management.service.IGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

import javax.validation.Valid;

import static com.fsoft.high_school_management.utils.sorting_and_filtering.PageMappingHelper.toPageDTO;

/**
 * @param <E> stands for Entity
 * @param <I> stands for In(RequestBodyDTO)
 * @param <O> stands for Out(ResponseBodyDTO)
 *            <p>
 *            AbstractDTO contains only one Id field, all the RequestBodyDTO extends it
 *            AuditableDTO contains many fields which need to describe how to audit a record, all the ResponseBodyDTO extends it
 */
public abstract class GenericController<E, I extends AbstractDTO, O extends AuditableDTO> {

    public static final String EXCEPTION = "Exception: ";

    private IGenericService<E> service;

    private IMapper<E, I, O> mapper;

    @Autowired
    public void setService(IGenericService<E> service) {
        this.service = service;
    }

    @Autowired
    public void setMapper(IMapper<E, I, O> mapper) {
        this.mapper = mapper;
    }

    /**
     * API for getting domain model object
     * HTTP GET
     * Method = get
     * This endpoint is used to get one record in database based on the id of an domain model object
     *
     * @return ResponseEntity<Object> stand for an response http package
     * @pathVariable id Long
     */
    @GetMapping("/{id}")
    public ResponseEntity<Object> get(@PathVariable(name = "id") Long id) {
        E entity = service.getOne(id);
        O dto = mapper.toDTO(entity);
        return ResponseEntity.ok(dto);
    }

    /**
     * API for creating domain model object
     * HTTP POST
     * Method = get
     * This endpoint is used to create a record in database based on an domain model object
     *
     * @return ResponseEntity<Object> stand for an response http package
     * @requestBody dto
     */
    @PostMapping
    public ResponseEntity<O> create(@Valid @RequestBody I requestDTO) {
        requestDTO.setId(null);
        E entity = mapper.toEntity(requestDTO);
        entity = service.create(entity);
        O responseDTO = mapper.toDTO(entity);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    /**
     * API for updating domain model object
     * HTTP PUT
     * Method = update
     * This endpoint is used to update one record in database based on the id and the fields of an domain model object we wanna update
     *
     * @return ResponseEntity<Object> stand for an response http package
     * @requestBody requestDTO
     * @pathVariabl id
     */
    @PutMapping("/{id}")
    public ResponseEntity<O> update(@Valid @RequestBody I requestDTO, @PathVariable(name = "id") Long id) {
        requestDTO.setId(id);
        E entity = mapper.toEntity(requestDTO);
        entity = service.update(entity);
        O responseDTO = mapper.toDTO(entity);
        return ResponseEntity.ok(responseDTO);
    }

    /**
     * API for deleting one record
     * HTTP DELETE
     * Method = delete
     * This endpoint is used to deactivate one record in database based on the id
     *
     * @return ResponseEntity<Object>
     * @pathVariable id Long
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") Long id) {
        service.deleteById(id);
        return ResponseEntity.ok("Delete this one successfully");
    }

    /**
     * API for deleting multiple records
     * HTTP DELETE
     * Method = delete
     * This endpoint is used to deactivate multiple record in database based on the id
     *
     * @return ResponseEntity<Object>
     * @requestBody ids Ids
     */
    @DeleteMapping
    public ResponseEntity<String> delete(@Valid @RequestBody Ids ids) {
        service.deleteByIds(ids.getIdArray());
        return ResponseEntity.ok("Delete all successfully");
    }
}
