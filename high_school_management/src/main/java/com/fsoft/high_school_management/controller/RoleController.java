package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.RoleResponseBody;
import com.fsoft.high_school_management.entity.Role;
import com.fsoft.high_school_management.service.IRoleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
@Api(value = "Role API")
public class RoleController {

    private IRoleService roleService;

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<RoleResponseBody>> getAllClassroom(){
        List<Role> roleList = this.roleService.getAllRoles();
        List<RoleResponseBody> roleResponseBodyList = new ArrayList<>();
        for(Role role: roleList) {
            RoleResponseBody roleResponseBody = new RoleResponseBody();
            roleResponseBody.setId(role.getId());
            roleResponseBody.setName(role.getName());
            roleResponseBodyList.add(roleResponseBody);
        }
        return new ResponseEntity<>(roleResponseBodyList, HttpStatus.OK);
    }
}
