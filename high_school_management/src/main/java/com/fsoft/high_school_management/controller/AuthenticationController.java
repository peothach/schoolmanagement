package com.fsoft.high_school_management.controller;

import com.fsoft.high_school_management.dto.AuthenticationResponse;
import com.fsoft.high_school_management.dto.ChangingPasswordRequestBody;
import com.fsoft.high_school_management.dto.CheckingOldPasswordRequestBody;
import com.fsoft.high_school_management.dto.Credential;
import com.fsoft.high_school_management.entity.Role;
import com.fsoft.high_school_management.exception.RoleRequestIsNotMatch;
import com.fsoft.high_school_management.service.IAccountService;
import com.fsoft.high_school_management.service.IRoleService;
import com.fsoft.high_school_management.utils.JwtUtils;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
public class AuthenticationController {

    private AuthenticationManager authenticationManager;

    private JwtUtils jwtTokenUtil;

    private IRoleService roleService;

    private IAccountService accountService;

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setJwtTokenUtil(JwtUtils jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setAccountService(IAccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody @Valid Credential credential) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(credential.getUsername(), credential.getPassword()));

        UserDetails userDetail = (UserDetails) authenticate.getPrincipal();
        Role tempRole = null;
        if (credential.getRoleId() != null) {
            Role role = this.roleService.getOne(credential.getRoleId());
            Optional<? extends GrantedAuthority> first = userDetail.getAuthorities().stream().filter(grantedAuthority -> grantedAuthority.getAuthority().equals(role.getName())).findFirst();
            if (!first.isPresent()) {
                throw new RoleRequestIsNotMatch();
            }
            tempRole = role;
        }
        Long personId = this.roleService.getPersonId(tempRole, userDetail.getUsername());
        String jwt = jwtTokenUtil.generateAccessToken(userDetail);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(userDetail, personId, jwt);
        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .body(authenticationResponse);
    }

    @PutMapping("/accounts/changePassword")
    public ResponseEntity<String> login(@RequestBody @Valid ChangingPasswordRequestBody changingPasswordRequestBody) {
        this.accountService.changePassword(changingPasswordRequestBody);
        return ResponseEntity.ok().body("Changing password is done successfully");
    }

    @PostMapping("/accounts/checkPassword")
    public ResponseEntity<String> checkOldPassword(@RequestBody @Valid CheckingOldPasswordRequestBody dto) {
        if(this.accountService.checkPassword(dto)){
            return ResponseEntity.ok().body("Match this old password");
        }
        return ResponseEntity.badRequest().body("Dont match with old password");
    }
}
