package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.ClassroomRequestBody;
import com.fsoft.high_school_management.dto.ClassroomResponseBody;
import com.fsoft.high_school_management.dto.TeacherResponseBody;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.TeachersClassrooms;
import com.fsoft.high_school_management.mapping.IMapper;
import com.fsoft.high_school_management.repository.ITeachersClassroomsRepository;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeacherResponseMappingResolver {
    @Autowired
    private ITeachersClassroomsRepository teachersClassroomsRepository;

    @Autowired
    private IMapper<Classroom, ClassroomRequestBody, ClassroomResponseBody> mapper;

    @ObjectFactory
    public TeacherResponseBody resolve(Teacher teacher, @TargetType Class<TeacherResponseBody> type) {
        List<TeachersClassrooms> teacherClassroomsList = teachersClassroomsRepository.findAllByTeacherIdAndActiveTrue(teacher.getId());
        List<ClassroomResponseBody> classroomList = teacherClassroomsList.stream()
                .map(TeachersClassrooms::getClassroom)
                .map(classroom -> mapper.toDTO(classroom))
                .collect(Collectors.toList());
        TeacherResponseBody teacherResponseBody = new TeacherResponseBody();
        teacherResponseBody.setClassrooms(classroomList);
        return teacherResponseBody;
    }
}
