package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.ClassroomRequestBody;
import com.fsoft.high_school_management.dto.ClassroomResponseBody;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.mapping.resolving.ClassroomRequestMappingResolver;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS , uses = {ClassroomRequestMappingResolver.class, AccountMapper.class})
public interface ClassroomMapper extends IMapper<Classroom, ClassroomRequestBody, ClassroomResponseBody> {

}
