package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.TeacherRequestBody;
import com.fsoft.high_school_management.dto.TeacherResponseBody;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.mapping.resolving.TeacherRequestMappingResolver;
import com.fsoft.high_school_management.mapping.resolving.TeacherResponseMappingResolver;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {
        TeacherRequestMappingResolver.class,
        AccountMapper.class,
        LongToSubjectMapper.class,
        TeacherResponseMappingResolver.class,
        StringToAccountMapper.class,
        SubjectMapper.class
})
public interface TeacherMapper extends IMapper<Teacher, TeacherRequestBody, TeacherResponseBody> {

}
