package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import com.fsoft.high_school_management.dto.common.AuditableDTO;
import org.mapstruct.Mapping;

import java.util.List;

/**
 *
 * @param <E> stands for Entity
 * @param <I> stands for In(RequestBodyDTO)
 * @param <O> stands for Out(ResponseBodyDTO)
 *
 * AbstractDTO contains only one Id field, all the RequestBodyDTO extends it
 * AuditableDTO contains many fields which need to describe how to audit a record, all the ResponseBodyDTO extends it
 *
 */
public interface IMapper<E, I extends AbstractDTO, O extends AuditableDTO>{
    E toEntity(I dto);

    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    O toDTO(E entity);

    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "lastModifiedBy", ignore = true)
    List<O> toDTOs(List<E> entities);
}
