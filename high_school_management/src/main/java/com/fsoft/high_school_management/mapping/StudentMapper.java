package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.StudentRequestBody;
import com.fsoft.high_school_management.dto.StudentResponseBody;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.mapping.resolving.StudentRequestMappingResolver;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS ,uses = {StudentRequestMappingResolver.class, AccountMapper.class, ClassroomMapper.class, StringToAccountMapper.class})
public interface StudentMapper extends IMapper<Student, StudentRequestBody, StudentResponseBody> {

}

