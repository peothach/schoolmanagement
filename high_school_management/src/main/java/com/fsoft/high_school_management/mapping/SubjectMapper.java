package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.dto.SubjectResponseBody;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.mapping.resolving.SubjectRequestMappingResolver;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS ,uses = {SubjectRequestMappingResolver.class, AccountMapper.class})
public interface SubjectMapper extends IMapper<Subject, SubjectRequestBody, SubjectResponseBody> {

}
