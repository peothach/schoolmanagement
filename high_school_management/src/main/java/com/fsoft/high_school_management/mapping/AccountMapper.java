package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.AccountRequestBody;
import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.mapping.resolving.AccountRequestMappingResolver;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, uses = {AccountRequestMappingResolver.class})
public interface AccountMapper extends IMapper<Account, AccountRequestBody, AccountResponseBody>{
}
