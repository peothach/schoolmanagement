package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.SubjectRequestBody;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SubjectRequestMappingResolver {

    private ISubjectRepository subjectRepository;

    private AuditableEntityHelper<Subject> auditableEntityHelper;

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Subject> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @ObjectFactory
    public Subject resolve(SubjectRequestBody dto, @TargetType Class<Subject> type) {
        boolean isAccessible = (dto != null && dto.getId() != null);
        if(isAccessible) {
            return subjectRepository.findById(dto.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException("No record found for operate mapping");
            });
        }
        return auditableEntityHelper.setupAuditableFields(new Subject());
    }
}
