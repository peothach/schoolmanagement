package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public class LongToSubjectMapper {
    @Autowired
    ISubjectRepository subjectRepository;

    Subject toSubject(Long id) {
        if (id == null){
            return null;
        }
        return subjectRepository.findById(id).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
    }

    Long toLong(Subject subject){
        return subject.getId();
    }

    List<Long> toLongs(List<Subject> subjectList){
        return subjectList.stream().map(Subject::getId).collect(Collectors.toList());
    }

}

