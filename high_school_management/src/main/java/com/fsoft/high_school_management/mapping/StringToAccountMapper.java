package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.utils.Constants;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public class StringToAccountMapper {
    @Autowired
    IAccountRepository accountRepository;

    Account toAccount(String username){
        return accountRepository.findByUsernameIgnoreCase(username).orElseThrow(() -> {
            throw new RecordNotFoundException(Constants.USERNAME_NOT_EXIST_MESSAGE);
        });
    }

    String toString(Account account){
        return account.getUsername();
    }

    List<String> toStrings(List<Account> accountList){
        return accountList.stream().map(Account::getUsername).collect(Collectors.toList());
    }
}
