package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.TeacherRequestBody;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.utils.AccountGenerator;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherRequestMappingResolver {
    private ITeacherRepository teacherRepository;

    private AuditableEntityHelper<Teacher> auditableEntityHelper;

    private AccountGenerator accountGenerator;

    @Autowired
    public void setTeacherRepository(ITeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Teacher> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @Autowired
    public void setAccountGenerator(AccountGenerator accountGenerator) {
        this.accountGenerator = accountGenerator;
    }

    @ObjectFactory
    public Teacher resolve(TeacherRequestBody teacherRequestBody, @TargetType Class<Teacher> type) {
        if (teacherRequestBody != null && teacherRequestBody.getId() != null) {
            return teacherRepository.findById(teacherRequestBody.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException();
            });
        }
        Teacher teacher = new Teacher();
        if (teacherRequestBody != null){
            teacher.setAccount(accountGenerator.generate(teacherRequestBody));
            teacher.setActive(Boolean.TRUE);
        }
        return auditableEntityHelper.setupAuditableFields(teacher);
    }
}
