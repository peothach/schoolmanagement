package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.ClassroomRequestBody;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IClassroomRepository;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassroomRequestMappingResolver {

    private IClassroomRepository classroomRepository;

    private AuditableEntityHelper<Classroom> auditableEntityHelper;

    @Autowired
    public void setClassroomRepository(IClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }
    
    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Classroom> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @ObjectFactory
    public Classroom resolve(ClassroomRequestBody classroomRequestBody, @TargetType Class<Classroom> type){
        if(classroomRequestBody != null && classroomRequestBody.getId() != null){
            return classroomRepository.findById(classroomRequestBody.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException();
            });
        }
        return auditableEntityHelper.setupAuditableFields(new Classroom());
    }
}
