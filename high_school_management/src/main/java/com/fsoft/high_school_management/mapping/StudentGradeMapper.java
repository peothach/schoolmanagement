package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.GradeRequestBody;
import com.fsoft.high_school_management.dto.LiteGradeResponseBody;
import com.fsoft.high_school_management.dto.StudentGradeResponseBody;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGradeRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class StudentGradeMapper implements IMapper<Grade, GradeRequestBody, StudentGradeResponseBody>{

    private IGradeRepository gradeRepository;

    private IStudentRepository studentRepository;

    private ISubjectRepository subjectRepository;

    private AuditableEntityHelper<Grade> auditableEntityHelper;

    @Autowired
    public void setGradeRepository(IGradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public Grade toEntity(GradeRequestBody gradeRequestBody) {
        if(gradeRequestBody != null && gradeRequestBody.getId() != null){
            Grade grade = gradeRepository.findById(gradeRequestBody.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException();
            });
            grade.setMark(gradeRequestBody.getMark());
            return auditableEntityHelper.setupAuditableFields(grade);
        }

        Assert.notNull(gradeRequestBody.getStudentId(), "missing student id for creating grade");
        Assert.notNull(gradeRequestBody.getSubjectId(), "missing subject id for creating grade");
        Student student = this.studentRepository.findById(gradeRequestBody.getStudentId()).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
        Subject subject = this.subjectRepository.findById(gradeRequestBody.getSubjectId()).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
        Grade grade = new Grade();
        grade.setStudent(student);
        grade.setSubject(subject);
        grade.setMark(gradeRequestBody.getMark());
        return auditableEntityHelper.setupAuditableFields(grade);
    }

    public StudentGradeResponseBody toDTO(Grade grade) {
        LiteGradeResponseBody gradeResponseBody = new LiteGradeResponseBody(grade.getId(), grade.getSubject().getName(), grade.getMark(), grade.getActive());

        return new StudentGradeResponseBody(
                grade.getStudent().getId(), grade.getStudent().getFullName(), grade.getStudent().getBirthdate(), grade.getStudent().getActive(), Stream.of(gradeResponseBody).collect(Collectors.toList()));
    }

    @Override
    public List<StudentGradeResponseBody> toDTOs(List<Grade> gradeList){
        List<StudentGradeResponseBody> studentGradeResponseBodyList = new ArrayList<>();
        List<Long> studentIdList = gradeList.stream()
                                            .map(grade -> grade.getStudent().getId())
                                            .distinct()
                                            .collect(Collectors.toList());
        for (Long studentId : studentIdList){
            List<LiteGradeResponseBody> gradeEachStudent = gradeList.stream()
                                                                    .filter(grade -> grade.getStudent().getId().equals(studentId))
                                                                    .map(LiteGradeResponseBody::new)
                                                                    .collect(Collectors.toList());
            Student currentStudent = gradeList.stream()
                                              .filter(grade -> grade.getStudent().getId().equals(studentId))
                                              .findFirst().orElseThrow(() -> {
                                                  throw new RecordNotFoundException("Impossible error - Not found student Id ");
                                              })
                                              .getStudent();
            StudentGradeResponseBody studentGradeResponseBody =  new StudentGradeResponseBody();
            studentGradeResponseBody.setStudentId(studentId);
            studentGradeResponseBody.setFullName(currentStudent.getFullName());
            studentGradeResponseBody.setBirthdate(currentStudent.getBirthdate());
            studentGradeResponseBody.setActive(currentStudent.getActive());
            studentGradeResponseBody.setGradeResponseBodyList(gradeEachStudent);            
            studentGradeResponseBodyList.add(studentGradeResponseBody);
        }
        return studentGradeResponseBodyList;
    }
}
