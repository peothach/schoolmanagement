package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.AccountRequestBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRequestMappingResolver {

    private IAccountRepository accountRepository;

    @Autowired
    public void setAccountRepository(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @ObjectFactory
    public Account resolve(AccountRequestBody dto, @TargetType Class<Account> type) {
        boolean isAccessible = (dto != null && dto.getId() != null);
        if(isAccessible) {
            return accountRepository.findById(dto.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException("No record found for operate mapping");
            });
        }
        return new Account();
    }
}