package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.GradeRequestBody;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGradeRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class GradeRequestMappingResolver {

    private IGradeRepository gradeRepository;

    private IStudentRepository studentRepository;

    private ISubjectRepository subjectRepository;

    private AuditableEntityHelper<Grade> auditableEntityHelper;

    @Autowired
    public void setGradeRepository(IGradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Grade> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @ObjectFactory
    public Grade resolve(GradeRequestBody gradeRequestBody, @TargetType Class<Grade> type){
        if(gradeRequestBody != null && gradeRequestBody.getId() != null){
            Grade grade = gradeRepository.findById(gradeRequestBody.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException();
            });
            grade.setMark(gradeRequestBody.getMark());
            return auditableEntityHelper.setupAuditableFields(grade);
        }

        Assert.notNull(gradeRequestBody.getStudentId(), "missing student id for creating grade");
        Assert.notNull(gradeRequestBody.getSubjectId(), "missing subject id for creating grade");
        Student student = this.studentRepository.findById(gradeRequestBody.getStudentId()).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
        Subject subject = this.subjectRepository.findById(gradeRequestBody.getSubjectId()).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
        Grade grade = new Grade();
        grade.setStudent(student);
        grade.setSubject(subject);
        grade.setMark(gradeRequestBody.getMark());
        return auditableEntityHelper.setupAuditableFields(grade);
    }
}
