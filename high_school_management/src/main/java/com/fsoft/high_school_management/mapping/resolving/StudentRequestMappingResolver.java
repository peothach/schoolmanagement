package com.fsoft.high_school_management.mapping.resolving;

import com.fsoft.high_school_management.dto.StudentRequestBody;
import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IClassroomRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.utils.AccountGenerator;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class StudentRequestMappingResolver {

    private IStudentRepository studentRepository;

    private IClassroomRepository classroomRepository;

    private AuditableEntityHelper<Student> auditableEntityHelper;

    private AccountGenerator accountGenerator;

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setClassroomsRepository(IClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Student> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @Autowired
    public void setAccountGenerator(AccountGenerator accountGenerator) {
        this.accountGenerator = accountGenerator;
    }

    @ObjectFactory
    public Student resolve(StudentRequestBody dto, @TargetType Class<Student> type) {
        Assert.notNull(dto, "The request body is null");
        Student student = null;
        if(dto.getId() != null) {
            student = studentRepository.findById(dto.getId()).orElseThrow(() -> {
                throw new RecordNotFoundException("No record found for operate mapping");
            });
        }
        else {
            student = auditableEntityHelper.setupAuditableFields(new Student());
            student.setAccount(accountGenerator.generate(dto));
            student.setActive(true);
        }
        if(dto.getClassroomId() != null){
            Classroom classroom = this.classroomRepository.findById(dto.getClassroomId()).orElseThrow(RecordNotFoundException::new);
            student.setClassroom(classroom);
        }
        return student;
    }
}
