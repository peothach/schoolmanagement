package com.fsoft.high_school_management.mapping;

import com.fsoft.high_school_management.dto.GradeRequestBody;
import com.fsoft.high_school_management.dto.GradeResponseBody;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.mapping.resolving.GradeRequestMappingResolver;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS , uses = {GradeRequestMappingResolver.class, AccountMapper.class, SubjectMapper.class, StudentMapper.class})
public interface GradeMapper extends IMapper<Grade, GradeRequestBody, GradeResponseBody> {
}