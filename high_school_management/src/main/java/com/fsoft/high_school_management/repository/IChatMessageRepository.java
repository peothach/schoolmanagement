package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.ChatMessage;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IChatMessageRepository extends JpaRepository<ChatMessage, Long> {

    @Modifying(clearAutomatically = true)
    @Query("update ChatMessage chatMessage set chatMessage.active = FALSE where chatMessage.id = :id")
    void deactivateById(@Param("id") Long id);

    Page<ChatMessage> findAllByRecipientId(Long id, Pageable pageable);

    List<ChatMessage> findAllByRecipientId(Long id);
}