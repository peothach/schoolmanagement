package com.fsoft.high_school_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

/**
 *
 * @param <E> stands for Entity
 */
@NoRepositoryBean
public interface IGenericRepository<E> extends JpaRepository<E, Long>, JpaSpecificationExecutor<E> {
    List<E> findAllByActiveIsTrue();

    Optional<E> findByIdAndActiveIsTrue(Long id);

    void deactivateById(Long id);
}