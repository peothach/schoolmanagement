package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.AccountsRoles;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IAccountsRolesRepository extends IGenericRepository<AccountsRoles>{

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update AccountsRoles s set s.active = FALSE where s.id = :id")
    void deactivateById(@Param("id") Long id);

    List<AccountsRoles> findByAccount_Id(Long accountId);

    @Query("select a.account.username from AccountsRoles a where a.role.name = :roleName")
    Optional<List<String>> findUsernamesByRoleName(@Param("roleName") String roleName);
}
