package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Subject;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ISubjectRepository extends IGenericRepository<Subject> {

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Subject s set s.active = false where s.id = :id")
    @Transactional
    void deactivateById(@Param("id") Long id);
}
