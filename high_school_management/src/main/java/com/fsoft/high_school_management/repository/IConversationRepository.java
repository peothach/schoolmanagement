package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IConversationRepository extends JpaRepository<Conversation, Long> {

    @Modifying(clearAutomatically = true)
    @Query("update Conversation conversation set conversation.active = FALSE where conversation.id = :id")
    void deactivateById(@Param("id") Long id);
}