package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface IStudentRepository extends IGenericRepository<Student> {

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Student s set s.active = false where s.id = :id")
    @Transactional
    void deactivateById(@Param("id") Long id);
    List<Student> findAllByClassroomId(Long classroomId);

    Optional<Student> findByAccount_Username(String username);
}
