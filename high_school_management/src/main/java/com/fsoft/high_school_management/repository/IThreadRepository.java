package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Thread;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface IThreadRepository extends JpaRepository<Thread, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Thread thread set thread.active = FALSE where thread.id = :id")
    void deactivateById(@Param("id") Long id);

    Page<Thread> findAllByActiveTrue(Pageable pageable);

    Page<Thread> findAllBySubforumIdAndActiveTrue(Long subforumId, Pageable pageable);

    List<Thread> findAllBySubforumIdAndActiveTrue(Long subforumId);

    Long countBySubforumIdAndActiveTrue(Long id);
}