package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IPostRepository extends JpaRepository<Post, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Post post set post.active = FALSE where post.id = :id")
    void deactivateById(@Param("id") Long id);

    Page<Post> findByThreadIdAndActiveTrue(Long threadId, Pageable pageable);

    List<Post> findByThreadIdAndActiveTrue(Long threadId);

    Long countByThreadIdAndActiveTrue(Long threadId);
}