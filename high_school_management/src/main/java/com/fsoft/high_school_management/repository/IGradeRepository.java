package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Grade;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IGradeRepository extends IGenericRepository<Grade> {

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Grade grade set grade.active = FALSE where grade.id = :id")
    void deactivateById(@Param("id") Long id);

    @Modifying(clearAutomatically = true)
    @Query("update Grade grade set grade.active = false where grade.student.id = :id")
    void deactivateByStudentId(@Param("id") Long id);

    @Modifying(clearAutomatically = true)
    @Query("update Grade grade set grade.active = false where grade.subject.id = :id")
    void deactivateBySubjectId(@Param("id") Long id);
}