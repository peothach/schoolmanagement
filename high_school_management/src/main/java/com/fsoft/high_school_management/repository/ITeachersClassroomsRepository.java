package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.TeachersClassrooms;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ITeachersClassroomsRepository extends IGenericRepository<TeachersClassrooms> {

    @Override
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update TeachersClassrooms teachersclassrooms set teachersclassrooms.active = FALSE where teachersclassrooms.id = :id")
    void deactivateById(@Param("id") Long id);

    List<TeachersClassrooms> findAllByTeacherIdAndActiveTrue(Long teacherId);

    List<TeachersClassrooms> findAllByClassroomIdAndActiveTrue(Long classroomId);
}
