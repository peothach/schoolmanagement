package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Conversation;
import com.fsoft.high_school_management.entity.ConversationAccount;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IConversationAccountRepository extends JpaRepository<ConversationAccount, Long> {

    List<ConversationAccount> findByAccountUsername(String username);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE ConversationAccount conversationAccount SET conversationAccount.active = FALSE WHERE conversationAccount.id = :id")
    void deactivateById(@Param("id") Long id);

    /**
     * This method signature is used to provide the conversation which satisfied conditions including it is related to a combination
     * of conversationId and accountId
     * @param conversationId
     * @param accountId
     * @return
     */
    @Query("SELECT c.conversation FROM ConversationAccount c WHERE  c.conversation.id = :conversationId AND c.account.id = :accountId")
    Optional<Conversation> findByConversation_IdAndAccount_Id(@Param("conversationId") Long conversationId, @Param("accountId") Long accountId);

    /**
     * This method signature is used to provide a list of conversations which have exactly the matched rows when searching for
     * its participants with input provided participants
     * @param participants
     * @param count
     * @return
     */
    @Query("SELECT o FROM ConversationAccount c  INNER JOIN c.account a INNER JOIN c.conversation o WHERE c.account.username IN :participants GROUP BY o.id HAVING COUNT(o.id) = :count")
    List<Conversation> findByParticipantsUsername(@Param("participants") List<String> participants, @Param("count") Long count);
}