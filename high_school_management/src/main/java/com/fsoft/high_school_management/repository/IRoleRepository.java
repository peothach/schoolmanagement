package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Role;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends IGenericRepository<Role>{

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Role s set s.active = FALSE where s.id = :id")
    void deactivateById(Long id);
}
