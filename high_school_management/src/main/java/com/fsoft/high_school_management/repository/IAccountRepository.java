package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IAccountRepository extends IGenericRepository<Account> {

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Account s set s.active = FALSE where s.id = :id")
    void deactivateById(@Param("id") Long id);

    Long countByUsernameStartingWith(String fullName);

    Optional<Account> findByUsernameIgnoreCase(String username);
}
