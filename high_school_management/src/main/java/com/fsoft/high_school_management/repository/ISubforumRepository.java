package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Subforum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ISubforumRepository extends JpaRepository<Subforum, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Subforum subforum set subforum.active = FALSE where subforum.id = :id")
    void deactivateById(@Param("id") Long id);


}