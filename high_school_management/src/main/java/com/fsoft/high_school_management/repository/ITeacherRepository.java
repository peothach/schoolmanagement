package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Teacher;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ITeacherRepository extends IGenericRepository<Teacher> {

    @Override
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Teacher teacher set teacher.active = FALSE where teacher.id = :id")
    void deactivateById(@Param("id") Long id);
    
    List<Teacher> findAllBySubjectIdAndActiveTrue(Long id);

    Optional<Teacher> findByAccount_Username(String username);
}
