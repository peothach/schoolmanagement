package com.fsoft.high_school_management.repository;

import com.fsoft.high_school_management.entity.Classroom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface IClassroomRepository extends IGenericRepository<Classroom> {

    @Override
    @Modifying(clearAutomatically = true)
    @Query("update Classroom classroom set classroom.active = FALSE where classroom.id = :id")
    @Transactional
    void deactivateById(@Param("id") Long id);

    Page<Classroom> findAllByNameContaining(String name, Pageable pageable);

    Page<Classroom> findAllByNameContainingAndActive(String name, Boolean active, Pageable pageable);

    Page<Classroom> findAllByActive(Boolean aBoolean, Pageable pageable);
}
