package com.fsoft.high_school_management;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighSchoolManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(HighSchoolManagementApplication.class, args);
	}
}
