package com.fsoft.high_school_management.dao;

import com.fsoft.high_school_management.entity.Subject;
import org.springframework.data.domain.Page;

public interface ISubjectDAO {
    Page<Subject> getSearch(String searchKey, Boolean active, String sortField, boolean order, int page, int size);
}
