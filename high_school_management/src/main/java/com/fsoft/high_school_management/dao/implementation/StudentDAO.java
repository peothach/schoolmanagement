package com.fsoft.high_school_management.dao.implementation;

import com.fsoft.high_school_management.dao.IStudentDAO;
import com.fsoft.high_school_management.dao.common.DaoUtils;
import com.fsoft.high_school_management.entity.Student;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;

@Component
public class StudentDAO implements IStudentDAO {

    private EntityManager entityManager;

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Page<Student> getSearch(Long classroomId, String searchKey, Boolean active, String sortField, boolean order, int page, int size) {
        final String FIELD_CLASSROOM_ID = "classroom.classroom_id";
        final String FIELD_NAME = "fullName";
        final String FIELD_ACTIVE = "active";
        final String FIELD_ADDRESS = "address";
        final String FIELD_PHONENUMBER = "phoneNumber";

        QueryBuilder queryBuilder = DaoUtils.getQueryBuilder(entityManager, Student.class);

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
        List<DaoUtils.Condition> conditions = new LinkedList<>();
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_CLASSROOM_ID}, classroomId, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_NAME, FIELD_ADDRESS, FIELD_PHONENUMBER}, searchKey, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_ACTIVE}, active, BooleanClause.Occur.MUST));


        BooleanQuery booleanQuery = DaoUtils.getBooleanQuery(booleanQueryBuilder, queryBuilder, conditions);

        FullTextQuery jpaQuery = DaoUtils.getFullTextQuery(entityManager, booleanQuery, Student.class);
        DaoUtils.getSortedAndPaginatedQuery(jpaQuery, queryBuilder, sortField, order, page, size);
        List<Student> students = jpaQuery.getResultList();
        return new PageImpl<>(students, PageRequest.of(page, size), jpaQuery.getResultSize());
    }

    public Page<Student> getSearch(String searchKey, Boolean active, String sortField, boolean order, int page, int size) {
        return this.getSearch(null, searchKey, active, sortField, order, page, size);
    }
}
