package com.fsoft.high_school_management.dao;

import com.fsoft.high_school_management.entity.Grade;
import org.springframework.data.domain.Page;

public interface IGradeDAO {
    Page<Grade> getSearch(Long studentId, String searchKey, String sortField, boolean order, int page, int size);
    Page<Grade> getClassroomGradesSearch(Long classroomId, Long subjectId, String searchKey, Boolean active, String sortField, boolean order, int page, int size);
}
