package com.fsoft.high_school_management.dao.implementation;

import com.fsoft.high_school_management.dao.ISubjectDAO;
import com.fsoft.high_school_management.dao.common.DaoUtils;
import com.fsoft.high_school_management.entity.Subject;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;

@Component
public class SubjectDAO implements ISubjectDAO {

    private EntityManager entityManager;

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Page<Subject> getSearch(String searchKey, Boolean active, String sortField, boolean order, int page, int size) {
        final String FIELD_NAME = "name";
        final String FIELD_ACTIVE = "active";

        QueryBuilder queryBuilder = DaoUtils.getQueryBuilder(entityManager, Subject.class);
        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
        List<DaoUtils.Condition> conditions = new LinkedList<>();
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_ACTIVE}, active, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_NAME}, searchKey, BooleanClause.Occur.MUST));
        BooleanQuery booleanQuery = DaoUtils.getBooleanQuery(booleanQueryBuilder, queryBuilder, conditions);

        FullTextQuery jpaQuery = DaoUtils.getFullTextQuery(entityManager, booleanQuery, Subject.class);
        DaoUtils.getSortedAndPaginatedQuery(jpaQuery, queryBuilder, sortField, order, page, size);
        List<Subject> subjects = jpaQuery.getResultList();

        return new PageImpl<>(subjects, PageRequest.of(page, size), jpaQuery.getResultSize());
    }
}
