package com.fsoft.high_school_management.dao.implementation;

import com.fsoft.high_school_management.dao.IGradeDAO;
import com.fsoft.high_school_management.dao.common.DaoUtils;
import com.fsoft.high_school_management.entity.Grade;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;

@Component
public class GradeDAO implements IGradeDAO {

    private EntityManager entityManager;

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public Page<Grade> getSearch(Long studentId, String searchKey, String sortField, boolean order, int page, int size) {
        final String FIELD_STUDENT_ID = "student.person_id";
        final String FIELD_SUBJECT_NAME = "subject.name";

        QueryBuilder queryBuilder = DaoUtils.getQueryBuilder(entityManager, Grade.class);

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();

        List<DaoUtils.Condition> conditions = new LinkedList<>();
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_STUDENT_ID}, studentId, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_SUBJECT_NAME}, searchKey, BooleanClause.Occur.MUST));

        BooleanQuery booleanQuery = DaoUtils.getBooleanQuery(booleanQueryBuilder, queryBuilder, conditions);

        FullTextQuery jpaQuery = DaoUtils.getFullTextQuery(entityManager, booleanQuery, Grade.class);
        DaoUtils.getSortedAndPaginatedQuery(jpaQuery, queryBuilder, sortField, order, page, size);
        List<Grade> students = jpaQuery.getResultList();

        return new PageImpl<>(students, PageRequest.of(page, size), jpaQuery.getResultSize());
    }

    public Page<Grade> getClassroomGradesSearch(Long classroomId, Long subjectId, String searchKey, Boolean active, String sortField, boolean order, int page, int size) {
        final String FIELD_CLASS_ID = "student.classroom.classroom_id";
        final String FIELD_SUBJECT_ID = "subject.subject_id";
        final String FIELD_STUDENT_NAME = "student.fullName";
        final String FIELD_ACTIVE = "student.active";
        final String FIELD_ADDRESS = "student.address";
        final String FIELD_PHONENUMBER = "student.phoneNumber";

        System.out.println(searchKey);

        QueryBuilder queryBuilder = DaoUtils.getQueryBuilder(entityManager, Grade.class);

        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
        List<DaoUtils.Condition> conditions = new LinkedList<>();
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_CLASS_ID}, classroomId, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_SUBJECT_ID}, subjectId, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_ACTIVE}, active, BooleanClause.Occur.MUST));
        conditions.add(new DaoUtils.Condition(new String[]{FIELD_STUDENT_NAME, FIELD_ADDRESS, FIELD_PHONENUMBER}, searchKey, BooleanClause.Occur.MUST));
        BooleanQuery booleanQuery = DaoUtils.getBooleanQuery(booleanQueryBuilder, queryBuilder, conditions);

        FullTextQuery jpaQuery = DaoUtils.getFullTextQuery(entityManager, booleanQuery, Grade.class);
        DaoUtils.getSortedAndPaginatedQuery(jpaQuery, queryBuilder, sortField, order, page, size);
        List<Grade> students = jpaQuery.getResultList();

        return new PageImpl<>(students, PageRequest.of(page, size), jpaQuery.getResultSize());
    }
}
