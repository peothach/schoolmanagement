package com.fsoft.high_school_management.dao;

import com.fsoft.high_school_management.entity.Student;
import org.springframework.data.domain.Page;

public interface IStudentDAO {
    Page<Student> getSearch(Long classroomId, String searchKey, Boolean active, String sortField, boolean order, int page, int size);
    Page<Student> getSearch(String searchKey, Boolean active, String sortField, boolean order, int page, int size);
}
