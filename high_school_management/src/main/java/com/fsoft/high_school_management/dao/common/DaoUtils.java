package com.fsoft.high_school_management.dao.common;

import com.fsoft.high_school_management.entity.common.Gender;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Sort;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class DaoUtils {
    public static FullTextQuery getSortedAndPaginatedQuery(FullTextQuery fullTextQuery, QueryBuilder queryBuilder, String sortField, Boolean order, Integer page, Integer size) {
        Sort sort = order ? queryBuilder.sort().byField(sortField).createSort() : queryBuilder.sort().byField(sortField).desc().createSort();
        fullTextQuery.setSort(sort);
        fullTextQuery.setMaxResults(size);
        fullTextQuery.setFirstResult(page * size);
        return fullTextQuery;
    }

    @SuppressWarnings("rawtypes")
    private static Lambda getFunction(Class<?> type, BooleanClause.Occur condition) {
        if (type == Boolean.class) {
            return (BooleanQuery.Builder booleanQueryBuilder,
                    QueryBuilder queryBuilder,
                    String[] fieldName,
                    Object value) -> {
                Boolean booleanValue = (Boolean) value;
                return booleanQueryBuilder.add(queryBuilder.keyword().onField(fieldName[0]).matching(booleanValue).createQuery(), condition);
            };
        }
        if (type == Date.class) {
            return (BooleanQuery.Builder booleanQueryBuilder,
                    QueryBuilder queryBuilder,
                    String[] fieldName,
                    Object value) -> {
                Date dateValue = (Date) value;
                return booleanQueryBuilder.add(queryBuilder.keyword().onField(fieldName[0]).matching(dateValue).createQuery(), condition);
            };
        }
        if (type == Gender.class) {
            return (BooleanQuery.Builder booleanQueryBuilder,
                    QueryBuilder queryBuilder,
                    String[] fieldName,
                    Object value) -> {
                Gender dateValue = (Gender) value;
                return booleanQueryBuilder.add(queryBuilder.keyword().onField(fieldName[0]).matching(dateValue).createQuery(), condition);
            };
        }
        if (type == Long.class) {
            return (BooleanQuery.Builder booleanQueryBuilder,
                    QueryBuilder queryBuilder,
                    String[] fieldName,
                    Object value) -> {
                Long longValue = (Long) value;
                return booleanQueryBuilder.add(queryBuilder.range().onField(fieldName[0]).from(longValue).to(longValue).createQuery(), condition);
            };
        }
        if (type == List.class) {
            return (BooleanQuery.Builder booleanQueryBuilder,
                    QueryBuilder queryBuilder,
                    String[] fieldName,
                    Object value) -> {
                List listValue = (List) value;

                return booleanQueryBuilder.add(queryBuilder.range().onField(fieldName[0]).from((Long) listValue.get(0)).to((Long) listValue.get(1)).createQuery(), condition);
            };
        }
        return (BooleanQuery.Builder booleanQueryBuilder,
                QueryBuilder queryBuilder,
                String[] fieldName,
                Object value) -> {
            String stringValue = (String) value;
            return booleanQueryBuilder.add(queryBuilder.keyword().wildcard().onFields(fieldName).matching("*" + stringValue.toLowerCase() + "*").createQuery(), condition);
        };
    }

    public static BooleanQuery getBooleanQuery(BooleanQuery.Builder booleanQueryBuilder,
                                               QueryBuilder queryBuilder,
                                               List<Condition> conditions) {
        conditions.forEach((element) -> {
            if (element.getValue() != null) {
                getFunction(element.getValue().getClass(), element.getCondition()).addCondition(booleanQueryBuilder, queryBuilder, element.getFieldNames(), element.getValue());
            }
        });
        BooleanQuery booleanQuery = booleanQueryBuilder.build();
        booleanQuery = booleanQuery.clauses().isEmpty() ?
                booleanQueryBuilder.add(queryBuilder.all().createQuery(), BooleanClause.Occur.MUST).build() :
                booleanQuery;
        return booleanQuery;
    }


    public static QueryBuilder getQueryBuilder(EntityManager entityManager, Class<?> type) {
        return Search.getFullTextEntityManager(entityManager).getSearchFactory()
                .buildQueryBuilder()
                .forEntity(type)
                .get();
    }

    public static FullTextQuery getFullTextQuery(EntityManager entityManager, BooleanQuery booleanQuery, Class<?> type) {
        return Search.getFullTextEntityManager(entityManager).createFullTextQuery(booleanQuery, type);
    }

    @FunctionalInterface
    interface Lambda {
        BooleanQuery.Builder addCondition(BooleanQuery.Builder booleanQueryBuilder,
                                          QueryBuilder queryBuilder,
                                          String[] fieldName,
                                          Object value);
    }

    public static class Condition {
        private String[] fieldNames;
        private Object value;
        private BooleanClause.Occur condition;

        public String[] getFieldNames() {
            return fieldNames;
        }

        public void setFieldNames(String[] fieldNames) {
            this.fieldNames = fieldNames;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public BooleanClause.Occur getCondition() {
            return condition;
        }

        public void setCondition(BooleanClause.Occur condition) {
            this.condition = condition;
        }

        public Condition(String[] fieldNames, Object value, BooleanClause.Occur condition) {
            this.fieldNames = fieldNames;
            this.value = value;
            this.condition = condition;
        }

        public Condition() {
        }
    }
}
