package com.fsoft.high_school_management.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(com.fsoft.high_school_management.service.implementation..*) "
            + "|| within(com.fsoft.high_school_management.repository..*) "
            + "|| within(com.fsoft.high_school_management.controller..*)")
    public void applicationPackagePointcut() {
        // This method purpose is only for declaring PointCut
    }

    /**
     *  This advice is have responsibilities for log all input and output states of a method of pointcuts when it has been invoked
     */
    @Around("applicationPackagePointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        String arguments = Arrays.toString(joinPoint.getArgs());
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();

        logger.debug("Enter: {}.{}() with argument[s] = {}", className, methodName, arguments);
        try {
            Object result = joinPoint.proceed();
            logger.debug("Exit: {}.{}() with result = {}", className, methodName, result);
            return result;
        } catch (IllegalArgumentException e) {
            logger.error("Illegal argument: {} in {}.{}()", arguments, className, methodName);
            throw e;
        }
    }

    /**
     *  This advice is have responsibilities for log all throw exception operation happened in pointcuts
     */
    @AfterThrowing(pointcut = "applicationPackagePointcut()", throwing = "error")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
        String message = error.getClass().toString() + ":" + error.getMessage();
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();

        logger.error("Exception in {}.{}() with cause = {} \n StackTrace: {}", className, methodName, message, error.getStackTrace());
    }
}
