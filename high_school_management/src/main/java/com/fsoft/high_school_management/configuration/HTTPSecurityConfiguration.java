package com.fsoft.high_school_management.configuration;

import com.fsoft.high_school_management.filter.ExceptionHandlerFilter;
import com.fsoft.high_school_management.filter.JWTRequestFilter;
import com.fsoft.high_school_management.service.implementation.CustomUserDetailService;
import com.fsoft.high_school_management.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import static com.fsoft.high_school_management.utils.Constants.*;

@Configuration
@EnableWebSecurity
public class HTTPSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private CustomUserDetailService userDetailsService;

    private JWTRequestFilter jwtRequestFilter;

    private ExceptionHandlerFilter exceptionHandlerFilter;

    @Autowired
    public void setUserDetailsService(CustomUserDetailService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Autowired
    public void setJwtRequestFilter(JWTRequestFilter jwtRequestFilter) {
        this.jwtRequestFilter = jwtRequestFilter;
    }

    @Autowired
    public void setExceptionHandlerFilter(ExceptionHandlerFilter exceptionHandlerFilter) {
        this.exceptionHandlerFilter = exceptionHandlerFilter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * This method is used to configure our authorities will not begin with 'ROLE_'
     *
     * @return GrantedAuthorityDefaults
     */
    @Bean
    public GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults("");
    }

    /**
     * This method is used to configure our AuthenticationProvider which we want Spring Security will use it to authenticate every requests
     *
     * @return AuthenticationProvider
     */
    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setHideUserNotFoundExceptions(false);
        return daoAuthenticationProvider;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    /**
     * This method is used for configure what we will expected Spring Security will handle when a HTTP request has been sent to our system
     * This is the centre configuration for the whole security system which include adding our custom filter, authorization requests purpose, turn of or on CSRF, CORS, SessionState..
     *
     * @param http HttpSecurity which is used for configuration purpose
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(Constants.SWAGGER_URL_WHITELIST).permitAll()
                .antMatchers(HttpMethod.POST, LOGIN_URL).permitAll()
                .antMatchers(HttpMethod.GET, ALL_ROLE_URL).permitAll()
                .antMatchers(WS_CONNECTION_URL).permitAll()
                .antMatchers(POST_URL).authenticated()
                .antMatchers(SUBFORUM_URL).authenticated()
                .antMatchers(THREAD_URL).authenticated()
                .antMatchers(HttpMethod.POST, CHECK_PASSWORD_URL).hasAnyAuthority(TEACHER, STUDENT, ADMINISTRATOR)
                .antMatchers(HttpMethod.PUT, CHANGE_PASSWORD_URL).hasAnyAuthority(TEACHER, STUDENT, ADMINISTRATOR)

                .antMatchers(GRADES_OF_A_STUDENT_URL).access("@securityService.isMyPage(authentication,\"STUDENT\",#studentId)")
                .antMatchers(INFO_OF_STUDENT_URL).access("@securityService.isMyPage(authentication,\"STUDENT\",#studentId)")
                .antMatchers(HttpMethod.GET, ALL_CLASSROOM_URL).hasAnyAuthority(STUDENT, ADMINISTRATOR)

                .antMatchers(HttpMethod.GET, INFO_OF_A_TEACHER_URL).access("@securityService.isMyPage(authentication,\"TEACHER\",#teacherId)")
                .antMatchers(HttpMethod.GET, GRADES_OF_A_SUBJECT_IN_A_CLASSROOM_URL).access("@securityService.isAccessible(authentication,#classroomId,#subjectId)")
                .antMatchers(HttpMethod.PUT, GRADES_URL).hasAnyAuthority(TEACHER, ADMINISTRATOR)
                .antMatchers(HttpMethod.DELETE, GRADES_URL).hasAnyAuthority(TEACHER, ADMINISTRATOR)

                .antMatchers(HttpMethod.GET, GET_CONVERSATION_BASED_RECIPIENT_URL).authenticated()
                .antMatchers(HttpMethod.GET, GET_MESSAGES_IN_A_CONVERSATION_URL).authenticated()
                .antMatchers(HttpMethod.GET, GET_CONVERSATIONS_OF_CURRENT_USER_URL).authenticated()
                .antMatchers(HttpMethod.GET, GET_ALL_SUPPORTER_USERNAME_URL).authenticated()
                .antMatchers(HttpMethod.GET, "/api/active").authenticated()
                .anyRequest().hasAuthority(ADMINISTRATOR)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(exceptionHandlerFilter, JWTRequestFilter.class);
    }

    /**
     * This override method is used for set the CORS mechanism for HTTP requests
     *
     * @return CorsFilter
     */
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin(FE_URL);
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }


}
