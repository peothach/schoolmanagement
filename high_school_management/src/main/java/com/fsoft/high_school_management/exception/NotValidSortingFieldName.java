package com.fsoft.high_school_management.exception;

public class NotValidSortingFieldName extends RuntimeException {
    public NotValidSortingFieldName() {
        super("You have provide a not valid field name for sorting purpose");
    }

    public NotValidSortingFieldName(String errorMessage) {
        super(errorMessage);
    }

    public NotValidSortingFieldName(Throwable err) {
        super(err);
    }

    public NotValidSortingFieldName(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
