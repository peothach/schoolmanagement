package com.fsoft.high_school_management.exception;

public class RecordNotFoundException extends RuntimeException{

    public RecordNotFoundException() {
        super("No record found for operate this request");
    }

    public RecordNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public RecordNotFoundException(Throwable err) {
        super(err);
    }

    public RecordNotFoundException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
