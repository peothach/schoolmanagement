package com.fsoft.high_school_management.exception;

import com.fsoft.high_school_management.dto.APIError;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice(basePackages = "com.fsoft.high_school_management")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (FieldError error : exception.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : exception.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        APIError apiError = new APIError(HttpStatus.BAD_REQUEST, exception.getClass().getSimpleName(), errors);
        return ResponseEntity.badRequest().body(apiError);
    }

    @ExceptionHandler(value = {RecordNotFoundException.class})
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    protected ResponseEntity<APIError> handleRecordNotFoundException(RecordNotFoundException exception) {
        APIError apiError = new APIError(HttpStatus.NOT_FOUND, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(RoleRequestIsNotMatch.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ResponseEntity<APIError> handleRoleRequestIsNotMatch(RoleRequestIsNotMatch exception) {
        APIError apiError = new APIError(HttpStatus.FORBIDDEN, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<APIError> handleBadCredentialsException(BadCredentialsException exception) {
        APIError apiError = new APIError(HttpStatus.UNAUTHORIZED, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<APIError> handleInternalAuthenticationServiceException(UsernameNotFoundException exception) {
        APIError apiError = new APIError(HttpStatus.UNAUTHORIZED, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {NotValidSortingFieldName.class})
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    protected ResponseEntity<APIError> handleNotValidSortingFieldNameException(NotValidSortingFieldName exception) {
        APIError apiError = new APIError(HttpStatus.NOT_FOUND, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {IOException.class})
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    protected ResponseEntity<APIError> handleIOException(IOException exception) {
        APIError apiError = new APIError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getClass().getSimpleName(), "Some IO operations has been broken - Can't exporting data to excel file as required");
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    protected ResponseEntity<APIError> handleDataIntegrityViolationException(DataIntegrityViolationException exception) {
        APIError apiError = new APIError(HttpStatus.BAD_REQUEST, exception.getClass().getSimpleName(), "Caused by : your input data violate the constraints with other record");
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    protected ResponseEntity<APIError> handleIllegalArgumentException(IllegalArgumentException exception) {
        APIError apiError = new APIError(HttpStatus.BAD_REQUEST, exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<APIError> handleUnwantedException(Exception exception) {
        APIError apiError = new APIError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getClass().getSimpleName(), "Some errors has been occurred");
        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}