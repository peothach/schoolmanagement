package com.fsoft.high_school_management.exception;

public class RoleRequestIsNotMatch extends RuntimeException{

    public RoleRequestIsNotMatch() {
        super("Role request is not match for any authorities this account has");
    }

    public RoleRequestIsNotMatch(String errorMessage) {
        super(errorMessage);
    }

    public RoleRequestIsNotMatch(Throwable err) {
        super(err);
    }

    public RoleRequestIsNotMatch(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
