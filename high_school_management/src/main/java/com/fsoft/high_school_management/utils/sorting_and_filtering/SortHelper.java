package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.QueryCriteria;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class SortHelper {

    private SortHelper(){
        // This constructor is used to constraint that class not be construct from outside
    }

    /**
     * This method is used for generate the Sort list based on the list of QueryCriteria
     *
     * @param criteria List<QueryCriteria> hold the QueryCriteria objects
     * @return List<Sort> is the generated List of Sort objects
     */
    public static List<Sort> generateSortList(List<QueryCriteria> criteria) {
        Assert.notNull(criteria, "The arguments is invalid");
        return criteria.stream().map(criterion -> {
            switch (criterion.getOperation()) {
                case SORT_ASC:
                    return Sort.by(Sort.Order.asc(criterion.getKey()));
                case SORT_DESC:
                    return Sort.by(Sort.Order.desc(criterion.getKey()));
                default:
                    return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    /**
     * This method is used for combine List of Sort object to a combined Sort object
     *
     * @param criteria List of QueryCriteria
     * @param <O> stand for Sort object
     * @return Optional<Sort> is the combined Sort object
     */
    public static <O extends Sort> Optional<Sort> combineSort(List<O> criteria) {
        Assert.notNull(criteria, "The arguments is invalid");
        Iterator<O> itr = criteria.iterator();
        if (itr.hasNext()) {
            Sort sort = (itr.next());
            while (itr.hasNext()) {
                sort = sort.and(itr.next());
            }
            return Optional.of(sort);
        }
        return Optional.empty();
    }
}
