package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.QueryCriteria;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryCriteriaParser {

    private static String wordRegex = "[a-zA-Z]\\w*";
    private static String valueRegex = "[\\w,\\s]+";
    private static String operatorRegex = "(:|~|<|>|!|\\+|-)";
    private static String timestampRegex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0 -9]{2}:[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}";
    private static String fullRegex = "(" + wordRegex + ")" + operatorRegex + "(" + timestampRegex + "|" + valueRegex + ")?,";
    private static final Pattern searchPattern = Pattern.compile(fullRegex);

    private QueryCriteriaParser(){
        // This constructor is used to constraint that class not be construct from outside
    }

    /**
     * This method is used to parsed the input String to List of QueryCriteria objects
     * In the URL it would be something like localhost:8080/api/person?filter=name:david,age>16&sort=age-&pageSize=10&pageIndex=0
     * This   name:david,age>16   is our operationString which follow the pattern (filedName)(operation)(value?)
     *
     * @param operationString String is the String has been mentioned above
     * @return List<QueryCriteria> which is be List of QueryCriteria objects
     */
    public static List<QueryCriteria> parseOperationString(String operationString) {
        List<QueryCriteria> queryCriteriaList = new ArrayList<>();
        if (operationString != null) {
            Matcher matcher = searchPattern.matcher(operationString + ",");
            while (matcher.find()) {
                QueryCriteria queryCriteria = new QueryCriteria();
                queryCriteria.setKey(matcher.group(1));
                queryCriteria.setOperation(QueryOperation.getSimpleOperation(matcher.group(2)));
                queryCriteria.setValue(matcher.group(3));
                if ((queryCriteria.getOperation() != QueryOperation.SORT_DESC && queryCriteria.getOperation() != QueryOperation.SORT_ASC) || queryCriteria.getValue() == null) {
                    queryCriteriaList.add(queryCriteria);
                }
            }
        }
        if(queryCriteriaList.isEmpty()) throw new IllegalArgumentException();
        return queryCriteriaList;
    }
}
