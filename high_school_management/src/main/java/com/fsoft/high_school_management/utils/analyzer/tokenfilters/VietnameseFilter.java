package com.fsoft.high_school_management.utils.analyzer.tokenfilters;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VietnameseFilter extends TokenFilter {
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private char[] curTermBuffer;
    private int curTermLength;
    private boolean keepLoop;
    private Map<Character, List<Character>> vietnameseCharacterListMap;

    /**
     * Construct a token stream filtering the given input.
     *
     * @param input
     */
    public VietnameseFilter(TokenStream input) {
        super(input);
        keepLoop = true;
        vietnameseCharacterListMap = new HashMap<>();
        vietnameseCharacterListMap.put('a', Stream.of(new Character[]{'á', 'à', 'ả', 'ã', 'ạ',
                        'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ',
                        'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ',})
                .collect(Collectors.toList()));
        vietnameseCharacterListMap.put('e', Stream.of(new Character[]{'é', 'è', 'ẻ', 'ẽ', 'ẹ',
                        'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ',})
                .collect(Collectors.toList()));
        vietnameseCharacterListMap.put('i', Stream.of(new Character[]{'í', 'ì', 'ỉ', 'ĩ', 'ị',})
                .collect(Collectors.toList()));
        vietnameseCharacterListMap.put('o', Stream.of(new Character[]{'ó', 'ò', 'ỏ', 'õ', 'ọ',
                'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ',
                'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ',}).collect(Collectors.toList()));
        vietnameseCharacterListMap.put('u', Stream.of(new Character[]{'ú', 'ù', 'ủ', 'ũ', 'ụ',
                'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự',}).collect(Collectors.toList()));
    }

    @Override
    public boolean incrementToken() throws IOException {
        if (!keepLoop) {
            keepLoop = true;
            return false;
        }
        if (input.incrementToken()) {
            curTermBuffer = termAtt.buffer().clone();
            curTermLength = termAtt.length();
            for (int i = 0; i < termAtt.length(); i++) {
                for (Map.Entry<Character, List<Character>> entry : vietnameseCharacterListMap.entrySet()) {
                    if (entry.getValue().contains(curTermBuffer[i])){
                        curTermBuffer[i] = entry.getKey();
                        break;
                    }
                }
            }
            return true;
        }
        termAtt.copyBuffer(curTermBuffer, 0, curTermLength);
        keepLoop = false;
        return true;
    }

    @Override
    public void reset() throws IOException {
        super.reset();
    }
}
