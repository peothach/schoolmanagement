package com.fsoft.high_school_management.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Optional;

public class PageableBuilder {

    private PageableBuilder() {}

    public static Pageable buildPage(Optional<String> order, Optional<String> sort, Optional<Integer> pageSize, Optional<Integer> pageIndex) {

        return sort
                .map(field -> PageRequest.of(pageIndex.orElse(0), pageSize.orElse(5), Sort.by(
                        order.orElse("asc")
                                .equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC,
                        field
                )))
                .orElseGet(() -> PageRequest.of(pageIndex.orElse(0),
                        pageSize.orElse(5),
                        Sort.by(Sort.Direction.ASC, "id")));
    }
}
