package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.QueryCriteria;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class SpecificationHelper<E> implements Specification<E> {

    private QueryCriteria criteria;

    public SpecificationHelper(QueryCriteria queryCriteria){
        this.criteria = queryCriteria;
    }

    /**
     * The default method must be override when we implement Specification<E> interface
     * to help Data JPA define the way construct a Specification object based on our QueryCriteria
     *
     * E stand for Entity
     * @param root Root<E> is stand for the subject after FROM term in SQL query, but in Date JPA context, we call it Root
     * @param query CriteriaQuery is used to process top-level queries operation
     * @param criteriaBuilder CriteriaBuilder is used to construct criteria queries, compound selections, expressions, predicates, orderings.
     * @return Predicate
     */
    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Assert.notNull(criteria, "The arguments is invalid");

        switch (criteria.getOperation()) {
            case EQUAL:{
                Class<?> fieldClassType = root.get(criteria.getKey()).getJavaType();
                return criteriaBuilder.equal(root.get(criteria.getKey()), fieldClassType.cast(criteria.getValue()));
            }
            case LIKE:
                return criteriaBuilder.like(criteriaBuilder.upper(root.get(criteria.getKey())), "%" + ((String) criteria.getValue()).toUpperCase(Locale.ROOT) + "%");
            case NEGATION:
                return criteriaBuilder.notEqual(root.get(criteria.getKey()), criteria.getValue());
            case LESS_THAN:
                return criteriaBuilder.lessThan(root.get(criteria.getKey()), criteria.getValue().toString());
            case GREATER_THAN:
                return criteriaBuilder.greaterThan(root.get(criteria.getKey()), criteria.getValue().toString());
            default:
                return null;
        }
    }

    /**
     * The method used to combine all the specification to one object to be used to query
     *
     * @param specificationList List<Specification> is used to hold our Specification objects
     * @param <E> stand for Entity
     * @param <S> stand for Specification
     * @return Optional<Specification<E>> is the combined Specification
     */
    public static <E, S extends Specification<E>> Optional<Specification<E>> combineSpecification(List<S> specificationList) {
        Assert.notNull(specificationList, "The arguments is invalid");
        Iterator<S> itr = specificationList.iterator();
        if (itr.hasNext()) {
            Specification<E> spec = Specification.where(itr.next());
            while (itr.hasNext()) {
                spec = spec.and(itr.next());
            }
            return Optional.of(spec);
        }
        return Optional.empty();
    }
}
