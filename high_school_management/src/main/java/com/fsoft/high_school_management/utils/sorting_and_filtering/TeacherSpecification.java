package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.QueryCriteria;
import com.fsoft.high_school_management.entity.Teacher;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class TeacherSpecification implements Specification<Teacher> {
    private QueryCriteria criteria;

    public TeacherSpecification(QueryCriteria criteria) {
        this.criteria = criteria;
    }


    public void setCriteria(QueryCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<Teacher> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if ("fullName".equals(criteria.getKey()) || "address".equals(criteria.getKey())) {
            return criteriaBuilder.like(root.get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
        }
        if (criteria.getValue() instanceof Boolean) {
            return criteriaBuilder.isTrue(root.get(criteria.getKey()));
        }
        if ("gender".equals(criteria.getKey()) || "phoneNumber".equals(criteria.getKey()) || "birthDate".equals(criteria.getKey())) {
            return criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
        }
        if ("subjectId".equals(criteria.getKey())) {

            return criteriaBuilder.equal(root.get("subject"), criteria.getValue());
        }
        return null;
    }
}
