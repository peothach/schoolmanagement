package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import com.fsoft.high_school_management.dto.common.AuditableDTO;
import com.fsoft.high_school_management.dto.common.PageDTO;
import com.fsoft.high_school_management.mapping.IMapper;
import java.util.List;
import org.springframework.data.domain.Page;

public class PageMappingHelper {

    private PageMappingHelper(){
        // This constructor is used to constraint that class not be construct from outside
    }

    /**
     * This method is used to map an Page<E> to Page<O>
     *
     * @param inPage is Page<E> standing for Pageable Object which received from repository query operation
     * @param mapper is a suitable mapper for this E domain model
     * @param <E> stand for entity
     * @param <I> stand for requestBodyDTO
     * @param <O> stand for responseBodyDTO
     * @return PageDTO<O>
     */
    public static <E, I extends AbstractDTO, O extends AuditableDTO> PageDTO<O> toPageDTO(Page<E> inPage, IMapper<E, I, O> mapper){

        List<E> oldContent = inPage.getContent();
        List<O> newContent = mapper.toDTOs(oldContent);

        PageDTO<O> outPage = new PageDTO<>();
        outPage.setContent(newContent);
        outPage.setSize(inPage.getSize());
        outPage.setTotalPages(inPage.getTotalPages());
        outPage.setCurrentPage(inPage.getNumber());
        outPage.setTotalElements(inPage.getTotalElements());
        outPage.setNumberOfElement(inPage.getNumberOfElements());
        outPage.setFirst(outPage.getCurrentPage() == 0);
        outPage.setLast(outPage.getCurrentPage() >= inPage.getTotalPages()-1);
        outPage.setEmpty(inPage.getTotalElements() == 0);
        return outPage;
    }
}
