package com.fsoft.high_school_management.utils;

public class Constants {

    public static final String ID_ID_NULL_MESSAGE = "The id is null and not valid";
    public static final String CRITERIA_NOT_VALID_MESSAGE = "No valid criteria have been provided";
    public static final String ENTITY_OBJECT_NOT_VALID_MESSAGE = "The entity object is null and not valid";
    public static final String STOMP_MESSAGE_HEADER_IS_NULL_MESSAGE = "The STOMP header is null and not valid";
    public static final String ILLEGAL_ARGUMENTS_MESSAGE = "Some of this args are not valid !";
    public static final String UNKNOWN_EXCEPTION_MESSAGE = "Something went wrong caused this deleting operation has not completed";
    public static final String USERNAME_NOT_EXIST_MESSAGE = "Username is not existed ! we cant find any credential match this username";
    public static final String EXCEPTION_MESSAGE_ATTRIBUTE = "exceptionMessage";
    public static final String TEACHER = "TEACHER";
    public static final String STUDENT = "STUDENT";
    public static final String ADMINISTRATOR = "ADMINISTRATOR";
    public static final String FE_URL = "http://localhost:3000";
    public static final String LOGIN_URL = "/api/login";
    public static final String WS_CONNECTION_URL = "/ws/**";
    public static final String ALL_ROLE_URL = "/api/roles/all";
    public static final String ALL_CLASSROOM_URL = "/api/classrooms/all";
    public static final String CHECK_PASSWORD_URL = "/api/accounts/checkPassword";
    public static final String CHANGE_PASSWORD_URL = "/api/accounts/changePassword";
    public static final String GRADES_OF_A_STUDENT_URL = "/api/students/{studentId}/grades";
    public static final String INFO_OF_STUDENT_URL = "/api/students/{studentId}";
    public static final String INFO_OF_A_TEACHER_URL = "/api/teachers/{teacherId}";
    public static final String GRADES_URL = "/api/grades";
    public static final String GRADES_OF_A_SUBJECT_IN_A_CLASSROOM_URL = "/api/classrooms/{classroomId}/subjects/{subjectId}/grades";
    public static final String STOMP_AUTHORIZATION_HEADER = "X-Authorization";
    public static final String GET_CONVERSATION_BASED_RECIPIENT_URL = "/api/conversations/recipient";
    public static final String GET_MESSAGES_IN_A_CONVERSATION_URL = "/api/conversations/**/messages";
    public static final String GET_CONVERSATIONS_OF_CURRENT_USER_URL = "/api/conversations";
    public static final String GET_ALL_SUPPORTER_USERNAME_URL = "/api/supporters";
    public static final String SUBFORUM_URL = "/api/subforums/**";
    public static final String THREAD_URL = "/api/threads/**";
    public static final String POST_URL = "/api/posts/**";


    public static final String[] SWAGGER_URL_WHITELIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/swagger-ui/**"
    };
}
