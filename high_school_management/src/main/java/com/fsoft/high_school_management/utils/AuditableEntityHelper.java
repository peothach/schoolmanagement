package com.fsoft.high_school_management.utils;

import com.fsoft.high_school_management.dto.CustomUserDetail;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.common.AuditableEntity;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuditableEntityHelper<E extends AuditableEntity> {

    private IAccountRepository accountRepository;

    @Autowired
    public void setAccountRepository(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public E setupAuditableFields(E entity){

        Date today = new Date();

        CustomUserDetail principal = (CustomUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountRepository.findByUsernameIgnoreCase(principal.getUsername()).orElseThrow(RecordNotFoundException::new);
        
        if(entity.getCreatedDate() == null) {
            entity.setCreatedDate(today);
        }
        if(entity.getCreatedBy() == null) {
            entity.setCreatedBy(account);
        }
        entity.setLastModifiedDate(today);
        entity.setLastModifiedBy(account);
        if(entity.getActive() == null){
            entity.setActive(true);
        }
        return entity;
    }
}
