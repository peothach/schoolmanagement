package com.fsoft.high_school_management.utils.sorting_and_filtering;

import com.fsoft.high_school_management.entity.common.Gender;

import java.util.Optional;

public class TeacherFilter {
    private Optional<String> fullName;
    private Optional<Long> subjectId;
    private Optional<String> address;
    private Optional<String> phoneNumber;
    private Optional<Gender> gender;

    public TeacherFilter() {
    }

    public TeacherFilter(Optional<String> fullName, Optional<Long> subjectId, Optional<String> address, Optional<String> phoneNumber, Optional<Gender> gender) {
        this.fullName = fullName;
        this.subjectId = subjectId;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
    }

    public Optional<String> getFullName() {
        return fullName;
    }

    public void setFullName(Optional<String> fullName) {
        this.fullName = fullName;
    }

    public Optional<Long> getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Optional<Long> subjectId) {
        this.subjectId = subjectId;
    }

    public Optional<String> getAddress() {
        return address;
    }

    public void setAddress(Optional<String> address) {
        this.address = address;
    }

    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Optional<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Optional<Gender> getGender() {
        return gender;
    }

    public void setGender(Optional<Gender> gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TeacherFilter)) {
            return false;
        }
        TeacherFilter comparedObj = (TeacherFilter) obj;
        return this.fullName.equals(comparedObj.getFullName()) &&
                this.address.equals(comparedObj.getAddress()) &&
                this.gender.equals(comparedObj.getGender()) &&
                this.subjectId.equals(comparedObj.getSubjectId()) &&
                this.phoneNumber.equals(comparedObj.getPhoneNumber());
    }
}
