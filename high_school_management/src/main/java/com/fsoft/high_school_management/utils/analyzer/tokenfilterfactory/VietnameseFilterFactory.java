package com.fsoft.high_school_management.utils.analyzer.tokenfilterfactory;

import com.fsoft.high_school_management.utils.analyzer.tokenfilters.VietnameseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.AbstractAnalysisFactory;
import org.apache.lucene.analysis.util.MultiTermAwareComponent;
import org.apache.lucene.analysis.util.TokenFilterFactory;

import java.util.Map;

public class VietnameseFilterFactory extends TokenFilterFactory implements MultiTermAwareComponent {
    /**
     * Initialize this factory via a set of key-value pairs.
     *
     * @param args
     */
    public VietnameseFilterFactory(Map<String, String> args) {
        super(args);
        if (!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters: " + args);
        }
    }

    @Override
    public AbstractAnalysisFactory getMultiTermComponent() {
        return this;
    }

    @Override
    public TokenStream create(TokenStream input) {
        return new VietnameseFilter(input);
    }
}
