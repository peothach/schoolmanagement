package com.fsoft.high_school_management.utils;

import org.hibernate.search.annotations.SortableField;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReflectionUtils {

    public static boolean checkValidField(String sortString, Class<?> classType) {
        Field[] superclassFields = classType.getSuperclass().getDeclaredFields();
        Field[] derivedFields = classType.getDeclaredFields();
        Field[] allFieldsArray = new Field[derivedFields.length + superclassFields.length];
        Arrays.setAll(allFieldsArray, i -> (i < superclassFields.length ? superclassFields[i] : derivedFields[i - superclassFields.length]));
        Optional<Field> requiredField = Arrays.stream(allFieldsArray).filter(field -> field.getName().toLowerCase(Locale.ROOT).equals(sortString)).findFirst();
        return requiredField.isPresent();
    }

    public static boolean isValidSortableField(String sortString, Class<?>... classType) {
        List<Field> fields = Stream.of(classType).map(Class::getDeclaredFields).map(Arrays::asList).reduce((first, seccond) -> {
            List<Field> result = new ArrayList<>();
            result.addAll(first);
            result.addAll(seccond);
            return result;
        }).orElse(new ArrayList<>()).stream().filter((element) -> element.getAnnotation(SortableField.class) != null).collect(Collectors.toList());

        String[] splitedSortString = sortString.split("\\.");
        String proccessedSortString = splitedSortString.length > 0 ? splitedSortString[splitedSortString.length - 1] : sortString;

        for (Field field : fields) {
            if (field.getAnnotation(SortableField.class).forField().equalsIgnoreCase(proccessedSortString) ||
                    field.getName().equalsIgnoreCase(proccessedSortString)) {
                return true;
            }
        }
        return false;
    }
}
