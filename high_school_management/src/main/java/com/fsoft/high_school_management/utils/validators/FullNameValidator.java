package com.fsoft.high_school_management.utils.validators;

import com.fsoft.high_school_management.utils.validators.annotations.FullName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FullNameValidator implements ConstraintValidator<FullName, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.matches("^[\\p{L} .'-]*$");
    }
}
