package com.fsoft.high_school_management.utils.validators.annotations;

import com.fsoft.high_school_management.utils.validators.FullNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = FullNameValidator.class)
public @interface FullName {
    String message() default "Full name format is invalid.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
