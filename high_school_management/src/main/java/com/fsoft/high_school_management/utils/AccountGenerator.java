package com.fsoft.high_school_management.utils;

import com.fsoft.high_school_management.dto.common.PersonRequestDTO;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AccountGenerator {

    private IAccountRepository accountRepository;

    @Autowired
    public void setAccountRepository(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account generate(PersonRequestDTO personDto) {
        final String DEFAULT_PASSWORD = "Admin123*";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        String fullName = personDto.getFullName();
        String[] name = fullName.split(" ");
        StringBuilder usernameBuilder = new StringBuilder();
        for (int i = 0; i < name.length; i++) {
            if (i == name.length - 1){
                usernameBuilder.insert(0,name[i].toLowerCase());
                break;
            }
            usernameBuilder.append(name[i].toLowerCase().charAt(0));
        }
        String username = usernameBuilder.toString();
        Long count = accountRepository.countByUsernameStartingWith(username);
        if (count != 0){
            username = usernameBuilder.append(count + 1).toString();
        }
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(bCryptPasswordEncoder.encode(DEFAULT_PASSWORD));
        account.setActive(true);
        return accountRepository.save(account);
    }
}
