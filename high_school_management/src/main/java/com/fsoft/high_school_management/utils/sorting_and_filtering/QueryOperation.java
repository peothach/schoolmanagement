package com.fsoft.high_school_management.utils.sorting_and_filtering;

import org.springframework.util.Assert;

public enum QueryOperation {

    EQUAL, LIKE, NEGATION, GREATER_THAN, LESS_THAN, SORT_ASC, SORT_DESC;

    /**
     *  Based on the input string, we will have the QueryOperation
     *
     * @param input String
     * @return SearchOperation
     */
    public static QueryOperation getSimpleOperation(String input) {
        Assert.notNull(input, "The argument is not valid");
        switch (input) {
            case "~":
                return EQUAL;
            case ":":
                return LIKE;
            case "!":
                return NEGATION;
            case ">":
                return GREATER_THAN;
            case "<":
                return LESS_THAN;
            case "+":
            case " ": // + is encoded in query strings as a space
                return SORT_ASC;
            case "-":
                return SORT_DESC;
            default:
                return null;
        }
    }
}
