package com.fsoft.high_school_management.listener;

@FunctionalInterface
public interface IActiveSessionChangingListener {

    void notifyActiveSessionChanging();
}
