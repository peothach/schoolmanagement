package com.fsoft.high_school_management.listener;

import com.fsoft.high_school_management.service.IActiveSessionManager;
import static com.fsoft.high_school_management.utils.Constants.STOMP_AUTHORIZATION_HEADER;
import com.fsoft.high_school_management.utils.JwtUtils;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class STOMPSessionEventListener implements IActiveSessionChangingListener {

    private SimpMessagingTemplate messagingTemplate;

    private IActiveSessionManager activeSessionManager;

    private JwtUtils jwtUtils;

    @Autowired
    public void setActiveSessionManager(IActiveSessionManager activeSessionManager) {
        this.activeSessionManager = activeSessionManager;
    }

    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setJwtUtils(JwtUtils jwtUtils) {
        this.jwtUtils = jwtUtils;
    }

    /**
     * This method is used for registering this bean as a handler for ActiveSessionChanging event
     * after it has been created
     */
    @PostConstruct
    public void postConstructBean(){
        activeSessionManager.registerListener(this);
    }

    /**
     * This method is used for removing this bean whose role as a handler for ActiveSessionChanging event
     * before it is almost destroyed
     */
    @PreDestroy
    public void beforeDestroyBean(){
        activeSessionManager.removeListener(this);
    }

    /**
     * This method select username and ip address when a client try to establish a STOMP connection
     * to keep knowing who is currently online in our system via an IActiveSessionManager implementation
     * @param event SessionConnectEvent
     */
    @EventListener
    private void handleSessionConnected(SessionConnectEvent event) {
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String token = accessor.getFirstNativeHeader(STOMP_AUTHORIZATION_HEADER);
        String username = jwtUtils.getUsername(token);
        String sessionId = accessor.getSessionId();
        activeSessionManager.addSession(sessionId, username);
    }

    /**
     * This method select username and ip address when a client try to establish a STOMP connection
     * to keep knowing who is currently online in our system via an IActiveSessionManager implementation
     * @param event SessionDisconnectEvent
     */
    @EventListener
    private void handleSessionDisconnect(SessionDisconnectEvent event) {
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String sessionId = accessor.getSessionId();
        activeSessionManager.removeSession(sessionId);
    }

    /**
     * This method is used to handle any ActiveSessionChanging event which in more detail will send a set of online usernames to the public channel named /topic/active
     * for every client can consume.
     */
    @Override
    public void notifyActiveSessionChanging() {
        Set<String> activeUsers = activeSessionManager.getAll();
        System.out.println(activeUsers);
        messagingTemplate.convertAndSend("/topic/active", activeUsers);
    }
}
