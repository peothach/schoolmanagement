package com.fsoft.high_school_management.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fsoft.high_school_management.entity.common.Person;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "Administrators")
public class Administrator extends Person {

    @OneToOne(targetEntity = Account.class, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
