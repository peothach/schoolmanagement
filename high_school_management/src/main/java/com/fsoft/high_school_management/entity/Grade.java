package com.fsoft.high_school_management.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fsoft.high_school_management.entity.common.AuditableEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.SortableField;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Indexed
@Table(name = "Grades")
public class Grade extends AuditableEntity {

    @Id
    @SortableField
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @SortableField
    @Field
    @Column(nullable = true)
    private Float mark;

    @ManyToOne(targetEntity = Student.class, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "student_id")
    @IndexedEmbedded(targetElement = Student.class)
    private Student student;

    @ManyToOne(targetEntity = Subject.class, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "subject_id")
    @IndexedEmbedded(targetElement = Subject.class)
    private Subject subject;

    public Grade(){
    }

    public Grade(Account createdBy, Date createdDate, Account lastModifiedBy, Date lastModifiedDate, Boolean active, Float mark, Student student, Subject subject) {
        super(createdBy, createdDate, lastModifiedBy, lastModifiedDate, active);
        this.mark = mark;
        this.student = student;
        this.subject = subject;
    }

    public Grade(Float mark, Student student, Subject subject) {
        this.mark = mark;
        this.student = student;
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
