package com.fsoft.high_school_management.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "post")
@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "content", nullable = false)
    private String content;

    @ManyToOne(targetEntity = Thread.class)
    @JoinColumn(name = "thread_id")
    private Thread thread;

    @ManyToOne(targetEntity = Account.class)
    @JoinColumn(name = "author_id")
    private Account author;

    @Column(name = "created_time", nullable = false)
    @CreationTimestamp
    private Date createdTime;

    @Column(name = "last_edited_time")
    private Date lastEditedTime;

    @Column(name = "active")
    private boolean active;

    public Post() {
    }

    public Post(Long id, String content, Thread thread, Account author, Date createdTime, Date lastEditedTime, boolean active) {
        this.id = id;
        this.content = content;
        this.thread = thread;
        this.author = author;
        this.createdTime = createdTime;
        this.lastEditedTime = lastEditedTime;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastEditedTime() {
        return lastEditedTime;
    }

    public void setLastEditedTime(Date lastEditedTime) {
        this.lastEditedTime = lastEditedTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}