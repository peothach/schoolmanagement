package com.fsoft.high_school_management.entity.common;

import com.fsoft.high_school_management.entity.Account;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.bridge.builtin.BooleanBridge;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
public abstract class AuditableEntity {

    @ManyToOne
    @JoinColumn(name = "created_by_account_id")
    protected Account createdBy;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    protected Date createdDate;

    @ManyToOne
    @JoinColumn(name = "last_modified_by_account_id")
    protected Account lastModifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    protected Date lastModifiedDate;

    @FieldBridge(impl = BooleanBridge.class)
    @Field
    @Column(nullable = false)
    protected Boolean active;

    public AuditableEntity(Account createdBy, Date createdDate, Account lastModifiedBy, Date lastModifiedDate, Boolean active) {
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.active = active;
    }

    protected AuditableEntity() {
    }

    public Account getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Account createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Account getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Account lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
