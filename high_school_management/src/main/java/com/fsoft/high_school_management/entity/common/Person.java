package com.fsoft.high_school_management.entity.common;

import com.fsoft.high_school_management.utils.analyzer.tokenfilterfactory.VietnameseFilterFactory;
import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.standard.StandardFilterFactory;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.sql.Date;

@MappedSuperclass
@AnalyzerDef(name = "customAnalyzer",
        tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = StandardFilterFactory.class),
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = VietnameseFilterFactory.class)
        }
)
public abstract class Person extends AuditableEntity {

    @SortableField(forField = "person_id")
    @Field(name = "person_id")
    @NumericField(forField = "person_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Field(name = "fullName")
    @ContainedIn
    @Analyzer(definition = "customAnalyzer")
    @Column(nullable = false)
    protected String fullName;

    @Column(nullable = false)
    @Field(name = "birthdate")
    @DateBridge(resolution = Resolution.SECOND)
    @Analyzer(definition = "customAnalyzer")
    protected Date birthdate;


    @Field(name = "address")
    @ContainedIn
    @Analyzer(definition = "customAnalyzer")
    @Column(nullable = false)
    protected String address;


    @Field(name = "phoneNumber")
    @ContainedIn
    @Analyzer(definition = "customAnalyzer")
    @Column(nullable = false)
    protected String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Field(name = "gender")
    @Analyzer(definition = "customAnalyzer")
    protected Gender gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
