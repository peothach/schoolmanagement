package com.fsoft.high_school_management.entity.common;

public enum Gender {
    MALE,
    FEMALE,
    OTHER;

    public static Gender getGender(String gender){
        if ("male".equalsIgnoreCase(gender)){
            return Gender.MALE;
        }
        if ("female".equalsIgnoreCase(gender)){
            return Gender.FEMALE;
        }
        if ("other".equalsIgnoreCase(gender)) {
            return Gender.OTHER;
        }
        return null;
    }

}
