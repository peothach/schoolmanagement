package com.fsoft.high_school_management.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "thread")
@Entity
public class Thread {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne( targetEntity = Account.class)
    @JoinColumn( name = "author_id")
    private Account author;

    @ManyToOne( targetEntity = Subforum.class)
    @JoinColumn(name = "subforum_id")
    private Subforum subforum;

    @CreationTimestamp
    @Column(name = "created_time",nullable = false)
    private Date createdTime;

    @Column(name = "active", nullable = false)
    private boolean active;

    public Thread() {
    }

    public Thread(Long id, String title, Account author, Subforum subforum, Date createdTime, boolean active) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.subforum = subforum;
        this.createdTime = createdTime;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public Subforum getSubforum() {
        return subforum;
    }

    public void setSubforum(Subforum subforum) {
        this.subforum = subforum;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}