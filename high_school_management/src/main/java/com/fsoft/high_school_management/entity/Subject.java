package com.fsoft.high_school_management.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fsoft.high_school_management.entity.common.AuditableEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.SortableField;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Indexed
@Table(name = "Subjects")
public class Subject extends AuditableEntity {

    @Id
    @SortableField
    @Field(name="subject_id")
    @NumericField(forField="subject_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Field
    @SortableField
    @Column(nullable = false, unique = true, length = 32)
    @Analyzer(definition = "customAnalyzer")
    private String name;

    public Subject() {
    }

    public Subject(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Subject(String name, Account createdBy, Date createdDate, Account lastModifiedBy, Date lastModifiedDate, Boolean active) {
        super(createdBy, createdDate, lastModifiedBy, lastModifiedDate, active);
        this.name = name;
    }

    public Subject(Long id, String name, Account createdBy, Date createdDate, Account lastModifiedBy, Date lastModifiedDate, Boolean active) {
        super(createdBy, createdDate, lastModifiedBy, lastModifiedDate, active);
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
