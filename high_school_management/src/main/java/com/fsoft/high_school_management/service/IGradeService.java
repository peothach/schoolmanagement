package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Grade;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IGradeService extends IGenericService<Grade>{

    void deactivateAllGradesOfAStudent(Long studentId);

    @Transactional(propagation = Propagation.REQUIRED)
    void deactivateAllGradesOfASubject(Long subjectId);

    Page<Grade> getGradesOfAStudent(Long classroomId, String subjectName, String sortString, boolean order, Integer pageSize, Integer pageIndex);

    Page<Grade> getAllGradesOfAClassroomInOnlyOneSubject(Long classroomId, Long subjectId, String searchKeyword, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex);

    Page<Grade> getAllGradesOfAClassroomInAllSubjects(Long classroomId, String searchKeyword, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex);
}