package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.ChatMessageRequestBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.ChatMessage;
import com.fsoft.high_school_management.entity.Conversation;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IChatMessageRepository;
import com.fsoft.high_school_management.repository.IConversationAccountRepository;
import com.fsoft.high_school_management.repository.IConversationRepository;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class ChatMessageService {

    private IChatMessageRepository chatMessageRepository;

    private IConversationRepository conversationRepository;

    private IConversationAccountRepository conversationAccountRepository;

    private IAccountRepository accountRepository;

    @Autowired
    public ChatMessageService(IChatMessageRepository chatMessageRepository, IConversationRepository conversationRepository, IConversationAccountRepository conversationAccountRepository, IAccountRepository accountRepository) {
        this.chatMessageRepository = chatMessageRepository;
        this.conversationRepository = conversationRepository;
        this.conversationAccountRepository = conversationAccountRepository;
        this.accountRepository = accountRepository;
    }

    public ChatMessage save(ChatMessageRequestBody chatMessageBody) {
        Objects.requireNonNull(chatMessageBody.getConversationId());
        UserDetails userDetail = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountRepository.findByUsernameIgnoreCase(userDetail.getUsername()).orElseThrow(RecordNotFoundException::new);
        Conversation conversation = conversationAccountRepository.findByConversation_IdAndAccount_Id(chatMessageBody.getConversationId(), account.getId()).orElseThrow(RecordNotFoundException::new);
        return chatMessageRepository.save(new ChatMessage(account, conversation, chatMessageBody.getContent(), new Date(), true));
    }

    public Page<ChatMessage> findAllByConversation(Long id, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return chatMessageRepository.findAllByRecipientId(id, pageable);
    }

    public List<ChatMessage> findAllByConversation(Long id){
        return chatMessageRepository.findAllByRecipientId(id);
    }
}
