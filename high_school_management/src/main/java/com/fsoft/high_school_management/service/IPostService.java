package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.dto.PostDto;
import com.fsoft.high_school_management.entity.Post;
import org.springframework.data.domain.Page;

public interface IPostService {
    Post save(PostDto dto);

    Post find(Long id);

    Page<Post> findByThread(Long threadId, int page, int size);

    Post save(Long id, PostDto dto);

    void delete(Long id);
}
