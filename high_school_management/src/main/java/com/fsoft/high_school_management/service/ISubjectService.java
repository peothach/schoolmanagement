package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Subject;
import org.springframework.data.domain.Page;

public interface ISubjectService extends IGenericService<Subject>{
    Page<Subject> getAll(String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex);
}
