package com.fsoft.high_school_management.service;

import org.springframework.security.core.Authentication;

public interface ISecurityService {
    boolean isMyPage(Authentication authentication, String api, Long id);
    boolean isAccessible(Authentication authentication, Long classroomId, Long subjectId);
}
