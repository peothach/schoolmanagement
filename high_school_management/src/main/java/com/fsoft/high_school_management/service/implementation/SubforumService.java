package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.SubforumDto;
import com.fsoft.high_school_management.entity.Subforum;
import com.fsoft.high_school_management.entity.Thread;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.ISubforumRepository;
import com.fsoft.high_school_management.repository.IThreadRepository;
import com.fsoft.high_school_management.service.ISubforumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubforumService implements ISubforumService {

    @Autowired
    ISubforumRepository subforumRepository;
    @Autowired
    IThreadRepository threadRepository;

    public Subforum save(SubforumDto dto) {
        Subforum saved = new Subforum(null, dto.getName(), dto.getDescription(), null, true);
        saved = subforumRepository.save(saved);
        return saved;
    }

    public Subforum find(Long id) {
        return subforumRepository.findById(id).orElseThrow(RecordNotFoundException::new);
    }

    public List<Subforum> find() {
        return subforumRepository.findAll().stream().filter((Subforum::isActive)).collect(Collectors.toList());
    }

    public void delete(Long id) {
        find(id);
        subforumRepository.deactivateById(id);
        List<Thread> threads = threadRepository.findAllBySubforumIdAndActiveTrue(id);
        threads.forEach((thread) ->{
            threadRepository.deactivateById(thread.getId());
        });

    }

    public Subforum save(SubforumDto dto, Long id) {
        Subforum saved = find(id);
        saved.setName(dto.getName());
        saved.setDescription(dto.getDescription());
        return subforumRepository.save(saved);
    }

    public SubforumDto countThread(SubforumDto dto){
        dto.setThreadCount(threadRepository.countBySubforumIdAndActiveTrue(dto.getId()));
        return dto;
    }
}
