package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dao.IGradeDAO;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.common.Person;
import com.fsoft.high_school_management.exception.NotValidSortingFieldName;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGradeRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.service.IGradeService;
import static com.fsoft.high_school_management.utils.ReflectionUtils.checkValidField;
import static com.fsoft.high_school_management.utils.ReflectionUtils.isValidSortableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GradeService extends GenericService<Grade> implements IGradeService {

    private IGradeDAO gradeDAO;

    private IGradeRepository gradeRepository;

    private IStudentRepository studentRepository;

    private ISubjectRepository subjectRepository;

    @Autowired
    public void setGradeDAO(IGradeDAO gradeDAO) {
        this.gradeDAO = gradeDAO;
    }

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setGradeRepository(IGradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deactivateAllGradesOfAStudent(Long studentId) {
        Student student = this.studentRepository.findById(studentId).orElseThrow(RecordNotFoundException::new);
        this.gradeRepository.deactivateByStudentId(student.getId());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deactivateAllGradesOfASubject(Long subjectId) {
        Student student = this.studentRepository.findById(subjectId).orElseThrow(RecordNotFoundException::new);
        this.gradeRepository.deactivateByStudentId(student.getId());
    }

    @Override
    public Page<Grade> getGradesOfAStudent(Long classroomId, String subjectName, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        sortString = sortString == null ? "id" : sortString;
        if (sortString != null && !checkValidField(sortString, Grade.class)) {
            throw new NotValidSortingFieldName();
        }
        return this.gradeDAO.getSearch(classroomId, subjectName, sortString, order, pageIndex, pageSize);
    }

    @Override
    public Page<Grade> getAllGradesOfAClassroomInOnlyOneSubject(Long classroomId, Long subjectId, String searchKeyword, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        sortString = sortString == null ? "student.person_id" : sortString;
        if (!isValidSortableField(sortString, Grade.class, Person.class)) {
            throw new NotValidSortingFieldName();
        }
        return this.gradeDAO.getClassroomGradesSearch(classroomId, subjectId, searchKeyword, active, sortString, order, pageIndex, pageSize);
    }

    @Override
    public Page<Grade> getAllGradesOfAClassroomInAllSubjects(Long classroomId, String searchKeyword, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        sortString = sortString == null ? "student.person_id" : sortString;
        if (!isValidSortableField(sortString, Grade.class, Person.class)) {
            throw new NotValidSortingFieldName();
        }
        int totalSubjects = Math.toIntExact(subjectRepository.count());
        return this.gradeDAO.getClassroomGradesSearch(classroomId, null, searchKeyword, active, sortString, order, pageIndex, totalSubjects * pageSize);
    }
}