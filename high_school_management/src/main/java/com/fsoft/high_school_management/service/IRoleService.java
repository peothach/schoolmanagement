package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Role;

import java.util.List;

public interface IRoleService extends IGenericService<Role> {
    List<Role> getAllRoles();

    Long getPersonId(Role role, String username);
}
