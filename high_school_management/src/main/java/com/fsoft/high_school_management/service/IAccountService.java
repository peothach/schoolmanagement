package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.dto.ChangingPasswordRequestBody;
import com.fsoft.high_school_management.dto.CheckingOldPasswordRequestBody;
import java.util.List;

public interface IAccountService {
    Long getAccountId(String username);

    boolean changePassword(ChangingPasswordRequestBody changingPasswordRequestBody);

    boolean checkPassword(CheckingOldPasswordRequestBody changingPasswordRequestBody);

    List<String> getAllAdministratorNames();
}
