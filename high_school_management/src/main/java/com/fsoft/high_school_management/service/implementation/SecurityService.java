package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.TeachersClassrooms;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.exception.RoleRequestIsNotMatch;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.repository.ITeachersClassroomsRepository;
import com.fsoft.high_school_management.service.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityService implements ISecurityService {
    private static final String TEACHER = "TEACHER";
    private static final String ADMIN = "ADMINISTRATOR";
    private static final String STUDENT = "STUDENT";

    IStudentRepository studentRepository;
    ITeacherRepository teacherRepository;
    ITeachersClassroomsRepository teachersClassroomsRepository;


    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setTeacherRepository(ITeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Autowired
    public void setTeachersClassroomsRepository(ITeachersClassroomsRepository teachersClassroomsRepository) {
        this.teachersClassroomsRepository = teachersClassroomsRepository;
    }

    public boolean isMyPage(Authentication authentication, String api, Long id) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String role = userDetails.getAuthorities().stream().findFirst().orElseThrow(RoleRequestIsNotMatch::new).getAuthority();
        if (ADMIN.equals(role)) {
            return true;
        }
        if (STUDENT.equals(role) && STUDENT.equals(api)) {
            Student student = studentRepository.findByAccount_Username(userDetails.getUsername())
                    .orElseThrow(RecordNotFoundException::new);
            return student.getId() == id;
        }
        if (TEACHER.equals(role) && TEACHER.equals(api)) {
            Teacher teacher = teacherRepository.findByAccount_Username(userDetails.getUsername())
                    .orElseThrow(RecordNotFoundException::new);
            return teacher.getId() == id;
        }
        return false;
    }

    public boolean isAccessible(Authentication authentication, Long classroomId, Long subjectId) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String role = userDetails.getAuthorities().stream().findFirst().orElseThrow(RoleRequestIsNotMatch::new).getAuthority();
        if (ADMIN.equals(role)) {
            return true;
        }
        if (!TEACHER.equals(role)) {
            return false;
        }
        Teacher teacher = teacherRepository.findByAccount_Username(userDetails.getUsername())
                .orElseThrow(RecordNotFoundException::new);
        List<TeachersClassrooms> teachersClassrooms = teachersClassroomsRepository.findAllByTeacherIdAndActiveTrue(teacher.getId());
        List<Long> classroomIds = teachersClassrooms.stream()
                .map((teacherClassroom) -> teacherClassroom.getClassroom().getId())
                .collect(Collectors.toList());
        return (teacher.getSubject().getId() == subjectId && classroomIds.contains(classroomId));
    }
}
