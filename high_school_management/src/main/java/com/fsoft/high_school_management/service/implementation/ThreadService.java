package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.ThreadDto;
import com.fsoft.high_school_management.entity.Post;
import com.fsoft.high_school_management.entity.Thread;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IPostRepository;
import com.fsoft.high_school_management.repository.IThreadRepository;
import com.fsoft.high_school_management.service.ISubforumService;
import com.fsoft.high_school_management.service.IThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ThreadService implements IThreadService {
    @Autowired
    IThreadRepository threadRepository;
    @Autowired
    IAccountRepository accountRepository;
    @Autowired
    ISubforumService subforumService;
    @Autowired
    IPostRepository postRepository;

    public Thread save(ThreadDto dto) {
        Thread saved = new Thread(null,
                dto.getTitle(),
                accountRepository.findByUsernameIgnoreCase(dto.getAuthorUsername()).orElseThrow(RecordNotFoundException::new),
                subforumService.find(dto.getSubforum().getId()),
                new Date(),
                true);
        saved = threadRepository.save(saved);
        return saved;
    }

    public Thread find(Long id) {
        return threadRepository.findById(id).orElseThrow(RecordNotFoundException::new);
    }

    public Page<Thread> find(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return threadRepository.findAllByActiveTrue(pageable);
    }

    public Page<Thread> findBySubforum(Long subforumId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return threadRepository.findAllBySubforumIdAndActiveTrue(subforumId, pageable);
    }

    public Thread save(Long id, ThreadDto dto) {
        Thread saved = find(id);
        saved.setTitle(dto.getTitle());
        saved.setSubforum(subforumService.find(dto.getSubforum().getId()));
        saved = threadRepository.save(saved);
        return saved;
    }

    public void delete(Long id) {
        find(id);
        threadRepository.deactivateById(id);
        List<Post> posts = postRepository.findByThreadIdAndActiveTrue(id);
        posts.forEach((post) -> {
            postRepository.deactivateById(post.getId());
        });
    }

    public ThreadDto countPost(ThreadDto threadDto){
        threadDto.setPostCount(postRepository.countByThreadIdAndActiveTrue(threadDto.getId()));
        return threadDto;
    }
}
