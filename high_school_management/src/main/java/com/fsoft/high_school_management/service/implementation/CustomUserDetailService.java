package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.CustomUserDetail;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IAccountsRolesRepository;
import com.fsoft.high_school_management.repository.IRoleRepository;
import com.fsoft.high_school_management.utils.Constants;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailService implements UserDetailsService {

    private IAccountRepository accountRepository;

    private IRoleRepository roleRepository;

    private IAccountsRolesRepository accountsRolesRepository;

    @Autowired
    public void setAccountRepository(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Autowired
    public void setRoleRepository(IRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Autowired
    public void setAccountsRolesRepository(IAccountsRolesRepository accountsRolesRepository) {
        this.accountsRolesRepository = accountsRolesRepository;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = this.accountRepository.findByUsernameIgnoreCase(username).orElseThrow(() -> {
            throw new UsernameNotFoundException(Constants.USERNAME_NOT_EXIST_MESSAGE);
        });
        List<String> roleList = this.accountsRolesRepository.findByAccount_Id(account.getId())
                                                            .stream()
                                                            .map(accountsRoles -> accountsRoles.getRole().getName())
                                                            .collect(Collectors.toList());
        return new CustomUserDetail(account, roleList);
    }
}
