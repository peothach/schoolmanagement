package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.entity.Role;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IRoleRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.service.IRoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends GenericService<Role> implements IRoleService {

    private IRoleRepository roleRepository;

    private IStudentRepository studentRepository;

    private ITeacherRepository teacherRepository;

    @Autowired
    public void setRoleRepository(IRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setTeacherRepository(ITeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public List<Role> getAllRoles(){
        return this.roleRepository.findAll();
    }

    @Override
    public Long getPersonId(Role role, String username){
        String name = role.getName();
        switch (name){
            case "ADMINISTRATOR":{
                return 1L;
            }
            case "STUDENT":{
                return this.studentRepository.findByAccount_Username(username).orElseThrow(RecordNotFoundException::new).getId();
            }
            case "TEACHER":{
                return this.teacherRepository.findByAccount_Username(username).orElseThrow(RecordNotFoundException::new).getId();
            }
        }
        throw new RecordNotFoundException();
    }
}
