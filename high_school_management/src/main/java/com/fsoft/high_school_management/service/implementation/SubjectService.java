package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dao.ISubjectDAO;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.exception.NotValidSortingFieldName;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGradeRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.service.ISubjectService;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.fsoft.high_school_management.utils.ReflectionUtils.checkValidField;

@Service
public class SubjectService extends GenericService<Subject> implements ISubjectService {

    private IGradeRepository gradeRepository;

    private ISubjectDAO subjectDAO;

    private IStudentRepository studentRepository;

    private AuditableEntityHelper<Grade> auditableEntityHelper;

    private ISubjectRepository subjectRepository;

    private ITeacherRepository teacherRepository;

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Autowired
    public void setSubjectDAO(ISubjectDAO subjectDAO) {
        this.subjectDAO = subjectDAO;
    }

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setGradeRepository(IGradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Grade> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @Autowired
    public void setTeacherRepository(ITeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public Page<Subject> getAll(String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        if(sortString != null && !checkValidField(sortString, Subject.class)){
            throw new NotValidSortingFieldName();
        }
        return subjectDAO.getSearch(searchKey, active, sortString == null ? "id" : sortString, order, pageIndex, pageSize);
    }

    @Override
    public Subject create(Subject entity) {
        Subject subject = super.create(entity);
        List<Student> students = this.studentRepository.findAll();
        Grade grade = null;
        for(Student student: students) {
            grade = new Grade(null, student, subject);
            grade = auditableEntityHelper.setupAuditableFields(grade);
            this.gradeRepository.save(grade);
        }
        return subject;
    }

    @Override
    public void deleteById(Long id) {
        if (id == null){
            throw new RecordNotFoundException();
        }
        subjectRepository.findById(id).orElseThrow(RecordNotFoundException::new);
        subjectRepository.deactivateById(id);
        List<Teacher> teachers = teacherRepository.findAllBySubjectIdAndActiveTrue(id).stream().
                peek((teacher -> teacher.setSubject(null))).collect(Collectors.toList());
        teacherRepository.saveAll(teachers);
    }

    @Override
    public void deleteByIds(Long[] ids) {
        for (Long id : ids){
            deleteById(id);
        }
    }
}
