package com.fsoft.high_school_management.service;

import org.springframework.data.domain.Page;


public interface IGenericService<E> {

    E create(E entity);

    E getOne(Long id);

    Page<E> getAll(String filter, String sort, Integer pageSize, Integer pageIndex);

    E update(E entity);

    void deleteById(Long id);

    void deleteByIds(Long[] ids);
}
