package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.dto.StudentGradeResponseBody;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

public interface IExcelGeneratingService {
    ByteArrayInputStream exportGradesOfALlSubjectInAClassroomToExcel(List<StudentGradeResponseBody> studentsGrades, Long classroomId) throws IOException;
    ByteArrayInputStream exportGradesOfASubjectInAClassroomToExcel(List<StudentGradeResponseBody> studentsGrades, Long classroomId, Long subjectId) throws IOException;
}
