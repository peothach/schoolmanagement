package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.PostDto;
import com.fsoft.high_school_management.entity.Post;
import com.fsoft.high_school_management.entity.Thread;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IPostRepository;
import com.fsoft.high_school_management.service.IPostService;
import com.fsoft.high_school_management.service.IThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PostService implements IPostService {
    @Autowired
    IPostRepository postRepository;
    @Autowired
    IThreadService threadService;
    @Autowired
    IAccountRepository accountRepository;

    public Post save(PostDto dto) {
        Thread thread;
        if (dto.getThread().getId() != null) {
            thread = threadService.find(dto.getThread().getId());
        } else {
            dto.getThread().setAuthorUsername(dto.getAuthorUsername());
            thread = threadService.save(dto.getThread());
        }

        Post saved = new Post(null,
                dto.getContent(),
                thread,
                accountRepository.findByUsernameIgnoreCase(dto.getAuthorUsername()).orElseThrow(RecordNotFoundException::new),
                new Date(),
                null,
                true);
        saved = postRepository.save(saved);
        return saved;
    }

    public Post find(Long id) {
        return postRepository.findById(id).orElseThrow(RecordNotFoundException::new);
    }

    public Page<Post> findByThread(Long threadId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return postRepository.findByThreadIdAndActiveTrue(threadId, pageable);
    }

    public Post save(Long id, PostDto dto) {
        Post saved = find(id);
        saved.setContent(dto.getContent());
        saved.setLastEditedTime(new Date());
        saved = postRepository.save(saved);
        return saved;
    }

    public void delete(Long id) {
        find(id);
        postRepository.deactivateById(id);
    }
}
