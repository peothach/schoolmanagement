package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.ChangingPasswordRequestBody;
import com.fsoft.high_school_management.dto.CheckingOldPasswordRequestBody;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IAccountsRolesRepository;
import com.fsoft.high_school_management.service.IAccountService;
import com.fsoft.high_school_management.utils.Constants;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements IAccountService {

    private IAccountRepository accountRepository;

    private IAccountsRolesRepository accountsRolesRepository;

    @Autowired
    public void setAccountRepository(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Autowired
    public void setAccountsRolesRepository(IAccountsRolesRepository accountsRolesRepository) {
        this.accountsRolesRepository = accountsRolesRepository;
    }

    @Override
    public Long getAccountId(String username){
        return this.accountRepository.findByUsernameIgnoreCase(username).orElseThrow(RecordNotFoundException::new).getId();
    }

    @Override
    public boolean changePassword(ChangingPasswordRequestBody changingPasswordRequestBody){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(bCryptPasswordEncoder.matches(changingPasswordRequestBody.getOldPassword(), principal.getPassword())){
            Account account = this.accountRepository.findByUsernameIgnoreCase(principal.getUsername()).orElseThrow(RecordNotFoundException::new);
            account.setPassword(bCryptPasswordEncoder.encode(changingPasswordRequestBody.getNewPassword()));
            this.accountRepository.save(account);
            return true;
        }
        else {
            throw new IllegalArgumentException("Password is mismatch");
        }
    }

    @Override
    public boolean checkPassword(CheckingOldPasswordRequestBody changingPasswordRequestBody) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return bCryptPasswordEncoder.matches(changingPasswordRequestBody.getOldPassword(), principal.getPassword()) ? true : false;
    }

    @Override
    public List<String> getAllAdministratorNames(){
        return this.accountsRolesRepository.findUsernamesByRoleName(Constants.ADMINISTRATOR).orElseThrow(RecordNotFoundException::new);
    }
}
