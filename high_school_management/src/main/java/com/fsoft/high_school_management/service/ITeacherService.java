package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.utils.sorting_and_filtering.TeacherFilter;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ITeacherService extends IGenericService<Teacher> {
    void setClassroom(Long teacherId, List<Long> classroomsIds);
    Page<Teacher> getAllActive(TeacherFilter teacherFilter,
                               Optional<Integer> pageSize,
                               Optional<Integer> pageIndex,
                               Optional<String> sort,
                               Optional<String> order);
}
