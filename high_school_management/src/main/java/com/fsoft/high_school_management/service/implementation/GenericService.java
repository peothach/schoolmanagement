package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.QueryCriteria;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGenericRepository;
import com.fsoft.high_school_management.service.IGenericService;
import com.fsoft.high_school_management.utils.Constants;
import static com.fsoft.high_school_management.utils.Constants.UNKNOWN_EXCEPTION_MESSAGE;
import static com.fsoft.high_school_management.utils.sorting_and_filtering.QueryCriteriaParser.parseOperationString;
import static com.fsoft.high_school_management.utils.sorting_and_filtering.SortHelper.combineSort;
import static com.fsoft.high_school_management.utils.sorting_and_filtering.SortHelper.generateSortList;
import com.fsoft.high_school_management.utils.sorting_and_filtering.SpecificationHelper;
import static com.fsoft.high_school_management.utils.sorting_and_filtering.SpecificationHelper.combineSpecification;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 *
 * @param <E> stands for Entity
 */
public abstract class GenericService<E> implements IGenericService<E> {



    private IGenericRepository<E> repository;

    @Autowired
    public void setRepository(IGenericRepository<E> repository) {
        this.repository = repository;
    }

    /**
     * This method is used to get one record in database based on the id of an domain model object
     *
     * @param entity E is the domain model object
     * @return E is domain model object itself which is has ben saved in database
     */
    @Override
    public E create(E entity) {
        Assert.notNull(entity, Constants.ENTITY_OBJECT_NOT_VALID_MESSAGE + " for creating operation");
        return repository.save(entity);
    }

    /**
     * This method is used to get one record in database based on the id of an domain model object
     *
     * @param id Long
     * @return E is domain model object which is saved in database
     */
    @Override
    public E getOne(Long id) {
        Assert.notNull(id, Constants.ID_ID_NULL_MESSAGE);
        Optional<E> entity = repository.findById(id);
        if(entity.isPresent()) {
            return entity.get();
        }
        else {
            throw new RecordNotFoundException();
        }
    }

    /**
     *
     * @param filter String is used to hold the Filter Operation from the url
     * @param sort String is used to hold the Sort Operation from the url
     * @param pageSize String is used to hold the pageSize for paging purpose
     * @param pageIndex String is used to hold the pageIndex for paging purpose
     * @return Page<E> which store all Filtered, Sorted records and paging information
     */
    @Override
    public Page<E> getAll(String filter, String sort, Integer pageSize, Integer pageIndex) {
        Assert.notNull(pageSize, "The pageSize is null and not valid");
        Assert.notNull(pageIndex, "The pageIndex is null and not valid");
        try {
            Specification<E> specs = null;

            if (filter != null) {
                // generate Specification based on operations in the string
                // parse search string into (filedName)(operation)(value?)
                List<QueryCriteria> queryCriteria = parseOperationString(filter);
                List<SpecificationHelper<E>> specList = queryCriteria.stream().map(SpecificationHelper<E>::new).collect(Collectors.toList());
                Optional<Specification<E>> combinedSpecification = combineSpecification(specList);
                if (combinedSpecification.isPresent())
                    specs = combinedSpecification.get();
                else
                    throw new IllegalArgumentException(Constants.CRITERIA_NOT_VALID_MESSAGE);
            }

            Sort sorts = null;
            if(sort != null){
                // generate Sort based on operations in the string
                List<QueryCriteria> queryCriteria = parseOperationString(sort);
                List<Sort> sortList = generateSortList(queryCriteria);
                Optional<Sort> combineSort = combineSort(sortList);
                if (combineSort.isPresent())
                    sorts = combineSort.get();
                else
                    throw new IllegalArgumentException(Constants.CRITERIA_NOT_VALID_MESSAGE);
            }

            Pageable pageable = sorts!= null ? PageRequest.of(pageIndex, pageSize, sorts) : PageRequest.of(pageIndex, pageSize);
            return (specs != null) ? repository.findAll(specs, pageable) : repository.findAll(pageable);
        }
        catch(Exception e){
            throw new IllegalArgumentException(Constants.ILLEGAL_ARGUMENTS_MESSAGE);
        }
    }

    /**
     * This method is used to update an record in database
     *
     * @param entity E is entity object we wanna update it.
     * @return E is domain model object itself which is has ben saved in database
     */
    @Override
    public E update(E entity) {
        Assert.notNull(entity, Constants.ENTITY_OBJECT_NOT_VALID_MESSAGE + " for updating operation");
        return repository.save(entity);
    }

    /**
     * This method is used for deleted multiple object
     *
     * @param id Long
     */
    @Override
    public void deleteById(Long id) {
        try {
            Assert.notNull(id, Constants.ID_ID_NULL_MESSAGE);
            repository.deactivateById(id);
        }
        catch (InvalidDataAccessApiUsageException exception) {
            throw new RecordNotFoundException("No record found to be deleted");
        }
        catch (Exception exception) {
            throw new RuntimeException(UNKNOWN_EXCEPTION_MESSAGE);
        }
    }

    /**
     * This method is used for deleted multiple object
     *
     * @param ids Ids
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deleteByIds(Long[] ids) {
        for(Long id : ids) {
            Assert.notNull(id, Constants.ID_ID_NULL_MESSAGE);
            repository.deactivateById(id);
        }
    }
}
