package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.listener.IActiveSessionChangingListener;
import java.util.Set;

public interface IActiveSessionManager {

    void addSession(String sessionId, String username);

    void removeSession(String sessionId);

    Set<String> getAll();

    void registerListener(IActiveSessionChangingListener listener);

    void removeListener(IActiveSessionChangingListener listener);

    void notifyListeners();
}
