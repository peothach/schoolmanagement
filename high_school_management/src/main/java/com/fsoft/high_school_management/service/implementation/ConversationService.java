package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.Participants;
import com.fsoft.high_school_management.entity.Account;
import com.fsoft.high_school_management.entity.Conversation;
import com.fsoft.high_school_management.entity.ConversationAccount;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IAccountRepository;
import com.fsoft.high_school_management.repository.IConversationAccountRepository;
import com.fsoft.high_school_management.repository.IConversationRepository;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class ConversationService {

    private static final String CONNECTED_CONVERSATION_NAME_TOKEN = " and ";

    private IConversationRepository conversationRepository;

    private IConversationAccountRepository conversationAccountRepository;

    private IAccountRepository accountRepository;

    @Autowired
    public ConversationService(IConversationRepository conversationRepository, IConversationAccountRepository conversationAccountRepository, IAccountRepository accountRepository) {
        this.conversationRepository = conversationRepository;
        this.conversationAccountRepository = conversationAccountRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * This function is used to check if username of the principal is exist or not in the set of participant's usernames
     * which is received from conversation creating request
     * @param participants Set<String>
     * @return
     */
    public Set<String> getAllParticipants(Set<String> participants){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String currentUsername = userDetails.getUsername();
        Set<String> outputParticipant = new HashSet<>(participants);
        if(!participants.contains(currentUsername)) {
            outputParticipant.add(currentUsername);
        }
        return outputParticipant;
    }

    /**
     * This function is used for creating a concrete name for the conversation based on the set of participant's names
     * @param participants Set<String>
     * @return
     */
    private String getConversationName(Set<String> participants){
        StringBuilder namingBuilder = new StringBuilder();
        participants.forEach(username -> namingBuilder.append(username).append(CONNECTED_CONVERSATION_NAME_TOKEN));
        namingBuilder.delete(namingBuilder.length() - CONNECTED_CONVERSATION_NAME_TOKEN.length(), namingBuilder.length());
        return namingBuilder.toString();
    }

    /**
     * This function is used for creating a conversation based on the list of participants is received from conversation creating request
     * @param participants Participants is the holder of participants in conversation creating request
     * @return
     */
    public Conversation create(Participants participants) {
        Set<String> usernames = new HashSet<>(participants.getUsernames());
        usernames = this.getAllParticipants(usernames);
        String conversationName = this.getConversationName(usernames);
        System.out.println(" Set of username "  + usernames);
        Conversation savedConversation = conversationRepository.save(new Conversation(conversationName, true));
        List<Account> accounts = new LinkedList<>();
        for (String username: usernames) {
            accounts.add(accountRepository.findByUsernameIgnoreCase(username).orElseThrow(RecordNotFoundException::new));
        }
        System.out.println(" List of account " + accounts);
        accounts.forEach(account -> conversationAccountRepository.save(new ConversationAccount(savedConversation, account,true)));
        return savedConversation;
    }

    /**
     * This function is used to check that if the combination of recipient's username and sender's username is both related to any conversation or not
     * @return
     */
    public List<Conversation> findAllByAccount() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<ConversationAccount> conversationAccounts = conversationAccountRepository.findByAccountUsername(userDetails.getUsername());
        return conversationAccounts.stream().map(ConversationAccount::getConversation).collect(Collectors.toList());
    }

    /**
     * This function is used to check that if the combination of recipient's username and sender's username is both related to any conversation or not
     * @param recipient String is recipient's username
     * @return
     */
    public Conversation findByRecipientUsername(String recipient) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String sender = userDetails.getUsername();
        List<String> participants = Arrays.asList(sender, recipient);
        int size = participants.size();
        Long number = Long.valueOf(size);
        List<Conversation> conversations = this.conversationAccountRepository.findByParticipantsUsername(participants, number);
        if(conversations.size() == 0){
            throw new RecordNotFoundException();
        }
        return conversations.get(0);
    }
}
