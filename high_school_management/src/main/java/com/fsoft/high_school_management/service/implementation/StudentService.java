package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dao.IStudentDAO;
import com.fsoft.high_school_management.entity.Grade;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.entity.common.Person;
import com.fsoft.high_school_management.exception.NotValidSortingFieldName;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IGradeRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ISubjectRepository;
import com.fsoft.high_school_management.service.IGradeService;
import com.fsoft.high_school_management.service.IStudentService;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import com.fsoft.high_school_management.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

import static com.fsoft.high_school_management.utils.ReflectionUtils.isValidSortableField;

@Service
public class StudentService extends GenericService<Student> implements IStudentService {

    private IGradeService gradeService;

    private IStudentDAO studentDAO;

    private ISubjectRepository subjectRepository;

    private IGradeRepository gradeRepository;

    private IStudentRepository studentRepository;

    private AuditableEntityHelper<Grade> auditableEntityHelper;

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setGradeService(IGradeService gradeService) {
        this.gradeService = gradeService;
    }

    @Autowired
    public void setStudentDAO(IStudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Autowired
    public void setSubjectRepository(ISubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Autowired
    public void setGradeRepository(IGradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @Autowired
    public void setDtoToAuditableEntityHelper(AuditableEntityHelper<Grade> auditableEntityHelper) {
        this.auditableEntityHelper = auditableEntityHelper;
    }

    @Override
    public Page<Student> getAll(String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        sortString = sortString == null ? "person_id" : sortString;
        if(sortString != null && !isValidSortableField(sortString, Student.class, Person.class)){
            throw new NotValidSortingFieldName();
        }
        return studentDAO.getSearch(searchKey, active, sortString, order, pageIndex, pageSize);
    }

    @Override
    public Page<Student> getAll(Long classroomId, String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex) {
        sortString = sortString == null ? "person_id" : sortString;
        if(sortString != null && !isValidSortableField(sortString, Student.class, Person.class)){
            throw new NotValidSortingFieldName();
        }
        return studentDAO.getSearch(classroomId, searchKey, active, sortString, order, pageIndex, pageSize);
    }

    @Override
    public Student create(Student entity) {
        Student student = super.create(entity);
        List<Subject> subjects = this.subjectRepository.findAll();
        Grade grade = null;
        for(Subject subject: subjects) {
            grade = new Grade(null, student, subject);
            grade = auditableEntityHelper.setupAuditableFields(grade);
            this.gradeRepository.save(grade);
        }
        return student;
    }

    @Override
    public void deleteById(Long id) {
        if (id == null){
            throw new RecordNotFoundException();
        }
        studentRepository.deactivateById(id);
        this.gradeService.deactivateAllGradesOfAStudent(id);
    }

    @Transactional
    @Override
    public void deleteByIds(Long[] ids) {
        for(Long id : ids) {
            Assert.notNull(id, Constants.ID_ID_NULL_MESSAGE);
            this.deleteById(id);
        }
    }
}
