package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.listener.IActiveSessionChangingListener;
import com.fsoft.high_school_management.service.IActiveSessionManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class ActiveSessionManager implements IActiveSessionManager {

    private List<IActiveSessionChangingListener> listeners = new ArrayList<>();

    private Map<String, String> map = new HashMap<>();

    /**
     * This method is used to add a new entry to our current online user map
     * and publish this change to event handlers
     * @param sessionId String
     * @param username String
     */
    @Override
    public void addSession(String sessionId, String username) {
        map.put(sessionId, username);
        notifyListeners();
    }

    /**
     * This method is used to add a new entry to our current online user map
     * and publish this change to event handlers
     * @param sessionId String
     */
    @Override
    public void removeSession(String sessionId) {
        map.remove(sessionId);
        notifyListeners();
    }

    /**
     * This method simply return all current online user
     * @return
     */
    @Override
    public Set<String> getAll() {
        Set<String> listUsername = new HashSet<>();
        for(Map.Entry<String, String> entry: map.entrySet()) {
            listUsername.add(entry.getValue());
        }
        return listUsername;
    }

    /**
     * This method simply register a new handler for any session changing event
     * @param listener
     */
    @Override
    public void registerListener(IActiveSessionChangingListener listener) {
        listeners.add(listener);
    }

    /**
     * This method simply remove an existing handler from list handler for session changing events
     * @param listener
     */
    @Override
    public void removeListener(IActiveSessionChangingListener listener) {
        listeners.remove(listener);
    }

    /**
     * This method is used for notify handle an session changing event has been occur, so handlers should perform some action
     */
    @Override
    public void notifyListeners() {
        listeners.forEach(IActiveSessionChangingListener::notifyActiveSessionChanging);
    }
}
