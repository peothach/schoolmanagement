package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.LiteGradeResponseBody;
import com.fsoft.high_school_management.dto.StudentGradeResponseBody;
import com.fsoft.high_school_management.service.IExcelGeneratingService;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

@Service
public class ExcelGeneratingService implements IExcelGeneratingService {

    @Override
    public ByteArrayInputStream exportGradesOfALlSubjectInAClassroomToExcel(List<StudentGradeResponseBody> studentsGrades, Long classroomId) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            String[] subjectName = getSubjectsName(studentsGrades);
            Sheet sheet = workbook.createSheet("Grades_Classroom_" + classroomId);
            writeStudentsGradesTitle(workbook, sheet, "List grades of all subjects in classroom " + classroomId);
            writeStudentsGradesHeader(workbook, sheet, subjectName);
            writeStudentsGradesContent(sheet, studentsGrades);
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    @Override
    public ByteArrayInputStream exportGradesOfASubjectInAClassroomToExcel(List<StudentGradeResponseBody> studentsGrades, Long classroomId, Long subjectId) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            String[] subjectName = getSubjectsName(studentsGrades);
            Sheet sheet = workbook.createSheet("Grades_Classroom_" + classroomId);
            writeStudentsGradesTitle(workbook, sheet, "List grades of subjects " + subjectId + " in classroom " + classroomId);
            writeStudentsGradesHeader(workbook, sheet, subjectName);
            writeStudentsGradesContent(sheet, studentsGrades);
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    private String[] getSubjectsName(List<StudentGradeResponseBody> studentsGrades){
        List<String> subjectsName = studentsGrades.get(0).getGradeResponseBodyList().stream().map(LiteGradeResponseBody::getSubjectName).collect(Collectors.toList());
        List<String> listColumns = new LinkedList<>();
        listColumns.add("Student_Id");
        listColumns.add("Student_Name");
        listColumns.add("Student_Birthdate");
        listColumns.addAll(subjectsName);
        return listColumns.toArray(new String[0]);
    }

    private void writeStudentsGradesTitle(Workbook workbook, Sheet sheet, String title){
        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeight((short) 200);
        titleFont.setColor(IndexedColors.RED.getIndex());

        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setFont(titleFont);

        Row titleRow = sheet.createRow(0);
        Cell cell = titleRow.createCell(0);
        cell.setCellValue(title);
        cell.setCellStyle(titleCellStyle);
    }

    private void writeStudentsGradesHeader(Workbook workbook, Sheet sheet, String[] columnsName){
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(1);
        for (int col = 0; col < columnsName.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(columnsName[col]);
            cell.setCellStyle(headerCellStyle);
        }
    }

    private void writeStudentsGradesContent(Sheet sheet, List<StudentGradeResponseBody> studentsGrades){

        int rowIdx = 2;
        for (StudentGradeResponseBody eachStudentGrades : studentsGrades) {
            Row row = sheet.createRow(rowIdx++);

            row.createCell(0).setCellValue(eachStudentGrades.getStudentId());
            row.createCell(1).setCellValue(eachStudentGrades.getFullName());
            row.createCell(2).setCellValue(eachStudentGrades.getBirthdate().toString());

            List<LiteGradeResponseBody> grades = eachStudentGrades.getGradeResponseBodyList();
            int currentIndex = 3;
            for(int i = 0; i< grades.size(); i++){

                Float mark = grades.get(i).getMark();
                if(mark != null){
                    row.createCell(currentIndex).setCellValue(mark);
                }
                else {
                    row.createCell(currentIndex).setCellValue("-");
                }
                currentIndex++;
            }
        }
    }
}
