package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.dto.ThreadDto;
import com.fsoft.high_school_management.entity.Thread;
import org.springframework.data.domain.Page;

public interface IThreadService {
    Thread save(ThreadDto dto);
    Thread find(Long id);
    Page<Thread> find(int page, int size);
    Page<Thread> findBySubforum(Long subforumId, int page, int size);
    Thread save(Long id, ThreadDto dto);
    void delete(Long id);
    ThreadDto countPost(ThreadDto threadDto);
}
