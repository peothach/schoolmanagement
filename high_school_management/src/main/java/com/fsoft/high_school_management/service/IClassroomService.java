package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Classroom;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface IClassroomService extends IGenericService<Classroom> {
    Page<Classroom> getAll(Optional<String> keyword, Optional<Boolean> active, Optional<String> order, Optional<String> sort, Optional<Integer> pageSize, Optional<Integer> pageIndex);

    List<Classroom> getListOfAllClassrooms();
}
