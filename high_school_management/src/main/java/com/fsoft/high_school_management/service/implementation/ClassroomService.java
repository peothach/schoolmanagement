package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.entity.Classroom;
import com.fsoft.high_school_management.entity.Student;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.TeachersClassrooms;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IClassroomRepository;
import com.fsoft.high_school_management.repository.IStudentRepository;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.repository.ITeachersClassroomsRepository;
import com.fsoft.high_school_management.service.IClassroomService;
import com.fsoft.high_school_management.utils.PageableBuilder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class ClassroomService extends GenericService<Classroom> implements IClassroomService {

    private IClassroomRepository classroomRepository;
    private IStudentRepository studentRepository;
    private ITeachersClassroomsRepository teachersClassroomsRepository;

    @Autowired
    public void setStudentRepository(IStudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Autowired
    public void setClassroomRepository(IClassroomRepository classroomsRepository) {
        this.classroomRepository = classroomsRepository;
    }


    @Autowired
    public void setTeachersClassroomsRepository(ITeachersClassroomsRepository teachersClassroomsRepository) {
        this.teachersClassroomsRepository = teachersClassroomsRepository;
    }

    @Override
    public Page<Classroom> getAll(Optional<String> keyword, Optional<Boolean> active, Optional<String> order, Optional<String> sort, Optional<Integer> pageSize, Optional<Integer> pageIndex) {
        Pageable pageable = PageableBuilder.buildPage(order, sort, pageSize, pageIndex);
        if (!active.isPresent()) {
            return keyword
                    .map(name -> classroomRepository.findAllByNameContaining(name, pageable))
                    .orElseGet(() -> classroomRepository.findAll(pageable));
        }
        return keyword
                .map(name -> classroomRepository.findAllByNameContainingAndActive(name, active.get(), pageable))
                .orElseGet(() -> classroomRepository.findAllByActive(active.get(), pageable));
    }

    @Override
    public List<Classroom> getListOfAllClassrooms() {
        return this.classroomRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        if (id == null || classroomRepository.findById(id).orElse(null) == null) {
            throw new RecordNotFoundException();
        }
        classroomRepository.deactivateById(id);
        List<Student> students = studentRepository.findAllByClassroomId(id).stream()
                .peek((student -> student.setClassroom(null))).collect(Collectors.toList());
        List<TeachersClassrooms> teachersClassroomsList = teachersClassroomsRepository.findAllByClassroomIdAndActiveTrue(id);
        teachersClassroomsList.forEach(teachersClassrooms -> {teachersClassroomsRepository.deactivateById(teachersClassrooms.getId());});
        studentRepository.saveAll(students);
    }

    @Override
    public void deleteByIds(Long[] ids) {
        for (Long id : ids) {
            deleteById(id);
        }
    }
}
