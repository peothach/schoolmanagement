package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.entity.Student;
import org.springframework.data.domain.Page;

public interface IStudentService extends IGenericService<Student>{
    Page<Student> getAll(String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex);
    Page<Student> getAll(Long classroomId, String searchKey, Boolean active, String sortString, boolean order, Integer pageSize, Integer pageIndex);
}