package com.fsoft.high_school_management.service;

import com.fsoft.high_school_management.dto.SubforumDto;
import com.fsoft.high_school_management.entity.Subforum;

import java.util.List;

public interface ISubforumService {
    Subforum save(SubforumDto dto);
    Subforum find(Long id);
    List<Subforum> find();
    void delete(Long id);
    Subforum save(SubforumDto dto, Long id);
    SubforumDto countThread(SubforumDto dto);
}
