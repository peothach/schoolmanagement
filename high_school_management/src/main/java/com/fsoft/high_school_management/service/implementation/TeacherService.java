package com.fsoft.high_school_management.service.implementation;

import com.fsoft.high_school_management.dto.QueryCriteria;
import com.fsoft.high_school_management.entity.Subject;
import com.fsoft.high_school_management.entity.Teacher;
import com.fsoft.high_school_management.entity.TeachersClassrooms;
import com.fsoft.high_school_management.exception.RecordNotFoundException;
import com.fsoft.high_school_management.repository.IClassroomRepository;
import com.fsoft.high_school_management.repository.ITeacherRepository;
import com.fsoft.high_school_management.repository.ITeachersClassroomsRepository;
import com.fsoft.high_school_management.service.ISubjectService;
import com.fsoft.high_school_management.service.ITeacherService;
import com.fsoft.high_school_management.utils.AuditableEntityHelper;
import com.fsoft.high_school_management.utils.PageableBuilder;
import com.fsoft.high_school_management.utils.sorting_and_filtering.TeacherFilter;
import com.fsoft.high_school_management.utils.sorting_and_filtering.TeacherSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeacherService extends GenericService<Teacher> implements ITeacherService {

    @Autowired
    private ITeacherRepository teacherRepository;

    @Autowired
    ITeachersClassroomsRepository teachersClassroomsRepository;

    @Autowired
    IClassroomRepository classroomsRepository;

    @Autowired
    ISubjectService subjectService;

    @Autowired
    AuditableEntityHelper<TeachersClassrooms> auditableEntityHelper;

    public void setClassroom(Long teacherId, List<Long> classroomsIds) {
        Teacher teacher = teacherRepository.findById(teacherId).orElseThrow(() -> {
            throw new RecordNotFoundException();
        });
        List<TeachersClassrooms> teachersClassrooms = teachersClassroomsRepository.findAllByTeacherIdAndActiveTrue(teacherId);

        List<TeachersClassrooms> removedList = teachersClassrooms.stream().filter(element -> !classroomsIds.contains(element.getClassroom().getId())
        ).collect(Collectors.toList());

        removedList.stream().forEach(element -> teachersClassroomsRepository.deactivateById(element.getId())
        );

        List<Long> teachersClassroomsIds = teachersClassrooms.stream()
                .map(element -> element.getClassroom().getId())
                .collect(Collectors.toList());
        List<TeachersClassrooms> createList = classroomsIds.stream()
                .filter(id -> !teachersClassroomsIds.contains(id))
                .map(id -> {
                    TeachersClassrooms createdTeachersClassrooms = new TeachersClassrooms();
                    createdTeachersClassrooms = auditableEntityHelper.setupAuditableFields(createdTeachersClassrooms);
                    createdTeachersClassrooms.setTeacher(teacher);
                    createdTeachersClassrooms.setClassroom(classroomsRepository.findById(id).orElseThrow(() -> {
                        throw new RecordNotFoundException();
                    }));
                    return createdTeachersClassrooms;
                }).collect(Collectors.toList());
        teachersClassroomsRepository.saveAll(createList);
    }


    QueryCriteria buildSearchCriteria(String fieldName, Object value) {
        QueryCriteria searchCriteria = new QueryCriteria();
        searchCriteria.setKey(fieldName);
        searchCriteria.setValue(value);
        return searchCriteria;
    }


    @Override
    public Page<Teacher> getAllActive(TeacherFilter teacherFilter,
                                      Optional<Integer> pageSize,
                                      Optional<Integer> pageIndex,
                                      Optional<String> sort,
                                      Optional<String> order) {

        Pageable pageable = PageableBuilder.buildPage(order, sort, pageSize, pageIndex);
        QueryCriteria active = buildSearchCriteria("active", Boolean.TRUE);
        TeacherSpecification activeSpecs = new TeacherSpecification(active);
        List<QueryCriteria> searchCriteriaList = new LinkedList<>();
        teacherFilter.getFullName().ifPresent(s -> searchCriteriaList.add(buildSearchCriteria("fullName", s)));
        teacherFilter.getSubjectId().ifPresent(aLong -> {
            Subject subject;
            try {
                subject = subjectService.getOne(aLong);
            } catch (Exception e) {
                subject = null;
            }
            if (subject != null) {
                searchCriteriaList.add(buildSearchCriteria("subjectId", subject));
            }
        });
        teacherFilter.getAddress().ifPresent(s -> searchCriteriaList.add(buildSearchCriteria("address", s)));
        teacherFilter.getPhoneNumber().ifPresent(s -> searchCriteriaList.add(buildSearchCriteria("phoneNumber", s)));
        teacherFilter.getGender().ifPresent(g -> searchCriteriaList.add(buildSearchCriteria("gender", g)));
        List<TeacherSpecification> teacherspecs = searchCriteriaList.stream().map(TeacherSpecification::new).collect(Collectors.toList());
        Specification<Teacher> specs = Specification.where(activeSpecs);
        for (TeacherSpecification spec : teacherspecs) {
            specs = specs.and(spec);
        }
        return teacherRepository.findAll(specs, pageable);
    }

    @Override
    public void deleteById(Long id) {
        if (id == null || teacherRepository.findById(id).orElse(null) == null) {
            throw new RecordNotFoundException();
        }
        teacherRepository.deactivateById(id);
        List<TeachersClassrooms> teachersClassroomsList = teachersClassroomsRepository.findAllByTeacherIdAndActiveTrue(id);
        teachersClassroomsList.forEach((teachersClassrooms -> teachersClassroomsRepository.deactivateById(teachersClassrooms.getId())));
    }

    @Override
    public void deleteByIds(Long[] ids) {
        for (Long id : ids){
            deleteById(id);
        }
    }
}
