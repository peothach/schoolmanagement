package com.fsoft.high_school_management.dto.common;

import com.fsoft.high_school_management.entity.common.Gender;
import com.fsoft.high_school_management.utils.validators.annotations.FullName;
import com.fsoft.high_school_management.utils.validators.annotations.PhoneNumber;

import javax.validation.constraints.NotNull;
import java.util.Date;

public abstract class PersonRequestDTO extends AbstractDTO {

    @NotNull
    @FullName
    protected String fullName;

    protected Date birthdate;

    protected String address;

    @PhoneNumber
    @NotNull
    protected String phoneNumber;

    protected Gender gender;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
