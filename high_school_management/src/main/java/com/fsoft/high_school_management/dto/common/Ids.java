package com.fsoft.high_school_management.dto.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ids {

    @JsonProperty("ids")
    private Long[] idArray;

    public Long[] getIdArray() {
        return idArray;
    }

    public void setIdArray(Long[] idArray) {
        this.idArray = idArray;
    }

    public Ids(Long[] idArray) {
        this.idArray = idArray;
    }

    public Ids(){
    }
}