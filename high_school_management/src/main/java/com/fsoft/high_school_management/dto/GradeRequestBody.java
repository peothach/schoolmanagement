package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsoft.high_school_management.dto.common.AbstractDTO;

public class GradeRequestBody extends AbstractDTO {

    private Float mark;

    @JsonProperty("student_id")
    private Long studentId;

    @JsonProperty("subject_id")
    private Long subjectId;

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }
}
