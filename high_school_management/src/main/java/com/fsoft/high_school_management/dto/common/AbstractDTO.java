package com.fsoft.high_school_management.dto.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AbstractDTO {
    protected Long id;

    @JsonProperty("status")
    protected Boolean active;

    protected AbstractDTO() {
    }

    protected AbstractDTO(Long id) {
        this.id = id;
    }

    protected AbstractDTO(Long id, Boolean active) {
        this.id = id;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
