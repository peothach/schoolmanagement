package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ForumPageDTO<T> {

    @JsonProperty("content")
    List<T> content;

    @JsonProperty("limit")
    private int size;

    @JsonProperty("page")
    private int currentPage;

    @JsonProperty("totalPages")
    private int totalPages;

    @JsonProperty("totalElements")
    private long totalElements;

    public ForumPageDTO(List<T> content, int size, int currentPage, int totalPages, long totalElements) {
        this.content = content;
        this.size = size;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

}
