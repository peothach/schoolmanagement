package com.fsoft.high_school_management.dto;

import javax.validation.constraints.NotBlank;

public class ChangingPasswordRequestBody {

    @NotBlank
    private String oldPassword;

    @NotBlank
    private String newPassword;

    public ChangingPasswordRequestBody(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public ChangingPasswordRequestBody() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
