package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fsoft.high_school_management.entity.Post;

import java.util.Date;

public class PostDto {
    private Long id;
    private String content;
    private ThreadDto thread;
    private String authorUsername;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date lastEditedTime;
    private boolean active;

    public PostDto() {
    }

    public PostDto(Post post) {
        this.id = post.getId();
        this.content = post.getContent();
        this.thread = new ThreadDto(post.getThread());
        this.authorUsername = post.getAuthor().getUsername();
        this.createdTime = post.getCreatedTime();
        this.lastEditedTime = post.getLastEditedTime();
        this.active = post.isActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ThreadDto getThread() {
        return thread;
    }

    public void setThread(ThreadDto thread) {
        this.thread = thread;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastEditedTime() {
        return lastEditedTime;
    }

    public void setLastEditedTime(Date lastEditedTime) {
        this.lastEditedTime = lastEditedTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
