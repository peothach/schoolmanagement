package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.entity.ChatMessage;

import java.util.Date;
import java.util.List;

public class ChatMessageBody {
    private Long id;
    private Long senderId;
    private Long conversationId;
    private String conversationName;
    private List<String> usernames;
    private String senderUsername;
    private String content;
    private Date timestamp;

    public ChatMessageBody() {
    }

    public ChatMessageBody(Long id, Long senderId, Long conversationId, String coversationName, List<String> usernames, String senderUsername, String conversationUsername, String content, Date timestamp) {
        this.id = id;
        this.senderId = senderId;
        this.conversationId = conversationId;
        this.conversationName = coversationName;
        this.usernames = usernames;
        this.senderUsername = senderUsername;
        this.content = content;
        this.timestamp = timestamp;
    }

    public ChatMessageBody(ChatMessage chatMessage) {
        this.id = chatMessage.getId();
        this.senderId = chatMessage.getSender().getId();
        this.conversationId = chatMessage.getRecipient().getId();
        this.conversationName = chatMessage.getRecipient().getName();
        this.senderUsername = chatMessage.getSender().getUsername();
        this.content = chatMessage.getMessage();
        this.timestamp = chatMessage.getTimestamp();
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
