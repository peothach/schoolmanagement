package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.entity.ChatMessage;
import java.util.Date;

public class ChatMessageResponseBody {
    private Long id;

    private Long senderId;

    private String senderUsername;

    private Long conversationId;

    private String conversationName;

    private String content;

    private Date timestamp;

    public ChatMessageResponseBody() {

    }

    public ChatMessageResponseBody(ChatMessage chatMessage) {
        this.id = chatMessage.getId();
        this.senderId = chatMessage.getSender().getId();
        this.conversationId = chatMessage.getRecipient().getId();
        this.conversationName = chatMessage.getRecipient().getName();
        this.senderUsername = chatMessage.getSender().getUsername();
        this.content = chatMessage.getMessage();
        this.timestamp = chatMessage.getTimestamp();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
