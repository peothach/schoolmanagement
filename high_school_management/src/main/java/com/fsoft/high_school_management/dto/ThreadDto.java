package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fsoft.high_school_management.entity.Thread;

import java.util.Date;

public class ThreadDto {
    private Long id;
    private String title;
    private String authorUsername;
    private SubforumDto subforum;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    private boolean active;
    private Long postCount;

    public ThreadDto() {
    }

    public ThreadDto(Thread thread) {
        this.id = thread.getId();
        this.title = thread.getTitle();
        this.authorUsername = thread.getAuthor().getUsername();
        this.subforum = new SubforumDto(thread.getSubforum());
        this.createdTime = thread.getCreatedTime();
        this.active = thread.isActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }

    public SubforumDto getSubforum() {
        return subforum;
    }

    public void setSubforum(SubforumDto subforum) {
        this.subforum = subforum;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getPostCount() {
        return postCount;
    }

    public void setPostCount(Long postCount) {
        this.postCount = postCount;
    }
}
