package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.PersonRequestDTO;

public class StudentRequestBody extends PersonRequestDTO {

    private Long classroomId;

    public Long getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(Long classroomId) {
        this.classroomId = classroomId;
    }
}
