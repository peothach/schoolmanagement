package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;
import io.swagger.annotations.ApiModel;
import java.util.Date;

@ApiModel
public class SubjectResponseBody extends AuditableDTO implements Cloneable{

    private String name;

    public SubjectResponseBody() {
        super();
    }

    public SubjectResponseBody(Long id, String name, Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active) {
        super(id, createdDate, createdBy, lastModifiedDate, lastModifiedBy, active);
        this.name = name;
    }

    public SubjectResponseBody(String name, Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active) {
        super(createdDate, createdBy, lastModifiedDate, lastModifiedBy, active);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
