package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsoft.high_school_management.dto.common.AuditableDTO;

import java.util.Date;
import java.util.List;

public class StudentGradeResponseBody extends AuditableDTO {
    private Long studentId;

    private String fullName;

    private Date birthdate;

    private Boolean active;

    @JsonProperty(value = "grades")
    private List<LiteGradeResponseBody> gradeResponseBodyList;

    public StudentGradeResponseBody() {
    }

    public StudentGradeResponseBody(Long studentId, String fullName, Date birthdate, Boolean active,List<LiteGradeResponseBody> gradeResponseBodyList) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.active = active;
        this.gradeResponseBodyList = gradeResponseBodyList;
    }

    @Override
    public Boolean getActive() {
        return active;
    }

    @Override
    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public List<LiteGradeResponseBody> getGradeResponseBodyList() {
        return gradeResponseBodyList;
    }

    public void setGradeResponseBodyList(List<LiteGradeResponseBody> gradeResponseBodyList) {
        this.gradeResponseBodyList = gradeResponseBodyList;
    }
}
