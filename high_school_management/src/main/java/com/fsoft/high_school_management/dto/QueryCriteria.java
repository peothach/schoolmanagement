package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.utils.sorting_and_filtering.QueryOperation;
import java.io.Serializable;

public class QueryCriteria implements Serializable {

    private String key;

    private QueryOperation operation;

    private Object value;

    public QueryCriteria(){
    }

    public QueryCriteria(String key, QueryOperation operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public QueryOperation getOperation() {
        return operation;
    }

    public void setOperation(QueryOperation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}