package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;
import com.fsoft.high_school_management.entity.common.Gender;
import io.swagger.annotations.ApiModel;

import java.util.Date;
import java.util.List;

@ApiModel
public class TeacherResponseBody extends AuditableDTO implements Cloneable {

    private String fullName;
    private Date birthdate;
    private String address;
    private String phoneNumber;
    private Gender gender;
    private SubjectResponseBody subject;
    private String account;
    private List<ClassroomResponseBody> classrooms;

    /**
     * @param id
     * @param createdDate
     * @param createdBy
     * @param lastModifiedDate
     * @param lastModifiedBy
     * @param active
     * @param fullName
     * @param birthdate
     * @param address
     * @param phoneNumber
     * @param gender
     * @param subject
     */
    public TeacherResponseBody(Long id, Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate,
            AccountResponseBody lastModifiedBy, Boolean active, String fullName, Date birthdate, String address,
            String phoneNumber, Gender gender, SubjectResponseBody subject, String account, List<ClassroomResponseBody> classrooms) {
        super(id, createdDate, createdBy, lastModifiedDate, lastModifiedBy, active);
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.subject = subject;
        this.account = account;
        this.classrooms = classrooms;
    }

    public TeacherResponseBody() {
        super();
    }



    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * @param birthdate the birthdate to set
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * @return the subject
     */
    public SubjectResponseBody getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(SubjectResponseBody subject) {
        this.subject = subject;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public List<ClassroomResponseBody> getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(List<ClassroomResponseBody> classrooms) {
        this.classrooms = classrooms;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
