package com.fsoft.high_school_management.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsoft.high_school_management.dto.AccountResponseBody;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AuditableDTO extends AbstractDTO {

    @JsonProperty("created_date")
    protected Date createdDate;

    @JsonProperty("created_by")
    protected AccountResponseBody createdBy;

    @JsonProperty("last_modified_date")
    protected Date lastModifiedDate;

    @JsonProperty("last_modified_by")
    protected AccountResponseBody lastModifiedBy;

    protected AuditableDTO(Long id, Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active) {
        super(id);
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.lastModifiedBy = lastModifiedBy;
        this.active = active;
    }

    protected AuditableDTO(Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active) {
        super();
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.lastModifiedBy = lastModifiedBy;
        this.active = active;
    }

    protected AuditableDTO(){

    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public AccountResponseBody getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AccountResponseBody createdBy) {
        this.createdBy = createdBy;
    }

    public AccountResponseBody getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(AccountResponseBody lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }
}
