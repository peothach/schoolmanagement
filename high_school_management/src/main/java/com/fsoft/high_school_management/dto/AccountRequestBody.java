package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@ApiModel
public class AccountRequestBody extends AbstractDTO {

    @NotNull(message = "Please provide username")
    private String username;

    @NotNull(message = "Please provide password")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
    private String password;

    public AccountRequestBody() {
    }

    public AccountRequestBody(Long id, String username, String password, boolean active){
        super(id, active);
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
