package com.fsoft.high_school_management.dto;

import java.util.List;

public class Participants {
    private List<String> usernames;

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public Participants(){

    }

    public Participants(List<String> usernames){
        this.usernames = usernames;
    }
}
