package com.fsoft.high_school_management.dto;

import javax.validation.constraints.NotBlank;

public class Credential {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private Long roleId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Credential(String username, String password, Long roleId) {
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }

    public Credential() {
        super();
    }
}
