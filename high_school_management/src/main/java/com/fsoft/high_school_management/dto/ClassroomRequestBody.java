package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ClassroomRequestBody extends AbstractDTO {

    @NotNull(message = "Please provide password")
    @Pattern(regexp = "^(\\d){1,2}(\\w)(\\d){1,2}$")
    private String name;

    public ClassroomRequestBody() {}

    public ClassroomRequestBody(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
