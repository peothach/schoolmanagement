package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.PersonRequestDTO;
import com.fsoft.high_school_management.entity.common.Gender;
import io.swagger.annotations.ApiModel;

import java.util.Date;

@ApiModel
public class TeacherRequestBody extends PersonRequestDTO {

    private Long subject;

    public TeacherRequestBody() {
    }

    public TeacherRequestBody(String fullName, Date birthdate, String address, String phoneNumber, Gender gender, Long subject) {
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.subject = subject;
    }

    public Long getSubject() {
        return subject;
    }

    public void setSubject(Long subject) {
        this.subject = subject;
    }
}
