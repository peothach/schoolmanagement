package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;

import java.util.Date;

public class ClassroomResponseBody extends AuditableDTO {
    private String name;

    public ClassroomResponseBody(Long id, Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active, String name) {
        super(id, createdDate, createdBy, lastModifiedDate, lastModifiedBy, active);
        this.name = name;
    }

    public ClassroomResponseBody() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
