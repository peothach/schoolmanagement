package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.entity.Grade;

public class LiteGradeResponseBody {
    private Long id;
    private String subjectName;
    private Float mark;
    private Boolean active;

    public LiteGradeResponseBody() {
    }

    public LiteGradeResponseBody(Grade grade) {
        this.id = grade.getId();
        this.subjectName = grade.getSubject().getName();
        this.mark = grade.getMark();
        this.active = grade.getActive();
    }

    public LiteGradeResponseBody(Long id, String subjectName, Float mark, Boolean active) {
        this.id = id;
        this.subjectName = subjectName;
        this.mark = mark;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
