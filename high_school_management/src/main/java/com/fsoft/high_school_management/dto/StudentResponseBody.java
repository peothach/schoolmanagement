package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.PersonResponseDTO;

public class StudentResponseBody extends PersonResponseDTO {

    private String account;

    private ClassroomResponseBody classroom;

    public ClassroomResponseBody getClassroom() {
        return classroom;
    }

    public void setClassroom(ClassroomResponseBody classroom) {
        this.classroom = classroom;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
