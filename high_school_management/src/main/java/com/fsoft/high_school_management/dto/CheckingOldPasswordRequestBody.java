package com.fsoft.high_school_management.dto;

import javax.validation.constraints.NotBlank;

public class CheckingOldPasswordRequestBody {

    @NotBlank
    private String oldPassword;

    public CheckingOldPasswordRequestBody(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public CheckingOldPasswordRequestBody() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
