package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;
import io.swagger.annotations.ApiModel;

@ApiModel
public class AccountResponseBody extends AuditableDTO {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
