package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;

public class GradeResponseBody extends AuditableDTO {

    private Float mark;

    private SubjectResponseBody subject;

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public SubjectResponseBody getSubject() {
        return subject;
    }

    public void setSubject(SubjectResponseBody subject) {
        this.subject = subject;
    }
}
