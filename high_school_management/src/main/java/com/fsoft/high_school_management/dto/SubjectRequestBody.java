package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AbstractDTO;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;

@ApiModel(value = "Subject Request Body")
public class SubjectRequestBody extends AbstractDTO {

    @NotNull(message = "Please provide subject name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubjectRequestBody() {
    }

    public SubjectRequestBody(Long id, String name) {
        super(id);
        this.name = name;
    }

    public SubjectRequestBody(String name) {
        this.name = name;
    }
}
