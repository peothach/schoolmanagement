package com.fsoft.high_school_management.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fsoft.high_school_management.entity.Subforum;

import java.util.Date;

public class SubforumDto {
    private Long id;
    private String name;
    private String description;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    private boolean active;
    private Long threadCount;

    public SubforumDto() {
    }

    public SubforumDto(Subforum subforum) {
        this.id = subforum.getId();
        this.name = subforum.getName();
        this.description = subforum.getDescription();
        this.createdTime = subforum.getCreatedTime();
        this.active = subforum.isActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(Long threadCount) {
        this.threadCount = threadCount;
    }
}
