package com.fsoft.high_school_management.dto.common;

import com.fsoft.high_school_management.dto.AccountResponseBody;
import com.fsoft.high_school_management.entity.common.Gender;

import java.util.Date;

public abstract class PersonResponseDTO extends AuditableDTO {

    protected String fullName;

    protected Date birthdate;

    protected String address;

    protected String phoneNumber;

    protected Gender gender;

    public PersonResponseDTO(){

    }

    public PersonResponseDTO(Date createdDate, AccountResponseBody createdBy, Date lastModifiedDate, AccountResponseBody lastModifiedBy, Boolean active, String fullName, Date birthdate, String address, String phoneNumber, Gender gender) {
        super(createdDate, createdBy, lastModifiedDate, lastModifiedBy, active);
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
