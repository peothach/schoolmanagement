package com.fsoft.high_school_management.dto;

import com.fsoft.high_school_management.dto.common.AuditableDTO;

public class RoleResponseBody extends AuditableDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
