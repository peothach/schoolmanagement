package com.fsoft.high_school_management.dto;

public class ChatMessageRequestBody {

    private Long conversationId;

    private String content;

    public ChatMessageRequestBody() {
    }

    public ChatMessageRequestBody(String content) {
        this.content = content;
    }

    public ChatMessageRequestBody(Long conversationId, String content) {
        this.conversationId = conversationId;
        this.content = content;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
